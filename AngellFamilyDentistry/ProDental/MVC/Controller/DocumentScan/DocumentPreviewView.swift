//
//  DocumentPreviewViewController.swift
//  LuxDentalAuto
//
//  Created by Berlin Raj on 08/07/17.
//  Copyright © 2017 srs. All rights reserved.
//

import UIKit

class DocumentPreviewView: UIView {

    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var imageView: UIImageView!
    
    var removeAction: (() -> Void)!
    var initialFrame: CGRect!
    
    func showWithImage(image: UIImage, fromRect rect: CGRect, removeAction: @escaping (()->Void)) {
        self.imageView.image = image
        self.scrollView.zoomScale = 1
        self.initialFrame = rect
        self.frame = initialFrame
        self.removeAction = removeAction
        self.scrollView.maximumZoomScale = 5.0

        let window = UIApplication.shared.delegate?.window
        window??.addSubview(self)
        
//        UIView.setAnimationCurve(UIViewAnimationCurve.easeIn)
        UIView.animate(withDuration: 0.3, animations: {
            self.frame = CGRect(x: 0, y: 0, width: screenSize.width, height: screenSize.height)
        })
//        UIView.commitAnimations()
    }
    
    @IBAction func buttonRemoveAction () {
        self.removeAction()
//        UIView.setAnimationCurve(UIViewAnimationCurve.easeOut)
        UIView.animate(withDuration: 0.3, animations: {
            self.frame = self.initialFrame
        }) { (finished) in
            if finished {
                self.removeFromSuperview()
            }
        }
//        UIView.commitAnimations()
    }
    
    @IBAction func buttonDoneAction () {
//        UIView.setAnimationCurve(UIViewAnimationCurve.easeOut)
        UIView.animate(withDuration: 0.3, animations: {
            self.frame = self.initialFrame
        }) { (finished) in
            if finished {
                self.removeFromSuperview()
            }
        }
//        UIView.commitAnimations()
        
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
extension DocumentPreviewView: UIScrollViewDelegate {
    
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return self.imageView
    }
}
