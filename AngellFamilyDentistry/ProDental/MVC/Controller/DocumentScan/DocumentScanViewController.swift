//
//  DocumentScanViewController.swift
//  LuxDentalAuto
//
//  Created by Berlin Raj on 08/07/17.
//  Copyright © 2017 srs. All rights reserved.
//

import UIKit
import AVFoundation

protocol DocumentImageCaptureDelegate {
    func documentImagePicker(_ picker: DocumentScanViewController, completedWithCardImage image: UIImage?)
    func documentImagePickerDidCancel(_ picker: DocumentScanViewController)
}

class DocumentScanViewController: UIViewController {
    
    var captureSession = AVCaptureSession()
    var stillImageOutput: AVCaptureStillImageOutput!
    
    @IBOutlet var previewView: UIView!
    var delegate: DocumentImageCaptureDelegate!
    
    var isFrontCameraActive: Bool = false
    var capturePressed: Bool = false
    
    @IBOutlet weak var imageViewBackground: UIImageView!
    @IBOutlet var captureView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let rect1 = CGRect(x: 0, y: 0, width: screenSize.width, height: screenSize.height)
        let rect2 = captureView.frame
        let maskLayer = CAShapeLayer()
        let path = CGMutablePath()
        path.addPath(CGPath(roundedRect: rect2, cornerWidth: 5, cornerHeight: 5, transform: nil))
        //        path.addRect(rect2)
        path.addRect(rect1)
        maskLayer.path = path
        
        maskLayer.fillRule = kCAFillRuleEvenOdd
        imageViewBackground.layer.mask = maskLayer
        
        //        let maskLayer = CALayer()
        //        maskLayer.fillMode = kCAFillModeRemoved
        //        maskLayer.cornerRadius = 5.0
        //        maskLayer.masksToBounds = true
        //        maskLayer.frame = CGRect(x: 130, y: 292, width: 505, height: 326)
        //        imageViewBackground.layer.mask = maskLayer
        //        imageViewBackground.layer.masksToBounds = true
        
        edgesForExtendedLayout = UIRectEdge()
        captureSession.beginConfiguration()
        captureSession.sessionPreset = AVCaptureSessionPreset640x480
        
        let previewLayer = AVCaptureVideoPreviewLayer(session: captureSession)
        previewLayer?.frame = captureView.frame
        previewLayer?.videoGravity = AVLayerVideoGravityResizeAspectFill
        self.previewView.layer.addSublayer(previewLayer!)
        
        let device = AVCaptureDevice.defaultDevice(withMediaType: AVMediaTypeVideo)
        isFrontCameraActive = device?.position == AVCaptureDevicePosition.front ? true : false
        do {
            let input = try AVCaptureDeviceInput(device: device)
            captureSession.addInput(input)
            
            stillImageOutput = AVCaptureStillImageOutput()
            stillImageOutput.outputSettings = [AVVideoCodecKey: AVVideoCodecJPEG]
            captureSession.addOutput(stillImageOutput)
        } catch {
            
        }
        
        captureSession.commitConfiguration()
        captureSession.startRunning()
    }
    @IBAction func toogleDevice() {
        if capturePressed == true {
            capturePressed = false
            return
        }
        captureSession.stopRunning()
        captureSession.beginConfiguration()
        
        for input in captureSession.inputs {
            captureSession.removeInput(input as! AVCaptureInput)
        }
        
        for device in AVCaptureDevice.devices(withMediaType: AVMediaTypeVideo) {
            if ((device as AnyObject).position == AVCaptureDevicePosition.front && isFrontCameraActive == false) || ((device as AnyObject).position == AVCaptureDevicePosition.back && isFrontCameraActive == true) {
                do {
                    let input = try AVCaptureDeviceInput(device: device as! AVCaptureDevice)
                    captureSession.addInput(input)
                } catch {
                    
                }
            }
        }
        self.isFrontCameraActive = !isFrontCameraActive
        captureSession.commitConfiguration()
        captureSession.startRunning()
    }
    
    @IBAction func captureImage() {
        capturePressed = true
        var videoConnection: AVCaptureConnection!
        
        for connection in stillImageOutput.connections as! [AVCaptureConnection] {
            for port in connection.inputPorts {
                if (port as AnyObject).mediaType == AVMediaTypeVideo {
                    videoConnection = connection
                    break;
                }
            }
            if videoConnection != nil
            {
                break;
            }
        }
        print("about to request a capture from: \(stillImageOutput)")
        stillImageOutput.captureStillImageAsynchronously(from: videoConnection) { (buffer, error) in
            let imageData = AVCaptureStillImageOutput.jpegStillImageNSDataRepresentation(buffer)
            let image = UIImage(data: imageData!)
            
            self.delegate.documentImagePicker(self, completedWithCardImage: image)
            self.capturePressed = false
        }
    }
    
    @IBAction func backAction() {
        if capturePressed == true {
            capturePressed = false
            return
        }
        captureSession.stopRunning()
        delegate.documentImagePickerDidCancel(self)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}
