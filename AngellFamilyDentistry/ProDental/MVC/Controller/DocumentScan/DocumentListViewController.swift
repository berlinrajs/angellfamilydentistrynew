//
//  DocumentListViewController.swift
//  LuxDentalAuto
//
//  Created by Berlin Raj on 08/07/17.
//  Copyright © 2017 srs. All rights reserved.
//

import UIKit

class DocumentListViewController: MCViewController {

    var arrayDocuments: [UIImage] = [UIImage]()
    @IBOutlet weak var collectionView: UICollectionView!
    
    var previewVC: DocumentPreviewView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
//        collectionView.register(DocumentImageCollectionViewCell.self, forCellWithReuseIdentifier: "DocumentCell")
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func buttonNextAction () {
        if arrayDocuments.count > 0 {
            let formVC = self.storyboard?.instantiateViewController(withIdentifier: "kDocumentPDFViewController") as! DocumentPDFViewController
            formVC.patient = self.patient
            formVC.arrayDocuments = self.arrayDocuments
            self.navigationController?.pushViewController(formVC, animated: true)
        } else {
            self.showAlert("PLEASE CAPTURE ANY DOCUMENT TO CONTINUE")
        }
    }
    
}

extension DocumentListViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.arrayDocuments.count + 1
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if indexPath.row == 0 {
            if UIImagePickerController.isSourceTypeAvailable(.camera) {
                let picker = self.storyboard?.instantiateViewController(withIdentifier: "kDocumentScanViewController") as! DocumentScanViewController
                picker.delegate = self
                self.present(picker, animated: true) {
                }
            }
        } else {
            if previewVC == nil {
                previewVC = Bundle.main.loadNibNamed("DocumentPreviewView", owner: nil, options: nil)!.first as! DocumentPreviewView
            }
            
            let theAttributes = collectionView.layoutAttributesForItem(at: indexPath)
            
            let cellFrameInView = collectionView.convert(theAttributes!.frame, to: self.view)
            
            previewVC.showWithImage(image: arrayDocuments[indexPath.row - 1], fromRect: cellFrameInView, removeAction: {
                self.arrayDocuments.remove(at: indexPath.row - 1)
                self.collectionView.reloadData()
            })
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "DocumentCell", for: indexPath) as! DocumentImageCollectionViewCell
        
        cell.previewImageView.image = indexPath.row == 0 ? UIImage(named: "CamCaptureButton") : arrayDocuments[indexPath.row - 1]
        cell.previewImageView.borderWidth = indexPath.row == 0 ? 0.0 : 2.0
//        collectionView.setla.layoutAttributesForItem(at: indexPath)
        
        return cell
    }
    
//    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
//        
//        let totalCellWidth = 119 * (arrayDocuments.count + 1)
//        let totalSpacingWidth = 10 * min(4, arrayDocuments.count)
//        
//        let totalCellHeight = 160 * (arrayDocuments.count + 1)
//        let totalSpacingHeight = 10 * max(1, arrayDocuments.count)/4
//        
//        let leftInset = (collectionView.frame.width - CGFloat(totalCellWidth + totalSpacingWidth)) / 2
//        let rightInset = leftInset
//        
//        let topInset = (collectionView.frame.height - CGFloat(totalCellHeight + totalSpacingHeight)) / 2
//        let bottomInset = topInset
//        
//        return UIEdgeInsetsMake(topInset, leftInset, bottomInset, rightInset)
//    }
    
//    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
//        return CGSize(width: max(119.0, collectionView.frame.width/CGFloat(arrayDocuments.count+1)), height: max(160, collectionView.frame.height/CGFloat(arrayDocuments.count + 1)))
//    }
}

extension DocumentListViewController: DocumentImageCaptureDelegate {
    func documentImagePickerDidCancel(_ picker: DocumentScanViewController) {
        picker.dismiss(animated: true, completion: nil)
    }
    
    func documentImagePicker(_ picker: DocumentScanViewController, completedWithCardImage image: UIImage?) {
        self.arrayDocuments.insert(image!, at: 0)
        self.collectionView.reloadData()
        picker.dismiss(animated: true, completion: nil)
    }
}
