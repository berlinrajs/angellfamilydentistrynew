//
//  DocumentPDFViewController.swift
//  LuxDentalAuto
//
//  Created by Berlin Raj on 08/07/17.
//  Copyright © 2017 srs. All rights reserved.
//

import UIKit

class DocumentPDFViewController: MCViewController {

    var arrayDocuments: [UIImage]!
    @IBOutlet weak var contentView: UIView!
    
    @IBOutlet weak var contentSizeHeight: NSLayoutConstraint!
    @IBOutlet weak var labelName: UILabel!
    @IBOutlet weak var labelDate: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        labelDate.text = "DATE: " + patient.dateToday
        
        labelName.text = "PATIENT NAME: " + patient.fullName.uppercased()
        
        for (idx, image) in arrayDocuments.enumerated() {
            let imageView = UIImageView(frame: CGRect(x: 50, y: (CGFloat(idx) * screenSize.height) + 210, width: screenSize.width - 100, height: screenSize.height - 274))
            imageView.contentMode = UIViewContentMode.scaleAspectFit
            imageView.image = image
            self.contentView.addSubview(imageView)
        }
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        contentSizeHeight.constant = CGFloat(arrayDocuments.count) * screenSize.height
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
