//
//  TreatmentFormSelectionVC.swift
//  MC For ES
//
//  Created by Berlin Raj on 09/08/17.
//  Copyright © 2017 SRS Web Solutions. All rights reserved.
//

import UIKit

class TreatmentFormSelectionVC: MCViewController {
    
    @IBOutlet weak var labelHostName: UILabel!
    @IBOutlet weak var labelHostIP: UILabel!
    @IBOutlet weak var tableViewFolder: UITableView!
    
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    var arrayFolderName: [TOSMBSessionFile] = [TOSMBSessionFile]()
//    @IBOutlet weak var headerView: UIView!
    
    var sharedSession: TOSMBSession!
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if let workStation = WorkStationConfig.sharedConfig {
            labelHostName.text = workStation.hostName
            labelHostIP.text = workStation.hostIp
            
            loadPage()
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        if !WorkStationConfig.hasSystemConfigured {
            changePCAction()
        }
    }
    
    
    
    @IBAction func changePCAction () {
        let pcConfigVC = self.storyboard?.instantiateViewController(withIdentifier: "kTreatmentPlanSelectPCVC") as! TreatmentPlanSelectPCVC
        self.present(pcConfigVC, animated: true, completion: nil)
    }
    
    func loadPage() {
        activityIndicator.startAnimating()
        
        let config = WorkStationConfig.sharedConfig!
        
        sharedSession = TOSMBSession(hostName: config.hostName, ipAddress: config.hostIp)
        sharedSession?.setLoginCredentialsWithUserName(config.authName, password: config.authPassword)
        sharedSession?.requestContentsOfDirectory(atFilePath: config.folderPath, success: { (result) in
            self.arrayFolderName.removeAll()
            
            if result != nil {
                for file in result! {
                    if let folder = file as? TOSMBSessionFile, (folder.directory || folder.isShareRoot || folder.name.hasSuffix(".jpeg") || folder.name.hasSuffix(".jpg") || folder.name.hasSuffix(".pdf")) {
                        self.arrayFolderName.append(folder)
                    }
                }
            }
            self.activityIndicator.stopAnimating()
            //            self.buttonSelect.isHidden = false
            //            UIView.animate(withDuration: 0.3, animations: {
            self.tableViewFolder.reloadData()
            //            })
        }) { (error) in
            self.activityIndicator.stopAnimating()
            //            self.buttonSelect.isHidden = false
            self.showAlert(error!.localizedDescription)
        }
    }
    
    func loadPreviousPage() {
        let config = WorkStationConfig.sharedConfig!
        config.removeLastTreeFromPath()
        WorkStationConfig.sharedConfig = config
        self.loadPage()
    }
}
extension TreatmentFormSelectionVC: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return WorkStationConfig.sharedConfig == nil ? 0 : 1 + max(1, arrayFolderName.count)
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let config = WorkStationConfig.sharedConfig!
        if indexPath.row == 0 {
            if config.folderPath == "/" {
                
            } else {
                self.loadPreviousPage()
            }
        } else {
            let contentObject = arrayFolderName[indexPath.row - 1]
            if contentObject.isShareRoot || contentObject.directory {
                config.appendPath(path: contentObject.name)
                WorkStationConfig.sharedConfig = config
                self.loadPage()
            } else {
                BRProgressHUD.show()
                if let config = WorkStationConfig.sharedConfig {
                    sharedSession = TOSMBSession(hostName: config.hostName!, ipAddress: config.hostIp!)
                    sharedSession?.setLoginCredentialsWithUserName(config.authName!, password: config.authPassword!)
                    
                    var downloadTask: TOSMBSessionDownloadTask!
                    let documentsPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0]
                    let path = "\(documentsPath)/\(contentObject.name!.fileName)"
                    
                    downloadTask = sharedSession?.downloadTaskForFile(atPath: config.folderPath + contentObject.name!, destinationPath: path, progressHandler: { (progress1, progress2) in
                        print("Download progress: \(progress1)/\(progress2)")
                    }, completionHandler: { (errorString) in
//                        BRProgressHUD.hide()
                        print("Download Success: \(errorString ?? "")")
                        
                        let previewVC = self.storyboard?.instantiateViewController(withIdentifier: "kTreatmentPlanPreviewVC") as! TreatmentPlanPreviewVC
                        previewVC.filePath = path
                        previewVC.patient = self.patient
                        self.navigationController?.pushViewController(previewVC, animated: true)
                        
                    }, failHandler: { (error) in
                        BRProgressHUD.hide()
                        self.showAlert(error!.localizedDescription)
                    })
                    
                    downloadTask?.resume()
                }
            }
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.row == 1 && arrayFolderName.count == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "emptyCell")!
            return cell
        }
        
        let cell = tableView.dequeueReusableCell(withIdentifier: indexPath.row == 0 ? "FolderCell" : "FolderSubCell") as! FolderListTableViewCell
        
        if indexPath.row == 0 {
            let config = WorkStationConfig.sharedConfig!
            cell.labelFolderName.text = config.selectedFolder == "/" ? "//\(config.hostIp!)/" : config.selectedFolder
        } else {
            let folder = self.arrayFolderName[indexPath.row - 1]
            cell.labelFolderName.text = folder.name
            cell.delegate = nil
            if folder.directory || folder.isShareRoot {
                cell.labelDetails.text = folder.filePath == nil ? "" : "Path : \(folder.filePath!)"
            } else {
                cell.labelDetails.text = folder.fileSize > 1000000 ? "Size : \(folder.fileSize/1000000) MB" : folder.fileSize > 1000 ? "Size : \(folder.fileSize/1000) KB" : "Size : \(folder.fileSize) bytes"
            }
            cell.customizeImage(ForFile: folder)
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return (indexPath.row == 1 && self.arrayFolderName.count == 0) ? tableView.frame.height - 120 : 60.0
    }
    
//    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
//        return 60.0
//    }
//    
//    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
//        return headerView
//    }
}

//extension TreatmentFormSelectionVC: FolderListTableViewCellDelegate {
//    func folderSelected () {
//        let config = WorkStationConfig.sharedConfig!
//        if config.selectedFolder == "/" {
//            self.showAlert("PLEASE SELECT A FOLDER")
//        } else {
//            if WorkStationConfig.hasSystemConfigured == false {
//                WorkStationConfig.sharedConfig = config
//                WorkStationConfig.hasSystemConfigured = true
//                self.navigationController!.parent!.showAlert("STORAGE CONFIGURED SUCCESSFULLY", completion: {
//                    (UIApplication.shared.delegate as! AppDelegate).checkAutologin()
//                })
//            } else {
//                WorkStationConfig.sharedConfig = config
//                SystemConfig.hasSystemConfig = true
//                SystemConfig.googleConfigured = false
//                
//                self.navigationController!.parent!.showAlert("STORAGE CONFIGURED SUCCESSFULLY", completion: {
//                    self.navigationController?.parent?.navigationController?.dismiss(animated: true, completion: nil)
//                })
//            }
//        }
//    }
//}
