//
//  TreatmentPlanPreviewVC.swift
//  MC For ES
//
//  Created by Berlin Raj on 10/08/17.
//  Copyright © 2017 SRS Web Solutions. All rights reserved.
//

import UIKit

class TreatmentPlanPreviewVC: MCViewController {

    @IBOutlet weak var labelNote1: UILabel!
    @IBOutlet weak var labelNote3: UILabel!
    @IBOutlet weak var labelNote2: UILabel!
    
    @IBOutlet weak var labelName: UILabel!
    
    @IBOutlet weak var signatureView: SignatureView!
    @IBOutlet weak var labelDate: DateLabel!
    
    @IBOutlet weak var scrollView: UIScrollView!
    
    @IBOutlet weak var layoutScrollViewSize: NSLayoutConstraint!
    @IBOutlet weak var layoutNotesTopSpace: NSLayoutConstraint!
    @IBOutlet weak var layoutSignatureTopSpace: NSLayoutConstraint!
    @IBOutlet weak var layoutScrollViewTopSpace: NSLayoutConstraint!
    
    @IBOutlet weak var webView: UIWebView!
    
    var filePath: String!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        signatureView.layer.borderColor = UIColor.black.cgColor
        signatureView.layer.borderWidth = 2
        
        labelDate.todayDate = patient.dateToday
        
        labelName.text = self.patient.fullName
        
        BRProgressHUD.show()
        if let url = URL(string: filePath!) {
            let pdfFileRequest = NSURLRequest(url: url)
            webView.loadRequest(pdfFileRequest as URLRequest)
        } else {
            BRProgressHUD.hide()
            self.showAlert("Invalid url path, Please try again") {
                super.buttonBackAction()
            }
        }
        
        scrollView.maximumZoomScale = 3.0
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func onSubmitButtonPressed() {
        if !signatureView.isSigned() {
            self.showAlert("PLEASE SIGN THE FORM")
        } else if !labelDate.dateTapped {
            self.showAlert("PLEASE TAP TO DATE")
        } else {
            PopupTextView.popUpView().showWithPlaceHolder("PLEASE TYPE NOTES HERE", completion: { (popUpView, textView) in
                popUpView.close()
                self.webView.scrollView.setZoomScale(1.0, animated: true)
                self.scrollView.setZoomScale(1.0, animated: true)
                BRProgressHUD.show()
                DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.3, execute: {
                    self.submitwithNotes(notes: textView.textValue!)
                })
            })
        }
    }
    
    func submitwithNotes(notes: String) {
        labelNote1.text = ""
        labelNote2.text = ""
        labelNote3.text = ""
        notes.setTextForArrayOfLabels([labelNote1, labelNote2, labelNote3])
        
        if filePath.hasSuffix(".pdf") {
            let url = CFURLCreateWithFileSystemPath(nil, filePath as CFString, .cfurlposixPathStyle, false)
            let pdf = CGPDFDocument.init(url!)
            let totalPages = pdf!.numberOfPages
            
            let width = pdf!.page(at: 1)?.getBoxRect(CGPDFBox.mediaBox).size.width
            let height = pdf!.page(at: 1)?.getBoxRect(CGPDFBox.mediaBox).size.height
            
            let resultHeight = ((height! * screenSize.width)/width!) * CGFloat(totalPages)
            self.layoutScrollViewTopSpace.constant = 0
            self.layoutScrollViewSize.constant = resultHeight
            
            let reminder = resultHeight < screenSize.height ? screenSize.height - resultHeight : resultHeight.truncatingRemainder(dividingBy: screenSize.height)
            if reminder < 150 {
                self.layoutNotesTopSpace.constant = reminder + 50
            } else if reminder < 284 {
                self.layoutSignatureTopSpace.constant = (reminder - 100) + 50
            }
            self.scrollView.isScrollEnabled = false
            self.webView.scrollView.isScrollEnabled = false
            super.onSubmitButtonPressed()
            
        } else if let image = UIImage(contentsOfFile: filePath) {
            let width = image.size.width
            let height = image.size.height
            
            let resultHeight = (height * screenSize.width)/width
            self.layoutScrollViewTopSpace.constant = 0
            self.layoutScrollViewSize.constant = resultHeight
            
            let reminder = resultHeight.truncatingRemainder(dividingBy: screenSize.height)
            if reminder < 150 {
                self.layoutNotesTopSpace.constant = reminder + 50
            } else if reminder < 284 {
                self.layoutSignatureTopSpace.constant = (reminder - 100) + 50
            }
            
            self.scrollView.isScrollEnabled = false
            self.webView.scrollView.isScrollEnabled = false
            super.onSubmitButtonPressed()
        }
    }
}
extension TreatmentPlanPreviewVC: UIWebViewDelegate {
    func webViewDidFinishLoad(_ webView: UIWebView) {
        BRProgressHUD.hide()
        webView.scrollView.isDirectionalLockEnabled = false
        for shadowView in self.webView.scrollView.subviews {
            print("\(shadowView)")
            if !shadowView.isKind(of: UIImageView.self) {
                shadowView.subviews[0].layer.shadowColor = UIColor.clear.cgColor
            }
        }
    }
    
    func webView(_ webView: UIWebView, didFailLoadWithError error: Error) {
        BRProgressHUD.hide()
        self.showAlert(error.localizedDescription) {
            super.buttonBackAction()
        }
    }
}

extension TreatmentPlanPreviewVC: UIScrollViewDelegate {
    func scrollViewDidZoom(_ scrollView: UIScrollView) {
        
    }
    
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return scrollView == pdfView ? nil : webView
    }
}
