//
//  TreatmentPlanPatientVC.swift
//  MC For ES
//
//  Created by Berlin Raj on 09/08/17.
//  Copyright © 2017 SRS Web Solutions. All rights reserved.
//

import UIKit

class TreatmentPlanPatientVC: MCViewController {

    @IBOutlet weak var textFieldFirstName: MCTextField!
    @IBOutlet weak var textFieldLastName: MCTextField!
    
    @IBOutlet weak var textFieldDate: MCTextField!
    @IBOutlet weak var textFieldMonth: MCTextField!
    @IBOutlet weak var textFieldYear: MCTextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        textFieldDate.textFormat = .Date
        textFieldMonth.textFormat = .Month
        textFieldYear.textFormat = .Year
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func buttonActionNext(_ sender : AnyObject) {
        self.view.endEditing(true)
        if textFieldFirstName.isEmpty {
            self.showAlert("PLEASE ENTER PATIENT ID OR PATIENT DETAILS")
        } else if textFieldLastName.isEmpty {
            self.showAlert("PLEASE ENTER PATIENT ID OR PATIENT DETAILS")
        } else if invalidDateofBirth {
            self.showAlert("PLEASE ENTER THE VALID DATE OF BIRTH")
        } else {
            patient.firstName = textFieldFirstName.text
            patient.lastName = textFieldLastName.text
            patient.dateOfBirth = getDateOfBirth()
            let appDetails = MCAppDetails.sharedAppDetails!
            
            func showMoreThanOneUserAlert()  {
                self.showAlert("More than one user found. Please handover the device to front desk to enter your patient id", completion: {
                    let verificationVC = mainStoryBoard.instantiateViewController(withIdentifier: "kVerificationViewController") as! VerificationViewController
                    verificationVC.patient = self.patient
                    verificationVC.completionOfVerification = {(object) in
                        self.navigationController?.dismiss(animated: true, completion: {
                            let response = object as! [String : AnyObject]
                            print(response)
                            reloadPatientValues()
                            let patientDetails = response["patientData"] as! [String: AnyObject]
                            self.patient.patientDetails = PatientDetails(details: patientDetails)
                            self.gotoNextForm()
                        })
                    }
                    self.navigationController?.present(verificationVC, animated: true, completion: nil)
                })
            }
            
            func APICall() {
                BRProgressHUD.show()
                let params = ["firstName": self.textFieldFirstName.text!, "lastName": self.textFieldLastName.text!, "dateOfBirth": DateFormatter(dateFormat: "MMM dd, yyyy").date(from: patient.dateOfBirth)!] as [String : AnyObject]
                ServiceManager.getDataFromServer(baseUrlString: hostUrl!, serviceName: "api/patients/", parameters: params, success: { (result) in
                    BRProgressHUD.hide()
                    guard let patientDetails = (result as? [[String : AnyObject]])?.first else {
                        self.showAlert("PLEASE CHECK THE DETAILS YOU HAVE ENTERED")
                        return
                    }
                    
                    self.patient.patientDetails = PatientDetails(details: patientDetails)
                    self.patient.patientDetails?.dateOfBirth = self.patient.dateOfBirth
                    self.gotoNextForm()
                }) { (error) in
                    if error.localizedDescription.contains("401") {
                        let params = ["userName": "srssoftwares", "password": "srssoftwares", "grant_type": "password"] as [String : AnyObject]
                        ServiceManager.postDataToServer(baseUrlString: hostUrl, serviceName: "token", parameters: params, success: { (result) in
                            if let accessToken = (result as AnyObject)["access_token"] as? String {
                                UserDefaults.standard.set("Bearer \(accessToken)", forKey: kAccessToken)
                                UserDefaults.standard.synchronize()
                            }
                            APICall()
                        }) { (error) in
                            
                        }
                    } else {
                        SystemConfig.sharedConfig?.checkConnectivity(completion: { (success, errorMessage) in
                            BRProgressHUD.hide()
                            self.patient.patientDetails = nil
                            if success {
                                self.showAlert("Something went wrong, Please contact our support team")
                            } else {
                                self.showAlert(errorMessage!)
                            }
                        })
                    }
                }
            }
            
            APICall()
            
            func reloadPatientValues() {
                let tempPatient = MCPatient(forms: self.patient.selectedForms)
                tempPatient.dateToday = self.patient.dateToday
                tempPatient.firstName = patient.firstName
                tempPatient.lastName = patient.lastName
                tempPatient.dateOfBirth = patient.dateOfBirth
                tempPatient.dentistName = patient.dentistName
                tempPatient.signRefused = patient.signRefused
                tempPatient.signRefusalOther = patient.signRefusalOther
                tempPatient.signRefusalTag = patient.signRefusalTag
                self.patient = tempPatient
            }
        }
    }
    
    override func gotoNextForm() {
        let step1Vc = self.storyboard?.instantiateViewController(withIdentifier: "kTreatmentFormSelectionVC") as! TreatmentFormSelectionVC
        step1Vc.patient = self.patient
        self.navigationController?.pushViewController(step1Vc, animated: true)
    }
    
    func getDateOfBirth() -> String {
        let dateFormatter = DateFormatter(dateFormat: "MMM dd, yyyy")
        let dob = dateFormatter.date(from: textFieldMonth.text! + " " + textFieldDate.text! + ", " + textFieldYear.text!)!
        
        let dateFormatter1 = DateFormatter()
        dateFormatter1.dateFormat = kCommonDateFormat
        return dateFormatter1.string(from: dob).uppercased()
    }
    
    var invalidDateofBirth: Bool {
        get {
            if textFieldMonth.isEmpty || textFieldDate.isEmpty || textFieldYear.isEmpty {
                return true
            } else if Int(textFieldDate.text!)! == 0 {
                return true
            } else if !textFieldYear.text!.isValidYear {
                return true
            } else if Int(textFieldYear.text!)! < 1900 {
                return true
            } else {
                let dateFormatter = DateFormatter(dateFormat: "dd-MMM-yyyy")
                
                let todayDate = dateFormatter.date(from: dateFormatter.string(from: Date()))
                let currentDate = dateFormatter.date(from: "\(textFieldDate.text!)-\(textFieldMonth.text!)-\(textFieldYear.text!)")
                
                if todayDate == nil || currentDate == nil {
                    return true
                }
                if todayDate!.timeIntervalSince(currentDate!) < 0 {
                    return true
                }
                return false
            }
        }
    }
}
