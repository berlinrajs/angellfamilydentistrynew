//
//  TeartmentPlanSelectPCVC.swift
//  MC For ES
//
//  Created by Berlin Raj on 10/08/17.
//  Copyright © 2017 SRS Web Solutions. All rights reserved.
//

import UIKit

class TreatmentPlanSelectPCVC: MCViewController {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var buttonScan: UIButton!
    
    var arrayDevices: [TONetBIOSNameServiceEntry] = [TONetBIOSNameServiceEntry]()
    var biosService: TONetBIOSNameService = TONetBIOSNameService()
    var ipResolver: TONetBIOSNameService = TONetBIOSNameService()
    
    @IBOutlet weak var headerView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.buttonScanAction()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    override func buttonBackAction() {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func buttonScanAction () {
        self.arrayDevices.removeAll()
        self.tableView.reloadData()
        self.activityIndicator.startAnimating()
        self.buttonScan.isHidden = true
        
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.3, execute: {
            self.biosService.startDiscovery(withTimeOut: 4.0, added: { (newEntry) in
                if newEntry == nil {
                    return
                }
                
                print("Success IP: \(newEntry?.ipAddressString ?? "") Name: \(newEntry?.name ?? "")")
                self.arrayDevices.append(newEntry!)
                self.tableView.reloadData()
                
            }) { (deletedEntry) in
                
            }
        })
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

extension TreatmentPlanSelectPCVC: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return max(1, arrayDevices.count)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return arrayDevices.count == 0 ? tableView.frame.height - 40 : 60
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if arrayDevices.count == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "emptyCell")!
            
            return cell
        }
        var cell = tableView.dequeueReusableCell(withIdentifier: "IPCell")
        
        if cell == nil {
            cell = UITableViewCell(style: UITableViewCellStyle.subtitle, reuseIdentifier: "IPCell") as UITableViewCell
        }
        
        let device = self.arrayDevices[indexPath.row]
        cell?.textLabel?.text = device.name
        
        //        let ipAddressString = "68.46.24.31"
        let ipAddressString = device.ipAddressString == nil ? self.ipResolver.resolveIPAddress(withName: device.name, type: device.type) : device.ipAddressString
        cell?.detailTextLabel?.text = ipAddressString!
        
        return cell!
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 60.0
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return headerView
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if arrayDevices.count == 0 {
            return
        }
        
        let device = arrayDevices[indexPath.row]
        
        let ipAddressString = device.ipAddressString == nil ? biosService.resolveIPAddress(withName: device.name!, type: device.type) : device.ipAddressString!
        let hostName = device.name == nil ? biosService.lookupNetworkName(forIPAddress: device.ipAddressString) : device.name!
        
        var questions = MCQuestion.arrayOfQuestions(questions: ["Username *", "Password *"])
        PopupField.popUpView().show(self, title: "Enter computer username & password", questions: questions, textFormats: [.Default, .SecureText]) { (popupView) in
            if questions[0].answer == nil || questions[0].answer!.isEmpty {
                self.showAlert("USERNAME MISSING!")
            } else if questions[1].answer == nil || questions[1].answer!.isEmpty {
                self.showAlert("PASSWORD MISSING!")
            } else {
                BRProgressHUD.show()
                let userName = questions[0].answer!
                let password = questions[1].answer == nil ? "" : questions[1].answer!
                
                let session = SMBFileServer(host: ipAddressString!, netbiosName: hostName ?? "", group: nil)
                session?.connect(asUser: userName, password: password, completion: { (success, error) in
                    popupView.close()
                    BRProgressHUD.hide()
                    if error == nil {
                        session?.disconnect({
                            BRProgressHUD.hide()
                            let config = WorkStationConfig()
                            config.hostIp = ipAddressString
                            config.hostName = hostName
                            config.authName = userName
                            config.authPassword = password
                            config.folderPath = "/"
                            
                            WorkStationConfig.sharedConfig = config
                            WorkStationConfig.hasSystemConfigured = true
                            self.dismiss(animated: true, completion: nil)
                        })
                    } else {
                        BRProgressHUD.hide()
                        self.showAlert(error!.localizedDescription)
                    }
                })
            }
        }
    }
}
