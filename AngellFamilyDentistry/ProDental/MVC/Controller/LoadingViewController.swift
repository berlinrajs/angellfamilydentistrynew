//
//  LoadingViewController.swift
//  MConsentOpenDental
//
//  Created by Berlin Raj on 24/04/17.
//  Copyright © 2017 SRS Web Solutions. All rights reserved.
//

import UIKit

let imageDownloadSession: URLSession = {() -> URLSession in
    let config = URLSessionConfiguration.default
    config.requestCachePolicy = .reloadIgnoringLocalCacheData
    config.urlCache = nil
    
    return URLSession.init(configuration: config)
}()

class LoadingViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        if MCAppDetails.hasPreviousUI == false {
            loadUI()
        } else if MCAppDetails.formsLoadded == false {
            loadForms()
        } else {
            (UIApplication.shared.delegate as! AppDelegate).checkAutologin()
        }
    }
    
//    static let imageDownloadSession: URLSession = {() -> URLSession in
//        let config = URLSessionConfiguration.default
//        config.requestCachePolicy = .reloadIgnoringLocalCacheData
//        config.urlCache = nil
//        
//        return URLSession.init(configuration: config)
//    }()
    
    func getImagefromURL(url: URL, completionBlock:@escaping (_ image : UIImage?) -> Void) {
        
        imageDownloadSession.dataTask(with: url) { (data, response, error) in
            if error == nil {
                DispatchQueue.main.async {
                    completionBlock(UIImage(data: data!))
                }
            } else {
                DispatchQueue.main.async {
                    completionBlock(nil)
                }
            }
            }.resume()
    }
    
    class func reloadImageFromURL(url: URL, completionBlock:@escaping (_ image : UIImage?) -> Void) {
        
        imageDownloadSession.dataTask(with: url) { (data, response, error) in
            if error == nil {
                DispatchQueue.main.async {
                    completionBlock(UIImage(data: data!))
                }
            } else {
                DispatchQueue.main.async {
                    completionBlock(nil)
                }
            }
            }.resume()
    }
    
    func loadUI() {
        BRProgressHUD.showWithTitle("LOADING UI...\nBackground Image")
        let appDetails = MCAppDetails.sharedAppDetails!
        self.getImagefromURL(url: URL(string: "\(kServerImagePath)/\(appDetails.appKey!)/\(appDetails.imageBackgroundName!)")!) { (image1) in
            appDetails.imageBackground = image1
            BRProgressHUD.showWithTitle("LOADING UI...\nBanner Image")
            self.getImagefromURL(url: URL(string: "\(kServerImagePath)/\(appDetails.appKey!)/\(appDetails.topLogoName!)")!) { (image2) in
                appDetails.topLogo = image2
                BRProgressHUD.showWithTitle("LOADING UI...\nBackArrow Image")
                self.getImagefromURL(url: URL(string: "\(kServerImagePath)/\(appDetails.appKey!)/\(appDetails.imageBackButtonName!)")!) { (image4) in
                    appDetails.imageBackButton = image4
                    BRProgressHUD.showWithTitle("LOADING UI...\nButton Image")
                    self.getImagefromURL(url: URL(string: "\(kServerImagePath)/\(appDetails.appKey!)/\(appDetails.imageNextButtonName!)")!) { (image5) in
                        appDetails.imageNextButton = image5
                        BRProgressHUD.showWithTitle("LOADING UI...\nAddress logo Image")
                        self.getImagefromURL(url: URL(string: "\(kServerImagePath)/\(appDetails.appKey!)/\(appDetails.imageAddressLogoName!)")!) { (image6) in
                            appDetails.imageAddressLogo = image6
                            MCAppDetails.sharedAppDetails = appDetails
                            MCAppDetails.hasPreviousUI = true
                            (UIApplication.shared.delegate as! AppDelegate).checkAutologin()
                        }
                    }
                }
            }
        }
    }
    
    class func reloadUI(completion: @escaping ((MCAppDetails)->Void)) {
        BRProgressHUD.showWithTitle("RELOADING UI...\nBackground Image")
        let appDetails = MCAppDetails.sharedAppDetails!
        reloadImageFromURL(url: URL(string: "\(kServerImagePath)/\(appDetails.appKey!)/\(appDetails.imageBackgroundName!)")!) { (image1) in
            appDetails.imageBackground = image1
            BRProgressHUD.showWithTitle("RELOADING UI...\nBanner Image")
            reloadImageFromURL(url: URL(string: "\(kServerImagePath)/\(appDetails.appKey!)/\(appDetails.topLogoName!)")!) { (image2) in
                appDetails.topLogo = image2
                BRProgressHUD.showWithTitle("RELOADING UI...\nBackArrow Image")
                reloadImageFromURL(url: URL(string: "\(kServerImagePath)/\(appDetails.appKey!)/\(appDetails.imageBackButtonName!)")!) { (image4) in
                    appDetails.imageBackButton = image4
                    BRProgressHUD.showWithTitle("RELOADING UI...\nButton Image")
                    reloadImageFromURL(url: URL(string: "\(kServerImagePath)/\(appDetails.appKey!)/\(appDetails.imageNextButtonName!)")!) { (image5) in
                        appDetails.imageNextButton = image5
                        BRProgressHUD.showWithTitle("RELOADING UI...\nAddress logo Image")
                        reloadImageFromURL(url: URL(string: "\(kServerImagePath)/\(appDetails.appKey!)/\(appDetails.imageAddressLogoName!)")!) { (image6) in
                            appDetails.imageAddressLogo = image6
                            completion(appDetails)
                        }
                    }
                }
            }
        }
    }
    
    func loadForms() {
        BRProgressHUD.showWithTitle("LOADING CONSENT FORMS...")
        let appDetails = MCAppDetails.sharedAppDetails!
        Forms.getAllForms { (isConnectionFailed, forms) -> Void in
            appDetails.formList = forms
            
            var index = 0
            
            func fetchCompleted () {
                print("FETCHING finished")
                MCAppDetails.sharedAppDetails = appDetails
                MCAppDetails.formsLoadded = true
                (UIApplication.shared.delegate as! AppDelegate).checkAutologin()
            }
            
            for form in appDetails.formList {
                if form.subForms != nil && form.subForms.count > 0 {
                    for subForm in form.subForms {
                        if subForm.formId != nil {
                            index = index + 1
                            print(subForm.formTitle)
                            
                            subForm.getFormDetails { (success, error) in
                                print("\(subForm.formTitle) Form Completed")
                                index = index - 1
                                if index == 0 {
                                    fetchCompleted()
                                }
                            }
                        }
                    }
                } else {
                    if form.formId != nil {
                        index = index + 1
                        print(form.formTitle)
                        
                        form.getFormDetails { (success, error) in
                            print("\(form.formTitle) Form Completed")
                            index = index - 1
                            if index == 0 {
                                fetchCompleted()
                            }
                        }
                    }
                }
            }
            
            if index == 0 {
                fetchCompleted()
            }
        }
    }
    
    class func reloadForms(completion: @escaping ((MCAppDetails)->Void)) {
        BRProgressHUD.showWithTitle("RELOADING CONSENT FORMS...")
        let appDetails = MCAppDetails.sharedAppDetails!
        Forms.getAllForms { (isConnectionFailed, forms) -> Void in
            appDetails.formList = forms
            
            func fetchCompleted () {
                print("FETCHING finished")
                completion(appDetails)
            }
            
            var index = 0
            for form in appDetails.formList {
                if form.subForms != nil && form.subForms.count > 0 {
                    for subForm in form.subForms {
                        if subForm.formId != nil {
                            index = index + 1
                            print(subForm.formTitle)
                            
                            subForm.getFormDetails { (success, error) in
                                print("\(subForm.formTitle) Form Completed")
                                index = index - 1
                                if index == 0 {
                                    fetchCompleted()
                                }
                            }
                        }
                    }
                } else {
                    if form.formId != nil {
                        index = index + 1
                        print(form.formTitle)
                        
                        form.getFormDetails { (success, error) in
                            print("\(form.formTitle) Form Completed")
                            index = index - 1
                            if index == 0 {
                                fetchCompleted()
                            }
                        }
                    }
                }
            }
            
            if index == 0 {
                fetchCompleted()
            }
        }
    }
}
