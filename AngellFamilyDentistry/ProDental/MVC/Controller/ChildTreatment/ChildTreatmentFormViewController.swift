//
//  ChildTreatmentFormViewController.swift
//   Angell Family Dentistry
//
//  Created by Office on 2/23/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

class ChildTreatmentFormViewController: MCViewController {

//    var patient : PDPatient!
    var textRanges : [NSRange]! = [NSRange]()
    
//    @IBOutlet weak var buttonBack: PDButton!
//    @IBOutlet weak var buttonSubmit: PDButton!

    @IBOutlet weak var imageViewSignature: UIImageView!
    @IBOutlet weak var labelDate: UILabel!
    @IBOutlet weak var labelDetails1: UILabel!
    @IBOutlet weak var labelDetails2: UILabel!
    @IBOutlet weak var tableViewOptions: UITableView!
    @IBOutlet weak var labelParentDetails: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
//        buttonSubmit.backgroundColor = UIColor.greenColor()
        
        imageViewSignature.image = patient.signature1
        labelDate.text = patient.dateToday
        labelParentDetails.text = labelParentDetails.text! + " \(patient.relationship!)."
        // Do any additional setup after loading the view.
        
        var patientInfo = "I am unable to accompany my child"

        let patientName = getText(patient.fullName)
        patientInfo = patientInfo + " \(patientName)"
        textRanges.append(patientInfo.rangeOfText(patientName))
        
        patientInfo = patientInfo + " DOB \(getText(patient.dateOfBirth))"
        textRanges.append(patientInfo.rangeOfText(getText(patient.dateOfBirth)))
        
        let attributedString = NSMutableAttributedString(string: patientInfo)
        for range in textRanges {
            attributedString.addAttribute(NSUnderlineStyleAttributeName, value: NSUnderlineStyle.styleSingle.rawValue, range: range)
        }
        
        attributedString.addAttributes([NSBaselineOffsetAttributeName: NSNumber(value: 0 as Float)], range: NSMakeRange(0, attributedString.length))
        labelDetails1.attributedText = attributedString
        
        let text : String! = patient.treatmentDate == nil ? "N/A" : patient.treatmentDate
        
        let patientReason = "To  Angell Family Dentistry for the treatment on \(getText(text))"
        let attributedStr = NSMutableAttributedString(string: patientReason)
        attributedStr.addAttribute(NSUnderlineStyleAttributeName, value: NSUnderlineStyle.styleSingle.rawValue, range: NSMakeRange(patientReason.count - getText(text).count, getText(text).count))
        attributedStr.addAttributes([NSBaselineOffsetAttributeName: NSNumber(value: 0 as Float)], range: NSMakeRange(0, attributedStr.length))
        labelDetails2.attributedText = attributedStr
        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

//    @IBAction func buttonActionBack(sender: AnyObject) {
//        self.navigationController?.popViewControllerAnimated(true)
//    }
    
    
    
//    @IBAction func buttonActionSubmit(sender: AnyObject) {
//        
//        if !Reachability.isConnectedToNetwork() {
//            let alertController = UIAlertController(title: "Angell Family Dentistry", message: "Your device is not connected to internet. Please go to settings to connect.", preferredStyle: UIAlertControllerStyle.Alert)
//            let alertOkAction = UIAlertAction(title: "Settings", style: UIAlertActionStyle.Destructive) { (action) -> Void in
//                let settingsUrl = NSURL(string: UIApplicationOpenSettingsURLString)
//                if let url = settingsUrl {
//                    UIApplication.sharedApplication().openURL(url)
//                }
//            }
//            let alertCancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.Destructive) { (action) -> Void in
//                
//            }
//            alertController.addAction(alertOkAction)
//            alertController.addAction(alertCancelAction)
//            self.presentViewController(alertController, animated: true, completion: nil)
//            return
//        }
//        let pdfManager = PDFManager()
//        pdfManager.authorizeDrive(self.view) { (success) -> Void in
//            if success {
//                self.buttonSubmit.hidden = true
//                self.buttonBack.hidden = true
//                pdfManager.createPDFForView(self.view, fileName: "TREATMENT_OF_MINOR_CHILD", patient: self.patient, completionBlock: { (finished) -> Void in
//                    if finished {
//                        self.patient = self.patient.patient()
//                        let formNames = (self.patient.selectedForms as NSArray).valueForKey("formTitle") as! [String]
//                        if formNames.contains(kEndodonticTreatment) {
//                            let endodonticTreatmentVC = self.storyboard?.instantiateViewControllerWithIdentifier("kEndodonticTreatmentVC") as! EndodonticTreatmentViewController
//                            endodonticTreatmentVC.patient = self.patient
//                            self.navigationController?.pushViewController(endodonticTreatmentVC, animated: true)
//                        } else if formNames.contains(kDentalCrown) {
//                            let dentalCrownVC = self.storyboard?.instantiateViewControllerWithIdentifier("kDentureCrownVC") as! DentalCrownViewController
//                            dentalCrownVC.patient = self.patient
//                            self.navigationController?.pushViewController(dentalCrownVC, animated: true)
//                        } else if formNames.contains(kOpiodForm) {
//                            let opiodVC = self.storyboard?.instantiateViewControllerWithIdentifier("kOpiodViewController") as! OpiodViewController
//                            opiodVC.patient = self.patient
//                            self.navigationController?.pushViewController(opiodVC, animated: true)
//                        }else if formNames.contains(kGeneralConsent) {
//                            let generalVC = self.storyboard?.instantiateViewControllerWithIdentifier("kGeneralConsentVC") as! GeneralConsentViewController
//                            generalVC.patient = self.patient
//                            self.navigationController?.pushViewController(generalVC, animated: true)
//                        }else if formNames.contains(kFeedbackForm) {
//                            let feedbackFormVC = self.storyboard?.instantiateViewControllerWithIdentifier("kFeedbackFormVC") as! FeedbackFormViewController
//                            self.navigationController?.pushViewController(feedbackFormVC, animated: true)
//                        } else {
//                            NSNotificationCenter.defaultCenter().postNotificationName(kFormsCompletedNotification, object: nil)
//                            self.navigationController?.popToRootViewControllerAnimated(true)
//                        }
//                    } else {
//                        self.buttonSubmit.hidden = false
//                        self.buttonBack.hidden = false
//                    }
//                })
//            }
//        }
//    }
}


extension ChildTreatmentFormViewController : UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 7
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cellChildTreatment", for: indexPath) as! ChildTreatmentTableViewCell
        let titles = ["TOOTH CLEANING", "SEALANTS", "FILLINGS", "EXTRACTIONS OF PRIMARY TEETH (BABY TEETH)", "EXTRACTION OF PERMANENT TEETH", "ROOT CANAL TREATMENT", "OTHER"]
//        cell.labelTitle.text = titles[indexPath.row]
        if indexPath.row == 6 &&  patient.selectedOptions.contains(titles[6]) && patient.otherTreatment != nil {
            let text = "OTHER \(getText(patient.otherTreatment!))"
            let attributedStr = NSMutableAttributedString(string: text)
            attributedStr.addAttribute(NSUnderlineStyleAttributeName, value: NSUnderlineStyle.styleSingle.rawValue, range: NSMakeRange(text.count - getText(patient.otherTreatment!).count, getText(patient.otherTreatment!).count))
            attributedStr.addAttributes([NSBaselineOffsetAttributeName: NSNumber(value: 0 as Float)], range: NSMakeRange(0, attributedStr.length))
            cell.labelTitle.attributedText = attributedStr
        } else {
            let text = titles[indexPath.row]
            let attributedStr = NSMutableAttributedString(string: text)
            attributedStr.addAttributes([NSBaselineOffsetAttributeName: NSNumber(value: 0 as Float)], range: NSMakeRange(0, attributedStr.length))
            cell.labelTitle.attributedText = attributedStr
        }
        cell.buttonRound.isSelected = patient.selectedOptions.contains(titles[indexPath.row])
        cell.backgroundColor = UIColor.clear
        cell.contentView.backgroundColor = UIColor.clear        
        return cell
        
    }
}
