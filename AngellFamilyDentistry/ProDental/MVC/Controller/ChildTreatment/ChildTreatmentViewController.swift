//
//  ChildTreatmentViewController.swift
//   Angell Family Dentistry
//
//  Created by Leojin Bose on 2/22/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

class ChildTreatmentViewController: MCViewController {

//    var isFromPreviousForm : Bool! = true
    var textRanges : [NSRange]! = [NSRange]()
    var selectedOptions : [String]! = [String]()
    
//    @IBOutlet var datePicker: UIDatePicker!
//    @IBOutlet var toolBar: UIToolbar!
    @IBOutlet weak var textFieldDate: MCTextField!
    @IBOutlet weak var textFieldOthers: MCTextField!
//    @IBOutlet weak var buttonBack: PDButton!
    @IBOutlet weak var signatureView: SignatureView!
    @IBOutlet weak var labelDate: DateLabel!
    @IBOutlet weak var labelDetails: UILabel!
    @IBOutlet weak var tableViewOptions: UITableView!
    @IBOutlet weak var radioButtonParent: RadioButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        textFieldOthers.isEnabled = false
        DateInputView.addDatePickerForTextField(textFieldDate)
//        self.buttonBack.hidden = isFromPreviousForm
//        textFieldDate.inputAccessoryView = toolBar
//        textFieldDate.inputView = datePicker
        
        
        var patientInfo = "I am unable to accompany my child"
        
        let patientName = getText(patient.fullName)
        patientInfo = patientInfo + " \(patientName)"
        textRanges.append(patientInfo.rangeOfText(patientName))
        
        patientInfo = patientInfo + " DOB \(getText(patient.dateOfBirth))"
        textRanges.append(patientInfo.rangeOfText(getText(patient.dateOfBirth)))
        
        let attributedString = NSMutableAttributedString(string: patientInfo)
        for range in textRanges {
            attributedString.addAttribute(NSUnderlineStyleAttributeName, value: NSUnderlineStyle.styleSingle.rawValue, range: range)
        }
        
        attributedString.addAttributes([NSBaselineOffsetAttributeName: NSNumber(value: 0 as Float)], range: NSMakeRange(0, attributedString.length))
        labelDetails.attributedText = attributedString
        
        // Do any additional setup after loading the view.
        labelDate.todayDate = patient.dateToday
        loadValues()
        // Do any additional setup after loading the view.
    }
    
    func loadValues() {
        //labelDate.setDate = patient.isDate1Tapped
        textFieldDate.text = patient.treatmentDate
        textFieldOthers.text = patient.otherTreatment
        if patient.selectedOptions != nil {
            self.selectedOptions = patient.selectedOptions
        }
        if let rel = patient.relationship {
            radioButtonParent.setSelectedWithTag(rel == "Parent" ? 1 : 2)
        }

    }
    
    func setValues() {
        patient.isDate1Tapped = labelDate.dateTapped
        patient.signature1 = signatureView.signatureImage()
        if !textFieldDate.isEmpty {
            patient.treatmentDate = textFieldDate.text
        } else {
            patient.treatmentDate = nil
        }
        if !textFieldOthers.isEmpty {
            patient.otherTreatment = textFieldOthers.text
        } else {
            patient.otherTreatment = nil
        }
        patient.selectedOptions = self.selectedOptions
        if radioButtonParent.selected != nil {
            patient.relationship = radioButtonParent.selected.tag == 1 ? "Parent" : "Guardian"
        }
    }
    
    @IBAction override func buttonBackAction() {
        self.setValues()
        super.buttonBackAction()
    }
    
    @IBAction func buttonActionNext(_ sender: AnyObject) {
        self.view.endEditing(true)
        if self.selectedOptions.count == 0 {
            self.showAlert("PLEASE SELECT ALL THAT APPLY")
        } else if self.selectedOptions.contains("OTHER") && textFieldOthers.isEmpty {
            self.showAlert("PLEASE TYPE OTHER TREATMENT")
        } else if radioButtonParent.selected == nil {
            self.showAlert("PLEASE SELECT RELATIONSHIP")
        } else if !signatureView.isSigned() {
            self.showAlert("PLEASE SIGN THE FORM")
        } else if !labelDate.dateTapped {
            self.showAlert("PLEASE SELECT DATE")
        } else {
            setValues()
            let childTreatmentFormVC = self.storyboard?.instantiateViewController(withIdentifier: "kChildTreatmentFormVC") as! ChildTreatmentFormViewController
            childTreatmentFormVC.patient = patient
            self.navigationController?.pushViewController(childTreatmentFormVC, animated: true)
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
//    @IBAction func toolbarDoneButtonAction(sender: AnyObject) {
//        textFieldDate.resignFirstResponder()
//        let dateFormatter = DateFormatter()
//        dateFormatter.dateFormat = "MMMM dd, yyyy"
//        textFieldDate.text = dateFormatter.stringFromDate(datePicker.date).uppercaseString
//    }
//    
//    @IBAction func datePickerDateChanged(sender: AnyObject) {
//        let dateFormatter = DateFormatter()
//        dateFormatter.dateFormat = "MMMM dd, yyyy"
//        textFieldDate.text = dateFormatter.stringFromDate(datePicker.date).uppercaseString
//    }
}

extension ChildTreatmentViewController : UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let titles = ["TOOTH CLEANING", "SEALANTS", "FILLINGS", "EXTRACTIONS OF PRIMARY TEETH (BABY TEETH)", "EXTRACTION OF PERMANENT TEETH", "ROOT CANAL TREATMENT", "OTHER"]
        let title = titles[indexPath.row]
        if selectedOptions.contains(title) {
            selectedOptions.remove(at: selectedOptions.index(of: title)!)
        } else {
            selectedOptions.append(title)
        }
        if !selectedOptions.contains(titles[6]) {
            textFieldOthers.text = ""
        }
        tableView.reloadData()
    }
}

extension ChildTreatmentViewController : UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 7
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cellChildTreatment", for: indexPath) as! ChildTreatmentTableViewCell
        let titles = ["TOOTH CLEANING", "SEALANTS", "FILLINGS", "EXTRACTIONS OF PRIMARY TEETH (BABY TEETH)", "EXTRACTION OF PERMANENT TEETH", "ROOT CANAL TREATMENT", "OTHER"]
        cell.labelTitle.text = titles[indexPath.row]
        cell.buttonRound.isSelected = selectedOptions.contains(titles[indexPath.row])
        cell.backgroundColor = UIColor.clear
        cell.contentView.backgroundColor = UIColor.clear
        textFieldOthers.isEnabled = selectedOptions.contains(titles[6])
        
        return cell
        
    }
}

extension ChildTreatmentViewController : UITextFieldDelegate {
    
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let maxLength = 40
        let currentString: NSString = textField.text! as NSString
        let newString: NSString =
            currentString.replacingCharacters(in: range, with: string) as NSString
        return newString.length <= maxLength
    }
}
