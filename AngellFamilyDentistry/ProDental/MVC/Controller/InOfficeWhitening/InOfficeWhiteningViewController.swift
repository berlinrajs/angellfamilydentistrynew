//
//  InOfficeWhiteningViewController.swift
//   Angell Family Dentistry
//
//  Created by Leojin Bose on 2/22/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

class InOfficeWhiteningViewController: MCViewController {

//    var patient : PDPatient!
//    var isFromPreviousForm : Bool! = true
//    @IBOutlet weak var buttonBack: PDButton!
    @IBOutlet weak var signatureView1: SignatureView!
    @IBOutlet weak var labelDate1: DateLabel!
    @IBOutlet weak var labelName: UILabel!
    @IBOutlet weak var labelDetails: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        
        let patientName = patient.fullName
        labelName.text = patientName
        
        labelDate1.todayDate = patient.dateToday
        loadValues()
        // Do any additional setup after loading the view.
    }
    
    func loadValues() {
        //labelDate1.setDate = patient.isDate1Tapped
    }
    
    func setValues() {
        patient.isDate1Tapped = labelDate1.dateTapped
        patient.signature1 = signatureView1.signatureImage()
    }

    
    @IBAction override func buttonBackAction() {
        self.setValues()
        super.buttonBackAction()
    }
    
    
     @IBAction func buttonActionNext(_ sender: AnyObject) {
        if !signatureView1.isSigned() {
            self.showAlert("PLEASE SIGN THE FORM")
        } else if !labelDate1.dateTapped {
            self.showAlert("PLEASE SELECT DATE")
        } else {
            setValues()
            let inOfficeWhiteningFormVC = self.storyboard?.instantiateViewController(withIdentifier: "kInOfficeWhiteningFormVC") as! InOfficeWhiteningFormViewController
            inOfficeWhiteningFormVC.patient = patient
            self.navigationController?.pushViewController(inOfficeWhiteningFormVC, animated: true)
        }
    }

    
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
