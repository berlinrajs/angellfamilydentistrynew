//
//  ConsentViewController.swift
//  MConsentSample
//
//  Created by Leojin Bose on 12/6/16.
//  Copyright © 2016 SRS Web Solutions. All rights reserved.
//

import UIKit

class ConsentViewController: MCViewController {
    
    @IBOutlet weak var labelFormTitle: UILabel!
    @IBOutlet weak var labelContent: UILabel!
    
    @IBOutlet weak var viewSignatures: UIView!
    @IBOutlet weak var viewDate: UIView!
    @IBOutlet weak var constraintBottomViewHeight: NSLayoutConstraint!
    @IBOutlet weak var constraintTextHeight: NSLayoutConstraint!
    @IBOutlet weak var constraintTitleHeight: NSLayoutConstraint!
    
    var signatureViews :[SignatureView] = [SignatureView]()
    var dateLabels :[DateLabel] = [DateLabel]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    var form: Forms {
        return patient.selectedForms.first!
    }
    
    override func loadView() {
        super.loadView()
        let htmlString = "<div style='text-align:justify; font-size:18px;font-family:\(labelContent.font.fontName);color:#FFFFFF;'>\(form.formContent!)</div>"
        if let htmlData = (htmlString.replacingOccurrences(of: "kClinicName", with: MCAppDetails.sharedAppDetails!.clinicName).replacingOccurrences(of: "kPatientName", with: patient.fullName).replacingOccurrences(of: "kDentistName", with: patient.dentistName != nil ? patient.dentistName : "").replacingOccurrences(of: "kToothNumber", with: patient.selectedForms.first!.toothNumbers == nil ? "" : patient.selectedForms.first!.toothNumbers)).data(using: String.Encoding.unicode) {
            do {
                let attributedText = try NSAttributedString(data: htmlData,
                                                            options: [NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType],
                                                            documentAttributes: nil)
                labelContent.attributedText = attributedText
                //                constraintTextHeight.constant = attributedText.heightWithConstrainedWidth(labelContent.frame.width)
            } catch let e as NSError {
                print("Couldn't translate \(form.formContent): \(e.localizedDescription) ")
            }
        }
        labelFormTitle.text = form.formTitle
        
        if let signatures = form.signatures {
            
            constraintBottomViewHeight.constant = 136.0 * CGFloat(signatures.count)
            
            viewSignatures.frame = CGRect(x: viewSignatures.frame.origin.x, y: viewSignatures.frame.origin.y, width: viewSignatures.frame.width, height: constraintBottomViewHeight.constant)
            viewDate.frame = CGRect(x: viewDate.frame.origin.x, y: viewDate.frame.origin.y, width: viewDate.frame.width, height: constraintBottomViewHeight.constant)
            
            //            DispatchQueue.global(qos: DispatchQoS.QoSClass.background).async {
            for (idx, signature) in signatures.enumerated() {
                let sign = ConsentControls.ConsentSignature()
                sign.frame = CGRect(x: 0, y: 136.0 * CGFloat(idx), width: self.viewSignatures.frame.width, height: 116.0)
                //                    DispatchQueue.main.async {
                self.viewSignatures.addSubview(sign)
                sign.labelTitle?.text = signature.title?.uppercased()
                //                    }
                self.signatureViews.append(sign.signatureView)
                
                let date = ConsentControls.ConsentDate()
                date.frame = CGRect(x: 0, y: 136.0 * CGFloat(idx), width: self.viewDate.frame.width, height: 116.0)
                date.dateLabel?.todayDate = self.patient.dateToday
                DispatchQueue.main.async {
                    date.labelTitle.text = "DATE"
                    self.viewDate.addSubview(date)
                }
                self.dateLabels.append(date.dateLabel)
            }
            //            }
        }
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        constraintTitleHeight.constant = form.formTitle.heightWithConstrainedWidth(screenSize.width - 70, font: labelFormTitle.font!) + 10
        let htmlString = "<div style='text-align:justify; font-size:18px;font-family:\(labelContent.font.fontName);color:#FFFFFF;'>\(form.formContent!)</div>"
        if let htmlData = (htmlString.replacingOccurrences(of: "kClinicName", with: MCAppDetails.sharedAppDetails!.clinicName).replacingOccurrences(of: "kPatientName", with: patient.fullName).replacingOccurrences(of: "kDentistName", with: patient.dentistName != nil ? patient.dentistName : "").replacingOccurrences(of: "kToothNumber", with: patient.selectedForms.first!.toothNumbers == nil ? "" : patient.selectedForms.first!.toothNumbers)).data(using: String.Encoding.unicode) {
            do {
                let attributedText = try NSAttributedString(data: htmlData,
                                                            options: [NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType],
                                                            documentAttributes: nil)
                constraintTextHeight.constant = attributedText.heightWithConstrainedWidth(labelContent.frame.width)
            } catch _ {
                
            }
        }
        if let signatures = form.signatures {
            constraintBottomViewHeight.constant = 136.0 * CGFloat(signatures.count)
        }
    }
    
    @IBAction func buttonActionDone(_ sender: AnyObject) {
        if hasEmptySignature() {
            self.showAlert("PLEASE SIGN THE FORM")
        } else if hasEmptyDate() {
            self.showAlert("PLEASE SELECT THE DATE")
        } else {
            for (idx, signature) in signatureViews.enumerated() {
                let sign = form.signatures?[idx]
                sign?.signature = signature.signatureImage()
            }
            let consentFormVC = self.storyboard?.instantiateViewController(withIdentifier: "kConsentFormVC") as! ConsentFormViewController
            consentFormVC.patient = self.patient
            self.navigationController?.pushViewController(consentFormVC, animated: true)
        }
    }
    
    func hasEmptySignature() -> Bool {
        for signature in signatureViews {
            if !signature.isSigned() {
                return true
            }
        }
        return false
    }
    
    func hasEmptyDate() -> Bool {
        for date in dateLabels {
            if !date.dateTapped {
                return true
            }
        }
        return false
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}
