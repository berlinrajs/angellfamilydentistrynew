//
//  ConsentFormViewController.swift
//  MConsentSample
//
//  Created by Leojin Bose on 12/6/16.
//  Copyright © 2016 SRS Web Solutions. All rights reserved.
//

import UIKit

let consentFormFont = UIFont(name: "Helvetica Neue", size: 16)!

class ConsentFormViewController: MCViewController {
    
    @IBOutlet weak var labelTitle : UILabel!
    @IBOutlet weak var constraintContainerviewHeight : NSLayoutConstraint!
    
    @IBOutlet weak var labelPatientName : UILabel!
    @IBOutlet weak var labelDOB: UILabel!
    @IBOutlet weak var viewContainer: UIView!
    
    var attributedConsentText: NSMutableAttributedString!
    var totalTextheight: CGFloat!
    
    var form: Forms {
        return patient.selectedForms.first!
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        labelTitle.text = form.formTitle
        labelPatientName.text = patient.fullName
        labelDOB.text = patient.dateOfBirth
        
        self.loadFormValues()
    }
    
    let horizontalMarginSpace: CGFloat = 60.0
    let verticalMarginSpace: CGFloat = 30.0
    let heightDetailsLabel: CGFloat = 25.0
    let heightSignatureView: CGFloat = 110.0
    
    func topMarginAt(Point y: CGFloat) -> CGFloat {
        
        let gap = y - (screenSize.height * CGFloat(Int(y/screenSize.height)))
        //        let gap = y.truncatingRemainder(dividingBy: screenSize.height)
        //        print("TOP GAP: \(gap)  \(gap2)")
        return gap
    }
    
    func bottomMarginAt(Point y: CGFloat) -> CGFloat {
        let gap = (screenSize.height * CGFloat(Int(y/screenSize.height) + 1)) - y
        //        let gap = screenSize.height - y.truncatingRemainder(dividingBy: screenSize.height)
        //        print("BOTTOM GAP: \(gap)   \(gap2)")
        return gap
    }
    
    func loadFormValues() {
        let htmlString = "<div style='text-align:justify; font-size:14px;font-family:\(consentFormFont.fontName);color:#000000;'>\(form.formContent!)</div>"
        //        if let htmlData = (htmlString.replacingOccurrences(of: "kClinicName", with: kClinicName).replacingOccurrences(of: "kPatientName", with: patient.fullName).replacingOccurrences(of: "kDentistName", with: patient.dentistName != nil ? patient.dentistName : "")).data(using: String.Encoding.unicode) {
        if let htmlData = htmlString.data(using: String.Encoding.unicode) {
            do {
                attributedConsentText =  try NSMutableAttributedString(data: htmlData, options: [NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType], documentAttributes: nil)
            } catch let e as NSError {
                print("Couldn't translate \(form.formContent): \(e.localizedDescription) ")
                attributedConsentText = NSMutableAttributedString()
            }
        } else {
            attributedConsentText = NSMutableAttributedString()
        }
        
        attributedConsentText.mutableString.replaceOccurrences(of: "kClinicName", with: MCAppDetails.sharedAppDetails!.clinicName, options: NSString.CompareOptions.forcedOrdering, range: NSMakeRange(0, attributedConsentText.mutableString.length))
        attributedConsentText.mutableString.replaceOccurrences(of: "kPatientName", with: patient.fullName, options: NSString.CompareOptions.forcedOrdering, range: NSMakeRange(0, attributedConsentText.mutableString.length))
        attributedConsentText.mutableString.replaceOccurrences(of: "kDentistName", with: patient.dentistName != nil ? patient.dentistName : "", options: NSString.CompareOptions.forcedOrdering, range: NSMakeRange(0, attributedConsentText.mutableString.length))
        if let toothNumbers = patient.selectedForms.first?.toothNumbers {
            attributedConsentText.mutableString.replaceOccurrences(of: "kToothNumber", with: toothNumbers, options: NSString.CompareOptions.forcedOrdering, range: NSMakeRange(0, attributedConsentText.mutableString.length))
        }
        
        self.loadContent(atPageIndex: 0, WithText: self.attributedConsentText)
    }
    
    func loadContent (atPageIndex page: Int, WithText text: NSAttributedString) {
        let y = max((CGFloat(page) * screenSize.height) + horizontalMarginSpace, 200.0)
        
        let textView = ConsentControls.ConsentTextView()
        textView.attributedText = text.trimmedAttributedText
        let pageView = UIView(frame: CGRect(x: 0, y: y, width: screenSize.width, height: (CGFloat(page + 1) * screenSize.height) - y - horizontalMarginSpace))
        textView.frame = CGRect(x: verticalMarginSpace, y: 0, width: screenSize.width - CGFloat(2 * verticalMarginSpace), height: pageView.frame.size.height)
        pageView.addSubview(textView)
        self.viewContainer.addSubview(pageView)
        
        let allRange: NSRange = NSMakeRange(0, textView.textStorage.length)
        
        textView.layoutManager.ensureLayout(for: textView.textContainer)
        textView.layoutManager.enumerateLineFragments(forGlyphRange: allRange) { (rect, usedRect, textContainer, resultRange, glyphRange) in
//            print("RECT: \(rect)")
//            print("USED RECT :\(usedRect)")
//            //            print("TEXT CONTAINER: \(textContainer)")
//            print("RESULT RANGE :\(text.length) \(resultRange.location + resultRange.length)")
//            print("GLYPH RANGE :\(glyphRange)")
//            print("Remaining Sting: \(text.attributedSubstring(from: resultRange))")
            
            let remainingText = text.attributedSubstring(from: resultRange)
            
            if usedRect == CGRect.zero && rect == CGRect.zero {
                if remainingText.string.count > 0 {
                    self.loadContent(atPageIndex: page + 1, WithText: remainingText)
                    return
//                } else {
//                    textView.frame.size.height = textView.attributedText.boundingRect(with: textView.frame.size, options: [.usesLineFragmentOrigin, .usesFontLeading], context: nil).size.height
//                    pageView.frame.size.height = textView.frame.height
//                    self.addDetailsLabel(atY: y + textView.frame.height + self.horizontalMarginSpace/2)
                }
            } else if textView.attributedText.length == resultRange.location + resultRange.length {
                textView.frame.size.height = text.heightWithConstrainedWidth(textView.frame.width)
                pageView.frame.size.height = textView.frame.height
                self.addDetailsLabel(atY: y + textView.frame.height + self.horizontalMarginSpace/2)
            }
        }
    }
    
    func addDetailsLabel(atY startY: CGFloat) {
        var y = startY
        if let values = form.requiredValues, values.count > 0 {
            for ques in values {
                let topMargin = topMarginAt(Point: y)
                if  topMargin < horizontalMarginSpace {
                    y = y + (horizontalMarginSpace - topMargin)
                }
                
                let bottomMargin = bottomMarginAt(Point: y)
                if bottomMargin < 50 + heightDetailsLabel {
                    y = y + bottomMargin + horizontalMarginSpace + heightDetailsLabel
                }
                
                let labelView = ConsentControls.PDFLabel()
                labelView.setText(ques)
                labelView.frame = CGRect(x: verticalMarginSpace, y: y, width: screenSize.width - CGFloat(2 * verticalMarginSpace), height: heightDetailsLabel)
                viewContainer.addSubview(labelView)
                y = y + heightDetailsLabel
            }
            self.addSignatures(atY: y + horizontalMarginSpace/2)
        } else {
            self.addSignatures(atY: startY)
        }
    }
    
    func addSignatures(atY startY: CGFloat) {
        var y = startY
        if let signatures = form.signatures {
            for signature in signatures {
                let topMargin = topMarginAt(Point: y)
                if  topMargin < horizontalMarginSpace {
                    y = y + (horizontalMarginSpace - topMargin)
                }
                
                let bottomMargin = bottomMarginAt(Point: y)
                if bottomMargin < 50 + heightSignatureView {
                    y = y + bottomMargin + horizontalMarginSpace + heightSignatureView
                }
                
                let sign = ConsentControls.ConsentSignatureDate()
                sign.labelTitleSignature.text = signature.title?.uppercased()
                sign.labelDate.text = patient.dateToday
                sign.signatureImageView.image = signature.signature
                sign.frame = CGRect(x: verticalMarginSpace, y: y, width: screenSize.width - CGFloat(2 * verticalMarginSpace), height: heightSignatureView)
                self.viewContainer.addSubview(sign)
                y = y + heightSignatureView
            }
            let totalContentHeight = y + bottomMarginAt(Point: y)
            constraintContainerviewHeight.constant = totalContentHeight
            self.view.layoutIfNeeded()
        } else {
            constraintContainerviewHeight.constant = startY + (screenSize.height - startY.remainder(dividingBy: screenSize.height))
            self.view.layoutIfNeeded()
        }
    }
}

extension UITextView {
    
    func getRemainingAttributedText(attributedString: NSAttributedString) -> NSAttributedString? {
        
        let location: Int = 0
        var length: Int = 0
        
        var heightToFit: CGFloat = attributedString.attributedSubstring(from: NSMakeRange(location, length)).heightWithConstrainedWidth(self.frame.width)
        
        while(heightToFit <= self.frame.height) {
            length = length + 1
            
            if length > attributedString.length {
                break;
            }
            heightToFit = attributedString.attributedSubstring(from: NSMakeRange(location, length)).heightWithConstrainedWidth(self.frame.width)
        }
        length = length - 1
        self.attributedText = attributedString.attributedSubstring(from: NSMakeRange(location, length))
        if length == attributedString.length {
            return nil
        }
        return attributedString.attributedSubstring(from: NSMakeRange(length, attributedString.length - length - 1))
    }
    
    func setAttributedTextToFit(attributedString: NSAttributedString) -> NSAttributedString? {
        
        let frameSetterRef = CTFramesetterCreateWithAttributedString(attributedString)
        var characterFitRange = CFRange()
        let _ = CTFramesetterSuggestFrameSizeWithConstraints(frameSetterRef, CFRangeMake(0, 0), nil, CGSize(width: self.frame.size.width, height: self.frame.size.height), &characterFitRange)
        
        self.attributedText = attributedString.attributedSubstring(from: NSMakeRange(0, characterFitRange.length as Int))
        
        if attributedString.length - (characterFitRange.length as Int) > 0 {
            return attributedString.attributedSubstring(from: NSMakeRange((characterFitRange.length as Int), attributedString.length - (characterFitRange.length as Int) - 1))
        } else {
            return nil
        }
    }
}
