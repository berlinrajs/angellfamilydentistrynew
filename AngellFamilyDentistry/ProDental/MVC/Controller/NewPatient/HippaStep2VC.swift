//
//  HippaStep2VC.swift
//   Angell Family Dentistry
//
//  Created by SRS Web Solutions on 05/05/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

class HippaStep2VC: MCViewController {
    
    @IBOutlet weak var signatureView: SignatureView!
    @IBOutlet weak var buttonCall: UIButton!
    @IBOutlet weak var buttonText: UIButton!
    @IBOutlet weak var labelPhone: FormLabel!
    @IBOutlet weak var labelDate: DateLabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        labelDate.todayDate = patient.dateToday
        loadValues()
        // Do any additional setup after loading the view.
    }
    
    func loadValues() {
        //labelDate.setDate = patient.isDate4Tapped
        labelPhone.text = patient.phoneNumber
        if let call = patient.isCallAvail {
            buttonCall.isSelected = call
        }
        if let text = patient.isTextAvail {
            buttonText.isSelected = text
        }
    }
    
    func setValues() {
        patient.isDate4Tapped = labelDate.dateTapped
        patient.signature4 = signatureView.signatureImage()
        patient.isCallAvail = buttonCall.isSelected
        patient.isTextAvail = buttonText.isSelected
    }
    
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func backAction(_ sender: AnyObject) {
        setValues()
        navigationController?.popViewController(animated: true)
    }
    @IBAction func nextAction(_ sender: AnyObject) {
        if !signatureView.isSigned() {
            self.showAlert("PLEASE SIGN THE FORM")
        } else if !labelDate.dateTapped {
            self.showAlert("PLEASE SELECT DATE")
        } else {
            if !buttonText.isSelected && !buttonCall.isSelected {
                self.showAlert("PLEASE SELECT ANY OF THE CONTACT OPTIONS")
            } else {
                setValues()
                let step3VC = self.storyboard?.instantiateViewController(withIdentifier: "AppointmentVC") as! AppointmentViewController
                step3VC.patient = patient
                navigationController?.pushViewController(step3VC, animated: true)
            }
        }
    }
    
    @IBAction func buttonSelected(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
