//
//  HippaStep3VC.swift
//   Angell Family Dentistry
//
//  Created by SRS Web Solutions on 05/05/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

class HippaStep3VC: MCViewController {

    @IBOutlet weak var labelName: UILabel!
    @IBOutlet weak var labelDate: DateLabel!
    @IBOutlet weak var signatureView: SignatureView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        labelDate.todayDate = patient.dateToday
        loadValues()
        // Do any additional setup after loading the view.
    }
    
    func loadValues() {
        //labelDate.setDate = patient.isDate1Tapped
    }
    
    func setValues() {
        patient.isDate1Tapped = labelDate.dateTapped
        patient.signature5 = signatureView.signatureImage()
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func actionBack(_ sender: AnyObject) {
        setValues()
        navigationController?.popViewController(animated: true)
    }
    @IBAction func actionNext(_ sender: AnyObject) {
        if !signatureView.isSigned() {
            self.showAlert("PLEASE SIGN THE FORM")
        } else if !labelDate.dateTapped {
            self.showAlert("PLEASE SELECT DATE")
        } else {
            setValues()
            let newPatientVC = self.storyboard?.instantiateViewController(withIdentifier: "kNewPatientVC") as! NewPatientViewController
            newPatientVC.patient = patient
            self.navigationController?.pushViewController(newPatientVC, animated: true)
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
