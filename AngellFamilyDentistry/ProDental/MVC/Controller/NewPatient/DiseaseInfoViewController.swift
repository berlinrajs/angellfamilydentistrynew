//
//  DiseaseInfoViewController.swift
//   Angell Family Dentistry
//
//  Created by Leojin Bose on 2/19/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

class DiseaseInfoViewController: MCViewController {
    
    @IBOutlet weak var textViewReasonForTodayDentalVisit: MCTextView!
    @IBOutlet weak var textFieldLastDentalVisit: MCTextField!
//    @IBOutlet var datePicker: UIDatePicker!
//    @IBOutlet var toolBar: UIToolbar!

    @IBOutlet weak var buttonConcerns: RadioButton!
    @IBOutlet weak var radioButtonPain: RadioButton!
    
    @IBOutlet var viewPopup: PDView!
    @IBOutlet weak var viewShadow: UIView!
    @IBOutlet weak var textViewOthers: PDTextView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        DateInputView.addDatePickerForTextField(textFieldLastDentalVisit)
//        datePicker.maximumDate = Date()

        
//        textFieldLastDentalVisit.inputView = datePicker
//        textFieldLastDentalVisit.inputAccessoryView = toolBar
//        textFieldLastDentalVisit.placeholder = patient.dateToday
        // Do any additional setup after loading the view.
        loadValues()
        // Do any additional setup after loading the view.
    }
    
    func loadValues() {
        textViewReasonForTodayDentalVisit.textValue = patient.reasonForTodayVisit
        //textViewReasonForTodayDentalVisit.checkEdited()
        textFieldLastDentalVisit.text = patient.lastDentalVisit
        radioButtonPain.setSelectedWithTag(patient.isHavingPain == true ? 1 : 2)
        if let _ = patient.anyConcerns {
            buttonConcerns.setSelectedWithTag(1)
        }
    }
    
    func setValues() {
        if !textFieldLastDentalVisit.isEmpty {
            patient.lastDentalVisit = textFieldLastDentalVisit.text
        } else {
            patient.lastDentalVisit = nil
        }
         patient.reasonForTodayVisit = textViewReasonForTodayDentalVisit.textValue!
        
        patient.isHavingPain = radioButtonPain.selected.tag == 1
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @IBAction override func buttonBackAction() {
        self.setValues()
        super.buttonBackAction()
    }
    
    @IBAction func buttonActionNext(_ sender: AnyObject) {
        self.view.endEditing(true)
        
        setValues()
        let agreementVC = self.storyboard?.instantiateViewController(withIdentifier: "kAgreementVC") as! AgreementViewController
        agreementVC.patient = patient
        self.navigationController?.pushViewController(agreementVC, animated: true)
    }
    
    
    @IBAction func buttonActionYesNo(_ sender: RadioButton) {
        if sender.tag == 1 {
            showPopup(1)
        }else{
            patient.painLocation = nil
        }
    }
    
    
    @IBAction func buttonActionConcerns(_ sender: RadioButton) {
        if sender.tag == 1 {
            showPopup(2)
        }
        else
        {
            patient.anyConcerns = nil
        }
    }
    
    @IBAction func buttonActionPopupOk(_ sender: AnyObject) {
        if textViewOthers.tag == 1 {
            if !textViewOthers.isEmpty && textViewOthers.text != "IF YES, WHERE" {
                patient.painLocation = textViewOthers.text
            } else {
                patient.painLocation = nil
                radioButtonPain.setSelectedWithTag(2)
            }
        } else {
            if !textViewOthers.isEmpty && textViewOthers.text != "IF YES TYPE HERE" {
                patient.anyConcerns = textViewOthers.text
            } else {
                patient.anyConcerns = nil
                buttonConcerns.setSelectedWithTag(2)
            }
        }
        textViewOthers.resignFirstResponder()
        self.viewPopup.removeFromSuperview()
        self.viewShadow.isHidden = true
    }
    
    func showPopup(_ tag : Int) {
        let frameSize = CGRect(x: 0, y: 0, width: 348, height: 174)
        self.viewPopup.frame = frameSize
        self.viewPopup.center = self.view.center
        self.viewShadow.addSubview(self.viewPopup)
        self.viewPopup.transform = CGAffineTransform(scaleX: 0.1, y: 0.1)
        textViewOthers.text = tag == 1 ? "IF YES, WHERE" : "IF YES TYPE HERE"
        textViewOthers.tag = tag
        textViewOthers.textColor = UIColor.lightGray
        self.viewShadow.isHidden = false
        UIView.beginAnimations(nil, context: nil)
        UIView.setAnimationDuration(0.3)
        self.viewPopup.transform = CGAffineTransform.identity
        UIView.commitAnimations()
    }

    
//    @IBAction func toolbarDoneButtonAction(sender: AnyObject) {
//        textFieldLastDentalVisit.resignFirstResponder()
//        let dateFormatter = DateFormatter()
//        dateFormatter.dateFormat = "MMMM dd, yyyy"
//        textFieldLastDentalVisit.text = dateFormatter.stringFromDate(datePicker.date).uppercaseString
//    }
//    
//    @IBAction func datePickerDateChanged(sender: AnyObject) {
//        let dateFormatter = DateFormatter()
//        dateFormatter.dateFormat = "MMMM dd, yyyy"
//        textFieldLastDentalVisit.text = dateFormatter.stringFromDate(datePicker.date).uppercaseString
//    }

}


extension DiseaseInfoViewController : UITextViewDelegate {
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        
        if textView == textViewOthers {
            if textView.text == "IF YES TYPE HERE" || textView.text == "IF YES, WHERE" {
                textView.text = ""
                textView.textColor = UIColor.black
            }
        } 
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = textView == textViewOthers ? textViewOthers.tag == 1 ? "IF YES, WHERE" : "IF YES TYPE HERE" : "BRIEFLY EXPLAIN"
            textView.textColor = UIColor.lightGray
        }
    }
    
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if text == "\n" {
            textView.resignFirstResponder()
        }
        return textView.text.count + (text.count - range.length) <= 140
        
    }
}
