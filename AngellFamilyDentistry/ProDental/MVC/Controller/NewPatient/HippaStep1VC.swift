//
//  HippaStep1VC.swift
//   Angell Family Dentistry
//
//  Created by SRS Web Solutions on 05/05/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

class HippaStep1VC: MCViewController {

    @IBOutlet weak var signatureView: SignatureView!
    @IBOutlet weak var labelDate: DateLabel!
        
    override func viewDidLoad() {
        super.viewDidLoad()
        labelDate.todayDate = patient.dateToday
        loadValues()
        // Do any additional setup after loading the view.
    }
    
    func loadValues() {
        //labelDate.setDate = patient.isDate3Tapped
    }
    
    func setValues() {
        patient.isDate3Tapped = labelDate.dateTapped
        patient.signature3 = signatureView.signatureImage()
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func backAction(_ sender: AnyObject) {
        setValues()
        navigationController?.popViewController(animated: true)
    }
    @IBAction func nextAction(_ sender: AnyObject) {
        if !signatureView.isSigned() {
            self.showAlert("PLEASE SIGN THE FORM")
        } else if !labelDate.dateTapped {
            self.showAlert("PLEASE SELECT DATE")
        } else {
            setValues()
            let step2VC = self.storyboard?.instantiateViewController(withIdentifier: "kHippaStep2VC") as! HippaStep2VC
            step2VC.patient = patient
            navigationController?.pushViewController(step2VC, animated: true)
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
