//
//  AddressInfoViewController.swift
//   Angell Family Dentistry
//
//  Created by Leojin Bose on 2/19/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

class AddressInfoViewController: MCViewController {
    
    @IBOutlet weak var textFieldAddressLine: MCTextField!
    
    @IBOutlet weak var textFieldState: MCTextField!
    @IBOutlet weak var textFieldCity: MCTextField!
    @IBOutlet weak var textFieldZipCode: MCTextField!
    @IBOutlet weak var textFieldEmail: MCTextField!
    @IBOutlet weak var textFieldPhone: MCTextField!
    @IBOutlet weak var textFieldSecurityNumber: MCTextField!
    @IBOutlet weak var textfieldCellNumber : MCTextField!
    @IBOutlet weak var textfieldApartmentNumber : MCTextField!
    
    @IBOutlet weak var radioButtonMaritialStatus: RadioButton!
    @IBOutlet weak var radioHearAboutUs: RadioButton!
    @IBOutlet weak var radioTexting : RadioButton!
    
    var arrayStates : [String] = [String]()
    var referenceDetails : String!
    var othersDetails : String!
    
    
    @IBOutlet weak var backButton: MCButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
//        textFieldSecurityNumber.placeholder = is18YearsOld ? "SOCIAL SECURITY NUMBER" : "SOCIAL SECURITY NUMBER"
        
        textFieldState.textFormat = .State
        textFieldZipCode.textFormat = .Zipcode
        textFieldEmail.textFormat = .Email
        textFieldPhone.textFormat = .Phone
        textFieldSecurityNumber.textFormat = .SocialSecurity
        textfieldCellNumber.textFormat = .Phone
        textfieldApartmentNumber.textFormat = .AlphaNumeric
        textFieldZipCode.textFormat = .Zipcode
        // Do any additional setup after loading the view.
        self.loadValues()
        // Do any additional setup after loading the view.
    }
    
    func loadValues() {
        textFieldAddressLine.text = patient.address
        textfieldApartmentNumber.text = patient.aptNumber
        textFieldState.text = patient.state
        
        textFieldZipCode.text = patient.zipCode
        textFieldCity.text = patient.city
        textFieldEmail.text = patient.email
        textFieldPhone.text = patient.homePhone
        textFieldSecurityNumber.text = patient.socialSecurityNumber

        radioButtonMaritialStatus.setSelectedWithTag(patient.maritialStatus!)
        
        radioHearAboutUs.setSelectedWithTag(patient.radioHearAboutUsTag!)
        
        referenceDetails = patient.referredByDetails
        othersDetails = patient.othersDetails
        
        textfieldCellNumber.text = patient.cellPhone
        radioTexting.setSelectedWithTag(patient.textingTag)
    }
    
    
    func setValues() {
        patient.address = textFieldAddressLine.text
        patient.aptNumber = textfieldApartmentNumber.text
        patient.state = textFieldState.text
        
        patient.zipCode = textFieldZipCode.text
        patient.city = textFieldCity.text
        patient.email = textFieldEmail.text?.lowercased()
        patient.homePhone = textFieldPhone.text
        patient.socialSecurityNumber = textFieldSecurityNumber.text

        patient.maritialStatus = radioButtonMaritialStatus.selected == nil ? 0 : radioButtonMaritialStatus.selected.tag
        
        patient.radioHearAboutUsTag = radioHearAboutUs.selected == nil ? 0 : radioHearAboutUs.selected.tag
        
        patient.referredByDetails = referenceDetails
        patient.othersDetails = othersDetails
        
        patient.cellPhone = textfieldCellNumber.isEmpty ? "" : textfieldCellNumber.text!
        patient.textingTag = radioTexting.selected == nil ? 0 : radioTexting.selected.tag
    }

       override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func buttonActionNext(_ sender: AnyObject) {
        self.view.endEditing(true)
        if let _ = findEmptyTextField() {
            self.showAlert("PLEASE ENTER ALL REQUIRED FIELDS")
        } else if !textFieldZipCode.text!.isZipCode {
            self.showAlert("PLEASE ENTER VALID ZIPCODE")
        } else if !textFieldSecurityNumber.isEmpty && !textFieldSecurityNumber.text!.isSocialSecurityNumber {
            self.showAlert("PLEASE ENTER VALID SOCIAL SECURITY NUMBER")
        } else if !textFieldPhone.text!.isPhoneNumber {
            self.showAlert("PLEASE ENTER VALID PHONE NUMBER")
        } else if !textFieldEmail.isEmpty && !textFieldEmail.text!.isValidEmail {
            self.showAlert("PLEASE ENTER VALID EMAIL")
        } else if radioButtonMaritialStatus.selected == nil  {
            self.showAlert("PLEASE SELECT YOUR MARITIAL STATUS")
        }else if radioTexting.selected == nil{
            self.showAlert("PLEASE ENTER ALL THE REQUIRED FIELDS")
        }else if radioTexting.selected.tag == 1 && textfieldCellNumber.isEmpty{
            self.showAlert("PLEASE ENTER THE CELL PHONE NUMBER")
        } else if !textfieldCellNumber.isEmpty && !textfieldCellNumber.text!.isPhoneNumber {
            self.showAlert("PLEASE ENTER A VALID CELL PHONE NUMBER")
        }else {
            self.setValues()
            patient.addressLine = textfieldApartmentNumber.isEmpty ? textFieldAddressLine.text! : "\(textFieldAddressLine.text!), \(textfieldApartmentNumber.text!)"
            let formNames = (patient.selectedForms as NSArray).value(forKey: "formTitle") as! [String]
            if formNames.first == kUpdateHIPAA {
                let step3VC = self.storyboard?.instantiateViewController(withIdentifier: "kUpdateHipaaAppointmentViewController") as! UpdateHipaaAppointmentViewController
                step3VC.patient = patient
                navigationController?.pushViewController(step3VC, animated: true)
            }else{
            let contactInfoVC = self.storyboard?.instantiateViewController(withIdentifier: "kContactInfoVC") as! ContactInfoViewController
            contactInfoVC.patient = patient
            self.navigationController?.pushViewController(contactInfoVC, animated: true)
            }
        }
    }
    
    
    @IBAction func radioHearAboutUsTag(_ sender: AnyObject) {
        if sender.tag == 8 {
            PopupTextField.popUpView().showInViewController(self, WithTitle: "REFERRED BY", placeHolder: "PLEASE SPECIFY", textFormat: TextFormat.Default, completion: { (popUpView, textField) in
                popUpView.close()
                if !textField.isEmpty {
                    self.referenceDetails = textField.text!
                } else {
                    self.referenceDetails = ""
                    self.radioHearAboutUs.deselectAllButtons()
                }
            })
        }else if sender.tag == 9 {
            PopupTextField.popUpView().showInViewController(self, WithTitle: "PLEASE SPECIFY", placeHolder: "", textFormat: TextFormat.Default, completion: { (popUpView, textField) in
                popUpView.close()
                if !textField.isEmpty {
                    self.othersDetails = textField.text!
                } else {
                    self.othersDetails = ""
                    self.radioHearAboutUs.deselectAllButtons()
                }
            })
        }
        
    }
    
    
    @IBAction override func buttonBackAction() {
        self.setValues()
        navigationController?.popViewController(animated: true)
    }
    
    func findEmptyTextField() -> UITextField? {
        let textFields = [textFieldCity,  textFieldPhone, textFieldState, textFieldZipCode, textFieldAddressLine]
        for textField in textFields {
            if textField!.isEmpty {
                return textField
            }
        }
        return nil
    }
    
    var is18YearsOld : Bool {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MMMM dd, yyyy"
        dateFormatter.timeZone = TimeZone.autoupdatingCurrent
        let birthDate = dateFormatter.date(from: patient.dateOfBirth.capitalized)
        if let bdate = birthDate{
            let ageComponents = Calendar.current.dateComponents([.year], from: bdate, to: Date())
            return ageComponents.year! >= 18

        }
        return true
    }
}

extension AddressInfoViewController : UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if textField == textFieldZipCode {
            return textField.formatZipCode(range, string: string)
        }
        
        if textField == textFieldSecurityNumber {
            return textField.formatSocialSecurityNumber(range, string: string)
        }
        if textField == textFieldPhone || textField == textfieldCellNumber {
           
            return textField.formatPhoneNumber(range, string: string)
        }
        return true
    }
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}
