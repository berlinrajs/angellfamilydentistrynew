//
//  AgreementViewController.swift
//   Angell Family Dentistry
//
//  Created by Leojin Bose on 2/19/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

class AgreementViewController: MCViewController {
    
    @IBOutlet weak var labelText1: UILabel!
    @IBOutlet weak var labelText2: UILabel!
    @IBOutlet weak var constraintLabel1Height: NSLayoutConstraint!
    @IBOutlet weak var constraintLabel2Height: NSLayoutConstraint!
   
    @IBOutlet weak var signatureView2: SignatureView!
       @IBOutlet weak var labelDate2: DateLabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        labelText2.setAttributedText()
        
      
        labelDate2.todayDate = patient.dateToday
        loadValues()
        // Do any additional setup after loading the view.
    }
    
    func loadValues() {
        //labelDate2.setDate = patient.isDate2Tapped
    }
    
    func setValues() {
        patient.isDate2Tapped = labelDate2.dateTapped
        patient.signature2 = signatureView2.signatureImage()
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @IBAction override func buttonBackAction() {
        self.setValues()
        super.buttonBackAction()
    }
    
     @IBAction func buttonActionNext(_ sender: AnyObject) {
        if  !signatureView2.isSigned() {
            self.showAlert("PLEASE SIGN THE FORM")
        } else if  !labelDate2.dateTapped {
            self.showAlert("PLEASE SELECT DATE")
        } else {
           
            setValues()
            let hippaStep1 = self.storyboard?.instantiateViewController(withIdentifier: "kHippaStep1VC") as! HippaStep1VC
            hippaStep1.patient = patient
            self.navigationController?.pushViewController(hippaStep1, animated: true)
        }
    }

}
