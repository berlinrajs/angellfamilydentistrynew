//
//  ParentInfoViewController.swift
//   Angell Family Dentistry
//
//  Created by Leojin Bose on 2/19/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

class ParentInfoViewController: MCViewController {
    
    @IBOutlet weak var labelDate: UILabel!
//    @IBOutlet var datePicker: UIDatePicker!
//    @IBOutlet var toolBar: UIToolbar!
    @IBOutlet weak var textFieldFirstName: MCTextField!
    @IBOutlet weak var textFieldLastName: MCTextField!
//    @IBOutlet weak var textFieldDateOfBirth: PDTextField!
    @IBOutlet weak var buttonYes: UIButton!
    @IBOutlet weak var buttonNo: UIButton!
    
    @IBOutlet weak var textfieldMonth : UITextField!
    @IBOutlet weak var textfieldDate : UITextField!
    @IBOutlet weak var textfieldYear : UITextField!
    
    @IBOutlet weak var labelDateText: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        MonthInputView.addMonthPickerForTextField(textfieldMonth)
//        datePicker.maximumDate = Date()
//        textFieldDateOfBirth.inputView = datePicker
//        textFieldDateOfBirth.inputAccessoryView = toolBar
        labelDate.text = patient.dateToday
        self.loadValues()
        
    }
    
    func loadValues() {
        
        if patient.isParent {
            buttonYes.isSelected = true
            buttonNo.isSelected = false
            textFieldFirstName.isEnabled = true
            textFieldLastName.isEnabled = true
            textfieldMonth.isEnabled = true
            textfieldDate.isEnabled = true
            textfieldYear.isEnabled = true
            labelDateText.isEnabled = true
            textFieldFirstName.text = patient.parentFirstName
            textFieldLastName.text = patient.parentLastName
            textfieldMonth.text = patient.parentDateOfBirth?.components(separatedBy: " ").first
            textfieldDate.text = patient.parentDateOfBirth?.components(separatedBy: ", ").first?.components(separatedBy: " ").last
            textfieldYear.text = patient.parentDateOfBirth?.components(separatedBy: ", ").last
        }
    }
    
    func setValues() {
        
        if buttonYes.isSelected {
            patient.parentFirstName = textFieldFirstName.text
            patient.parentLastName = textFieldLastName.text
            patient.parentDateOfBirth = textfieldMonth.text! + " " + textfieldDate.text! + ", " + textfieldYear.text!
            patient.isParent = true
        } else {
            patient.parentFirstName = nil
            patient.parentLastName = nil
            patient.parentDateOfBirth = nil
            patient.isParent = false

        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func buttonActionNext(_ sender: AnyObject) {
        func gotoAddressInfoVC() {
            setValues()
            let addressInfoVC = self.storyboard?.instantiateViewController(withIdentifier: "kAddressInfoVC") as! AddressInfoViewController
            addressInfoVC.patient = patient
            self.navigationController?.pushViewController(addressInfoVC, animated: true)
        }
        
        self.view.endEditing(true)
        if buttonYes.isSelected {
            if textFieldFirstName.isEmpty {
                self.showAlert("PLEASE ENTER FIRST NAME")
            } else if textFieldLastName.isEmpty {
                self.showAlert("PLEASE ENTER LAST NAME")
            } else if invalidDateofBirth {
                self.showAlert("PLEASE ENTER THE VALID DATE OF BIRTH")
            } else {
                gotoAddressInfoVC()
            }
        } else {
            gotoAddressInfoVC()
        }
    }
    var invalidDateofBirth: Bool {
        get {
            if textfieldMonth.isEmpty || textfieldDate.isEmpty || textfieldYear.isEmpty {
                return true
            } else if Int(textfieldDate.text!)! == 0{
                return true
            } else if !textfieldYear.text!.isValidYear {
                return true
            } else {
                
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "dd-MMM-yyyy"
                
                let todayDate = dateFormatter.date(from: dateFormatter.string(from: Date()))
                let currentDate = dateFormatter.date(from: "\(textfieldDate.text!)-\(textfieldMonth.text!)-\(textfieldYear.text!)")
                
                if todayDate == nil || currentDate == nil {
                    return true
                }
                if todayDate!.timeIntervalSince(currentDate!) < 0 {
                    return true
                }
                return false
            }
        }
    }

    @IBAction override func buttonBackAction() {
        setValues()
        super.buttonBackAction()
    }
    
    
    @IBAction func buttonActionYesNo(_ sender: UIButton) {
        sender.isSelected = true
        if sender == buttonYes {
            buttonNo.isSelected = false
            textFieldFirstName.isEnabled = true
            textFieldLastName.isEnabled = true
            textfieldMonth.isEnabled = true
            textfieldDate.isEnabled = true
            textfieldYear.isEnabled = true
            labelDateText.isEnabled = true
        } else {
            buttonYes.isSelected = false
            textFieldFirstName.isEnabled = false
            textFieldLastName.isEnabled = false
            textfieldMonth.isEnabled = false
            textfieldDate.isEnabled = false
            textfieldYear.isEnabled = false
            labelDateText.isEnabled = false
            textFieldLastName.text = ""
            textFieldFirstName.text = ""
            textfieldMonth.text = ""
            textfieldDate.text = ""
            textfieldYear.text = ""
        }
    }
}
extension ParentInfoViewController: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == textfieldDate{
            return textField.formatDate(range, string: string)
        }else if textField == textfieldYear{
            return textField.formatNumbers(range, string: string, count: 4)
        }
        return true
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        return textField.resignFirstResponder()
    }
}
