//
//  DentalCrownViewController.swift
//   Angell Family Dentistry
//
//  Created by Office on 2/23/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

class DentalCrownViewController: MCViewController {

//    var isFromPreviousForm : Bool! = true
    var textRanges : [NSRange]! = [NSRange]()

    
//    @IBOutlet weak var buttonBack: PDButton!
    @IBOutlet weak var signatureView1: SignatureView!
    @IBOutlet weak var signatureView2: SignatureView!
    @IBOutlet weak var labelDate1: DateLabel!
    @IBOutlet weak var labelDate2: DateLabel!
    @IBOutlet weak var labelName: UILabel!
    @IBOutlet weak var labelDetails: UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        labelName.text = patient.fullName

        let form = patient.selectedForms.filter { (formObj) -> Bool in
            return formObj.formTitle == kDentalCrown
        }
        
        let patientName = getText(patient.fullName)
        labelDetails.text = labelDetails.text?.replacingOccurrences(of: "FIRSTNAME LASTNAME", with: patientName).replacingOccurrences(of: "kToothNumber", with:getText(form[0].toothNumbers)).replacingOccurrences(of: "kDoctorName", with: patient.dentistName)
//        self.buttonBack.hidden = isFromPreviousForm
        
        labelDate1.todayDate = patient.dateToday
        labelDate2.todayDate = patient.dateToday
        loadValues()
        // Do any additional setup after loading the view.
    }
    
    func loadValues() {
        //labelDate1.setDate = patient.isDate1Tapped
        //labelDate2.setDate = patient.isDate2Tapped
    }
    
    func setValues() {
        patient.isDate1Tapped = labelDate1.dateTapped
        patient.isDate2Tapped = labelDate2.dateTapped
        patient.signature1 = signatureView1.signatureImage()
        patient.signature2 = signatureView2.signatureImage()
    }
    

    @IBAction override func buttonBackAction() {
        self.setValues()
        super.buttonBackAction()
    }
    
   @IBAction func buttonActionNext(_ sender: AnyObject) {
        if !signatureView1.isSigned() || !signatureView2.isSigned() {
            self.showAlert("PLEASE SIGN THE FORM")
        } else if !labelDate1.dateTapped || !labelDate2.dateTapped {
            self.showAlert("PLEASE SELECT DATE")
        } else {
            setValues()
            let dentalCrownFormVC = self.storyboard?.instantiateViewController(withIdentifier: "kDentureCrownFormVC") as! DentalCrownFormViewController
            dentalCrownFormVC.patient = patient
            self.navigationController?.pushViewController(dentalCrownFormVC, animated: true)
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
