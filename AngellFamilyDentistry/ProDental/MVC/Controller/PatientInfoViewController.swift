//
//  PatientInfoViewController.swift
//  Always Great Smiles
//
//  Created by Leojin Bose on 2/19/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

class PatientInfoViewController: MCViewController {
    
    @IBOutlet weak var labelDate: UILabel!
    @IBOutlet weak var textFieldFirstName: MCTextField!
    @IBOutlet weak var textFieldLastName: MCTextField!
    
    @IBOutlet weak var textFieldMInitial: MCTextField!
    @IBOutlet weak var textFieldPreferedName: MCTextField!
    
    @IBOutlet weak var textFieldDate: MCTextField!
    @IBOutlet weak var textFieldMonth: MCTextField!
    @IBOutlet weak var textFieldYear: MCTextField!
    @IBOutlet weak var labelDentist: UILabel!
    @IBOutlet weak var labelPlace: UILabel!
    @IBOutlet weak var dropDownDentistName: BRDropDown!
//    @IBOutlet weak var textFieldPatientID: MCTextField!
//    @IBOutlet weak var constraintPatientIDHeight: NSLayoutConstraint!
    
    @IBOutlet weak var radioNewPatient: RadioButton!
    
    var dentistNames = [String: String]()
    
    var isNewPatient: Bool {
        get {
            return radioNewPatient.isSelected
        } set {
            radioNewPatient.isSelected = newValue
//            self.constraintPatientIDHeight.constant = newValue ? 0.0 : 150.0
//            UIView.animate(withDuration: 0.3, animations: {
//                self.view.layoutIfNeede                                                                                                                                                                                                                                                          d()
//            }, completion: { (finished) in
//                self.textFieldPatientID.isEnabled = !newValue
//                self.textFieldPatientID.text = ""
//            })
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        textFieldDate.textFormat = .Date
        textFieldMonth.textFormat = .Month
        textFieldYear.textFormat = .Year
        textFieldMInitial.textFormat = .MiddleInitial
        textFieldPreferedName.textFormat = .Default
//        textFieldPatientID.textFormat = .AlphaNumeric
        
//        self.isNewPatient = !SystemConfig.isConfigured
        
        self.dropDownDentistName.isHidden = true
        self.labelDentist.isHidden = true
        
////        if SystemConfig.isConfigured {
//            BRProgressHUD.show()
//            DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.3) {
//                self.loadDentistNames()
//            }
////        } else {
        if MCAppDetails.sharedAppDetails!.dentistNames.count == 0 {
            dropDownDentistName.isHidden = true
            labelDentist.isHidden = true
        } else if MCAppDetails.sharedAppDetails!.dentistNames.count > 1 {
            dropDownDentistName.items = MCAppDetails.sharedAppDetails!.dentistNames as [String]
            dropDownDentistName.placeholder = isDentistNameNeeded ? "-- DENTIST NAME * --" : "-- DENTIST NAME --"
            dropDownDentistName.isHidden = false
            labelDentist.isHidden = true
        } else {
            labelDentist.text = "DENTIST: " + MCAppDetails.sharedAppDetails!.dentistNames[0].uppercased()
            dropDownDentistName.isHidden = true
            labelDentist.isHidden = false
        }
     //   }
    
        labelPlace.text = MCAppDetails.sharedAppDetails!.cityState.uppercased()
        labelDate.text = patient.dateToday
        
        radioNewPatient.isSelected = false
        
        let formNames = (patient.selectedForms as NSArray).value(forKey: "formTitle") as! [String]
        
        if formNames.contains(kToothExtraction) {
            loadToothExractionQuestions()
        }
        if formNames.contains(kMedicalHistoryForm) {
            loadMedicalQuestions()
        }
        //        textFieldFirstName.text = patient.visitorFirstName == nil ? "" : patient.visitorFirstName
        //        textFieldLastName.text = patient.visitorLastName == nil ? "" : patient.visitorLastName
        //        radioNewPatient.isSelected = true
        // Do any additional setup after loading the view.
    }
    
    func loadToothExractionQuestions() {
        PDOption.fetchQuestionsToothExtractionForm1 { (result, success) -> Void in
            if success {
                self.patient.toothExtractionQuestions1 = result!
            }
        }
        
        PDOption.fetchQuestionsToothExtractionForm2 { (result, success) -> Void in
            if success {
                self.patient.toothExtractionQuestions2 = result!
            }
        }
    }
    
    
    func loadMedicalQuestions() {
        PDQuestion.fetchQuestionsForm1 { (result, success) -> Void in
            if success {
                self.patient.medicalHistoryQuestions1 = result!
            }
        }
        
        PDOption.fetchQuestionsForm2 { (result, success) -> Void in
            if success {
                self.patient.medicalHistoryQuestions2 = result!
            }
        }
        PDOption.fetchQuestionsForm3 { (result, success) -> Void in
            if success {
                self.patient.medicalHistoryQuestions3 = result!
            }
        }
        PDOption.fetchQuestionsForm4 { (result, success) -> Void in
            if success {
                self.patient.medicalHistoryQuestions4 = result!
            }
        }
    }
    
    func loadDentistNames () {
        ServiceManager.getDataFromServer(baseUrlString: hostUrl, serviceName: "api/providers", parameters: nil, success: { (result) in
            BRProgressHUD.hide()
            self.dentistNames.removeAll()
            for dict in result as! [[String: String]] {
                let name = dict["FirstName"]!.trimSpaceAndLine + " " + dict["LastName"]!.trimSpaceAndLine
                let providerID = dict["ProviderId"]!.trimSpaceAndLine
                self.dentistNames[name] = providerID
            }
            self.loadDentistDropDown()
        }) { (error) in
            SystemConfig.sharedConfig?.checkConnectivity(completion: { (success, errorMessage) in
                BRProgressHUD.hide()
                self.dropDownDentistName.isHidden = true
                self.labelDentist.isHidden = true
                if success {
                    self.showAlert("SOMETHING WENT WRONG, PLEASE CONTACT SUPPORT")
                } else {
                    self.showAlert(errorMessage!)
                }
            })
        }
    }
    
    func loadDentistDropDown () {
        if self.dentistNames.count == 0 {
            self.dropDownDentistName.isHidden = true
            self.labelDentist.isHidden = true
        } else if self.dentistNames.count > 1 {
            self.dropDownDentistName.items = self.dentistNames.map({ (key, value) -> String in
                return key
            })
            self.dropDownDentistName.placeholder = self.isDentistNameNeeded ? "-- DENTIST NAME * --" : "-- DENTIST NAME --"
            self.dropDownDentistName.isHidden = false
            self.labelDentist.isHidden = true
        } else {
            self.labelDentist.text = "DENTIST: " + self.dentistNames.map({ (key, value) -> String in
                return key
            })[0].uppercased()
            self.dropDownDentistName.isHidden = true
            self.labelDentist.isHidden = false
        }
    }
    
    @IBAction func buttonExistingPatientAction () {
        //self.loadDentistDropDown()
    }
//        if !isNewPatient {
//            CustomAlert.firstTimeAlert().showWithTitle("Connection with Dentrix is required to retrieve the current patient information. Please contact support team for assistance.", buttonTitles: ["Request Support", "Cancel"], completion: { (buttonIndex) in
//                if buttonIndex == 1 {
//                    self.sendCallRequest()
//                }
//                self.isNewPatient = true
//            })
//        } else {
//            self.constraintPatientIDHeight.constant = isNewPatient ? 0.0 : 150.0
//            UIView.animate(withDuration: 0.3, animations: {
//                self.view.layoutIfNeeded()
//            }, completion: { (finished) in
//                self.textFieldPatientID.isEnabled = !self.isNewPatient
//                self.textFieldPatientID.text = ""
//                //                self.pdfView?.setContentOffset(CGPoint(x: 0, y: self.isNewPatient ? 0.0 : 150.0), animated: true)
//            })
//            self.loadDentistDropDown()
//        }
//    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func buttonActionNext(_ sender : AnyObject) {
        dropDownDentistName.selected = false
        self.view.endEditing(true)
        if self.dentistNames.count > 1 && isDentistNameNeeded && dropDownDentistName.selectedIndex == 0 {
            self.showAlert("PLEASE SELECT THE DENTIST NAME")
        } else if textFieldFirstName.isEmpty {
            self.showAlert("PLEASE ENTER CHART ID OR PATIENT DETAILS")
        } else if textFieldLastName.isEmpty {
            self.showAlert("PLEASE ENTER CHART ID OR PATIENT DETAILS")
        } else if invalidDateofBirth {
            self.showAlert("PLEASE ENTER THE VALID DATE OF BIRTH")
        } else if radioNewPatient.selected == nil {
            self.showAlert("PLEASE SELECT ALL THE REQUIRED FIELDS")
        } else {
//            if textFieldPatientID.isEmpty {
//                patient.patientChartId = nil
                patient.firstName = textFieldFirstName.text!
                patient.lastName = textFieldLastName.text!
                patient.dateOfBirth = getDateOfBirth()
                patient.initial = textFieldMInitial.text!
                patient.preferredName = textFieldPreferedName.text!
//            } else {
//                patient.patientChartId = textFieldPatientID.text!
//                patient.firstName = nil
//                patient.lastName = nil
//                patient.dateOfBirth = nil
//            }
          
            patient.dentistName = dropDownDentistName.selectedIndex == 0 ? "" : dropDownDentistName.selectedOption
  
            
            
//            if isNewPatient {
//                self.patient.patientDetails = nil
//                patient.dentistName = isDentistNameNeeded ? (self.dentistNames.count > 1 ? dropDownDentistName.selectedOption! : (self.dentistNames.count > 0 ? self.dentistNames.map({ (key, value) -> String in
//                    return key
//                })[0] : "")) : ""
//                patient.providerID = self.dentistNames.count > 1 ? self.dentistNames[dropDownDentistName.selectedOption!]! : (self.dentistNames.count > 0 ? self.dentistNames.map({ (key, value) -> String in
//                    return value
//                })[0] : "")
//            }
            
            func showMoreThanOneUserAlert()  {
                self.showAlert("More than one user found. Please handover the device to front desk to enter your patient id", completion: {
                    let verificationVC = mainStoryBoard.instantiateViewController(withIdentifier: "kVerificationViewController") as! VerificationViewController
                    verificationVC.patient = self.patient
                    verificationVC.completionOfVerification = {(object) in
                        self.navigationController?.dismiss(animated: true, completion: {
                            let response = object as! [String : AnyObject]
                            print(response)
                            reloadPatientValues()
                            let patientDetails = response["patientData"] as! [String: AnyObject]
                            self.patient.patientDetails = PatientDetails(details: patientDetails)
                            
                            self.gotoNextForm()
                        })
                    }
                    self.navigationController?.present(verificationVC, animated: true, completion: nil)
                })
            }
            
            func APICall() {
                BRProgressHUD.show()
                
                let serviceName = "api/patients/"
                let parameters = ["firstName": self.textFieldFirstName.text!, "lastName": self.textFieldLastName.text!, "dateOfBirth": DateFormatter(dateFormat: "yyyy-MM-dd").string(from: DateFormatter(dateFormat: "MMM dd, yyyy").date(from: patient.dateOfBirth)!)] as [String : AnyObject]
                
                ServiceManager.getDataFromServer(baseUrlString: hostUrl!, serviceName: serviceName, parameters: parameters, success: { (result) in
                    BRProgressHUD.hide()
                    
                    guard let patientDetails = (result as? [[String : AnyObject]])?.first else {
                        self.showAlert("PLEASE CHECK THE DETAILS YOU HAVE ENTERED")
                        return
                    }
                    
                    if (result as! [[String : AnyObject]]).count > 1 {
                        showMoreThanOneUserAlert()
//                            var patientArray = [MCPerson]()
//                            for dict in (result as! [[String : AnyObject]]) {
//                                patientArray.append(MCPerson(details: dict))
//                            }
//                            PatientListViewController.showPatientListViewController(list: patientArray, fromVC: self, withNewPatient: false, completion: { (selectedPerson) in
//                                if selectedPerson != nil {
//                                    self.patient.patientChartId = selectedPerson!.patientChartId
//                                    APICall()
//                                }
//                            })
                        return
                    }
                    
                    self.patient.patientDetails = PatientDetails(details: patientDetails)
                    self.patient.patientDetails?.dateOfBirth = self.patient.dateOfBirth
                    self.patient.dentistName = self.dentistNames.count > 0 ? (self.dentistNames.filter({ (key, value) -> Bool in
                        return value == self.patient.providerID
                        })[0]).key : ""
                    
                    self.gotoNextForm()
                }) { (error) in
                    if error.localizedDescription.contains("401") {
                        let params = ["userName": "srssoftwares", "password": "srssoftwares", "grant_type": "password"] as [String : AnyObject]
                        ServiceManager.postDataToServer(baseUrlString: hostUrl, serviceName: "token", parameters: params, success: { (result) in
                            if let accessToken = (result as AnyObject)["access_token"] as? String {
                                UserDefaults.standard.set("Bearer \(accessToken)", forKey: kAccessToken)
                                UserDefaults.standard.synchronize()
                            }
                            APICall()
                        }) { (error) in
                            BRProgressHUD.hide()
                            self.showAlert(error.localizedDescription)
                        }
                    } else {
                        SystemConfig.sharedConfig?.checkConnectivity(completion: { (success, errorMessage) in
                            BRProgressHUD.hide()
                            self.patient.patientDetails = nil
                            if success {
                                self.showAlert("Something went wrong, Please contact our support team")
                            } else {
                                self.showAlert(errorMessage!)
                            }
                        })
                    }
                }
            }
            
            if self.isNewPatient == true {
                if self.isNewPatient == true && !(self.patient.selectedForms.map({ (form) -> String in
                    return form.formTitle
                }).contains(kNewPatientSignInForm)) {
                    if let patientSignInForm = MCAppDetails.sharedAppDetails!.formList[0].subForms.filter({ (form) -> Bool in
                        return (form.formTitle ?? "") == kNewPatientSignInForm
                    }).first {
                        self.patient.selectedForms.insert(patientSignInForm, at: 0)
                    }
                }
                
                gotoNextForm()
            } else {
                APICall()
            }
            
            func reloadPatientValues() {
                let tempPatient = MCPatient(forms: self.patient.selectedForms)
                tempPatient.dateToday = self.patient.dateToday
                tempPatient.firstName = patient.firstName
                tempPatient.lastName = patient.lastName
                tempPatient.dateOfBirth = patient.dateOfBirth
                tempPatient.dentistName = patient.dentistName
                tempPatient.signRefused = patient.signRefused
                tempPatient.signRefusalOther = patient.signRefusalOther
                tempPatient.signRefusalTag = patient.signRefusalTag
                self.patient = tempPatient
            }
        }
    }
    
    var isDentistNameNeeded: Bool {
        get {
            if isNewPatient {
                return true
            }
            return false
        }
    }
    
    func getDateOfBirth() -> String {
        let dateFormatter = DateFormatter(dateFormat: "MMM dd, yyyy")
        let dob = dateFormatter.date(from: textFieldMonth.text! + " " + textFieldDate.text! + ", " + textFieldYear.text!)!
        
        let dateFormatter1 = DateFormatter(dateFormat: kCommonDateFormat)
        return dateFormatter1.string(from: dob).uppercased()
    }
    
    var invalidDateofBirth: Bool {
        get {
            if textFieldMonth.isEmpty || textFieldDate.isEmpty || textFieldYear.isEmpty {
                return true
            } else if Int(textFieldDate.text!)! == 0 {
                return true
            } else if !textFieldYear.text!.isValidYear {
                return true
            } else if Int(textFieldYear.text!)! < 1900 {
                return true
            } else {
                let dateFormatter = DateFormatter(dateFormat: "dd-MMM-yyyy")
                
                let todayDate = dateFormatter.date(from: dateFormatter.string(from: Date()))
                let currentDate = dateFormatter.date(from: "\(textFieldDate.text!)-\(textFieldMonth.text!)-\(textFieldYear.text!)")
                
                if todayDate == nil || currentDate == nil {
                    return true
                }
                if todayDate!.timeIntervalSince(currentDate!) < 0 {
                    return true
                }
                return false
            }
        }
    }
}
