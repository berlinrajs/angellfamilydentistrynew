//
//  LoginViewController.swift
//  OptimaDentistry
//
//  Created by SRS Web Solutions on 12/07/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

class LoginViewController: MCViewController {

    @IBOutlet weak var textFieldUserName: MCTextField!
    @IBOutlet weak var textFieldPassword: MCTextField!
    @IBOutlet weak var labelVersion: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        textFieldUserName.textFormat = .Email
        textFieldPassword.textFormat = .SecureText
        
        labelVersion.text = Bundle.main.object(forInfoDictionaryKey: kCFBundleVersionKey as String) as? String
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func buttonSubmitAction() {
        if textFieldUserName.isEmpty || !textFieldUserName.text!.isValidEmail {
            self.showAlert("PLEASE ENTER THE VALID EMAIL")
        } else if textFieldPassword.isEmpty {
            self.showAlert("PLEASE ENTER THE PASSWORD")
        } else {
            self.submitAction()
        }
    }
    
    func submitAction() {
        self.view.endEditing(true)
        if Reachability.isConnectedToNetwork() {
            BRProgressHUD.showWithTitle("Logging In...")
            ServiceManager.loginWithUsername(textFieldUserName.text!, password: textFieldPassword.text!) { (success, result, error) -> (Void) in
                if success {
                    if let errorMessage = MCAppDetails.errorFoundInDict(details: result!) {
                        BRProgressHUD.hide()
                        self.showAlert(errorMessage)
                        return
                    }
                    MCAppDetails.sharedAppDetails = MCAppDetails.init(WithClinicDetails: result!)
                    MCAppDetails.loggedIn = true
                    UserDefaults.standard.setValue(self.textFieldUserName.text!, forKey: kAppLoginUsernameKey)
                    UserDefaults.standard.setValue(self.textFieldPassword.text!, forKey: kAppLoginPasswordKey)
                    UserDefaults.standard.synchronize()
                    (UIApplication.shared.delegate as! AppDelegate).checkAutologin()
                } else {
                    BRProgressHUD.hide()
                    if error == nil {
                        self.showAlert("PLEASE CHECK YOUR INTERNET CONNECTION AND TRY AGAIN")
                    } else {
                        self.showAlert(error!.localizedDescription.uppercased())
                    }
                }
            }
        } else {
            self.showAlert("Your device is not connected to internet. Please go to settings to connect.", buttonTitles: ["Settings", "Cancel"], completion: { (buttonIndex) in
                if buttonIndex == 0 {
                    let settingsUrl = URL(string: UIApplicationOpenSettingsURLString)
                    if let url = settingsUrl {
                        UIApplication.shared.openURL(url)
                    }
                }
            })
        }
    }
}
extension LoginViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == textFieldUserName {
            textFieldPassword.becomeFirstResponder()
        } else {
            textField.resignFirstResponder()
            self.submitAction()
        }
        return true
    }
}
