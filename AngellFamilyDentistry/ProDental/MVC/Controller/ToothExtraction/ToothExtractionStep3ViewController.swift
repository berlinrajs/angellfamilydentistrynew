//
//  ToothExtractionStep3ViewController.swift
//   Angell Family Dentistry
//
//  Created by Office on 2/24/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

class ToothExtractionStep3ViewController: MCViewController {

    @IBOutlet weak var signatureView1: SignatureView!
    @IBOutlet weak var signatureView2: SignatureView!
    @IBOutlet weak var signatureView3: SignatureView!

    @IBOutlet weak var labelDate1: DateLabel!
    @IBOutlet weak var labelName: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        labelName.text = patient.fullName
        
        labelDate1.todayDate = patient.dateToday
        loadValues()
        // Do any additional setup after loading the view.
    }
    
    func loadValues() {
        //labelDate1.setDate = patient.isDate1Tapped
    }
    
    func setValues() {
        patient.isDate1Tapped = labelDate1.dateTapped
        patient.signature1 = signatureView1.signatureImage()
        patient.signature2 = signatureView2.signatureImage()
        patient.signature3 = signatureView3.signatureImage()
    }
    
    @IBAction override func buttonBackAction() {
        self.setValues()
        super.buttonBackAction()
    }
    
    @IBAction func buttonActionNext(_ sender: AnyObject) {
        if !signatureView1.isSigned() || !signatureView2.isSigned() || !signatureView3.isSigned() {
            self.showAlert("PLEASE SIGN THE FORM")
        } else if !labelDate1.dateTapped {
            self.showAlert("PLEASE SELECT DATE")
        } else {
            setValues()
            let toothExtractionFormVC = self.storyboard?.instantiateViewController(withIdentifier: "kToothExtractionFormVC") as! ToothExtractionFormViewController
            toothExtractionFormVC.patient = patient
            self.navigationController?.pushViewController(toothExtractionFormVC, animated: true)
        }
    }


    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
