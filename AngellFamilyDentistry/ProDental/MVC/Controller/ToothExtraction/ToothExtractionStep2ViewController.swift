//
//  ToothExtractionStep2ViewController.swift
//   Angell Family Dentistry
//
//  Created by Office on 2/24/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

class ToothExtractionStep2ViewController: MCViewController {

//    var patient : PDPatient!
//    var toothExtractionStep2 : [PDOption]! = [PDOption]()

    
    @IBOutlet weak var textField1: MCTextField!
    @IBOutlet weak var textFieldOthers: MCTextField!
    @IBOutlet weak var collectionViewoptions: UICollectionView!

    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.loadValues()
//        self.fetchData()
        // Do any additional setup after loading the view.
    }
    
    
//    func fetchData() {
//        PDOption.fetchQuestionsToothExtractionForm2 { (result, success) -> Void in
//            if success {
//                self.toothExtractionStep2.appendContentsOf(result!)
//                self.collectionViewoptions.reloadData()
//            }
//        }
//    }
    
    func loadValues() {
        textFieldOthers.text = patient.othersText2
        textField1.text = patient.prognosisProcedure

    }
    
    func setValues() {
        if !textFieldOthers.isEmpty {
            patient.othersText2 = textFieldOthers.text
        } else {
            patient.othersText2 = nil
        }
        if !textField1.isEmpty {
            patient.prognosisProcedure = textField1.text
        } else {
            patient.prognosisProcedure = nil
        }
    }
    
    @IBAction override func buttonBackAction() {
        self.setValues()
        super.buttonBackAction()
    }
    
    
    @IBAction func buttonActionNext(_ sender: AnyObject) {
        self.view.endEditing(true)
        let selectedOptions = self.patient.toothExtractionQuestions2.filter { (option) -> Bool in
            return option.isSelected != nil && option.isSelected == true
        }
        
        if selectedOptions.count == 0 && !textFieldOthers.isEnabled {
            self.showAlert("PLEASE SELECT ANY OPTION")
        } else if textFieldOthers.isEnabled && textFieldOthers.isEmpty {
            self.showAlert("PLEASE ENTER OTHER REASON")
        } else {
            self.setValues()
            
//            let step2Text : String! = ((selectedOptions as NSArray).valueForKey("question") as! [String]).joinWithSeparator(", ")
//            patient.toothExtractionQuestions2 = step2Text.isEmpty ? nil : step2Text
            let toothExtractionStep3VC = self.storyboard?.instantiateViewController(withIdentifier: "kToothExtractionStep3VC") as! ToothExtractionStep3ViewController
            toothExtractionStep3VC.patient = patient
            self.navigationController?.pushViewController(toothExtractionStep3VC, animated: true)
        }
    }
    
    @IBAction func buttonActionOthers(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        textFieldOthers.isEnabled = sender.isSelected
        if sender.isSelected == false {
            textFieldOthers.text = ""
        }

    }


    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}


extension ToothExtractionStep2ViewController : UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let obj = self.patient.toothExtractionQuestions2[indexPath.item]
        if let selected = obj.isSelected {
            obj.isSelected = !selected
        } else {
            obj.isSelected = true
        }
        collectionView.reloadData()
    }
}

extension ToothExtractionStep2ViewController : UICollectionViewDataSource {
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.patient.toothExtractionQuestions2.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cellMedicalHistory", for: indexPath) as! MedicalHistoryCollectionViewCell
        let obj = self.patient.toothExtractionQuestions2[indexPath.item]
        cell.configureCellOption(obj)
        return cell
    }
}

extension ToothExtractionStep2ViewController : UITextFieldDelegate {
    
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        let maxLength = 25
        let currentString: NSString = textField.text! as NSString
        let newString: NSString =
            currentString.replacingCharacters(in: range, with: string) as NSString
        return newString.length <= maxLength
    }
}

