//
//  ToothExtractionFormViewController.swift
//   Angell Family Dentistry
//
//  Created by Office on 2/24/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

class ToothExtractionFormViewController: MCViewController {

    var textRanges : [NSRange]! = [NSRange]()
    
    @IBOutlet weak var labelDate: UILabel!
    @IBOutlet weak var imageViewSignature1: UIImageView!
    @IBOutlet weak var imageViewSignature2: UIImageView!
    @IBOutlet weak var imageViewSignature3: UIImageView!

//    @IBOutlet weak var buttonSubmit: PDButton!
//    @IBOutlet weak var buttonBack: PDButton!
    @IBOutlet weak var labelDetails: UILabel!
    
    @IBOutlet var arrayOptionButtons1: [UIButton]!
    @IBOutlet var arrayOptionButtons2: [UIButton]!
    
    @IBOutlet weak var labelOther1: FormLabel!
    @IBOutlet weak var labelOther2: FormLabel!
    
    @IBOutlet weak var prognosisProcedure: FormLabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        imageViewSignature1.image = patient.signature1
        imageViewSignature2.image = patient.signature2
        imageViewSignature3.image = patient.signature3
        
        labelDate.text = patient.dateToday
        
        
        var patientInfo = "Patient Name:"

        let patientName = getText(patient.fullName)
        patientInfo = patientInfo + " \(patientName)"
        textRanges.append(patientInfo.rangeOfText(patientName))
        
        patientInfo = patientInfo + " DOB: \(getText(patient.dateOfBirth))"
        textRanges.append(patientInfo.rangeOfText(getText(patient.dateOfBirth)))
        
        let form = patient.selectedForms.filter { (formObj) -> Bool in
            return formObj.formTitle == kToothExtraction
        }
        
        patientInfo = patientInfo + "\nI request to extract tooth/teeth# \(getText(form[0].toothNumbers))"
        textRanges.append(patientInfo.rangeOfText(getText(form[0].toothNumbers)))

        
        patientInfo = patientInfo + "\nThe doctor has recommended this treatment because of"
        
        let selectedOptions1 = self.patient.toothExtractionQuestions1.filter { (option) -> Bool in
            return option.isSelected != nil && option.isSelected == true
        }
        
        if selectedOptions1.count > 0 {
            let step1Text : String! = ((selectedOptions1 as NSArray).value(forKey: "question") as! [String]).joined(separator: ", ")
            let optionArray = step1Text.capitalized.components(separatedBy: ", ")
            for button in arrayOptionButtons1 {
                button.isSelected = optionArray.contains(button.title(for: UIControlState())!.capitalized)
            }
        }
        
        if let otherText1 = patient.othersText1 {
            labelOther1.text = otherText1
            arrayOptionButtons1.last?.isSelected = true
        }
        
        if let prognosis = patient.prognosisProcedure {
            prognosisProcedure.text = prognosis
        } else {
            prognosisProcedure.text = getText("N/A")
        }
        
        let attributedString = NSMutableAttributedString(string: patientInfo)
        for range in textRanges {
            attributedString.addAttribute(NSUnderlineStyleAttributeName, value: NSUnderlineStyle.styleSingle.rawValue, range: range)
        }
        
        attributedString.addAttributes([NSBaselineOffsetAttributeName: NSNumber(value: 0 as Float)], range: NSMakeRange(0, attributedString.length))
        labelDetails.attributedText = attributedString
        

        let selectedOptions2 = self.patient.toothExtractionQuestions2.filter { (option) -> Bool in
            return option.isSelected != nil && option.isSelected == true
        }
        if selectedOptions2.count > 0 {
            let step2Text : String! = ((selectedOptions2 as NSArray).value(forKey: "question") as! [String]).joined(separator: ", ")
            let optionArray = step2Text.capitalized.components(separatedBy: ", ")
            for button in arrayOptionButtons2 {
                button.isSelected = optionArray.contains(button.title(for: UIControlState())!.capitalized)
            }
        }
        if let otherText2 = patient.othersText2 {
            labelOther2.text = otherText2
            arrayOptionButtons2.last?.isSelected = true
        }
    }

//    @IBAction func buttonActionBack(sender: AnyObject) {
//        self.navigationController?.popViewControllerAnimated(true)
//    }
    
    
    
//    @IBAction func buttonActionSubmit(sender: AnyObject) {
//        
//        if !Reachability.isConnectedToNetwork() {
//            let alertController = UIAlertController(title: "Angell Family Dentistry", message: "Your device is not connected to internet. Please go to settings to connect.", preferredStyle: UIAlertControllerStyle.Alert)
//            let alertOkAction = UIAlertAction(title: "Settings", style: UIAlertActionStyle.Destructive) { (action) -> Void in
//                let settingsUrl = NSURL(string: UIApplicationOpenSettingsURLString)
//                if let url = settingsUrl {
//                    UIApplication.sharedApplication().openURL(url)
//                }
//            }
//            let alertCancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.Destructive) { (action) -> Void in
//                
//            }
//            alertController.addAction(alertOkAction)
//            alertController.addAction(alertCancelAction)
//            self.presentViewController(alertController, animated: true, completion: nil)
//            return
//        }
//        let pdfManager = PDFManager()
//        pdfManager.authorizeDrive(self.view) { (success) -> Void in
//            if success {
//                self.buttonSubmit.hidden = true
//                self.buttonBack.hidden = true
//                pdfManager.createPDFForView(self.view, fileName: "TOOTH_EXTRACTION", patient: self.patient, completionBlock: { (finished) -> Void in
//                    if finished {
//                        self.patient = self.patient.patient()
//                        let formNames = (self.patient.selectedForms as NSArray).valueForKey("formTitle") as! [String]
//                        if formNames.contains(kPartialDenture) {
//                            let dentureAdjustmentVC = self.storyboard?.instantiateViewControllerWithIdentifier("kDentureAdjustmentVC") as! DentureAdjustmentViewController
//                            dentureAdjustmentVC.patient = self.patient
//                            self.navigationController?.pushViewController(dentureAdjustmentVC, animated: true)
//                        } else if formNames.contains(kInofficeWhitening) {
//                            let inOfficeWhiteningVC = self.storyboard?.instantiateViewControllerWithIdentifier("kInOfficeWhiteningVC") as! InOfficeWhiteningViewController
//                            inOfficeWhiteningVC.patient = self.patient
//                            self.navigationController?.pushViewController(inOfficeWhiteningVC, animated: true)
//                        } else if formNames.contains(kTreatmentOfChild) {
//                            let childTreatmentVC = self.storyboard?.instantiateViewControllerWithIdentifier("kChildTreatmentVC") as! ChildTreatmentViewController
//                            childTreatmentVC.patient = self.patient
//                            self.navigationController?.pushViewController(childTreatmentVC, animated: true)
//                        } else if formNames.contains(kEndodonticTreatment) {
//                            let endodonticTreatmentVC = self.storyboard?.instantiateViewControllerWithIdentifier("kEndodonticTreatmentVC") as! EndodonticTreatmentViewController
//                            endodonticTreatmentVC.patient = self.patient
//                            self.navigationController?.pushViewController(endodonticTreatmentVC, animated: true)
//                        } else if formNames.contains(kDentalCrown) {
//                            let dentalCrownVC = self.storyboard?.instantiateViewControllerWithIdentifier("kDentureCrownVC") as! DentalCrownViewController
//                            dentalCrownVC.patient = self.patient
//                            self.navigationController?.pushViewController(dentalCrownVC, animated: true)
//                        } else if formNames.contains(kOpiodForm) {
//                            let opiodVC = self.storyboard?.instantiateViewControllerWithIdentifier("kOpiodViewController") as! OpiodViewController
//                            opiodVC.patient = self.patient
//                            self.navigationController?.pushViewController(opiodVC, animated: true)
//                        }else if formNames.contains(kGeneralConsent) {
//                            let generalVC = self.storyboard?.instantiateViewControllerWithIdentifier("kGeneralConsentVC") as! GeneralConsentViewController
//                            generalVC.patient = self.patient
//                            self.navigationController?.pushViewController(generalVC, animated: true)
//                        }else if formNames.contains(kFeedbackForm) {
//                            let feedbackFormVC = self.storyboard?.instantiateViewControllerWithIdentifier("kFeedbackFormVC") as! FeedbackFormViewController
//                            self.navigationController?.pushViewController(feedbackFormVC, animated: true)
//                        } else {
//                            NSNotificationCenter.defaultCenter().postNotificationName(kFormsCompletedNotification, object: nil)
//                            self.navigationController?.popToRootViewControllerAnimated(true)
//                        }
//                    } else {
//                        self.buttonSubmit.hidden = false
//                        self.buttonBack.hidden = false
//                    }
//                })
//            }
//        }
//    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
