//
//  UpdateHippaVC.swift
//  AngellFamilyDentistry
//
//  Created by Leojin Bose on 7/4/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

class UpdateHippaVC: MCViewController {
    
//    @IBOutlet weak var buttonBack: PDButton!
    @IBOutlet weak var labelName: UILabel!
    @IBOutlet weak var labelDate: DateLabel!
    @IBOutlet weak var signatureView: SignatureView!
//    var isFromPreviousForm : Bool! = true
    @IBOutlet weak var textfieldRepresentativeName : UITextField!
    @IBOutlet weak var textfieldRepresentativeRelationship : UITextField!
    @IBOutlet weak var viewRepresentative : UIView!
    @IBOutlet weak var radioRepresentative : RadioButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        labelDate.todayDate = patient.dateToday
        self.loadValues()
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func onRadioRepresentativePressed (_ sender : RadioButton){
        if sender.tag == 1{
            viewRepresentative.isUserInteractionEnabled = false
            viewRepresentative.alpha = 0.5
            textfieldRepresentativeName.text = ""
            textfieldRepresentativeRelationship.text = ""
        }else{
            viewRepresentative.isUserInteractionEnabled = true
            viewRepresentative.alpha = 1.0
        }
    }
    
    func loadValues() {
        viewRepresentative.isUserInteractionEnabled = patient.representativeTag != 1
        viewRepresentative.alpha = patient.representativeTag != 1 ? 1.0 : 0.5

        textfieldRepresentativeName.text = patient.representativeName
        textfieldRepresentativeRelationship.text = patient.representativeRelationship
        radioRepresentative.setSelectedWithTag(patient.representativeTag)
        //labelDate.setDate = patient.isDate1Tapped

    }
    
    func setValues() {
        patient.UpdateHippasignature1 = signatureView.signatureImage()
        patient.representativeName = textfieldRepresentativeName.text
        patient.representativeRelationship = textfieldRepresentativeRelationship.text
        patient.representativeTag = radioRepresentative.selected!.tag
        patient.isDate1Tapped = labelDate.dateTapped
    }
    
    
    @IBAction func actionBack(_ sender: AnyObject) {
        self.setValues()
        navigationController?.popViewController(animated: true)
    }
    @IBAction func actionNext(_ sender: AnyObject) {
        if !signatureView.isSigned() {
            self.showAlert("PLEASE SIGN THE FORM")
        } else if !labelDate.dateTapped {
            self.showAlert("PLEASE SELECT DATE")
        }else if radioRepresentative.selected.tag == 2 && (textfieldRepresentativeName.isEmpty || textfieldRepresentativeRelationship.isEmpty){
            self.showAlert("PLEASE ENTER THE PERSONAL REPRESENTATIVE DETAILS")
        }
        else {
            self.setValues()
            let formNames = (patient.selectedForms as NSArray).value(forKey: "formTitle") as! [String]
            if formNames.first == kUpdatePatientInformation {
                let HippanewPatientVC = self.storyboard?.instantiateViewController(withIdentifier: "kUpdateNewPatientVC") as! UpdateNewPatientViewController
                HippanewPatientVC.patient = patient
                self.navigationController?.pushViewController(HippanewPatientVC, animated: true)
                
            }
            else
            {
                let HippanewPatientVC = self.storyboard?.instantiateViewController(withIdentifier: "kUpdateHippaNewPatientVC") as! UpdateHippaNewPatientVC
                HippanewPatientVC.patient = patient
                self.navigationController?.pushViewController(HippanewPatientVC, animated: true)
            }

        }
    }
}

extension UpdateHippaVC : UITextFieldDelegate{
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}
