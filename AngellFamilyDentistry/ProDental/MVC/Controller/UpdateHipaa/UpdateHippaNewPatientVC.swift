//
//  UpdateHippaNewPatientVC.swift
//  AngellFamilyDentistry
//
//  Created by Leojin Bose on 7/4/16.
//  Copyright © 2016 SRS. All rights reserved.
//



import UIKit


class UpdateHippaNewPatientVC: MCViewController {
    
    var textRanges : [NSRange]! = [NSRange]()
//    @IBOutlet weak var labelPatientDetails: UILabel!
 
//    @IBOutlet weak var buttonSubmit: PDButton!
//    @IBOutlet weak var buttonBack: PDButton!
    
//    @IBOutlet weak var labelDate1: UILabel!
//    @IBOutlet weak var imageSignature: UIImageView!
    @IBOutlet weak var scrollView: UIScrollView!
    
    
    @IBOutlet weak var labelPatientName : UILabel!
    @IBOutlet weak var labelDate1 : UILabel!
    @IBOutlet weak var labelName1 : UILabel!
    @IBOutlet weak var labelPhone1 : UILabel!
    @IBOutlet weak var labelName2 : UILabel!
    @IBOutlet weak var labelPhone2 : UILabel!
    @IBOutlet weak var radioAppointmnet1 : RadioButton!
    @IBOutlet weak var radioAppointment2 : RadioButton!
    @IBOutlet weak var radioHealth1 : RadioButton!
    @IBOutlet weak var radioHealth2 : RadioButton!
    @IBOutlet weak var radioFinancial1 : RadioButton!
    @IBOutlet weak var radioFinancial2 : RadioButton!
    @IBOutlet weak var labelHomeAddress : UILabel!
    @IBOutlet weak var labelHomePhone : UILabel!
    @IBOutlet weak var labelWorkPhone : UILabel!

    @IBOutlet weak var labelCellPhone : UILabel!
    @IBOutlet weak var labelEmailAddress : UILabel!
    @IBOutlet weak var radioTexting : RadioButton!
    @IBOutlet weak var signatureImageView : UIImageView!
    @IBOutlet weak var labelDate2 : UILabel!
    @IBOutlet weak var labelRepresentativeName : UILabel!
    @IBOutlet weak var labelRepresentativeRelationship : UILabel!
 
    override func viewDidLoad() {
        super.viewDidLoad()
//        buttonSubmit.backgroundColor = UIColor.greenColor()
        
        loadData()
    }
    
    func loadData()  {
        labelPatientName.text = patient.fullName
        labelDate1.text = patient.dateOfBirth
        labelName1.text = patient.appointmentName1.isEmpty ? "N/A" : "\(patient.appointmentName1)/ \(patient.appointmentRelationship1)"
        labelPhone1.text = patient.appointmentPhone1.setText()
        labelName2.text =  patient.appointmentName2.isEmpty ? "N/A" : "\(patient.appointmentName2)/ \(patient.appointmentRelationship2)"
        labelPhone2.text = patient.appointmentPhone2.setText()
        radioAppointmnet1.setSelectedWithTag(patient.appointmentTag1)
        radioAppointment2.setSelectedWithTag(patient.appointmentTag2)
        radioHealth1.setSelectedWithTag(patient.appointmentHealthTag1)
        radioHealth2.setSelectedWithTag(patient.appointmentHealthTag2)
        radioFinancial1.setSelectedWithTag(patient.appointmentFinancialTag1)
        radioFinancial2.setSelectedWithTag(patient.appointmentFinancialTag2)
        labelWorkPhone.text = patient.workPhone
        
        var address: [String] = [String]()
        if patient.addressLine.count > 0 {
            address.append(patient.address)
        }
        if patient.city.count > 0 {
            address.append(patient.city)
        }
        if patient.state.count > 0 {
            address.append(patient.state)
        }
        if patient.zipCode.count > 0 {
            address.append(patient.zipCode)
        }
        labelHomeAddress.text = (address as NSArray).componentsJoined(by: ", ").setText()
        
        labelHomePhone.text = patient.homePhone?.setText()
        labelCellPhone.text = patient.cellPhone?.setText()
        labelEmailAddress.text = patient.email?.setText()
        radioTexting.setSelectedWithTag(patient.textingTag)
        signatureImageView.image = patient.UpdateHippasignature1
        labelDate2.text = patient.dateToday?.setText()
        labelRepresentativeName.text = patient.representativeName?.setText()
        labelRepresentativeRelationship.text = patient.representativeRelationship?.setText()
    }
    
//    @IBAction func buttonActionBack(sender: AnyObject) {
//        self.navigationController?.popViewControllerAnimated(true)
//    }
    
    
//    @IBAction func buttonActionSubmit(sender: AnyObject) {
//        
//        if !Reachability.isConnectedToNetwork() {
//            let alertController = UIAlertController(title: "Angell Family Dentistry", message: "Your device is not connected to internet. Please go to settings to connect.", preferredStyle: UIAlertControllerStyle.Alert)
//            let alertOkAction = UIAlertAction(title: "Settings", style: UIAlertActionStyle.Destructive) { (action) -> Void in
//                let settingsUrl = NSURL(string: UIApplicationOpenSettingsURLString)
//                if let url = settingsUrl {
//                    UIApplication.sharedApplication().openURL(url)
//                }
//            }
//            let alertCancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.Destructive) { (action) -> Void in
//                
//            }
//            alertController.addAction(alertOkAction)
//            alertController.addAction(alertCancelAction)
//            self.presentViewController(alertController, animated: true, completion: nil)
//            return
//        }
//        let pdfManager = PDFManager()
//        pdfManager.authorizeDrive(self.view) { (success) -> Void in
//            if success {
//                self.buttonSubmit.hidden = true
//                self.buttonBack.hidden = true
//                pdfManager.createPDFForScrollView(self.scrollView, fileName: "NEW_PATIENT", patient: self.patient, completionBlock: { (finished) -> Void in
//                    if finished {
//                        self.patient = self.patient.patient()
//                        let formNames = (self.patient.selectedForms as NSArray).valueForKey("formTitle") as! [String]
//                        if formNames.contains(kMedicalHistoryForm) {
//                            let medicalHistoryStep1VC = self.storyboard?.instantiateViewControllerWithIdentifier("kMedicalHistoryStep1VC") as! MedicalHistoryStep1ViewController
//                            medicalHistoryStep1VC.patient = self.patient
//                            self.navigationController?.pushViewController(medicalHistoryStep1VC, animated: true)
//                        } else if formNames.contains(kPatientAuthorization) {
//                            let patientAuthorizationVC = self.storyboard?.instantiateViewControllerWithIdentifier("kPatientAuthorizationVC") as! PatientAuthorizationViewController
//                            patientAuthorizationVC.patient = self.patient
//                            self.navigationController?.pushViewController(patientAuthorizationVC, animated: true)
//                        } else if formNames.contains(kInsuranceCard) {
//                            let cardCapture = self.storyboard?.instantiateViewControllerWithIdentifier("kCardImageCaptureVC") as! CardImageCaptureVC
//                            cardCapture.patient = self.patient
//                            self.navigationController?.pushViewController(cardCapture, animated: true)
//                        } else if formNames.contains(kDrivingLicense) {
//                            let cardCapture = self.storyboard?.instantiateViewControllerWithIdentifier("kCardImageCaptureVC") as! CardImageCaptureVC
//                            cardCapture.patient = self.patient
//                            cardCapture.isDrivingLicense = true
//                            self.navigationController?.pushViewController(cardCapture, animated: true)
//                        }else if formNames.contains(kDentalImplants) {
//                            let dentalImplantVC = self.storyboard?.instantiateViewControllerWithIdentifier("kDentalImplantsVC") as! DentalImplantsViewController
//                            dentalImplantVC.patient = self.patient
//                            self.navigationController?.pushViewController(dentalImplantVC, animated: true)
//                        } else if formNames.contains(kToothExtraction) {
//                            let toothExtractionVC = self.storyboard?.instantiateViewControllerWithIdentifier("kToothExtractionVC") as! ToothExtractionViewController
//                            toothExtractionVC.patient = self.patient
//                            self.navigationController?.pushViewController(toothExtractionVC, animated: true)
//                        } else if formNames.contains(kPartialDenture) {
//                            let dentureAdjustmentVC = self.storyboard?.instantiateViewControllerWithIdentifier("kDentureAdjustmentVC") as! DentureAdjustmentViewController
//                            dentureAdjustmentVC.patient = self.patient
//                            self.navigationController?.pushViewController(dentureAdjustmentVC, animated: true)
//                        } else if formNames.contains(kInofficeWhitening) {
//                            let inOfficeWhiteningVC = self.storyboard?.instantiateViewControllerWithIdentifier("kInOfficeWhiteningVC") as! InOfficeWhiteningViewController
//                            inOfficeWhiteningVC.patient = self.patient
//                            self.navigationController?.pushViewController(inOfficeWhiteningVC, animated: true)
//                        } else if formNames.contains(kTreatmentOfChild) {
//                            let childTreatmentVC = self.storyboard?.instantiateViewControllerWithIdentifier("kChildTreatmentVC") as! ChildTreatmentViewController
//                            childTreatmentVC.patient = self.patient
//                            self.navigationController?.pushViewController(childTreatmentVC, animated: true)
//                        } else if formNames.contains(kEndodonticTreatment) {
//                            let endodonticTreatmentVC = self.storyboard?.instantiateViewControllerWithIdentifier("kEndodonticTreatmentVC") as! EndodonticTreatmentViewController
//                            endodonticTreatmentVC.patient = self.patient
//                            self.navigationController?.pushViewController(endodonticTreatmentVC, animated: true)
//                        } else if formNames.contains(kDentalCrown) {
//                            let dentalCrownVC = self.storyboard?.instantiateViewControllerWithIdentifier("kDentureCrownVC") as! DentalCrownViewController
//                            dentalCrownVC.patient = self.patient
//                            self.navigationController?.pushViewController(dentalCrownVC, animated: true)
//                            
//                        }
//                        else if formNames.contains(kOpiodForm) {
//                            let opiodVC = self.storyboard?.instantiateViewControllerWithIdentifier("kOpiodViewController") as! OpiodViewController
//                            opiodVC.patient = self.patient
//                            self.navigationController?.pushViewController(opiodVC, animated: true)
//                        }
//                        else if formNames.contains(kGeneralConsent) {
//                            let generalVC = self.storyboard?.instantiateViewControllerWithIdentifier("kGeneralConsentVC") as! GeneralConsentViewController
//                            generalVC.patient = self.patient
//                            self.navigationController?.pushViewController(generalVC, animated: true)
//                        }else if formNames.contains(kFeedbackForm) {
//                            let feedbackFormVC = self.storyboard?.instantiateViewControllerWithIdentifier("kFeedbackFormVC") as! FeedbackFormViewController
//                            self.navigationController?.pushViewController(feedbackFormVC, animated: true)
//                        }                        else {
//                            NSNotificationCenter.defaultCenter().postNotificationName(kFormsCompletedNotification, object: nil)
//                            self.navigationController?.popToRootViewControllerAnimated(true)
//                        }
//                    } else {
//                        self.buttonSubmit.hidden = false
//                        self.buttonBack.hidden = false
//                    }
//                })
//            }
//        }
//    }
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}
