//
//  AddressInfoViewController.swift
//   Angell Family Dentistry
//
//  Created by Leojin Bose on 2/19/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

class UpdateHipaaAddressInfoViewController: MCViewController {
    
    @IBOutlet weak var textFieldAddressLine: MCTextField!
    
    @IBOutlet weak var textFieldState: MCTextField!
    @IBOutlet weak var textFieldCity: MCTextField!
    @IBOutlet weak var textFieldZipCode: MCTextField!
    @IBOutlet weak var textFieldEmail: MCTextField!
    @IBOutlet weak var textFieldPhone: MCTextField!
//    @IBOutlet weak var textFieldSecurityNumber: PDTextField!
    @IBOutlet weak var textfieldCellNumber : MCTextField!
    @IBOutlet weak var textfieldWorkNumber : MCTextField!

    @IBOutlet weak var textfieldApartmentNumber : MCTextField!
    
    @IBOutlet weak var radioButtonMaritialStatus: RadioButton!
//    @IBOutlet weak var radioHearAboutUs: RadioButton!
    @IBOutlet weak var radioTexting : RadioButton!
    
    var arrayStates : [String] = [String]()
    var referenceDetails : String!
    var othersDetails : String!
    
    
    @IBOutlet weak var backButton: MCButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        textFieldState.textFormat = .State
        textFieldZipCode.textFormat = .Zipcode
        textFieldEmail.textFormat = .Email
        textFieldPhone.textFormat = .Phone
         textfieldCellNumber.textFormat = .Phone
        textfieldWorkNumber.textFormat = .Phone
        textfieldApartmentNumber.textFormat = .AlphaNumeric
        textFieldZipCode.textFormat = .Zipcode
        self.loadValues()
        // Do any additional setup after loading the view.
    }
    
    func loadValues() {
        textFieldAddressLine.text = patient.address
        textfieldApartmentNumber.text = patient.aptNumber
        textFieldState.text = patient.state
        
        textFieldZipCode.text = patient.zipCode
        textFieldCity.text = patient.city
        textFieldEmail.text = patient.email
        textFieldPhone.text = patient.homePhone
        
        radioButtonMaritialStatus.setSelectedWithTag(patient.maritialStatus!)
        
//        radioHearAboutUs.setSelectedWithTag(patient.radioHearAboutUsTag!)
        
        referenceDetails = patient.referredByDetails
        othersDetails = patient.othersDetails
        
        textfieldCellNumber.text = patient.cellPhone
        textfieldWorkNumber.text = patient.workPhone == "N/A" ? "" : patient.workPhone
        radioTexting.setSelectedWithTag(patient.textingTag)
    }
    
    
    func setValues() {
        patient.address = textFieldAddressLine.text
        patient.aptNumber = textfieldApartmentNumber.text
        patient.state = textFieldState.text
        patient.workPhone = textfieldWorkNumber.isEmpty ? "N/A" : textfieldWorkNumber.text!
        
        patient.zipCode = textFieldZipCode.text
        patient.city = textFieldCity.text
        patient.email = textFieldEmail.text?.lowercased()
        patient.homePhone = textFieldPhone.text
        
        patient.maritialStatus = radioButtonMaritialStatus.selected == nil ? 0 : radioButtonMaritialStatus.selected.tag
        
//        patient.radioHearAboutUsTag = radioHearAboutUs.selectedButton == nil ? 0 : radioHearAboutUs.selectedButton.tag
        
        patient.referredByDetails = referenceDetails
        patient.othersDetails = othersDetails
        
        patient.cellPhone = textfieldCellNumber.isEmpty ? "" : textfieldCellNumber.text!
        patient.textingTag = radioTexting.selected == nil ? 0 : radioTexting.selected.tag
    }

       override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func buttonActionNext(_ sender: AnyObject) {
        self.view.endEditing(true)
        if let _ = findEmptyTextField() {
            self.showAlert("PLEASE ENTER ALL REQUIRED FIELDS")
        } else if !textFieldZipCode.text!.isZipCode {
            self.showAlert("PLEASE ENTER VALID ZIPCODE")
//        } else if (!textFieldSecurityNumber.isEmpty || is18YearsOld) && !textFieldSecurityNumber.text!.isSocialSecurityNumber {
//            self.showAlert("PLEASE ENTER VALID SOCIAL SECURITY NUMBER")
        } else if !textFieldPhone.text!.isPhoneNumber {
            self.showAlert("PLEASE ENTER VALID PHONE NUMBER")
        }else if !textfieldWorkNumber.isEmpty && !textfieldWorkNumber.text!.isPhoneNumber {
            self.showAlert("PLEASE ENTER VALID WORK NUMBER")
        }else if !textFieldEmail.isEmpty && !textFieldEmail.text!.isValidEmail {
            self.showAlert("PLEASE ENTER VALID EMAIL")
        } else if radioButtonMaritialStatus.selected == nil  {
            self.showAlert("PLEASE SELECT YOUR MARITIAL STATUS")
//        }else if radioHearAboutUs.selectedButton == nil  {
//            self.showAlert("PLEASE SELECT HOW DID YOU HEAR ABOUT US")
//            self.presentViewController(alert, animated: true, completion: nil)
//        }else if !textfieldCellNumber.isEmpty && !textfieldCellNumber.text!.isPhoneNumber{
//            self.showAlert("PLEASE ENTER THE VALID CELL PHONE NUMBER")
//            self.presentViewController(alert, animated: true, completion: nil)
        }else if radioTexting.selected == nil{
            self.showAlert("PLEASE ENTER ALL THE REQUIRED FIELDS")
        }else if radioTexting.selected.tag == 1 && textfieldCellNumber.isEmpty{
            self.showAlert("PLEASE ENTER THE CELL PHONE NUMBER")
        } else if !textfieldCellNumber.isEmpty && !textfieldCellNumber.text!.isPhoneNumber {
            self.showAlert("PLEASE ENTER A VALID CELL PHONE NUMBER")
        }else {
            self.setValues()
            patient.addressLine = textfieldApartmentNumber.isEmpty ? textFieldAddressLine.text! : "\(textFieldAddressLine.text!), \(textfieldApartmentNumber.text!)"
            let formNames = (patient.selectedForms as NSArray).value(forKey: "formTitle") as! [String]
            if formNames.first == kUpdateHIPAA {
                let step3VC = self.storyboard?.instantiateViewController(withIdentifier: "kUpdateHipaaAppointmentViewController") as! UpdateHipaaAppointmentViewController
                step3VC.patient = patient
                navigationController?.pushViewController(step3VC, animated: true)
            }else{
            let contactInfoVC = self.storyboard?.instantiateViewController(withIdentifier: "kContactInfoVC") as! ContactInfoViewController
            contactInfoVC.patient = patient
            self.navigationController?.pushViewController(contactInfoVC, animated: true)
            }
        }
    }
    

    
    
//    @IBAction func radioHearAboutUsTag(sender: AnyObject) {
//        if sender.tag == 8{
//            PopupTextField.sharedInstance.showWithPlaceHolder("REFERRED BY", keyboardType: UIKeyboardType.Default, textFormat: .Default, completion: { (textField, isEdited) in
//                if isEdited{
//                    self.referenceDetails = textField.text
//                }else{
//                    self.referenceDetails = ""
////                    self.radioHearAboutUs.deselectAllButtons()
//                }
//            })
//        }else if sender.tag == 9{
//            PopupTextField.sharedInstance.showWithPlaceHolder("PLEASE SPECIFY", keyboardType: UIKeyboardType.Default, textFormat: .Default, completion: { (textField, isEdited) in
//                if isEdited{
//                    self.othersDetails = textField.text
//                }else{
//                    self.othersDetails = ""
////                    self.radioHearAboutUs.deselectAllButtons()
//                }
//            })
//            
//        }
//        
//    }
    
    @IBAction override func buttonBackAction() {
        self.setValues()
        super.buttonBackAction()
    }
    
    func findEmptyTextField() -> UITextField? {
        let textFields = [textFieldCity,  textFieldPhone, textFieldState, textFieldZipCode, textFieldAddressLine]
        for textField in textFields {
            if textField!.isEmpty {
                return textField
            }
        }
        return nil
    }
}

extension UpdateHipaaAddressInfoViewController : UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if textField == textFieldZipCode {
            return textField.formatZipCode(range, string: string)
        }
        
        if textField == textFieldPhone || textField == textfieldCellNumber || textField == textfieldWorkNumber{
           
            return textField.formatPhoneNumber(range, string: string)
        }
        return true
    }
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}
