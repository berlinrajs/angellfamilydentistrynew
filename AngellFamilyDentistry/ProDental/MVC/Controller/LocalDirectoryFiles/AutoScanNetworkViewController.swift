//
//  AutoScanNetworkViewController.swift
//  MConsent
//
//  Created by Berlin Raj on 06/07/17.
//  Copyright © 2017 SRS Web Solutions. All rights reserved.
//

import UIKit

class AutoScanNetworkViewController: MCViewController {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var buttonScan: UIButton!
    
    var biosService: TONetBIOSNameService!
    var ipResolver: TONetBIOSNameService!
    
    var scanIndex: Int = 0
    var arrayDevices: [TONetBIOSNameServiceEntry] = [TONetBIOSNameServiceEntry]()
    @IBOutlet weak var headerView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        ipResolver = TONetBIOSNameService()
        
        if !SystemConfig.hasSystemConfig {
            self.buttonBack?.isHidden = true
        } else {
            self.buttonBack?.isHidden = false
        }
        
        self.buttonScanAction()
        // Do any additional setup after loading the view.
    }
    
    override func buttonBackAction() {
        biosService.stopDiscovery()
        self.dismiss(animated: true, completion: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        biosService.stopDiscovery()
        super.viewWillDisappear(animated)
    }
    
    @IBAction func buttonScanAction () {
        biosService = TONetBIOSNameService()
        self.arrayDevices.removeAll()
        self.tableView.reloadData()
        self.activityIndicator.startAnimating()
        self.buttonScan.isHidden = true
        
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.3, execute: {
            self.biosService.startDiscovery(withTimeOut: 4.0, added: { (newEntry) in
                if newEntry == nil {
                    return
                }
                let ipAddressString = newEntry!.ipAddressString == nil ? self.ipResolver.resolveIPAddress(withName: newEntry!.name, type: newEntry!.type) : newEntry!.ipAddressString
                if ipAddressString == nil {
                    return
                }
                self.scanIndex = self.scanIndex + 1
                
                SystemConfig.checkConnectivity(WithIp: ipAddressString!) { (success, errorMessage) in
                    if success {
                        self.arrayDevices.append(newEntry!)
                        self.tableView.reloadData()
                        self.scanIndex = max(0, self.scanIndex - 1)
                    } else {
                        print(errorMessage!)
                        self.scanIndex = max(0, self.scanIndex - 1)
                    }
                }
            }) { (deletedEntry) in
                
            }
        })
    }
    
    @IBAction func buttonSettingAction () {
        biosService.stopDiscovery()
        scanIndex = 0
        self.activityIndicator.stopAnimating()
        self.buttonScan.isHidden = false
        
        PopupTextField.popUpView().showInViewController(self, WithTitle: "Server IP Address", placeHolder: "255.255.255.255", textFormat: .AlphaNumeric) { (popUpview, textField) in
            if !textField.isEmpty && textField.text!.isValidIP {
                popUpview.close()
                let config = SystemConfig()
                config.hostIp = textField.text!
                
                config.checkConnectivity { (success, errorMessage) in
                    if success {
                        self.showAlert("Successfully connected with Dentrix", completion: {
                            if SystemConfig.hasSystemConfig {
                                SystemConfig.sharedConfig = config
                                SystemConfig.hasSystemConfig = true
                                
                                self.dismiss(animated: true, completion: nil)
                                NotificationCenter.default.post(name: NSNotification.Name(rawValue: kIPChangedNotification), object: nil)
                            } else {
                                SystemConfig.sharedConfig = config
                                SystemConfig.hasSystemConfig = true
                                
                                (UIApplication.shared.delegate as! AppDelegate).checkAutologin()
                            }
                        })
                    } else {
                        self.showAlert(errorMessage!)
                    }
                }
            } else if !textField.isEmpty {
                self.showAlert("Invalid IP address")
            } else {
                popUpview.close()
            }
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
//    @IBAction func buttonSkipAction () {
//        biosService.stopDiscovery()
//        self.showCustomAlert("You have selected to SKIP connection with Dentrix. All completed forms will be securely  transferred to your Google drive.") {
//            let config = SystemConfig()
//            config.hostIp = ""
//            config.hostName = ""
//
//            if SystemConfig.hasSystemConfig {
//                SystemConfig.sharedConfig = config
//                SystemConfig.hasSystemConfig = true
//
//                self.dismiss(animated: true, completion: nil)
//                NotificationCenter.default.post(name: NSNotification.Name(rawValue: kIPChangedNotification), object: nil)
//            } else {
//                SystemConfig.sharedConfig = config
//                SystemConfig.hasSystemConfig = true
//
//                (UIApplication.shared.delegate as! AppDelegate).checkAutologin()
//            }
//        }
//    }
}

extension AutoScanNetworkViewController: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return max(1, arrayDevices.count)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return arrayDevices.count == 0 ? tableView.frame.height - 40 : 60
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if arrayDevices.count == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "emptyCell")!
            
            return cell
        }
        var cell = tableView.dequeueReusableCell(withIdentifier: "IPCell")
        
        if cell == nil {
            cell = UITableViewCell(style: UITableViewCellStyle.subtitle, reuseIdentifier: "IPCell") as UITableViewCell
        }
        
        let device = self.arrayDevices[indexPath.row]
        cell?.textLabel?.text = device.name
        
        let ipAddressString = device.ipAddressString == nil ? self.ipResolver.resolveIPAddress(withName: device.name, type: device.type) : device.ipAddressString
        cell?.detailTextLabel?.text = ipAddressString ?? ""
        
        return cell!
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 60.0
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return headerView
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if arrayDevices.count == 0 {
            return
        }
        
        let device = arrayDevices[indexPath.row]
        let ipAddressString = device.ipAddressString == nil ? self.ipResolver.resolveIPAddress(withName: device.name, type: device.type) : device.ipAddressString
        
        if ipAddressString == nil {
            self.showAlert("Invalid IP Address!!!")
            return
        }
        
        let config = SystemConfig()
        config.hostIp = ipAddressString!
        config.hostName = device.name ?? ""
        
        config.checkConnectivity { (success, errorMessage) in
            if success {
                self.showAlert("Successfully connected with Dentrix", completion: {
                    if SystemConfig.hasSystemConfig {
                        SystemConfig.sharedConfig = config
                        SystemConfig.hasSystemConfig = true
                        
                        self.dismiss(animated: true, completion: nil)
                        NotificationCenter.default.post(name: NSNotification.Name(rawValue: kIPChangedNotification), object: nil)
                    } else {
                        SystemConfig.sharedConfig = config
                        SystemConfig.hasSystemConfig = true
                        
                        (UIApplication.shared.delegate as! AppDelegate).checkAutologin()
                    }
                })
            } else {
                self.showAlert(errorMessage!)
            }
        }
    }
}
