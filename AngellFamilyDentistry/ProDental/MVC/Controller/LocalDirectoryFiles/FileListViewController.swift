//
//  FileListViewController.swift
//  MConsent For OD
//
//  Created by Berlin Raj on 04/07/17.
//  Copyright © 2017 SRS Web Solutions. All rights reserved.
//

import UIKit

class FileListViewController: MCViewController {

    var arrayFiles: [FormFile] = [FormFile]()
    var arrayDateSection: [String] = [String]()
    
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        loadValues()
    }
    
    func loadValues() {
        let documentsPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0]
        let filesToSearch = kOutputFileMustBeImage == true ? "jpg" : "pdf"
        
        let filePaths = Bundle.paths(forResourcesOfType: filesToSearch, inDirectory: documentsPath)
        
        for path in filePaths {
            let formFile = FormFile(Path: path)
            arrayFiles.append(formFile)
            
            if !arrayDateSection.contains(formFile.creationDateString) {
                arrayDateSection.append(formFile.creationDateString)
            }
        }
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MMM dd, yyyy"
        
        arrayDateSection.sort { (date1, date2) -> Bool in
            return dateFormatter.date(from: date1)! > dateFormatter.date(from: date2)!
        }
        
        tableView.reloadData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}


extension FileListViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if arrayFiles.count > 0 {
            let cell = tableView.cellForRow(at: indexPath) as! FileListTableViewCell
            let previewVC = self.storyboard?.instantiateViewController(withIdentifier: "kFilePreviewViewController") as! FilePreviewViewController
            previewVC.formFile = cell.formFile
            self.navigationController?.pushViewController(previewVC, animated: true)
        }
    }
}

extension FileListViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return arrayFiles.count == 0 ? tableView.frame.height : 60.0
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return max(1, arrayDateSection.count)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if arrayFiles.count == 0 {
            return 1
        }
        
        let arraySectionFile = arrayFiles.filter { (formFile) -> Bool in
            return formFile.creationDateString == arrayDateSection[section]
        }
        
        return arraySectionFile.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if arrayFiles.count == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "emptyCell")!
            
            return cell
        }
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MMM dd, yyyy hh:mm:ss a"
        
        let arraySectionFile = arrayFiles.filter { (formFile) -> Bool in
            return formFile.creationDateString == arrayDateSection[indexPath.section]
            }.sorted { (frm1, frm2) -> Bool in
                return dateFormatter.date(from: frm1.creationDateTime)! > dateFormatter.date(from: frm2.creationDateTime)!
        }
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! FileListTableViewCell
        
        cell.configWith(FormFile: arraySectionFile[indexPath.row])
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return arrayFiles.count == 0 ? 0 : 40.0
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if arrayFiles.count == 0 {
            return nil
        }
        var header = tableView.dequeueReusableHeaderFooterView(withIdentifier: "Header") as? FileListSectionHeader
        
        if header == nil {
            header = FileListSectionHeader(reuseIdentifier: "Header")
        }
        
        header?.labelHeader.text = arrayDateSection[section]
        
        return header
    }
    
    
    //DELETE A FILE
//    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
//        
//        return true
//    }
//    
//    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
//        if editingStyle == .delete {
//            let path = arrayFiles[indexPath.row]
//            let fileManager = FileManager.default
//            if fileManager.fileExists(atPath: path) {
//                do {
//                    try fileManager.removeItem(atPath: path)
//                    self.arrayFiles.remove(at: indexPath.row)
//                    if self.arrayFiles.count == 0 {
//                        tableView.beginUpdates()
//                        tableView.reloadRows(at: [indexPath], with: UITableViewRowAnimation.automatic)
//                        tableView.endUpdates()
//                    } else {
//                        tableView.beginUpdates()
//                        tableView.deleteRows(at: [indexPath], with: UITableViewRowAnimation.automatic)
//                        tableView.endUpdates()
//                    }
//                } catch {
//                    self.showAlert("DELETION FAILED")
//                }
//            } else {
//                self.showAlert("FILE NOT EXISTS")
//            }
//        }
//    }
}
