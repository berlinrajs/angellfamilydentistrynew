//
//  ScanIPAddressViewController.swift
//  MConsent
//
//  Created by Berlin Raj on 06/07/17.
//  Copyright © 2017 SRS Web Solutions. All rights reserved.
//

import UIKit

class ScanIPAddressViewController: MCViewController {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var buttonScan: UIButton!
    
    //    var arrayDevices: [TONetBIOSNameServiceEntry] = [TONetBIOSNameServiceEntry]()
    var biosService: TONetBIOSNameService = TONetBIOSNameService()
    var scanIndex: Int = 0
    var arrayDevices: [MMDevice] = [MMDevice]()
    var lanScanner: MMLANScanner!
    
    @IBOutlet weak var headerView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.buttonScanAction()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        lanScanner.stop()
    }
    
    @IBAction func buttonScanAction () {
        self.arrayDevices.removeAll()
        self.tableView.reloadData()
        self.activityIndicator.startAnimating()
        self.buttonScan.isHidden = true
        
        self.lanScanner = MMLANScanner(delegate: self)
        lanScanner.start()
        //        biosService = TONetBIOSNameService()
        //        let _ = biosService.startDiscovery(withTimeOut: 4.0, added: { (addEntry) in
        //            if let entry = addEntry {
        //
        //                let ipAddressString = entry.ipAddressString == nil ? self.biosService.resolveIPAddress(withName: entry.name, type: entry.type) : entry.ipAddressString
        //                print("NAME: \(ipAddressString ?? "") GROUP: \(entry.group!)")
        //                ServiceManager.fetchDataFromPostService("http://\(ipAddressString!)/opmaster/", serviceName: "test-remote-mysql.php", parameters: nil, success: { (result) in
        //                    print(result)
        //                    print("NAME: \(ipAddressString ?? "") GROUP: \(entry.group)")
        //                    self.arrayDevices.append(entry)
        //                    self.tableView.reloadData()
        //                }, failure: { (error) in
        //                    print("NAME: \(ipAddressString ?? "")")
        //                    print(error.localizedDescription)
        //                })
        //            }
        //        }) { (removeEntry) in
        //            if let entry = removeEntry {
        //                if self.arrayDevices.contains(entry) {
        //                    self.arrayDevices.remove(at: self.arrayDevices.index(of: entry)!)
        //                }
        //                self.tableView.reloadData()
        //            }
        //        }
        //
        //        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 4.0) {
        //            self.activityIndicator.stopAnimating()
        //            self.buttonScan.isHidden = false
        //        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

extension ScanIPAddressViewController: MMLANScannerDelegate {
    func lanScanDidFailedToScan() {
        scanIndex = 0
        checkFinished()
    }
    
    func lanScanDidFindNewDevice(_ device: MMDevice!) {
//        device.ipAddress = "68.46.24.31"
        device.ipAddress = device.ipAddress == nil ? self.biosService.resolveIPAddress(withName: device.hostname, type: TONetBIOSNameServiceType.workStation) : device.ipAddress
        device.hostname = device.hostname == nil ? self.biosService.lookupNetworkName(forIPAddress: device.ipAddress) : device.hostname
        
        print("NAME: \(device.hostname ?? "") IP: \(device.ipAddress ?? "")")
        scanIndex = scanIndex + 1
        
        SystemConfig.checkConnectivity(WithIp: device.ipAddress!) { (success, errorMessage) in
            if success {
                self.arrayDevices.append(device)
                self.tableView.reloadData()
                self.scanIndex = self.scanIndex - 1
                self.checkFinished()
            } else {
                self.scanIndex = self.scanIndex - 1
                self.checkFinished()
                print("Failed IP: \(device.ipAddress ?? "") Name: \(device.hostname ?? "")")
                print(errorMessage!)
            }
        }
    }
    
    func lanScanDidFinishScanning(with status: MMLanScannerStatus) {
        lanScanner.stop()
        checkFinished()
    }
    
    func lanScanProgressPinged(_ pingedHosts: Float, from overallHosts: Int) {
        
    }
    
    func checkFinished() {
        if scanIndex <= 0 && !lanScanner.isScanning {
            self.activityIndicator.stopAnimating()
            self.buttonScan.isHidden = false
        } else if scanIndex > 0 || lanScanner.isScanning {
            self.activityIndicator.startAnimating()
            self.buttonScan.isHidden = true
        }
    }
}

extension ScanIPAddressViewController: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return max(1, arrayDevices.count)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return arrayDevices.count == 0 ? tableView.frame.height - 40 : 60
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if arrayDevices.count == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "emptyCell")!
            
            return cell
        }
        var cell = tableView.dequeueReusableCell(withIdentifier: "IPCell")
        
        if cell == nil {
            cell = UITableViewCell(style: UITableViewCellStyle.subtitle, reuseIdentifier: "IPCell") as UITableViewCell
        }
        
        let device = self.arrayDevices[indexPath.row]
        //        if device.name == nil {
        //            biosService.lookupNetworkName(forIPAddress: device.ipAddressString, success: { (hostName) in
        //                cell?.textLabel?.text = hostName
        //            }, failure: {
        //
        //            })
        //        } else {
        cell?.textLabel?.text = device.hostname
        //        }
        
        //        if device.ipAddressString == nil {
        //            biosService.resolveIPAddress(withName: device.name, type: device.type, success: { (ipAddressString) in
        //                cell?.detailTextLabel?.text = ipAddressString
        //            }, failure: {
        //
        //            })
        //        } else {
        cell?.detailTextLabel?.text = device.ipAddress
        //        }
        
        return cell!
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 60.0
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return headerView
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if arrayDevices.count == 0 {
            return
        }
        
//        if biosService.discovering {
//            self.activityIndicator.stopAnimating()
//            biosService.stopDiscovery()
//            self.buttonScan.isHidden = false
//        }
        
        let device = arrayDevices[indexPath.row]
        
        let hostName = device.hostname == nil ? biosService.lookupNetworkName(forIPAddress: device.ipAddress) : device.hostname
        let ipAddressString = device.ipAddress == nil ? biosService.resolveIPAddress(withName: device.hostname, type: TONetBIOSNameServiceType.workStation) : device.ipAddress
        
        let config = SystemConfig()
        config.hostIp = ipAddressString
        config.hostName = hostName
        
        if SystemConfig.hasSystemConfig {
            SystemConfig.sharedConfig = config
            SystemConfig.hasSystemConfig = true
            
            self.parent?.dismiss(animated: true, completion: nil)
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: kIPChangedNotification), object: nil)
        } else {
            SystemConfig.sharedConfig = config
            SystemConfig.hasSystemConfig = true
            
            (UIApplication.shared.delegate as! AppDelegate).checkAutologin()
        }
    }
}
