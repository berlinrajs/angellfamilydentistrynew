//
//  FilePreviewViewController.swift
//  MConsent For OD
//
//  Created by Berlin Raj on 04/07/17.
//  Copyright © 2017 SRS Web Solutions. All rights reserved.
//

import UIKit

class FilePreviewViewController: MCViewController {

    var formFile: FormFile!
//    var session: TOSMBSession!
    
    @IBOutlet weak var constraintContainerviewHeight : NSLayoutConstraint!
    @IBOutlet weak var pdfPreviewView: UIWebView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        BRProgressHUD.show()
        let pdfFileRequest = NSURLRequest(url: URL(string: formFile.filePath)!)
        pdfPreviewView.loadRequest(pdfFileRequest as URLRequest)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func onSubmitButtonPressed() {
        BRProgressHUD.show()
        self.buttonSubmit?.isUserInteractionEnabled = false
        self.buttonBack?.isUserInteractionEnabled = false
        PDFManager.sharedInstance().uploadFileToLocal(formFile.filePath, fileName: formFile.filePath.components(separatedBy: "/").last!) { (success, errorMessage) in
            if success {
                BRProgressHUD.hide()
                self.buttonSubmit?.isUserInteractionEnabled = true
                self.buttonBack?.isUserInteractionEnabled = true
                self.showAlert("File uploaded successfully to the Server", completion: {
                    super.buttonBackAction()
                })
            } else {
                SystemConfig.sharedConfig?.checkConnectivity(completion: { (finished, errorMessage) in
                    BRProgressHUD.hide()
                    self.buttonSubmit?.isUserInteractionEnabled = true
                    self.buttonBack?.isUserInteractionEnabled = true
                    if finished {
                        self.showAlert("Something went wrong, Please contact our support team")
                    } else {
                        self.showAlert(errorMessage!)
                    }
                })
            }
        }
//        if let config = SystemConfig.sharedConfig, SystemConfig.hasSystemConfig == true {
//            BRProgressHUD.show()
//            let pdfData = try! Data(contentsOf: URL(fileURLWithPath: formFile.filePath))
//
//            ServiceManager.uploadFormToServer(fileName: formFile.filePath.components(separatedBy: "/").last!, pdfData: pdfData, completion: { (success, errorMessage) in
//                if success {
//                    BRProgressHUD.hide()
//                    self.buttonSubmit?.isUserInteractionEnabled = true
//                    self.buttonBack?.isUserInteractionEnabled = true
//                    self.showAlert("File uploaded successfully to the Server", completion: {
//                        super.buttonBackAction()
//                    })
//                    
//                } else {
//                    BRProgressHUD.hide()
//                    self.buttonSubmit?.isUserInteractionEnabled = true
//                    self.buttonBack?.isUserInteractionEnabled = true
//                    self.showAlert(errorMessage == nil ? "Something went wrong, please check your network connection and try again later" : errorMessage!)
//                    print("Error: \(errorMessage ?? "NONE")")
//
//                }
//            })
//        }
    }
    
    func getFolderName () -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MMM dd, yyyy"
        return dateFormatter.string(from: Date())
    }
    
    func getTimeStamp() -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "hh-mm-ss"
        return dateFormatter.string(from: Date())
    }
}

extension FilePreviewViewController: UIWebViewDelegate {
    func webViewDidFinishLoad(_ webView: UIWebView) {
        BRProgressHUD.hide()
        webView.scrollView.isPagingEnabled = true
    }
    
    func webView(_ webView: UIWebView, didFailLoadWithError error: Error) {
        BRProgressHUD.hide()
        self.showAlert(error.localizedDescription) { 
            super.buttonBackAction()
        }
    }
}
