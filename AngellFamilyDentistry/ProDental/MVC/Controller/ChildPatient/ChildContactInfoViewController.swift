//
//  ContactInfoViewController.swift
//   Angell Family Dentistry
//
//  Created by Leojin Bose on 2/19/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

class ChildContactInfoViewController: MCViewController {
    
//    @IBOutlet weak var textFieldEmployer: PDTextField!
//    @IBOutlet weak var textFieldEmployerPhoneNumber: PDTextField!
    @IBOutlet weak var textFieldEmergencyContact: MCTextField!
    @IBOutlet weak var textFieldEmergencyContactNumber: MCTextField!

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        textFieldEmergencyContactNumber.textFormat = .Phone
       
        self.loadValues()
        // Do any additional setup after loading the view.
    }
    
    func loadValues() {
//        textFieldEmployer.text = patient.employerName
//        textFieldEmployerPhoneNumber.text = patient.employerPhoneNumber
        textFieldEmergencyContact.text = patient.emergencyContactName
        textFieldEmergencyContactNumber.text = patient.emergencyContactPhoneNumber
    }
    
    func setValues() {
//        if !textFieldEmployer.isEmpty {
//            patient.employerName = textFieldEmployer.text
//        } else {
//            patient.employerName = nil
//        }
//        if !textFieldEmployerPhoneNumber.isEmpty {
//            patient.employerPhoneNumber = textFieldEmployerPhoneNumber.text
//        } else {
//            patient.employerPhoneNumber = nil
//        }
        patient.emergencyContactName = textFieldEmergencyContact.text
        patient.emergencyContactPhoneNumber = textFieldEmergencyContactNumber.text
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @IBAction func buttonActionNext(_ sender: AnyObject) {
        self.view.endEditing(true)
//        if !textFieldEmployerPhoneNumber.isEmpty && !textFieldEmployerPhoneNumber.text!.isPhoneNumber {
//            let alert = Extention.alert("PLEASE ENTER VALID EMPLOYER PHONE NUMBER")
//            self.presentViewController(alert, animated: true, completion: nil)
        if textFieldEmergencyContact.isEmpty {
            self.showAlert("PLEASE ENTER EMERGENCY CONTACT")
        }  else if textFieldEmergencyContactNumber.isEmpty {
            self.showAlert("PLEASE ENTER EMERGENCY CONTACT NUMBER")
        } else if !textFieldEmergencyContactNumber.text!.isPhoneNumber {
            self.showAlert("PLEASE ENTER VALID EMERGENCY CONTACT NUMBER")
        } else {
            setValues()
            let diseaseInfoVC = self.storyboard?.instantiateViewController(withIdentifier: "kChildDiseaseInfoVC") as! ChildDiseaseInfoViewController
            diseaseInfoVC.patient = patient
            self.navigationController?.pushViewController(diseaseInfoVC, animated: true)
        }
    }
    @IBAction override func buttonBackAction() {
        self.setValues()
        _ = self.navigationController?.popViewController(animated: true)
    }
   

}

extension ChildContactInfoViewController : UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if textField == textFieldEmergencyContactNumber {
            
            return textField.formatPhoneNumber(range, string: string)
        }
        return true
    }
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}
