//
//  AppointmentViewController.swift
//  AngellFamilyDentistry
//
//  Created by Bala Murugan on 7/13/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

class ChildAppointmentViewController: MCViewController {
    
    @IBOutlet weak var textfieldName1 : UITextField!
    @IBOutlet weak var textfieldRelationship1 : UITextField!
    @IBOutlet weak var textfieldPhone1 : MCTextField!
    @IBOutlet weak var radioAppointment1 : RadioButton!
    @IBOutlet weak var radioHealth1 : RadioButton!
    @IBOutlet weak var radioFinancial1 : RadioButton!
    @IBOutlet weak var textfieldName2 : UITextField!
    @IBOutlet weak var textfieldRelationship2 : UITextField!
    @IBOutlet weak var textfieldPhone2 : MCTextField!
    @IBOutlet weak var radioAppointment2 : RadioButton!
    @IBOutlet weak var radioHealth2 : RadioButton!
    @IBOutlet weak var radioFinancial2 : RadioButton!


    override func viewDidLoad() {
        super.viewDidLoad()
        textfieldPhone1.textFormat = .Phone
        textfieldPhone2.textFormat = .Phone
        autoFillData()
        // Do any additional setup after loading the view.
    }
    
    func autoFillData(){
        textfieldName1.text = patient.appointmentName1
        textfieldName2.text = patient.appointmentName2
        textfieldRelationship1.text = patient.appointmentRelationship1
        textfieldPhone1.text = patient.appointmentPhone1
        radioAppointment1.setSelectedWithTag(patient.appointmentTag1)
        radioHealth1.setSelectedWithTag(patient.appointmentHealthTag1)
        radioFinancial1.setSelectedWithTag(patient.appointmentFinancialTag1)
        textfieldRelationship2.text = patient.appointmentRelationship2
        textfieldPhone2.text = patient.appointmentPhone2
        radioAppointment2.setSelectedWithTag(patient.appointmentTag2)
        radioHealth2.setSelectedWithTag(patient.appointmentHealthTag2)
        radioFinancial2.setSelectedWithTag(patient.appointmentFinancialTag2)
        
        
    }
    
    
    func setValues() {
        patient.appointmentName1 = textfieldName1.text!
        patient.appointmentPhone1 = textfieldPhone1.text!
        patient.appointmentRelationship1 = textfieldRelationship1.text!
        patient.appointmentTag1 = radioAppointment1.selected == nil ? 0 : radioAppointment1.selected.tag
        patient.appointmentHealthTag1 = radioHealth1.selected == nil ? 0 : radioHealth1.selected.tag
        patient.appointmentFinancialTag1 = radioFinancial1.selected == nil ? 0 : radioFinancial1.selected.tag
        
        patient.appointmentName2 = textfieldName2.text!
        patient.appointmentPhone2 = textfieldPhone2.text!
        patient.appointmentRelationship2 = textfieldRelationship2.text!
        patient.appointmentTag2 = radioAppointment2.selected == nil ? 0 : radioAppointment2.selected.tag
        patient.appointmentHealthTag2 = radioHealth2.selected == nil ? 0 : radioHealth2.selected.tag
        patient.appointmentFinancialTag2 = radioFinancial2.selected == nil ? 0 : radioFinancial2.selected.tag
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func onBackButtonPressed (_ sender : UIButton){
        self.setValues()
        self.navigationController?.popViewController(animated: true)
    }
    

    @IBAction func onNextButtonPressed (_ sender : UIButton){
        if (!textfieldPhone1.isEmpty && !textfieldPhone1.text!.isPhoneNumber) || (!textfieldPhone2.isEmpty && !textfieldPhone2.text!.isPhoneNumber){
            self.showAlert("PLEASE ENTER THE VALID PHONE NUMBER")
        }else{
            self.setValues()
            let _ = (patient.selectedForms as NSArray).value(forKey: "formTitle") as! [String]
            
            let step3VC = self.storyboard?.instantiateViewController(withIdentifier: "kChildHippaStep3VC") as! ChildHippaStep3VC
            step3VC.patient = patient
            navigationController?.pushViewController(step3VC, animated: true)
        }
        
    }

}

extension ChildAppointmentViewController : UITextFieldDelegate{
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == textfieldPhone1 || textField == textfieldPhone2 {
            return textField.formatPhoneNumber(range, string: string)
        }
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}
