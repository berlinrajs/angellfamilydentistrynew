//
//  NewPatientViewController.swift
//   Angell Family Dentistry
//
//  Created by Leojin Bose on 2/19/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit


class ChildPatientViewController: MCViewController {
    
    var textRanges : [NSRange]! = [NSRange]()
    @IBOutlet weak var labelPatientDetails: UILabel!
    @IBOutlet weak var labelDate1: UILabel!
    @IBOutlet weak var labelDate2: UILabel!
    @IBOutlet weak var labelText2: UILabel!
    @IBOutlet weak var labelText1: UILabel!
    @IBOutlet weak var imageViewSignature1: UIImageView!
    @IBOutlet weak var imageViewSignature2: UIImageView!
    @IBOutlet weak var constraintPatientDetailsHeight: NSLayoutConstraint!
    
    @IBOutlet weak var constraintLabel2Height: NSLayoutConstraint!
    @IBOutlet weak var constraintLabel1Height: NSLayoutConstraint!
    
    @IBOutlet weak var radioHearAboutUs: RadioButton!
//    @IBOutlet weak var radioMaritalStatus: RadioButton!
//    @IBOutlet weak var buttonSubmit: PDButton!
//    @IBOutlet weak var buttonBack: PDButton!
    
    @IBOutlet weak var imageViewSignature3: UIImageView!
    @IBOutlet weak var imageViewSignature4: UIImageView!
    @IBOutlet weak var imageViewSignature5: UIImageView!
    
    @IBOutlet weak var labelDate3: UILabel!
    @IBOutlet weak var labelDate4: UILabel!
    @IBOutlet weak var labelDate5: UILabel!
    
    @IBOutlet weak var labelPhoneNumber: UILabel!
    @IBOutlet weak var buttonCall: UIButton!
    @IBOutlet weak var buttonText: UIButton!
    @IBOutlet weak var labelName2: UILabel!
    
    @IBOutlet weak var scrollView: UIScrollView!
    
    @IBOutlet weak var labelOtherDetails: UILabel!
    @IBOutlet weak var labelReferedby: UILabel!
    @IBOutlet weak var labelTitle: UILabel!
    
   
    @IBOutlet weak var labelAuthorizationName1 : UILabel!
    @IBOutlet weak var labelPhone1 : UILabel!
    @IBOutlet weak var labelAuthorizationName2 : UILabel!
    @IBOutlet weak var labelPhone2 : UILabel!
    @IBOutlet weak var radioAppointmnet1 : RadioButton!
    @IBOutlet weak var radioAppointment2 : RadioButton!
    @IBOutlet weak var radioHealth1 : RadioButton!
    @IBOutlet weak var radioHealth2 : RadioButton!
    @IBOutlet weak var radioFinancial1 : RadioButton!
    @IBOutlet weak var radioFinancial2 : RadioButton!
    @IBOutlet weak var radioTexting : RadioButton!
    @IBOutlet weak var labelHomeAddress : UILabel!
    @IBOutlet weak var labelHomePhone : UILabel!
    @IBOutlet weak var labelCellPhone : UILabel!
    @IBOutlet weak var labelEmailAddress : UILabel!


    override func viewDidLoad() {
        super.viewDidLoad()
        labelTitle.text = patient.selectedForms.first?.formTitle == kChildSignInForm ? "CHILD INFORMATION" : "PATIENT INFORMATION"
//        buttonSubmit.backgroundColor = UIColor.greenColor()
        
        var patientInfo = "Patient Name:"
        
        
        let patientName = getText(patient.fullName)
        patientInfo = patientInfo + " \(patientName)"
        textRanges.append(patientInfo.rangeOfText(patientName))
        
        patientInfo = patientInfo + " Date of Birth: \(getText(patient.dateOfBirth))"
        textRanges.append(patientInfo.rangeOfText(getText(patient.dateOfBirth)))
        let text = getText("N/A")

        if let parentFirstName = patient.parentFirstName {
            let parentName = getText("\(parentFirstName) \(patient.parentLastName!)")
            patientInfo = patientInfo + " Parent/Guardian: \(parentName)"
            textRanges.append(patientInfo.rangeOfText(parentName))
            
            patientInfo = patientInfo + " Date of Birth: \(getText(patient.parentDateOfBirth!))"
            textRanges.append(patientInfo.rangeOfText(getText(patient.parentDateOfBirth!)))
        } else {
            patientInfo = patientInfo + " Parent/Guardian: \(text)"
            textRanges.append(patientInfo.rangeOfText(text))
            
            patientInfo = patientInfo + " Date of Birth: \(text)"
            textRanges.append(patientInfo.rangeOfText(text))
        }
        
        patientInfo = patientInfo + " Preferred Name: \(getText(patient.preferredName))"
        textRanges.append(patientInfo.rangeOfText(getText(patient.preferredName)))
        
        patientInfo = patientInfo + " Address: \(getText(patient.address))"
        textRanges.append(patientInfo.rangeOfText(getText(patient.address)))
        
        patientInfo = patientInfo + " City: \(getText(patient.city))"
        textRanges.append(patientInfo.rangeOfText(getText(patient.city)))
        
        patientInfo = patientInfo + " State: \(getText(patient.state))"
        textRanges.append(patientInfo.rangeOfText(getText(patient.state)))
        
        patientInfo = patientInfo + " Zip Code: \(getText(patient.zipCode))"
        textRanges.append(patientInfo.rangeOfText(getText(patient.zipCode)))
        
        if let securityNumber = patient.socialSecurityNumber {
            patientInfo = patientInfo + " Social Security #(required for all patient's 18 and over): \(getText(securityNumber))"
            textRanges.append(patientInfo.rangeOfText(getText(securityNumber)))
        } else {
            patientInfo = patientInfo + " Social Security #(required for all patient's 18 and over): \(text)"
            textRanges.append(patientInfo.rangeOfText(text))
        }
        
        
        
        patientInfo = patientInfo + " Ph #: \(getText(patient.homePhone))"
        textRanges.append(patientInfo.rangeOfText(getText(patient.homePhone)))
        
        patientInfo = patientInfo + " Email: \(getText(patient.email))"
        textRanges.append(patientInfo.rangeOfText(getText(patient.email)))
        
        if let employer = patient.employerName {
            patientInfo = patientInfo + " Employer: \(getText(employer))"
            textRanges.append(patientInfo.rangeOfText(getText(employer)))
        } else {
            patientInfo = patientInfo + " Employer: \(text)"
            textRanges.append(patientInfo.rangeOfText(text))

        }
        
        if let employerContact = patient.employerPhoneNumber {
            patientInfo = patientInfo + " Phone Number: \(getText(employerContact))"
            textRanges.append(patientInfo.rangeOfText(getText(employerContact)))
        } else {
            patientInfo = patientInfo + " Phone Number: \(text)"
            textRanges.append(patientInfo.rangeOfText(text))
        }
        
        patientInfo = patientInfo + " Emergency Contact: \(getText(patient.emergencyContactName))"
        textRanges.append(patientInfo.rangeOfText(getText(patient.emergencyContactName)))
        
        patientInfo = patientInfo + " Phone Number: \(getText(patient.emergencyContactPhoneNumber))"
        textRanges.append(patientInfo.rangeOfText(getText(patient.emergencyContactPhoneNumber)))
        
        if let lastVisit = patient.lastDentalVisit {
            patientInfo = patientInfo + " Last Dental Visit: \(getText(lastVisit))"
            textRanges.append(patientInfo.rangeOfText(getText(lastVisit)))
        } else {
            patientInfo = patientInfo + " Last Dental Visit: \(text)"
            textRanges.append(patientInfo.rangeOfText(text))

        }
        
//        if let reason = patient.reasonForTodayVisit {
//            patientInfo = patientInfo + " Reason for today's Dental Visit: \(getText(reason))"
//            textRanges.append(patientInfo.rangeOfText(getText(reason)))
//        } else {
//            patientInfo = patientInfo + " Reason for today's Dental Visit: \(text)"
//            textRanges.append(patientInfo.rangeOfText(text))
//
//        }
        
        if patient.isHavingPain == true {
            if let painLocation = patient.painLocation {
                patientInfo = patientInfo + " Having pain? YES  If Yes where: \(getText(painLocation))"
                textRanges.append(patientInfo.rangeOfText(getText(painLocation)))
            } else {
                patientInfo = patientInfo + " Having pain? YES  If Yes where: \(text)"
                textRanges.append(patientInfo.rangeOfText(text))

            }
        } else {
            patientInfo = patientInfo + " Having pain? NO  If Yes where: \(text)"
            textRanges.append(patientInfo.rangeOfText(text))

        }
        
        if let concerns = patient.anyConcerns {
            patientInfo = patientInfo + " Any concerns: \(getText(concerns))"
            textRanges.append(patientInfo.rangeOfText(getText(concerns)))
        } else {
            patientInfo = patientInfo + " Any concerns: \(text)"
            textRanges.append(patientInfo.rangeOfText(text))

        }
        
        // Do any additional setup after loading the view.
        let attributedString = NSMutableAttributedString(string: patientInfo)
        for range in textRanges {
            attributedString.addAttribute(NSUnderlineStyleAttributeName, value: NSUnderlineStyle.styleSingle.rawValue, range: range)
        }
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineSpacing = 8
        paragraphStyle.alignment = NSTextAlignment.justified
        attributedString.addAttributes([NSBaselineOffsetAttributeName: NSNumber(value: 0 as Float), NSParagraphStyleAttributeName : paragraphStyle], range: NSMakeRange(0, attributedString.length))
        labelPatientDetails.attributedText = attributedString
        
        var rect = labelPatientDetails.frame
        labelPatientDetails.sizeToFit()
        rect.size.height = labelPatientDetails.frame.height
        labelPatientDetails.frame = rect
        
       // imageViewSignature1.image = patient.signature1
        imageViewSignature2.image = patient.signature2
       // labelDate1.text = patient.dateToday
        labelDate2.text = patient.dateToday
        labelText1.setAttributedText()
        labelText2.setAttributedText()
        
        constraintPatientDetailsHeight?.constant = labelPatientDetails.attributedText!.heightWithConstrainedWidth(screenSize.width - 60.0) * 2
        constraintLabel1Height?.constant = labelText1.attributedText!.heightWithConstrainedWidth(screenSize.width - 60.0)
        constraintLabel2Height?.constant = labelText2.attributedText!.heightWithConstrainedWidth(screenSize.width - 60.0)
        
        
        imageViewSignature3.image = patient.signature3
        imageViewSignature4.image = patient.signature4
        imageViewSignature5.image = patient.signature5
        labelDate3.text = patient.dateToday
        labelDate4.text = patient.dateToday
        labelDate5.text = patient.dateToday
        labelPhoneNumber.text = patient.phoneNumber
        if let call = patient.isCallAvail {
            buttonCall.isSelected = call
        }
        if let text = patient.isTextAvail {
            buttonText.isSelected = text
        }
//        buttonText.selected = patient.isTextAvail
       // labelName2.text = patient.firstName + " " + patient.lastName
        
        
//        radioMaritalStatus.setSelectedWithTag(patient.maritialStatus!)
        
        radioHearAboutUs.setSelectedWithTag(patient.radioHearAboutUsTag!)
        
        
        if(patient.radioHearAboutUsTag == 8){
        
            self.labelReferedby.text = patient.referredByDetails
        } else {
        
         self.labelReferedby.text = "N/A"
            
            patient.referredByDetails = nil
        
        }
        if(patient.radioHearAboutUsTag == 9) {
        
        self.labelOtherDetails.text = patient.othersDetails
        
        } else {
        
        self.labelOtherDetails.text = "N/A"
            
            patient.othersDetails = nil
        
        }
        
        labelAuthorizationName1.text = patient.appointmentName1 == "N/A" ? "N/A" : "\(patient.appointmentName1)/ \(patient.appointmentRelationship1)"
        labelPhone1.text = patient.appointmentPhone1
        labelAuthorizationName2.text =  patient.appointmentName2 == "N/A" ? "N/A" : "\(patient.appointmentName2)/ \(patient.appointmentRelationship2)"
        labelPhone2.text = patient.appointmentPhone2
        radioAppointmnet1.setSelectedWithTag(patient.appointmentTag1)
        radioAppointment2.setSelectedWithTag(patient.appointmentTag2)
        radioHealth1.setSelectedWithTag(patient.appointmentHealthTag1)
        radioHealth2.setSelectedWithTag(patient.appointmentHealthTag2)
        radioFinancial1.setSelectedWithTag(patient.appointmentFinancialTag1)
        radioFinancial2.setSelectedWithTag(patient.appointmentFinancialTag2)
        radioTexting.setSelectedWithTag(patient.textingTag)
        
        var address: [String] = [String]()
        if patient.addressLine.count > 0 {
            address.append(patient.addressLine)
        }
        if patient.city.count > 0 {
            address.append(patient.city)
        }
        if patient.state.count > 0 {
            address.append(patient.state)
        }
        if patient.zipCode.count > 0 {
            address.append(patient.zipCode)
        }
        labelHomeAddress.text = (address as NSArray).componentsJoined(by: ", ").setText()
        
        labelHomePhone.text = patient.phoneNumber
        labelCellPhone.text = patient.cellPhone
        labelEmailAddress.text = patient.email

    }
    
    @IBAction override func onSubmitButtonPressed() {
        self.buttonBack?.isUserInteractionEnabled = false
        self.buttonSubmit?.isUserInteractionEnabled = false
        
        BRProgressHUD.show()
        DispatchQueue.main.async {
            ServiceManager.sendPatientDetails(self.patient, completion: { (result, error) in
                if result {
                    super.onSubmitButtonPressed()
                } else {
                    SystemConfig.sharedConfig?.checkConnectivity(completion: { (finished, errorMessage) in
                        BRProgressHUD.hide()
                        self.buttonBack?.isUserInteractionEnabled = true
                        self.buttonSubmit?.isUserInteractionEnabled = true
                        if finished {
                            self.showAlert("Something went wrong, Please contact our support team")
                        } else {
                            self.showAlert(errorMessage!)
                        }
                    })
                }
            })
        }
    }
    
//    @IBAction func buttonActionBack(sender: AnyObject) {
//        self.navigationController?.popViewControllerAnimated(true)
//    }
//    
//    
//    
//    @IBAction func buttonActionSubmit(sender: AnyObject) {
//        
//        if !Reachability.isConnectedToNetwork() {
//            let alertController = UIAlertController(title: "Angell Family Dentistry", message: "Your device is not connected to internet. Please go to settings to connect.", preferredStyle: UIAlertControllerStyle.Alert)
//            let alertOkAction = UIAlertAction(title: "Settings", style: UIAlertActionStyle.Destructive) { (action) -> Void in
//                let settingsUrl = NSURL(string: UIApplicationOpenSettingsURLString)
//                if let url = settingsUrl {
//                    UIApplication.sharedApplication().openURL(url)
//                }
//            }
//            let alertCancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.Destructive) { (action) -> Void in
//                
//            }
//            alertController.addAction(alertOkAction)
//            alertController.addAction(alertCancelAction)
//            self.presentViewController(alertController, animated: true, completion: nil)
//            return
//        }
//        let pdfManager = PDFManager()
//        pdfManager.authorizeDrive(self.view) { (success) -> Void in
//            if success {
//                self.buttonSubmit.hidden = true
//                self.buttonBack.hidden = true
//                pdfManager.createPDFForScrollView(self.scrollView, fileName: "NEW_PATIENT", patient: self.patient, completionBlock: { (finished) -> Void in
//                    if finished {
//                        self.patient.selectedForms.removeFirst()
//                        self.patient = self.patient.patient()
//                        let formNames = (self.patient.selectedForms as NSArray).valueForKey("formTitle") as! [String]
//                       
//                         if formNames.contains(kUpdatePatientInformation) {
////                            let UpdatePatientVC = self.storyboard?.instantiateViewControllerWithIdentifier("kUpdateParentInfoVC") as! UpdateParentInfoViewController
////                            UpdatePatientVC.patient = self.patient
////                            self.navigationController?.pushViewController(UpdatePatientVC, animated: true)
//                            let addressInfoVC = self.storyboard?.instantiateViewControllerWithIdentifier("kUpdateAddressInfoVC") as! UpdateAddressInfoViewController
//                            addressInfoVC.patient = self.patient
//                            self.navigationController?.pushViewController(addressInfoVC, animated: true)
//
//                        } else if formNames.contains(kUpdateHIPAA) {
//                            let step3VC = self.storyboard?.instantiateViewControllerWithIdentifier("kUpdateHipaaAppointmentViewController") as! UpdateHipaaAppointmentViewController
//                            step3VC.patient = self.patient
//                            self.navigationController?.pushViewController(step3VC, animated: true)
//                        } else if formNames.contains(kMedicalHistoryForm) {
//                            let medicalHistoryStep1VC = self.storyboard?.instantiateViewControllerWithIdentifier("kMedicalHistoryStep1VC") as! MedicalHistoryStep1ViewController
//                            medicalHistoryStep1VC.patient = self.patient
//                            self.navigationController?.pushViewController(medicalHistoryStep1VC, animated: true)
//                         }
//                        else if formNames.contains(kPatientAuthorization) {
//                            let patientAuthorizationVC = self.storyboard?.instantiateViewControllerWithIdentifier("kPatientAuthorizationVC") as! PatientAuthorizationViewController
//                            patientAuthorizationVC.patient = self.patient
//                            self.navigationController?.pushViewController(patientAuthorizationVC, animated: true)
//                        } else if formNames.contains(kInsuranceCard) {
//                            let cardCapture = self.storyboard?.instantiateViewControllerWithIdentifier("kCardImageCaptureVC") as! CardImageCaptureVC
//                            cardCapture.patient = self.patient
//                            self.navigationController?.pushViewController(cardCapture, animated: true)
//                        } else if formNames.contains(kDrivingLicense) {
//                            let cardCapture = self.storyboard?.instantiateViewControllerWithIdentifier("kCardImageCaptureVC") as! CardImageCaptureVC
//                            cardCapture.patient = self.patient
//                            cardCapture.isDrivingLicense = true
//                            self.navigationController?.pushViewController(cardCapture, animated: true)
//                        }else if formNames.contains(kDentalImplants) {
//                            let dentalImplantVC = self.storyboard?.instantiateViewControllerWithIdentifier("kDentalImplantsVC") as! DentalImplantsViewController
//                            dentalImplantVC.patient = self.patient
//                            self.navigationController?.pushViewController(dentalImplantVC, animated: true)
//                        } else if formNames.contains(kToothExtraction) {
//                            let toothExtractionVC = self.storyboard?.instantiateViewControllerWithIdentifier("kToothExtractionVC") as! ToothExtractionViewController
//                            toothExtractionVC.patient = self.patient
//                            self.navigationController?.pushViewController(toothExtractionVC, animated: true)
//                        } else if formNames.contains(kPartialDenture) {
//                            let dentureAdjustmentVC = self.storyboard?.instantiateViewControllerWithIdentifier("kDentureAdjustmentVC") as! DentureAdjustmentViewController
//                            dentureAdjustmentVC.patient = self.patient
//                            self.navigationController?.pushViewController(dentureAdjustmentVC, animated: true)
//                        } else if formNames.contains(kInofficeWhitening) {
//                            let inOfficeWhiteningVC = self.storyboard?.instantiateViewControllerWithIdentifier("kInOfficeWhiteningVC") as! InOfficeWhiteningViewController
//                            inOfficeWhiteningVC.patient = self.patient
//                            self.navigationController?.pushViewController(inOfficeWhiteningVC, animated: true)
//                        } else if formNames.contains(kTreatmentOfChild) {
//                            let childTreatmentVC = self.storyboard?.instantiateViewControllerWithIdentifier("kChildTreatmentVC") as! ChildTreatmentViewController
//                            childTreatmentVC.patient = self.patient
//                            self.navigationController?.pushViewController(childTreatmentVC, animated: true)
//                        } else if formNames.contains(kEndodonticTreatment) {
//                            let endodonticTreatmentVC = self.storyboard?.instantiateViewControllerWithIdentifier("kEndodonticTreatmentVC") as! EndodonticTreatmentViewController
//                            endodonticTreatmentVC.patient = self.patient
//                            self.navigationController?.pushViewController(endodonticTreatmentVC, animated: true)
//                        } else if formNames.contains(kDentalCrown) {
//                            let dentalCrownVC = self.storyboard?.instantiateViewControllerWithIdentifier("kDentureCrownVC") as! DentalCrownViewController
//                            dentalCrownVC.patient = self.patient
//                            self.navigationController?.pushViewController(dentalCrownVC, animated: true)
//                            
//                        }
//                        else if formNames.contains(kOpiodForm) {
//                            let opiodVC = self.storyboard?.instantiateViewControllerWithIdentifier("kOpiodViewController") as! OpiodViewController
//                            opiodVC.patient = self.patient
//                            self.navigationController?.pushViewController(opiodVC, animated: true)
//                        }
//                        else if formNames.contains(kGeneralConsent) {
//                            let generalVC = self.storyboard?.instantiateViewControllerWithIdentifier("kGeneralConsentVC") as! GeneralConsentViewController
//                            generalVC.patient = self.patient
//                            self.navigationController?.pushViewController(generalVC, animated: true)
//                        }else if formNames.contains(kFeedbackForm) {
//                            let feedbackFormVC = self.storyboard?.instantiateViewControllerWithIdentifier("kFeedbackFormVC") as! FeedbackFormViewController
//                            self.navigationController?.pushViewController(feedbackFormVC, animated: true)
//                        }
//                            else {
//                            NSNotificationCenter.defaultCenter().postNotificationName(kFormsCompletedNotification, object: nil)
//                            self.navigationController?.popToRootViewControllerAnimated(true)
//                        }
//                    } else {
//                        self.buttonSubmit.hidden = false
//                        self.buttonBack.hidden = false
//                    }
//                })
//            }
//        }
//    }
    
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
