//
//  OpiodViewController.swift
//   Angell Family Dentistry
//
//  Created by SRS Web Solutions on 05/05/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

class OpiodViewController: MCViewController {

    @IBOutlet weak var signatureView: SignatureView!
    @IBOutlet weak var labelDate: DateLabel!
    @IBOutlet weak var labelName: UILabel!
    @IBOutlet weak var labelDetails: UILabel!
    
//    @IBOutlet weak var buttonBack: UIButton!
//    var isFromPreviousForm: Bool = true
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let form = patient.selectedForms.filter { (obj) -> Bool in
            return obj.formTitle == kOpiodForm
            }[0]
        
        if form.toothNumbers != nil {
            let string = "Please consider this information carefully before agreeing to take your \(form.toothNumbers!) prescription."
            let attString = NSMutableAttributedString(string: string)
            attString.addAttributes([NSUnderlineStyleAttributeName: 1], range: (string as NSString).range(of: form.toothNumbers!))
            labelDetails.attributedText = attString
        }
        
        labelName.text = patient.fullName
        
//        self.buttonBack.hidden = isFromPreviousForm
        // Do any additional setup after loading the view.
        labelDate.todayDate = patient.dateToday
        loadValues()
        // Do any additional setup after loading the view.
    }
    
    func loadValues() {
        //labelDate.setDate = patient.isDate1Tapped
    }
    
    func setValues() {
        patient.isDate1Tapped = labelDate.dateTapped
        patient.signature1 = signatureView.signatureImage()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func backAction(_ sender: AnyObject) {
        self.setValues()
        navigationController?.popViewController(animated: true)
    }

    @IBAction func nextAction(_ sender: AnyObject) {
        if !signatureView.isSigned() {
            self.showAlert("PLEASE SIGN THE FORM")
        } else if labelDate.text == "Tap to Date" {
            self.showAlert("PLEASE SELECT DATE")
        } else {
            self.setValues()
            let formVC = self.storyboard?.instantiateViewController(withIdentifier: "kOpiodFormViewController") as! OpiodFormViewController
            formVC.patient = self.patient
            navigationController?.pushViewController(formVC, animated: true)
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
