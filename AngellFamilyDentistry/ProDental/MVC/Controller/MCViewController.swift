//
//  MCViewController.swift
//  WestgateSmiles
//
//  Created by Leojin Bose on 2/25/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit
// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}

// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func > <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l > r
  default:
    return rhs < lhs
  }
}


let mainStoryBoard = UIStoryboard(name: "Main", bundle: Bundle.main)
let patientStoryBoard = UIStoryboard(name: "Patient", bundle: Bundle.main)
let medicalStoryBoard = UIStoryboard(name: "medicalHistory", bundle: Bundle.main)
let consentStoryBoard = UIStoryboard(name: "Consent1", bundle: Bundle.main)
let consentOldStoryBoard = UIStoryboard(name: "Consent", bundle: Bundle.main)

class MCViewController: UIViewController {
    
    @IBOutlet var buttonSubmit: MCButton?
    @IBOutlet var buttonBack: MCButton?
    
    @IBOutlet var pdfView: UIScrollView?
    
    var patient: MCPatient!
    var completion: ((_ success: Bool) -> Void)?

    override func viewDidLoad() {
        super.viewDidLoad()
        edgesForExtendedLayout = UIRectEdge()
        
        configureNavigationButtons()
        // Do any additional setup after loading the view.
    }
    
    func configureNavigationButtons() {
        self.buttonSubmit?.backgroundColor = UIColor.green
        self.buttonBack?.isHidden = self.buttonSubmit == nil && isFromPreviousForm
        
        if self.buttonBack != nil && self.buttonSubmit == nil && isFromPreviousForm {
            self.navigationController?.viewControllers.removeSubrange(1...self.navigationController!.viewControllers.index(of: self)! - 1)
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        self.report_memory()
        // Dispose of any resources that can be recreated.
    }
    
    func report_memory() {
        var taskInfo = mach_task_basic_info()
        var count = mach_msg_type_number_t(MemoryLayout<mach_task_basic_info>.size)/4
        let kerr: kern_return_t = withUnsafeMutablePointer(to: &taskInfo) {
            $0.withMemoryRebound(to: integer_t.self, capacity: 1) {
                task_info(mach_task_self_, task_flavor_t(MACH_TASK_BASIC_INFO), $0, &count)
            }
        }
        
        if kerr == KERN_SUCCESS {
            print("Memory used in bytes: \(taskInfo.resident_size)")
        }
        else {
            print("Error with task_info(): " +
                (String(cString: mach_error_string(kerr), encoding: String.Encoding.ascii) ?? "unknown error"))
        }
    }
    
    var isFromPreviousForm: Bool {
        get {
            if navigationController?.viewControllers.count > 3 && navigationController?.viewControllers[2].isKind(of: VerificationViewController.self) == true {
                navigationController?.viewControllers.remove(at: 2)
                return false
            }
            return navigationController?.viewControllers.count > 3 ? true : false
        }
    }
    
    @IBAction func buttonBackAction() {
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func onSubmitButtonPressed() {
        let pdfManager = PDFManager.sharedInstance()
        self.buttonSubmit?.isHidden = true
        self.buttonBack?.isHidden = true
        BRProgressHUD.show()
        pdfManager.uploadToGoogleDrive(self.pdfView == nil ? self.view : self.pdfView!, patient: self.patient, completionBlock: { (finished, _ errorMessage: String?) -> Void in
            if finished {
                BRProgressHUD.hide()
                if let _ = self.completion {
                    self.completion!(true)
                } else {
                    self.patient.selectedForms.removeFirst()
                    self.gotoNextForm()
                }
            } else {
                SystemConfig.sharedConfig?.checkConnectivity(completion: { (finished, errorMessage) in
                    BRProgressHUD.hide()
                    self.buttonSubmit?.isHidden = false
                    self.buttonBack?.isHidden = false
                    if finished {
                        self.showAlert("Something went wrong, Please contact our support team")
                    } else {
                        self.showAlert(errorMessage!)
                    }
                })
            }
        })
    }
    
    func gotoNextForm() {
        let formNames = (patient.selectedForms as NSArray).value(forKey: "formTitle") as! [String]
        
        if formNames.count > 0{
        
        if formNames.first! == kNewPatientSignInForm {
            let addressInfoVC = consentOldStoryBoard.instantiateViewController(withIdentifier: "kAddressInfoVC") as! AddressInfoViewController
            addressInfoVC.patient = patient
            self.navigationController?.pushViewController(addressInfoVC, animated: true)
        } else if formNames.first! == kChildSignInForm {
            let parentInfoVC = consentOldStoryBoard.instantiateViewController(withIdentifier: "kChildParentInfoVC") as! ChildParentInfoViewController
            parentInfoVC.patient = patient
            self.navigationController?.pushViewController(parentInfoVC, animated: true)
        } else if formNames.first! == kUpdatePatientInformation {
//            let UpdateparentInfoVC = consentOldStoryBoard.instantiateViewController(withIdentifier: "kUpdateParentInfoVC") as! UpdateParentInfoViewController
//            UpdateparentInfoVC.patient = patient
//            self.navigationController?.pushViewController(UpdateparentInfoVC, animated: true)
            let addressInfoVC = consentOldStoryBoard.instantiateViewController(withIdentifier: "kUpdateAddressInfoVC") as! UpdateAddressInfoViewController
            addressInfoVC.patient = patient
            self.navigationController?.pushViewController(addressInfoVC, animated: true)
        } else if formNames.first! == kUpdateHIPAA {
            let addressInfoVC = consentOldStoryBoard.instantiateViewController(withIdentifier: "kUpdateHipaaAddressInfoViewController") as! UpdateHipaaAddressInfoViewController
            addressInfoVC.patient = patient
            self.navigationController?.pushViewController(addressInfoVC, animated: true)
        } else if formNames.first! == kMedicalHistoryForm {
            let medicalHistoryStep1VC = medicalStoryBoard.instantiateViewController(withIdentifier: "kMedicalHistoryStep1VC") as! MedicalHistoryStep1ViewController
            medicalHistoryStep1VC.patient = patient
            self.navigationController?.pushViewController(medicalHistoryStep1VC, animated: true)
        } else if formNames.first! == kPatientAuthorization {
            let patientAuthorizationVC = consentOldStoryBoard.instantiateViewController(withIdentifier: "kPatientAuthorizationVC") as! PatientAuthorizationViewController
            patientAuthorizationVC.patient = patient
            self.navigationController?.pushViewController(patientAuthorizationVC, animated: true)
        }  else if formNames.first! == kInsuranceCard {
            let cardCapture = consentOldStoryBoard.instantiateViewController(withIdentifier: "kCardImageCaptureVC") as! CardImageCaptureVC
            cardCapture.patient = self.patient
            self.navigationController?.pushViewController(cardCapture, animated: true)
        } else if formNames.first! == kDrivingLicense {
            let cardCapture = consentOldStoryBoard.instantiateViewController(withIdentifier: "kCardImageCaptureVC") as! CardImageCaptureVC
            cardCapture.patient = self.patient
            cardCapture.isDrivingLicense = true
            self.navigationController?.pushViewController(cardCapture, animated: true)
        }else if formNames.first! == kDocumentScan {
            let scanDocument = mainStoryBoard.instantiateViewController(withIdentifier: "kDocumentListViewController") as! DocumentListViewController
            scanDocument.patient = self.patient
            self.navigationController?.pushViewController(scanDocument, animated: true)
        } else if formNames.first! == kDentalImplants {
            let dentalImplantVC = consentOldStoryBoard.instantiateViewController(withIdentifier: "kDentalImplantsVC") as! DentalImplantsViewController
            dentalImplantVC.patient = patient
            self.navigationController?.pushViewController(dentalImplantVC, animated: true)
        } else if formNames.first! == kToothExtraction{
            let toothExtractionVC = consentOldStoryBoard.instantiateViewController(withIdentifier: "kToothExtractionVC") as! ToothExtractionViewController
            toothExtractionVC.patient = patient
            self.navigationController?.pushViewController(toothExtractionVC, animated: true)
        } else if formNames.first! == kPartialDenture {
            let dentureAdjustmentVC = consentOldStoryBoard.instantiateViewController(withIdentifier: "kDentureAdjustmentVC") as! DentureAdjustmentViewController
            dentureAdjustmentVC.patient = patient
            self.navigationController?.pushViewController(dentureAdjustmentVC, animated: true)
        } else if formNames.first! == kInofficeWhitening{
            let inOfficeWhiteningVC = consentOldStoryBoard.instantiateViewController(withIdentifier: "kInOfficeWhiteningVC") as! InOfficeWhiteningViewController
            inOfficeWhiteningVC.patient = patient
            self.navigationController?.pushViewController(inOfficeWhiteningVC, animated: true)
        } else if formNames.first! == kTreatmentOfChild {
            let childTreatmentVC = consentOldStoryBoard.instantiateViewController(withIdentifier: "kChildTreatmentVC") as! ChildTreatmentViewController
            childTreatmentVC.patient = patient
            self.navigationController?.pushViewController(childTreatmentVC, animated: true)
        } else if formNames.first! == kEndodonticTreatment {
            let endodonticTreatmentVC = consentOldStoryBoard.instantiateViewController(withIdentifier: "kEndodonticTreatmentVC") as! EndodonticTreatmentViewController
            endodonticTreatmentVC.patient = patient
            self.navigationController?.pushViewController(endodonticTreatmentVC, animated: true)
        } else if formNames.first! == kDentalCrown {
            let dentalCrownVC = consentOldStoryBoard.instantiateViewController(withIdentifier: "kDentureCrownVC") as! DentalCrownViewController
            dentalCrownVC.patient = patient
            self.navigationController?.pushViewController(dentalCrownVC, animated: true)
        } else if formNames.first! == kOpiodForm {
            let opiodVC = consentOldStoryBoard.instantiateViewController(withIdentifier: "kOpiodViewController") as! OpiodViewController
            opiodVC.patient = self.patient
            self.navigationController?.pushViewController(opiodVC, animated: true)
        } else if formNames.first! == kGeneralConsent {
            let generalVC = consentOldStoryBoard.instantiateViewController(withIdentifier: "kGeneralConsentVC") as! GeneralConsentViewController
            generalVC.patient = self.patient
            self.navigationController?.pushViewController(generalVC, animated: true)
        }  else if formNames.count > 0 {
            if patient.selectedForms.first?.formId != nil {
                let consentVC = mainStoryBoard.instantiateViewController(withIdentifier: "kConsentVC") as! ConsentViewController
                consentVC.patient = self.patient
                self.navigationController?.pushViewController(consentVC, animated: true)
            } else {
                NotificationCenter.default.post(name: Notification.Name(rawValue: kFormsCompletedNotification), object: nil)
                _ = self.navigationController?.popToRootViewController(animated: true)
            }
//            //Consent Forms
        }
        }else {
            NotificationCenter.default.post(name: Notification.Name(rawValue: kFormsCompletedNotification), object: nil)
            _ = self.navigationController?.popToRootViewController(animated: true)
        }
    }
}
