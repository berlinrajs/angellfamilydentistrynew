//
//  PrivacyPracticesStep2VC.swift
//  MConsentOpenDental
//
//  Created by Berlin Raj on 22/05/17.
//  Copyright © 2017 SRS Web Solutions. All rights reserved.
//

import UIKit

class PrivacyPracticesStep2VC: MCViewController {

    @IBOutlet weak var labelName: UILabel!
    @IBOutlet weak var labelDetails: UILabel!
    @IBOutlet weak var labelDate: DateLabel!
    @IBOutlet weak var signatureView: SignatureView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        loadValues()
    }
    
    func loadValues() {
        labelName.text =  patient.fullName
        labelDate.todayDate = patient.dateToday
        labelDetails.text = labelDetails.text!.replacingOccurrences(of: "kPatientName", with: patient.fullName)
    }

    func saveValues() {
        self.view.endEditing(true)
    }
    
    override func buttonBackAction() {
        saveValues()
        super.buttonBackAction()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func buttonNextAction() {
        if !signatureView.isSigned() {
            self.showAlert("PLEASE SIGN THE FORM")
        } else if !labelDate.dateTapped {
            self.showAlert("PLEASE TAP TO DATE")
        } else {
            saveValues()
            
            let formVC = self.storyboard?.instantiateViewController(withIdentifier: "kPrivacyPracticesFormVC") as! PrivacyPracticesFormVC
            formVC.patient = self.patient
            formVC.privacyAcknSignature = signatureView.signatureImage()
            self.navigationController?.pushViewController(formVC, animated: true)
        }
    }
}
