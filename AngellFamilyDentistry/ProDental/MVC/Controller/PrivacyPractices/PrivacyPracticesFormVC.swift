//
//  PrivacyPracticesFormVC.swift
//  KellyZhao
//
//  Created by Berlin on 31/01/17.
//  Copyright © 2017 srs. All rights reserved.
//

import UIKit

class PrivacyPracticesFormVC: MCViewController {
    
    @IBOutlet weak var labelPatientName2: UILabel!
    @IBOutlet weak var labelTodayDate: UILabel!
    
    @IBOutlet weak var labelOfficer: UILabel!
    @IBOutlet weak var labelPhone: UILabel!
    @IBOutlet weak var labelFax: UILabel!
    @IBOutlet weak var labelEmail: UILabel!
    @IBOutlet weak var labelAddress: UILabel!
    
    var privacyAcknSignature: UIImage!
    
    @IBOutlet weak var labelName: FormLabel!
    @IBOutlet weak var labelPrintName: FormLabel!
    @IBOutlet weak var labelDate: FormLabel!
    @IBOutlet weak var signatureView: UIImageView!
    
    @IBOutlet weak var radioRefusal: RadioButton!
    @IBOutlet weak var labelRefusalOther1: UILabel!
    @IBOutlet weak var labelRefusalOther2: UILabel!
    @IBOutlet weak var labelRefusalOther3: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        loadValues()
    }
    
    func loadValues() {
        labelPatientName2.text = patient.fullName
        labelTodayDate.text = patient.dateOfBirth
        
        let appDetails = MCAppDetails.sharedAppDetails!
        
        labelOfficer.text = appDetails.clinicName
        labelPhone.text = appDetails.phoneNumber
        labelFax.text = appDetails.faxNumber
        labelEmail.text = appDetails.email
        
        labelAddress.text = appDetails.address
        
        if patient.signRefused == false {
            signatureView.image = privacyAcknSignature
            labelName.text = patient.fullName
            labelDate.text = patient.dateToday
            labelPrintName.text = patient.fullName
        } else {
            labelPrintName.text = patient.fullName
            patient.signRefusalOther.setTextForArrayOfLabels([labelRefusalOther1, labelRefusalOther2, labelRefusalOther3])
            radioRefusal.setSelectedWithTag(patient.signRefusalTag)
        }
    }
}
