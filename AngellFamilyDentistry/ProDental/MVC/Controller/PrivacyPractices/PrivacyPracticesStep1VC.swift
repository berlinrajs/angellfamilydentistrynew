//
//  PrivacyPracticesStep1VC.swift
//  KellyZhao
//
//  Created by Berlin on 31/01/17.
//  Copyright © 2017 srs. All rights reserved.
//

import UIKit

class PrivacyPracticesStep1VC: MCViewController {
    
    @IBOutlet weak var labelPatientName: UILabel!
    @IBOutlet weak var labelDOB: UILabel!
    
    @IBOutlet weak var labelOfficer: UILabel!
    @IBOutlet weak var labelPhone: UILabel!
    @IBOutlet weak var labelFax: UILabel!
    @IBOutlet weak var labelEmail: UILabel!
    @IBOutlet weak var labelAddress: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        loadValues()
    }
    
    func loadValues() {
        labelPatientName.text = patient.fullName
//        labelPatientName2.text = patient.fullName
        
//        labelDate1.todayDate = patient.dateToday
        labelDOB.text = patient.dateOfBirth
        
        let appDetails = MCAppDetails.sharedAppDetails!
        
        labelOfficer.text = appDetails.clinicName
        labelPhone.text = appDetails.phoneNumber
        labelFax.text = appDetails.faxNumber
        labelEmail.text = appDetails.email
        
        labelAddress.text = appDetails.address
    }
    
    func saveValues() {
//        patient.privacyRelationName = textFieldRelationName.text!
    }
//
//    @IBAction func buttonRefuseAction() {
//        buttonRefuse.isSelected = !buttonRefuse.isSelected
//    }
    
    override func buttonBackAction() {
        saveValues()
        super.buttonBackAction()
    }
    
    @IBAction func buttonNextAction () {
//        if !buttonRefuse.isSelected && !signatureView.isSigned() {
//            self.showAlert("PLEASE SIGN THE FORM")
//        } else {
            saveValues()
        
        HipaaSignatureRefusalPopup.popUpView().showInViewController(viewController: self, completion: { (popUpView, refused, textFieldOther, refusalTag) in
            popUpView.close()
            self.patient.signRefused = refused
            self.patient.signRefusalTag = refused == false ? 0 : refusalTag
            self.patient.signRefusalOther = refused == false ? "" : refusalTag == 4 ? textFieldOther.text! : ""
            
            self.gotoNextForm()
        }, error: { (message) in
            self.showAlert(message)
        })
//            let formVC = self.storyboard?.instantiateViewController(withIdentifier: "kPrivacyPracticesFormVC") as! PrivacyPracticesFormVC
//            formVC.patient = self.patient
//            formVC.patientSignature = signatureView.signatureImage()
//            self.navigationController?.pushViewController(formVC, animated: true)
//        }
    }
    
    override func gotoNextForm() {
        if patient.signRefused == true {
            let formVC = self.storyboard?.instantiateViewController(withIdentifier: "kPrivacyPracticesFormVC") as! PrivacyPracticesFormVC
            formVC.patient = self.patient
            self.navigationController?.pushViewController(formVC, animated: true)
        } else {
            let signRefusalVC = consentStoryBoard.instantiateViewController(withIdentifier: "kPrivacyPracticesStep2VC") as! PrivacyPracticesStep2VC
            signRefusalVC.patient = self.patient
            self.navigationController?.pushViewController(signRefusalVC, animated: true)
        }
    }
}
