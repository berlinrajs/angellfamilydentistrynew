//
//  VerificationViewController.swift
//  DiamondDentalAuto
//
//  Created by Bala Murugan on 10/6/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

class VerificationViewController: MCViewController {

    @IBOutlet weak var buttonNext: UIButton!
    @IBOutlet weak var textfieldPatientId : MCTextField!
    var manager: AFHTTPSessionManager?
    
    var completionOfVerification: ((AnyObject?) -> Void)?

    override func viewDidLoad() {
        textfieldPatientId.textFormat = .AlphaNumeric
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
       // let _ = isFromPreviousForm
    }
    
    override func buttonBackAction() {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func onNextButtonPressed (withSender sender : UIButton){
        if textfieldPatientId.isEmpty{
            self.showAlert("PLEASE ENTER THE PATIENT CHART ID")
        }else{
            BRProgressHUD.show()
            self.buttonNext.isUserInteractionEnabled = false
            
            ServiceManager.postDataToServer(baseUrlString: hostUrl, serviceName: "api/patients/FetchPatientByChartId/", parameters: ["ChartId" : self.textfieldPatientId.text!] as [String: AnyObject], success: { (result) in
                self.buttonNext.isUserInteractionEnabled = true
                BRProgressHUD.hide()
                
                if (result as AnyObject)["status"] as! String == "success" && (result as AnyObject)["message"] as! String == "Patient Found" {
                    self.completionOfVerification?(result as AnyObject)
                } else {
                    self.showAlert("NO USER FOUND")
                }
            }) { (error) in
                self.buttonNext.isUserInteractionEnabled = true
                BRProgressHUD.hide()
                self.showAlert(error.localizedDescription)
            }
        }
    }
}
