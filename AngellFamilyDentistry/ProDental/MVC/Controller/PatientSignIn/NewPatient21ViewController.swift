//
//  NewPatient21ViewController.swift
//  MConsent Dentrix
//
//  Created by Berlin Raj on 28/05/17.
//  Copyright © 2017 SRS Web Solutions. All rights reserved.
//

import UIKit

class NewPatient21ViewController: MCViewController {

    @IBOutlet weak var textfieldEmail : MCTextField!
    @IBOutlet weak var textfieldHomePhone : MCTextField!
    @IBOutlet weak var textfieldCellPhone : MCTextField!
    @IBOutlet weak var textfieldWorkPhone : MCTextField!
    @IBOutlet weak var textfieldExt : MCTextField!
    @IBOutlet weak var textfieldDrivingLicense : MCTextField!
    @IBOutlet weak var textfieldSocialSecurity : MCTextField!
    @IBOutlet weak var dropdownMaritalStatus: BRDropDown!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        textfieldEmail.textFormat = .Email
        textfieldHomePhone.textFormat = .Phone
        textfieldCellPhone.textFormat = .Phone
        textfieldWorkPhone.textFormat = .Phone
        textfieldExt.textFormat = .ExtensionCode
        textfieldDrivingLicense.textFormat = .DrivingLicense
        textfieldSocialSecurity.textFormat = .SocialSecurity
        dropdownMaritalStatus.items = ["SINGLE","MARRIED","CHILD","OTHER"]
        dropdownMaritalStatus.placeholder = "-- SELECT MARITAL STATUS * --"
        loadValue()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func loadValue (){
        textfieldExt.setSavedText(text: patient.extensionCode)
        textfieldDrivingLicense.setSavedText(text: patient.drivingLicense)
        textfieldSocialSecurity.setSavedText(text: patient.socialSecurityNumber)
        textfieldEmail.setSavedText(text: patient.email)
        textfieldHomePhone.setSavedText(text: patient.homePhone)
        textfieldCellPhone.setSavedText(text: patient.cellPhone)
        textfieldWorkPhone.setSavedText(text: patient.workPhone)
        dropdownMaritalStatus.selectedIndex = patient.maritalStatus == nil ? 0 : patient.maritalStatus
        
        if let patientDetails = patient.patientDetails {
            textfieldEmail.setSavedText(text: patient.email != nil ? patient.email : patientDetails.email)
            textfieldHomePhone.setSavedText(text: patient.homePhone != nil ? patient.homePhone : patientDetails.homePhone)
            textfieldCellPhone.setSavedText(text: patient.cellPhone != nil ? patient.cellPhone : patientDetails.cellPhone)
            textfieldWorkPhone.setSavedText(text: patient.workPhone != nil ? patient.workPhone : patientDetails.workPhone)
            textfieldDrivingLicense.setSavedText(text: patient.drivingLicense != nil ? patient.drivingLicense : patientDetails.drivingLicense)
            textfieldSocialSecurity.setSavedText(text: patient.socialSecurityNumber != nil ? patient.socialSecurityNumber : patientDetails.socialSecurityNumber)
            dropdownMaritalStatus.selectedIndex = patient.maritalStatus == nil ? (patientDetails.maritalStatus == nil ? 0 : patientDetails.maritalStatus) : patient.maritalStatus
        }
    }
    
    func saveValue (){
        patient.email = textfieldEmail.getText()
        patient.homePhone = textfieldHomePhone.getText()
        patient.cellPhone = textfieldCellPhone.getText()
        patient.workPhone = textfieldWorkPhone.getText()
        patient.extensionCode = textfieldExt.getText()
        patient.drivingLicense = textfieldDrivingLicense.getText()
        patient.socialSecurityNumber = textfieldSocialSecurity.getText()
        patient.maritalStatus = dropdownMaritalStatus.selectedIndex
    }
    
    @IBAction override func buttonBackAction() {
        saveValue()
        super.buttonBackAction()
    }
    
    @IBAction func onNextButtonPressed (withSender sender : UIButton){
        if !textfieldEmail.isEmpty && !textfieldEmail.text!.isValidEmail{
            self.showAlert("PLEASE ENTER THE VALID EMAIL")
        }else if !textfieldHomePhone.isEmpty && !textfieldHomePhone.text!.isPhoneNumber{
            self.showAlert("PLEASE ENTER THE VALID HOME PHONE NUMBER")
        }else if !textfieldWorkPhone.isEmpty && !textfieldWorkPhone.text!.isPhoneNumber{
            self.showAlert("PLEASE ENTER THE VALID WORK PHONE NUMBER")
        }else if textfieldCellPhone.isEmpty || !textfieldCellPhone.text!.isPhoneNumber{
            self.showAlert("PLEASE ENTER THE VALID CELLPHONE NUMBER")
        }else if !textfieldExt.isEmpty && !textfieldExt.text!.isValidExt{
            self.showAlert("PLEASE ENTER THE VALID EXTENSION CODE")
        } else if !textfieldSocialSecurity.isEmpty && !textfieldSocialSecurity.text!.isSocialSecurityNumber {
            self.showAlert("PLEASE ENTER A VALID SOCIAL SECURITY NUMBER")
        } else if dropdownMaritalStatus.selectedOption == nil {
            self.showAlert("PLEASE SELECT YOUR MARITAL STATUS")
        }else{
            saveValue()
            let patientSignIn = self.storyboard?.instantiateViewController(withIdentifier: "NewPatient31VC") as! NewPatient31ViewController
            patientSignIn.patient = self.patient
            self.navigationController?.pushViewController(patientSignIn, animated: true)
        }
    }
}
