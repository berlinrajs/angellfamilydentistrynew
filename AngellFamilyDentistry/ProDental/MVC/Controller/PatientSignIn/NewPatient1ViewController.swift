//
//  NewPatient1ViewController.swift
//  AnthonyParrella
//
//  Created by Bala Murugan on 2/9/17.
//  Copyright © 2017 SRS Web Solutions. All rights reserved.
//

import UIKit

class NewPatient1ViewController: MCViewController {

    @IBOutlet weak var textfieldAddress : MCTextField!
    @IBOutlet weak var textfieldState : MCTextField!
    @IBOutlet weak var textfieldCity : MCTextField!
    @IBOutlet weak var textfieldZipcode : MCTextField!
    @IBOutlet weak var radioGender : RadioButton!
    @IBOutlet weak var radioParent: RadioButton!
    
    @IBOutlet weak var textFieldFirstName: MCTextField!
    @IBOutlet weak var textFieldLastName: MCTextField!
    @IBOutlet weak var labelSubtitle: UILabel!
//    @IBOutlet weak var dropdownMaritalStatus : BRDropDown!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        textfieldState.textFormat = .State
        textfieldZipcode.textFormat = .Zipcode
        
        loadValue()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func loadValue (){
        textfieldAddress.setSavedText(text: patient.address)
        textfieldState.setSavedText(text: patient.state)
        textfieldCity.setSavedText(text: patient.city)
        textfieldZipcode.setSavedText(text: patient.zip)
        radioGender.setSelectedWithTag(patient.gender == nil ? 0 : patient.gender)
        radioParent.setSelected(patient.isParent != nil && patient.isParent == true)
        radioParentAction()
        if let patientDetails = patient.patientDetails {
            //if patientDetails.preferredName != nil{
            // }
            textfieldAddress.setSavedText(text: patient.address != nil ? patient.address : patientDetails.address)
            textfieldState.setSavedText(text: patient.state != nil ? patient.state : patientDetails.state)
            textfieldCity.setSavedText(text: patient.city != nil ? patient.city : patientDetails.city)
            textfieldZipcode.setSavedText(text: patientDetails.zipCode)
            radioGender.setSelectedWithTag(patient.gender != nil ? patient.gender : patientDetails.gender == nil ? 0 : patientDetails.gender)
        }
    }

    func saveValue (){
        patient.address = textfieldAddress.getText()
        patient.state = textfieldState.getText()
        patient.city = textfieldCity.getText()
        patient.zip = textfieldZipcode.getText()
        patient.gender = radioGender.selected == nil ? 0 : radioGender.selected.tag
        patient.isParent = radioParent.selected == nil ? false : radioParent.isSelected
        patient.parentLastName = textFieldLastName.text!
        patient.parentFirstName = textFieldFirstName.text!
    }
    
    @IBAction func radioParentAction () {
        if radioParent.isSelected {
            textFieldFirstName.text = patient.parentFirstName
            textFieldLastName.text = patient.parentLastName
            
            textFieldFirstName.isEnabled = true
            textFieldLastName.isEnabled = true
            labelSubtitle.isEnabled = true
        } else {
            textFieldFirstName.text = ""
            textFieldLastName.text = ""
            
            textFieldFirstName.isEnabled = false
            textFieldLastName.isEnabled = false
            labelSubtitle.isEnabled = false
        }
    }

    @IBAction override func buttonBackAction() {
        saveValue()
        super.buttonBackAction()
    }
    
    @IBAction func onNextButtonPressed (withSender sender : UIButton){
        if textfieldAddress.isEmpty || textfieldCity.isEmpty || textfieldState.isEmpty || textfieldZipcode.isEmpty || radioGender.selected == nil {
            self.showAlert("PLEASE ENTER ALL THE REQUIRED FIELDS")
        } else if radioParent.isSelected && (textFieldLastName.isEmpty || textFieldFirstName.isEmpty) {
            self.showAlert("PLEASE ENTER ALL THE REQUIRED FIELDS")
        } else if !textfieldZipcode.text!.isZipCode {
            self.showAlert("PLEASE ENTER THE VALID ZIPCODE")
        } else if radioParent.selected == nil {
            self.showAlert("PLEASE SELECT ARE YOU PATIENT'S PARENT OR NOT")
        } else {
            saveValue()
            if patient.isParent == true {
                let patientSignIn = self.storyboard?.instantiateViewController(withIdentifier: "NewPatient2VC") as! NewPatient2ViewController
                patientSignIn.patient = self.patient
                self.navigationController?.pushViewController(patientSignIn, animated: true)
            } else {
                let patientSignIn = self.storyboard?.instantiateViewController(withIdentifier: "NewPatient21VC") as! NewPatient21ViewController
                patientSignIn.patient = self.patient
                self.navigationController?.pushViewController(patientSignIn, animated: true)
            }

        }
    }

}
