//
//  NewPatient3ViewController.swift
//  AnthonyParrella
//
//  Created by Bala Murugan on 2/11/17.
//  Copyright © 2017 SRS Web Solutions. All rights reserved.
//

import UIKit

class NewPatient3ViewController: MCViewController {

    @IBOutlet weak var textfieldName : MCTextField!
    @IBOutlet weak var textfieldAddress : MCTextField!
    @IBOutlet weak var textFieldCity: MCTextField!
    @IBOutlet weak var textFieldState: MCTextField!
    @IBOutlet weak var textFieldZip: MCTextField!
    @IBOutlet weak var textfieldOccupation : MCTextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        textFieldState.textFormat = .State
        textFieldZip.textFormat = .Zipcode
        
        loadValue()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    func loadValue (){
        textfieldName.setSavedText(text: patient.employer)
        textfieldAddress.setSavedText(text: patient.employerAddress)
        textfieldOccupation.setSavedText(text: patient.occupation)
        textFieldCity.setSavedText(text: patient.employerCity)
        textFieldState.setSavedText(text: patient.employerState)
        textFieldZip.setSavedText(text: patient.employerZip)
    }
    
    func saveValue (){
        patient.employer = textfieldName.text!
        patient.employerAddress = textfieldAddress.text!
        patient.occupation = textfieldOccupation.text!
        patient.employerState = textFieldState.text!
        patient.employerZip = textFieldZip.text!
        patient.employerCity = textFieldCity.text!
    }
    
    @IBAction override func buttonBackAction() {
        saveValue()
        super.buttonBackAction()
    }
    
    @IBAction func onNextButtonPressed (withSender sender : UIButton){
        if !textFieldZip.isEmpty && !textFieldZip.text!.isZipCode {
            self.showAlert("PLEASE ENTER A VALID ZIP CODE")
        } else {
            saveValue()
            if patient.maritalStatus == 2 {
                let patientSignIn = self.storyboard?.instantiateViewController(withIdentifier: "NewPatient5VC") as! NewPatient5ViewController
                patientSignIn.patient = self.patient
                self.navigationController?.pushViewController(patientSignIn, animated: true)
            } else {
                let patientSignIn = self.storyboard?.instantiateViewController(withIdentifier: "NewPatient6VC") as! NewPatient6ViewController
                patientSignIn.patient = self.patient
                self.navigationController?.pushViewController(patientSignIn, animated: true)
            }
        }
    }
}
