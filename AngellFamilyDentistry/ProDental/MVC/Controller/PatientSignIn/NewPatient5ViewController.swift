//
//  NewPatient5ViewController.swift
//  AnthonyParrella
//
//  Created by Bala Murugan on 2/11/17.
//  Copyright © 2017 SRS Web Solutions. All rights reserved.
//

import UIKit

class NewPatient5ViewController: MCViewController {

    @IBOutlet weak var textfieldName : MCTextField!
    @IBOutlet weak var textfieldEmployer : MCTextField!
    @IBOutlet weak var textfieldWorkPhone : MCTextField!
    @IBOutlet weak var textfieldExtension : MCTextField!
    @IBOutlet weak var textfieldSocialSecurity : MCTextField!
    @IBOutlet weak var textfieldBirthdate : MCTextField!
    @IBOutlet weak var textfieldDrivingLicense : MCTextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        textfieldWorkPhone.textFormat = .Phone
        textfieldExtension.textFormat = .ExtensionCode
        textfieldSocialSecurity.textFormat = .SocialSecurity
        textfieldBirthdate.textFormat = .DateIn1980
        textfieldDrivingLicense.textFormat = .AlphaNumeric
        loadValue()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    func loadValue (){
        textfieldName.setSavedText(text: patient.spouseName)
        textfieldEmployer.setSavedText(text: patient.spouseEmployer)
        textfieldWorkPhone.setSavedText(text: patient.spouseWorkPhone)
        textfieldExtension.setSavedText(text: patient.spouseExtension)
        textfieldSocialSecurity.setSavedText(text: patient.spouseSocialSecurity)
        textfieldBirthdate.setSavedText(text: patient.spouseBirthDate)
        textfieldDrivingLicense.setSavedText(text: patient.spouseDrivingLicense)

    }
    
    func saveValue (){
        patient.spouseName = textfieldName.getText()
        patient.spouseEmployer = textfieldEmployer.getText()
        patient.spouseWorkPhone = textfieldWorkPhone.getText()
        patient.spouseExtension = textfieldExtension.getText()
        patient.spouseSocialSecurity = textfieldSocialSecurity.getText()
        patient.spouseBirthDate = textfieldBirthdate.getText()
        patient.spouseDrivingLicense = textfieldDrivingLicense.getText()

    }
    
    @IBAction override func buttonBackAction() {
        saveValue()
        super.buttonBackAction()
    }
    
    @IBAction func onNextButtonPressed (withSender sender : UIButton){
        if !textfieldWorkPhone.isEmpty && !textfieldWorkPhone.text!.isPhoneNumber{
            self.showAlert("PLEASE ENTER THE VALID WORK PHONE NUMBER")
        }else if !textfieldExtension.isEmpty && !textfieldExtension.text!.isValidExt{
            self.showAlert("PLEASE ENTER THE VALID EXTENSION CODE")
        }else if !textfieldSocialSecurity.isEmpty && !textfieldSocialSecurity.text!.isSocialSecurityNumber{
            self.showAlert("PLEASE ENTER THE VALID SOCIAL SECURITY NUMBER")
        }else{
        saveValue()
            let patientSignIn = self.storyboard?.instantiateViewController(withIdentifier: "NewPatient6VC") as! NewPatient6ViewController
            patientSignIn.patient = self.patient
            self.navigationController?.pushViewController(patientSignIn, animated: true)
        }
    }
}
