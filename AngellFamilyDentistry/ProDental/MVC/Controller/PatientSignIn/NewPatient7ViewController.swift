//
//  NewPatient7ViewController.swift
//  AnthonyParrella
//
//  Created by Bala Murugan on 2/11/17.
//  Copyright © 2017 SRS Web Solutions. All rights reserved.
//

import UIKit

class NewPatient7ViewController: MCViewController {
    
    var isPrimaryInsurance : Bool!
    
    @IBOutlet weak var dropDownRelation : BRDropDown!
    @IBOutlet weak var textfieldName : MCTextField!
    @IBOutlet weak var textfieldBirthdate : MCTextField!
    @IBOutlet weak var textfieldInsuredId : MCTextField!
    @IBOutlet weak var textfieldEmployer : MCTextField!
    @IBOutlet weak var textfieldCompanyName : MCTextField!
    @IBOutlet weak var textfieldCompanyAddress : MCTextField!
    @IBOutlet weak var textfieldCompanyPhone : MCTextField!
    @IBOutlet weak var textfieldGroupNumber : MCTextField!
    @IBOutlet weak var labelTitle : UILabel!

    override func viewDidLoad() {
        super.viewDidLoad()
        dropDownRelation.items = ["SELF","SPOUSE","PARENT","GUARDIAN","OTHER"]
        dropDownRelation.placeholder = "-- RELATIONSHIP TO PATIENT --"
        dropDownRelation.delegate = self
        textfieldBirthdate.textFormat = .DateIn1980
        textfieldInsuredId.textFormat = .AlphaNumeric
        textfieldCompanyPhone.textFormat = .Phone
        textfieldGroupNumber.textFormat = .AlphaNumeric
        labelTitle.text = isPrimaryInsurance == true ? "PRIMARY DENTAL INSURANCE" : "SECONDARY DENTAL INSURANCE"
        loadValue()

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func loadValue (){
        let insurance : Insurance = isPrimaryInsurance == true ? patient.primaryInsurance : patient.secondaryInsurance
        textfieldBirthdate.setSavedText(text: insurance.insuredBirthdate)
        textfieldEmployer.setSavedText(text: insurance.insuredEmployer)
        textfieldCompanyAddress.setSavedText(text: insurance.companyAddress)
        textfieldCompanyPhone.setSavedText(text: insurance.companyPhone)
        dropDownRelation.selectedIndex = insurance.relationTag
        
        textfieldName.setSavedText(text: insurance.insuredName)
        textfieldCompanyName.setSavedText(text: insurance.companyName)
        textfieldInsuredId.setSavedText(text: insurance.insuredId)
        textfieldGroupNumber.setSavedText(text: insurance.groupNumber)
        
        if let patientDetails = patient.patientDetails {
            if let insuranceAuto = isPrimaryInsurance == true ? patientDetails.primaryDentalInsurance : patientDetails.secondaryDentalInsurance {
                textfieldName.setSavedText(text: insurance.insuredName != nil ? insurance.insuredName : insuranceAuto.name)
                textfieldCompanyName.setSavedText(text: insurance.companyName != nil ? insurance.companyName : insuranceAuto.companyName)
                textfieldInsuredId.setSavedText(text: insurance.insuredId != nil ? insurance.insuredId : insuranceAuto.insuredID)
                textfieldGroupNumber.setSavedText(text: insurance.groupNumber != nil ? insurance.groupNumber : insuranceAuto.groupNumber)
                dropDownRelation.selectedIndex = insuranceAuto.relation == nil ? 0 : insuranceAuto.relation!.index
            }
        }
    }
    
    func saveValue (){
        let insurance : Insurance = isPrimaryInsurance == true ? patient.primaryInsurance : patient.secondaryInsurance

        insurance.insuredName = textfieldName.getText()
        insurance.insuredBirthdate = textfieldBirthdate.getText()
        insurance.insuredId = textfieldInsuredId.getText()
        insurance.insuredEmployer = textfieldEmployer.getText()
        insurance.companyName = textfieldCompanyName.getText()
        insurance.companyAddress = textfieldCompanyAddress.getText()
        insurance.companyPhone = textfieldCompanyPhone.getText()
        insurance.groupNumber = textfieldGroupNumber.getText()
        insurance.relationTag = dropDownRelation.selectedOption == nil ? 0 : dropDownRelation.selectedIndex
    }
    
    @IBAction override func buttonBackAction() {
        saveValue()
        super.buttonBackAction()
    }
    
    @IBAction func onNextButtonPressed (withSender sender : UIButton){
        if !textfieldCompanyPhone.isEmpty && !textfieldCompanyPhone.text!.isPhoneNumber{
            self.showAlert("PLEASE ENTER THE VALID COMPANY PHONE NUMBER")
        }else{
            saveValue()
            if isPrimaryInsurance == true{
//            CustomAlert.yesOrNoAlertView().showWithQuestion("DO YOU HAVE SECONDARY DENTAL INSURANCE?", completion: { (index) in
                if patient.haveSecondaryInsurance {
                    let patientSignIn = self.storyboard?.instantiateViewController(withIdentifier: "NewPatient7VC") as! NewPatient7ViewController
                    patientSignIn.isPrimaryInsurance = false
                    patientSignIn.patient = self.patient
                    self.navigationController?.pushViewController(patientSignIn, animated: true)
                }else{
                    let patientSignIn = self.storyboard?.instantiateViewController(withIdentifier: "NewPatient8VC") as! NewPatient8ViewController
                    patientSignIn.patient = self.patient
                    self.navigationController?.pushViewController(patientSignIn, animated: true)
                }
//            })
            }else{
                let patientSignIn = self.storyboard?.instantiateViewController(withIdentifier: "NewPatient8VC") as! NewPatient8ViewController
                patientSignIn.patient = self.patient
                self.navigationController?.pushViewController(patientSignIn, animated: true)
            }
        }
    }
    
}

extension NewPatient7ViewController : BRDropDownDelegate{
    func dropDown(_ dropDown: BRDropDown, selectedAtIndex index: Int, selectedOption option: String?) {
        let insurance : Insurance = isPrimaryInsurance == true ? patient.primaryInsurance : patient.secondaryInsurance

        if index == 1 {
            textfieldName.setSavedText(text: patient.isParent == true ? patient.parentFirstName + " " + patient.parentLastName : patient.fullName)
            textfieldBirthdate.setSavedText(text: patient.isParent == true ? "" : patient.dateOfBirth)
            textfieldEmployer.setSavedText(text: patient.employer)
            
        }else if index == 2 {
            textfieldName.setSavedText(text: patient.spouseName)
            textfieldBirthdate.setSavedText(text: patient.spouseBirthDate)
            textfieldEmployer.setSavedText(text: patient.spouseEmployer)
        }else{
            textfieldName.setSavedText(text: "")
            textfieldBirthdate.setSavedText(text: "")
            textfieldEmployer.setSavedText(text: "")
        }
        
        if index == 5{
            PopupTextField.popUpView().showInViewController(self, WithTitle: "RELATIONSHIP TO PATIENT", placeHolder: "PLEASE SPECIFY", textFormat: TextFormat.Default, completion: { (popUp, textField) in
                if textField.isEmpty{
                    insurance.relation = "N/A"
                    dropDown.reset()
                }else{
                    insurance.relation = textField.text!
                }
                popUp.close()
            })
            
        }else{
            insurance.relation = option!
        }
    }
}
