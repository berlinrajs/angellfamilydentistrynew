//
//  NewPatient6ViewController.swift
//  AnthonyParrella
//
//  Created by Bala Murugan on 2/11/17.
//  Copyright © 2017 SRS Web Solutions. All rights reserved.
//

import UIKit

class NewPatient6ViewController: MCViewController {

    @IBOutlet weak var textFieldName : MCTextField!
    @IBOutlet weak var dropDownRelationship : BRDropDown!
    @IBOutlet weak var textfieldWorkPhone : MCTextField!
    @IBOutlet weak var textfieldHomePhone : MCTextField!
    @IBOutlet weak var textfieldExtension : MCTextField!
    @IBOutlet weak var textfieldBillingAddress : MCTextField!
    @IBOutlet weak var textfieldSocialSecurity : MCTextField!
    @IBOutlet weak var textfieldEmployer : MCTextField!
    @IBOutlet weak var textfieldDrivingLicense : MCTextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        dropDownRelationship.items = ["SELF","SPOUSE","PARENT","GUARDIAN","OTHER"]
        dropDownRelationship.placeholder = "-- RELATIONSHIP TO PATIENT --"
        dropDownRelationship.delegate = self
        textfieldWorkPhone.textFormat = .Phone
        textfieldHomePhone.textFormat = .Phone
        textfieldExtension.textFormat = .ExtensionCode
        textfieldSocialSecurity.textFormat = .SocialSecurity
        textfieldDrivingLicense.textFormat = .DrivingLicense
        loadValue()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func loadValue (){
        textFieldName.setSavedText(text: patient.responsibleName)
        textfieldWorkPhone.setSavedText(text: patient.responsibleWorkPhone)
        textfieldHomePhone.setSavedText(text: patient.responsibleHomePhone)
        textfieldExtension.setSavedText(text: patient.responsibleExtension)
        textfieldBillingAddress.setSavedText(text: patient.responsibleBillingAddress)
        textfieldSocialSecurity.setSavedText(text: patient.responsibleSocialSecurity)
        textfieldEmployer.setSavedText(text: patient.responsibleEmployer)
        textfieldDrivingLicense.setSavedText(text: patient.responsibleDrivingLicense)
        dropDownRelationship.selectedIndex = patient.responsibleRelationTag
        
    }
    
    func saveValue (){
        patient.responsibleName = textFieldName.getText()
        patient.responsibleWorkPhone = textfieldWorkPhone.getText()
        patient.responsibleHomePhone = textfieldHomePhone.getText()
        patient.responsibleExtension = textfieldExtension.getText()
        patient.responsibleBillingAddress = textfieldBillingAddress.getText()
        patient.responsibleSocialSecurity = textfieldSocialSecurity.getText()
        patient.responsibleEmployer = textfieldEmployer.getText()
        patient.responsibleDrivingLicense = textfieldDrivingLicense.getText()
        patient.responsibleRelationTag = dropDownRelationship.selectedOption == nil ? 0 : dropDownRelationship.selectedIndex
    }
    
    @IBAction override func buttonBackAction() {
        saveValue()
        super.buttonBackAction()
    }
    
    @IBAction func onNextButtonPressed (withSender sender : UIButton){
        if !textfieldWorkPhone.isEmpty && !textfieldWorkPhone.text!.isPhoneNumber{
            self.showAlert("PLEASE ENTER THE VALID WORK PHONE NUMBER")
        }else if !textfieldExtension.isEmpty && !textfieldExtension.text!.isValidExt{
            self.showAlert("PLEASE ENTER THE VALID EXTENSION CODE")
        }else if !textfieldHomePhone.isEmpty && !textfieldHomePhone.text!.isPhoneNumber{
            self.showAlert("PLEASE ENTER THE VALID HOME PHONE NUMBER")
        }else if !textfieldSocialSecurity.isEmpty && !textfieldSocialSecurity.text!.isSocialSecurityNumber{
            self.showAlert("PLEASE ENTER THE VALID SOCIAL SECURITY NUMBER")
        }else{
            saveValue()
//            CustomAlert.yesOrNoAlertView().showWithQuestion("DO YOU HAVE PRIMARY DENTAL INSURANCE?", completion: { (index) in
                if patient.havePrimaryInsurance{
                    let patientSignIn = self.storyboard?.instantiateViewController(withIdentifier: "NewPatient7VC") as! NewPatient7ViewController
                    patientSignIn.isPrimaryInsurance = true
                    patientSignIn.patient = self.patient
                    self.navigationController?.pushViewController(patientSignIn, animated: true)
                }
                else {
                    let patientSignIn = self.storyboard?.instantiateViewController(withIdentifier: "NewPatient8VC") as! NewPatient8ViewController
                    patientSignIn.patient = self.patient
                    self.navigationController?.pushViewController(patientSignIn, animated: true)
                }
                
//            })
        }
    }
}

extension NewPatient6ViewController : BRDropDownDelegate{
    func dropDown(_ dropDown: BRDropDown, selectedAtIndex index: Int, selectedOption option: String?) {
        if index == 1{
            textFieldName.setSavedText(text: patient.isParent == true ? patient.parentFirstName + " " + patient.parentLastName : patient.fullName)
            textfieldWorkPhone.setSavedText(text: patient.workPhone)
            textfieldHomePhone.setSavedText(text: patient.homePhone)
            textfieldExtension.setSavedText(text: patient.extensionCode)
            textfieldBillingAddress.setSavedText(text: patient.address)
            textfieldSocialSecurity.setSavedText(text: patient.socialSecurityNumber)
            textfieldDrivingLicense.setSavedText(text: patient.drivingLicense)
            textfieldEmployer.setSavedText(text: patient.employer)

            
        }else if index == 2{
            textFieldName.setSavedText(text: patient.spouseName)
            textfieldWorkPhone.setSavedText(text: patient.spouseWorkPhone)
            textfieldHomePhone.setSavedText(text: "")
            textfieldExtension.setSavedText(text: patient.spouseExtension)
            textfieldSocialSecurity.setSavedText(text: patient.spouseSocialSecurity)
            textfieldDrivingLicense.setSavedText(text: patient.spouseDrivingLicense)
            textfieldEmployer.setSavedText(text: patient.spouseEmployer)
            textfieldBillingAddress.setSavedText(text: "")
        }else{
            textFieldName.setSavedText(text: "")
            textfieldWorkPhone.setSavedText(text: "")
            textfieldHomePhone.setSavedText(text: "")
            textfieldExtension.setSavedText(text: "")
            textfieldBillingAddress.setSavedText(text: "")
            textfieldSocialSecurity.setSavedText(text: "")
            textfieldDrivingLicense.setSavedText(text: "")
            textfieldEmployer.setSavedText(text: "")
        }
        
        if index == 5{
            PopupTextField.popUpView().showInViewController(self, WithTitle: "RELATIONSHIP TO PATIENT", placeHolder: "PLEASE SPECIFY", textFormat: TextFormat.Default, completion: { (popUp, textField) in
                if textField.isEmpty{
                    self.patient.responsibleRelation = "N/A"
                    dropDown.reset()
                }else{
                    self.patient.responsibleRelation = textField.text!
                }
                popUp.close()
            })
            
        }else{
            patient.responsibleRelation = option!
        }
    }
}
