//
//  NewPatient8ViewController.swift
//  AnthonyParrella
//
//  Created by Bala Murugan on 2/11/17.
//  Copyright © 2017 SRS Web Solutions. All rights reserved.
//

import UIKit

class NewPatient8ViewController: MCViewController {

    @IBOutlet weak var textfieldName : MCTextField!
    @IBOutlet weak var textfieldRelation : MCTextField!
    @IBOutlet weak var textfieldWorkPhone : MCTextField!
    @IBOutlet weak var textfieldHomePhone : MCTextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        textfieldHomePhone.textFormat = .Phone
        textfieldWorkPhone.textFormat = .Phone
        loadValue()

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func loadValue (){
        textfieldName.setSavedText(text: patient.nearByName)
        textfieldRelation.setSavedText(text: patient.nearByRelation)
        textfieldWorkPhone.setSavedText(text: patient.nearByWorkPhone)
        textfieldHomePhone.setSavedText(text: patient.nearByHomePhone)
    }
    
    func saveValue (){
        patient.nearByName = textfieldName.getText()
        patient.nearByRelation = textfieldRelation.getText()
        patient.nearByWorkPhone = textfieldWorkPhone.getText()
        patient.nearByHomePhone = textfieldHomePhone.getText()
    }
    
    @IBAction override func buttonBackAction() {
        saveValue()
        super.buttonBackAction()
    }
    
    @IBAction func onNextButtonPressed (withSender sender : UIButton){
        if !textfieldWorkPhone.isEmpty && !textfieldWorkPhone.text!.isPhoneNumber{
            self.showAlert("PLEASE ENTER THE VALID WORK PHONE NUMBER")
        }else if !textfieldHomePhone.isEmpty && !textfieldHomePhone.text!.isPhoneNumber{
            self.showAlert("PLEASE ENTER THE VALID HOME PHONE NUMBER")
        }else{
            saveValue()
            let patientSignIn = self.storyboard?.instantiateViewController(withIdentifier: "NewPatient4VC") as! NewPatient4ViewController
            patientSignIn.patient = self.patient
            self.navigationController?.pushViewController(patientSignIn, animated: true)
        }
    }

}
