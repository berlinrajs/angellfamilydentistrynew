//
//  NewPatient4ViewController.swift
//  AnthonyParrella
//
//  Created by Bala Murugan on 2/11/17.
//  Copyright © 2017 SRS Web Solutions. All rights reserved.
//

import UIKit

class NewPatient4ViewController: MCViewController {
    
    @IBOutlet weak var textFieldReference : MCTextField!
    @IBOutlet weak var textFieldFamilyMembers : MCTextField!
    @IBOutlet weak var textfieldPreviousDentist : MCTextField!
    @IBOutlet weak var textfieldLastVisit : MCTextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        textfieldLastVisit.textFormat = .DateInCurrentYear
        loadValue()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        (textfieldLastVisit.inputView as! DateInputView).datePicker.maximumDate = Date()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func loadValue (){
        textFieldReference.setSavedText(text: patient.reference)
        textFieldFamilyMembers.setSavedText(text: patient.familyMembers)
        textfieldPreviousDentist.setSavedText(text: patient.previousDentist)
        textfieldLastVisit.setSavedText(text: patient.lastVisit)
    }
    
    func saveValue (){
        patient.reference = textFieldReference.getText()
        patient.familyMembers = textFieldFamilyMembers.getText()
        patient.previousDentist = textfieldPreviousDentist.getText()
        patient.lastVisit = textfieldLastVisit.getText()
    }
    
    @IBAction override func buttonBackAction() {
        saveValue()
        super.buttonBackAction()
    }
    
    @IBAction func onNextButtonPressed (withSender sender : UIButton){
        saveValue()
        let patientSignIn = self.storyboard?.instantiateViewController(withIdentifier: "NewPatient9VC") as! NewPatient9ViewController
        patientSignIn.patient = self.patient
        self.navigationController?.pushViewController(patientSignIn, animated: true)
    }
}
