//
//  NewPatient101ViewController.swift
//  MConsent Dentrix
//
//  Created by Berlin Raj on 26/05/17.
//  Copyright © 2017 SRS Web Solutions. All rights reserved.
//

import UIKit

class NewPatient101ViewController: MCViewController {

    @IBOutlet var tableViewQuestions: UITableView!
    @IBOutlet var buttonVerified: UIButton!

    var selectedIndex: Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if arrayAllergy.count == 0 {
            fetchAllergies()
        } else {
            self.tableViewQuestions.reloadData()
        }
    }
    
    var arrayAllergy: [Allergy] {
        get {
            if patient.medicalHistory.allergies == nil {
                patient.medicalHistory.allergies = [Allergy]()
            }
            return patient.medicalHistory.allergies
        } set {
            self.patient.medicalHistory.allergies = newValue
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func fetchAllergies() {
        BRProgressHUD.show()
        
        Allergy.getAllergyList({ (result) in
            BRProgressHUD.hide()
            self.arrayAllergy = result
            if let patientDetails = self.patient.patientDetails {
                for allergy in self.arrayAllergy {
                    allergy.isSelected = patientDetails.existingAlertIds.contains(allergy.allergyId)
                }
            }
            self.tableViewQuestions.reloadData()
        }) { (error) in
            SystemConfig.sharedConfig?.checkConnectivity(completion: { (finished, errorMessage) in
                BRProgressHUD.hide()
                self.arrayAllergy.removeAll()
                self.tableViewQuestions.reloadData()
                if finished {
                    self.showAlert("Something went wrong, Please contact our support team")
                } else {
                    self.showAlert(errorMessage!)
                }
            })
        }
    }
    
    @IBAction func buttonVerifiedAction(withSender sender: UIButton) {
        sender.isSelected = !sender.isSelected
    }
    
    @IBAction func onNextButtonPressed (withSender sender : UIButton){
        if buttonVerified.isSelected == false {
            self.showAlert("PLEASE CONFIRM THAT YOU HAVE ANSWERED ABOVE QUESTIONS")
        } else {
            if arrayAllergy.count <= (selectedIndex + 1) * 15 {
                let patientSignIn = self.storyboard?.instantiateViewController(withIdentifier: "NewPatient10VC") as! NewPatient10ViewController
                patientSignIn.patient = self.patient
                patientSignIn.selectedIndex = 1
                self.navigationController?.pushViewController(patientSignIn, animated: true)
                
            } else {
                let patientSignIn = self.storyboard?.instantiateViewController(withIdentifier: "NewPatient101VC") as! NewPatient101ViewController
                patientSignIn.patient = self.patient
                patientSignIn.selectedIndex = self.selectedIndex + 1
                self.navigationController?.pushViewController(patientSignIn, animated: true)
            }
        }
    }
}

extension NewPatient101ViewController : UITableViewDataSource,UITableViewDelegate {
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayAllergy.count - (selectedIndex * 15) > 14 ? 15 : arrayAllergy.count - (selectedIndex * 15)
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 42
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! AllergyTableViewCell
        let indexValue = (selectedIndex * 15) + indexPath.row
        cell.delegate = self
        cell.configureCell(self.arrayAllergy[indexValue], patnt: self.patient)
        
        return cell
    }
}

extension NewPatient101ViewController : PatientInfoCellDelegate{
    
    func radioButtonTappedForCell(cell: UITableViewCell) {
        let allergy = (cell as! AllergyTableViewCell).allergy!
        if allergy.isSelected {
            let placeHolder = "PLEASE EXPLAIN"
            PopupTextField.popUpView().showInViewController(self, WithTitle: "", placeHolder: placeHolder, textFormat: TextFormat.Default) { (popUpView, textField) in
                if textField.isEmpty{
                    allergy.allergyDescription = "yes"
                }else{
                    allergy.allergyDescription = textField.text!
                }
                popUpView.close()
            }
        } else {
            allergy.allergyDescription = "no"
        }
    }
}
