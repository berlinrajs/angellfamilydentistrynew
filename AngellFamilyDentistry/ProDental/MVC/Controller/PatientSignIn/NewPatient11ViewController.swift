//
//  NewPatient11ViewController.swift
//  AnthonyParrella
//
//  Created by Bala Murugan on 2/13/17.
//  Copyright © 2017 SRS Web Solutions. All rights reserved...
//

import UIKit

class NewPatient11ViewController: MCViewController {

    @IBOutlet weak var textviewVisit : MCTextView!
    @IBOutlet weak var radioDentalHealth : RadioButton!
    @IBOutlet weak var radioBristles : RadioButton!
    @IBOutlet weak var dropdownBrush : BRDropDown!
    @IBOutlet weak var dropdownFloss : BRDropDown!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        dropdownBrush.items = ["ONCE A DAY","TWICE A DAY","THRICE A DAY","NEVER"]
        dropdownBrush.placeholder = "-- PLEASE SELECT --"
        dropdownFloss.items = ["ONCE A WEEK","TWICE A WEEK","THRICE A WEEK","DAILY","NEVER"]
        dropdownFloss.placeholder = "-- PLEASE SELECT --"
        loadValue()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func loadValue (){
        textviewVisit.textValue = patient.reasonTodayVisit == nil || patient.reasonTodayVisit == "N/A" ? "" : patient.reasonTodayVisit
        radioDentalHealth.setSelectedWithTag(patient.dentalHealthTag)
        radioBristles.setSelectedWithTag(patient.bristlesTag)
        dropdownBrush.selectedIndex = patient.brushTag
        dropdownFloss.selectedIndex = patient.flossTag
    }
    
    func saveValue (){
        patient.reasonTodayVisit = textviewVisit.isEmpty ? "N/A" : textviewVisit.text!
        patient.dentalHealthTag = radioDentalHealth.selected == nil ? 0 : radioDentalHealth.selected.tag
        patient.bristlesTag = radioBristles.selected == nil ? 0 : radioBristles.selected.tag
        patient.brushTag = dropdownBrush.selectedOption == nil ? 0 : dropdownBrush.selectedIndex
        patient.brush = dropdownBrush.selectedOption == nil ? "N/A" : dropdownBrush.selectedOption!
        patient.flossTag = dropdownFloss.selectedIndex
        patient.floss = dropdownFloss.selectedOption == nil ? "N/A" : dropdownFloss.selectedOption!
    }
    
    @IBAction override func buttonBackAction() {
        saveValue()
        super.buttonBackAction()
    }
    
    @IBAction func onNextButtonPressed (withSender sender : UIButton){
//        if textviewVisit.isEmpty || radioDentalHealth.selected == nil || dropdownBrush.selectedOption == nil || dropdownFloss.selectedOption == nil{
//            self.showAlert("PLEASE ENTER ALL THE REQUIRED FIELDS")
//        }else{
            saveValue()
            let patientSignIn = self.storyboard?.instantiateViewController(withIdentifier: "NewPatient12VC") as! NewPatient12ViewController
            patientSignIn.patient = self.patient
            self.navigationController?.pushViewController(patientSignIn, animated: true)
//        }
    }

}
