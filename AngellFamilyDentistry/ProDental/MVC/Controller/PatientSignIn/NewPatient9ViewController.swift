//
//  NewPatient9ViewController.swift
//  AnthonyParrella
//
//  Created by Bala Murugan on 2/11/17.
//  Copyright © 2017 SRS Web Solutions. All rights reserved.
//

import UIKit

class NewPatient9ViewController: MCViewController {
    
    @IBOutlet weak var radioPhysician : RadioButton!
    @IBOutlet weak var viewPhysician : UIView!
    @IBOutlet weak var textfieldPhysicianName : MCTextField!
    @IBOutlet weak var textfieldPhysicianPhone : MCTextField!
    @IBOutlet weak var textfieldLastVisit : MCTextField!
    @IBOutlet weak var radioPhysicalHealth : RadioButton!
    @IBOutlet weak var viewWomen : UIView!
    @IBOutlet weak var radioBirthControl : RadioButton!
    @IBOutlet weak var radioPregnant : RadioButton!
    @IBOutlet weak var radioNursing : RadioButton!

    override func viewDidLoad() {
        super.viewDidLoad()
        textfieldPhysicianPhone.textFormat = .Phone
        textfieldLastVisit.textFormat = .DateInCurrentYear
        loadValue()

    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        (textfieldLastVisit.inputView as! DateInputView).datePicker.maximumDate = Date()
    }


    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func onPersonalPhysicianButtonPressed (withSender sender : UIButton){
        if sender.tag == 1{
            viewPhysician.isUserInteractionEnabled = true
            viewPhysician.alpha = 1.0
        }else{
            viewPhysician.isUserInteractionEnabled = false
            viewPhysician.alpha = 0.5
            textfieldPhysicianName.text = ""
            textfieldPhysicianPhone.text = ""
            textfieldLastVisit.text = ""
        }
    }
    
    @IBAction func onPregnantButtonPressed (withSender sender : RadioButton)
    {
        if sender.tag == 1{
            PopupTextField.popUpView().showInViewController(self, WithTitle: "NUMBER OF WEEKS?", placeHolder: "TYPE HERE", textFormat: TextFormat.Number, completion: { (popUp, textField) in
                if textField.isEmpty{
                    self.patient.numberOfWeeks = "N/A"
                    sender.setSelectedWithTag(2)
                }else{
                    self.patient.numberOfWeeks = textField.text!
                }
                popUp.close()
            })
        }else{
            self.patient.numberOfWeeks = "N/A"

        }
    }
    
    
    func loadValue (){
        radioPhysician.setSelectedWithTag(patient.personalPhysicianTag)
        if patient.personalPhysicianTag == 1{
            viewPhysician.isUserInteractionEnabled = true
            viewPhysician.alpha = 1.0
            textfieldPhysicianName.setSavedText(text: patient.personalPhysicianName)
            textfieldPhysicianPhone.setSavedText(text: patient.personalPhysicianPhone)
            textfieldLastVisit.setSavedText(text: patient.personalPhysicianLastVisit)
        }else{
            viewPhysician.isUserInteractionEnabled = false
            viewPhysician.alpha = 0.5
            textfieldPhysicianName.text = ""
            textfieldPhysicianPhone.text = ""
            textfieldLastVisit.text = ""
        }
        radioPhysicalHealth.setSelectedWithTag(patient.physicalHealthTag)
        
        if patient.gender == 1{
            viewWomen.isUserInteractionEnabled = false
            viewWomen.alpha = 0.5
        }else{
            viewWomen.isUserInteractionEnabled = true
            viewWomen.alpha = 1.0
            radioBirthControl.setSelectedWithTag(patient.birthControlTag)
            radioPregnant.setSelectedWithTag(patient.pregnantTag)
            radioNursing.setSelectedWithTag(patient.nursingTag)
        }

    }
    
    func saveValue (){
        patient.personalPhysicianTag = radioPhysician.selected.tag
        patient.personalPhysicianName = textfieldPhysicianName.getText()
        patient.personalPhysicianPhone = textfieldPhysicianPhone.getText()
        patient.personalPhysicianLastVisit = textfieldLastVisit.getText()
        patient.physicalHealthTag = radioPhysicalHealth.selected == nil ? 0 : radioPhysicalHealth.selected.tag
        patient.birthControlTag = radioBirthControl.selected == nil ? 0 : radioBirthControl.selected.tag
        patient.pregnantTag = radioPregnant.selected == nil ? 0 : radioPregnant.selected.tag
        patient.nursingTag = radioNursing.selected == nil ? 0 : radioNursing.selected.tag
    }
    
    @IBAction override func buttonBackAction() {
        saveValue()
        super.buttonBackAction()
    }
    
    @IBAction func onNextButtonPressed (withSender sender : UIButton){
//        if radioPhysician.selected.tag == 1 && (textfieldPhysicianName.isEmpty || textfieldPhysicianPhone.isEmpty || textfieldLastVisit.isEmpty){
//            self.showAlert("PLEASE ENTER ALL THE REQUIRED FIELDS")
//        }else
        if !textfieldPhysicianPhone.isEmpty && !textfieldPhysicianPhone.text!.isPhoneNumber{
            self.showAlert("PLEASE ENTER THE VALID PHYSICIAN PHONE NUMBER")
//        }else if radioPhysicalHealth.selected == nil{
//            self.showAlert("PLEASE ENTER ALL THE REQUIRED FIELDS")
        }else if patient.gender == 2 && (radioBirthControl.selected == nil || radioPregnant.selected == nil || radioNursing.selected == nil){
            self.showAlert("PLEASE ENTER ALL THE REQUIRED FIELDS")
        }else{
            saveValue()
            let patientSignIn = self.storyboard?.instantiateViewController(withIdentifier: "NewPatient10VC") as! NewPatient10ViewController
            patientSignIn.patient = self.patient
            self.navigationController?.pushViewController(patientSignIn, animated: true)
        }
    }
    



}
