//
//  NewPatientFormViewController.swift
//  AnthonyParrella
//
//  Created by Bala Murugan on 2/14/17.
//  Copyright © 2017 SRS Web Solutions. All rights reserved.
//

import UIKit

class NewPatientFormViewController: MCViewController {
    
    var signPatient : UIImage!
    @IBOutlet weak var labelDate : UILabel!
    @IBOutlet weak var labelEmail : UILabel!
    @IBOutlet weak var labelName : UILabel!
    
    @IBOutlet weak var radioParent: RadioButton!
    @IBOutlet weak var labelParentName: UILabel!
    @IBOutlet weak var labelSignedName: UILabel!
    
//    @IBOutlet weak var labelPreferredName : UILabel!
    @IBOutlet weak var radioGender : RadioButton!
    @IBOutlet weak var labelBirthdate : UILabel!
    @IBOutlet weak var labelSocialSecurity : UILabel!
    @IBOutlet weak var labelAge : UILabel!
    @IBOutlet weak var labelAddress : UILabel!
    @IBOutlet weak var radioMaritalStatus : RadioButton!
    @IBOutlet weak var labelHomePhone : UILabel!
    @IBOutlet weak var labelCellPhone : UILabel!
    @IBOutlet weak var labelWorkPhone : UILabel!
    @IBOutlet weak var labelExtension : UILabel!
    @IBOutlet weak var labelDrivingLicense : UILabel!
    @IBOutlet weak var labelEmployer : UILabel!
    @IBOutlet weak var labelEmployerAddress : UILabel!
    @IBOutlet weak var labelOccupation : UILabel!
    @IBOutlet weak var labelReference : UILabel!
    @IBOutlet weak var labelFamilyMembers : UILabel!
    @IBOutlet weak var labelPreviousDentist : UILabel!
    @IBOutlet weak var labelDentistLastDate : UILabel!
    
    @IBOutlet weak var labelSpouseName : UILabel!
    @IBOutlet weak var labelSpouseEmployer : UILabel!
    @IBOutlet weak var labelSpouseWorkPhone : UILabel!
    @IBOutlet weak var labelSpouseExtension : UILabel!
    @IBOutlet weak var labelSpouseSocialSecurity : UILabel!
    @IBOutlet weak var labelSpouseBirthdate : UILabel!
    @IBOutlet weak var labelSpouseDrivingLicense : UILabel!
    @IBOutlet weak var labelResponsibleName : UILabel!
    @IBOutlet weak var labelResponsibleWorkPhone : UILabel!
    @IBOutlet weak var labelResponsibleExtension : UILabel!
    @IBOutlet weak var labelResponsibleHomePhone : UILabel!
    @IBOutlet weak var labelResponsibleBilligAddress : UILabel!
    @IBOutlet weak var labelResponsibleRelation : UILabel!
    @IBOutlet weak var labelResponsibleEmployer : UILabel!
    @IBOutlet weak var labelResponsibleSocialSecurity : UILabel!
    @IBOutlet weak var labelResponsibleDrivingLicense : UILabel!
    
    @IBOutlet weak var labelPrimaryCompanyName : UILabel!
    @IBOutlet weak var labelPrimaryCompanyAddress : UILabel!
    @IBOutlet weak var labelPrimaryCompanyPhone : UILabel!
    @IBOutlet weak var labelPrimaryGroup : UILabel!
    @IBOutlet weak var labelPrimaryName : UILabel!
    @IBOutlet weak var labelPrimaryRelation : UILabel!
    @IBOutlet weak var labelPrimaryBirthdate : UILabel!
    @IBOutlet weak var labelPrimaryId : UILabel!
    @IBOutlet weak var labelPrimaryEmployer : UILabel!
    
    @IBOutlet weak var labelSecondaryCompanyName : UILabel!
    @IBOutlet weak var labelSecondaryCompanyAddress : UILabel!
    @IBOutlet weak var labelSecondaryCompanyPhone : UILabel!
    @IBOutlet weak var labelSecondaryGroup : UILabel!
    @IBOutlet weak var labelSecondaryName : UILabel!
    @IBOutlet weak var labelSecondaryRelation : UILabel!
    @IBOutlet weak var labelSecondaryBirthdate : UILabel!
    @IBOutlet weak var labelSecondaryId : UILabel!
    @IBOutlet weak var labelSecondaryEmployer : UILabel!
    
    @IBOutlet weak var radioPhysician : RadioButton!
    @IBOutlet weak var labelPhysicianName : UILabel!
    @IBOutlet weak var labelPhysicianPhone : UILabel!
    @IBOutlet weak var labelPhysicianLastVisit : UILabel!
    @IBOutlet weak var labelNearByName : UILabel!
    @IBOutlet weak var labelNearByRelation : UILabel!
    @IBOutlet weak var labelNearByWorkPhone : UILabel!
    @IBOutlet weak var labelNearByHomePhone : UILabel!
    
    @IBOutlet weak var radioPhysicalHealth : RadioButton!
    @IBOutlet      var buttonMedicalHistory1 : [RadioButton]!
//    @IBOutlet      var buttonMedicalHistory2 : [RadioButton]!
//    @IBOutlet      var buttonMedicalHistory3 : [RadioButton]!
//    @IBOutlet      var buttonMedicalHistory4 : [RadioButton]!
//    @IBOutlet      var buttonMedicalHistory5 : [RadioButton]!
    @IBOutlet      var buttonMedicalHistory6 : [RadioButton]!
    @IBOutlet weak var labelPhysicianCare : UILabel!
    @IBOutlet weak var labelPrescription : UILabel!
    @IBOutlet weak var radioBirthControl : RadioButton!
    @IBOutlet weak var radioPregnant : RadioButton!
    @IBOutlet weak var radioNursing : RadioButton!
    @IBOutlet weak var labelWeeks : UILabel!
    @IBOutlet      var labelMedications : [UILabel]!
    @IBOutlet      var  labelDentistVisitToday : [UILabel]!
    @IBOutlet weak var radioDentalHealth : RadioButton!
    @IBOutlet weak var labelFloss : UILabel!
    @IBOutlet weak var labelBrush : UILabel!
    @IBOutlet weak var radioBristles : RadioButton!
    @IBOutlet weak var signaturePatient : UIImageView!
    @IBOutlet weak var labelDate1 : UILabel!
    
    @IBOutlet weak var tableAllergy1: UITableView!
    @IBOutlet weak var tableAllergy2: UITableView!
    
    @IBOutlet weak var heightConstraintllergy: NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loadValue()
        tableAllergy1.reloadData()
        tableAllergy2.reloadData()
    }
    
    var arrayAllergy: [Allergy] {
        get {
            return patient.medicalHistory.allergies
        }
    }
    
    override func viewDidLayoutSubviews() {
        heightConstraintllergy.constant = CGFloat(arrayAllergy.count <= 52 ? 0 : Int((arrayAllergy.count - 51)/2) * 20)
        super.viewDidLayoutSubviews()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func loadValue (){
        radioParent.isSelected = patient.isParent
        if patient.isParent {
            labelParentName.text = patient.parentFirstName + " " + patient.parentLastName
        }
        
        labelSignedName.text = patient.isParent == true ? patient.parentFirstName + " " + patient.parentLastName : patient.fullName
        
        labelDate.text = patient.dateToday
        labelEmail.text = patient.email
        labelName.text = patient.fullName
//        labelPreferredName.text = patient.preferredName
        radioGender.setSelectedWithTag(patient.gender)
        labelBirthdate.text = patient.dateOfBirth
        labelSocialSecurity.text = patient.socialSecurityNumber.socialSecurityNumber
        labelAge.text = "\(patient.patientAge!)"
        
        var address = [String]()
        
        if patient.city.count > 0 {
            address.append(patient.city)
        }
        if patient.state.count > 0 {
            address.append(patient.state)
        }
        if patient.zip.count > 0 {
            address.append(patient.zip)
        }
        let addressData = "\(patient.address) \(patient.city)"
        labelAddress.text = "\(addressData) \(patient.state) \(patient.zip)"
        radioMaritalStatus.setSelectedWithTag(patient.maritalStatus)
        labelHomePhone.text = patient.homePhone
        labelCellPhone.text = patient.cellPhone
        labelWorkPhone.text = patient.workPhone
        labelExtension.text = patient.extensionCode
        labelDrivingLicense.text = patient.drivingLicense
        
        if patient.isEmployed {
            labelEmployer.text = patient.employer
            
            address.removeAll()
            if patient.employerAddress.count > 0 {
                address.append(patient.employerAddress)
            }
            if patient.employerCity.count > 0 {
                address.append(patient.employerCity)
            }
            if patient.employerState.count > 0 {
                address.append(patient.employerState)
            }
            if patient.employerZip.count > 0 {
                address.append(patient.employerZip)
            }
            labelEmployerAddress.text = address.joined(separator: ", ")
            //        labelHowLongThere.text = patient.employerHowLong
            labelOccupation.text = patient.occupation
        }
//        labelTimeToReach.text = patient.timeReachYou
        labelReference.text = patient.reference
        labelFamilyMembers.text = patient.familyMembers
        labelPreviousDentist.text = patient.previousDentist
        labelDentistLastDate.text = patient.lastVisit
        
        if patient.maritalStatus == 2 {
            labelSpouseName.text = patient.spouseName
            labelSpouseEmployer.text = patient.spouseEmployer
            labelSpouseWorkPhone.text = patient.spouseWorkPhone
            labelSpouseExtension.text = patient.spouseExtension
            labelSpouseSocialSecurity.text = patient.spouseSocialSecurity == nil ? "" : patient.spouseSocialSecurity.socialSecurityNumber
            labelSpouseBirthdate.text = patient.spouseBirthDate
            labelSpouseDrivingLicense.text = patient.spouseDrivingLicense
        }
        
        labelResponsibleName.text = patient.responsibleName
        labelResponsibleWorkPhone.text = patient.responsibleWorkPhone
        labelResponsibleExtension.text = patient.responsibleExtension
        labelResponsibleHomePhone.text = patient.responsibleHomePhone
        labelResponsibleBilligAddress.text = patient.responsibleBillingAddress
        labelResponsibleRelation.text = patient.responsibleRelation
        labelResponsibleEmployer.text = patient.responsibleEmployer
        labelResponsibleSocialSecurity.text = patient.responsibleSocialSecurity == nil ? "" : patient.responsibleSocialSecurity.socialSecurityNumber
        labelResponsibleDrivingLicense.text = patient.responsibleDrivingLicense
        
        if(patient.havePrimaryInsurance)
        {
            labelPrimaryCompanyName.text = patient.primaryInsurance.companyName
            labelPrimaryCompanyAddress.text = patient.primaryInsurance.companyAddress
            labelPrimaryCompanyPhone.text = patient.primaryInsurance.companyPhone
            labelPrimaryGroup.text = patient.primaryInsurance.groupNumber
            labelPrimaryName.text = patient.primaryInsurance.insuredName
            labelPrimaryRelation.text = patient.primaryInsurance.relation
            labelPrimaryBirthdate.text = patient.primaryInsurance.insuredBirthdate
            labelPrimaryId.text = patient.primaryInsurance.insuredId
            labelPrimaryEmployer.text = patient.primaryInsurance.insuredEmployer
            
            if patient.haveSecondaryInsurance {
                labelSecondaryCompanyName.text = patient.secondaryInsurance.companyName
                labelSecondaryCompanyAddress.text = patient.secondaryInsurance.companyAddress
                labelSecondaryCompanyPhone.text = patient.secondaryInsurance.companyPhone
                labelSecondaryGroup.text = patient.secondaryInsurance.groupNumber
                labelSecondaryName.text = patient.secondaryInsurance.insuredName
                labelSecondaryRelation.text = patient.secondaryInsurance.relation
                labelSecondaryBirthdate.text = patient.secondaryInsurance.insuredBirthdate
                labelSecondaryId.text = patient.secondaryInsurance.insuredId
                labelSecondaryEmployer.text = patient.secondaryInsurance.insuredEmployer
            }
        }
        radioPhysician.setSelectedWithTag(patient.personalPhysicianTag)
        if radioPhysician.isSelected {
            labelPhysicianName.text = patient.personalPhysicianName
            labelPhysicianPhone.text = patient.personalPhysicianPhone
            labelPhysicianLastVisit.text = patient.personalPhysicianLastVisit
        }
        labelNearByName.text = patient.nearByName
        labelNearByRelation.text = patient.nearByRelation
        labelNearByWorkPhone.text = patient.nearByWorkPhone
        labelNearByHomePhone.text = patient.nearByHomePhone
        
        radioPhysicalHealth.setSelectedWithTag(patient.physicalHealthTag)
        for btn in buttonMedicalHistory1 {
            btn.isSelected = patient.medicalHistory.medicalQuestions[0][btn.tag - 1].selectedOption
        }
        
//        for (idx,btn) in buttonMedicalHistory2.enumerated(){
//            btn.isSelected = patient.medicalHistory.medicalQuestions[1][idx].selectedOption
//        }
//        for (idx,btn) in buttonMedicalHistory3.enumerated(){
//            btn.isSelected = patient.medicalHistory.medicalQuestions[2][idx].selectedOption
//        }
//        for (idx,btn) in buttonMedicalHistory4.enumerated(){
//            btn.isSelected = patient.medicalHistory.medicalQuestions[3][idx].selectedOption
//        }
//        for (idx,btn) in buttonMedicalHistory5.enumerated(){
//            btn.isSelected = patient.medicalHistory.medicalQuestions[4][idx].selectedOption
//        }
        
        for btn in buttonMedicalHistory6 {
            btn.isSelected = patient.medicalHistory.medicalQuestions[1][btn.tag - 1].selectedOption
        }
        
        labelPhysicianCare.text = patient.medicalHistory.medicalQuestions[0][0].answer
        labelPrescription.text = patient.medicalHistory.medicalQuestions[0][2].answer
        radioBirthControl.setSelectedWithTag(patient.birthControlTag)
        radioPregnant.setSelectedWithTag(patient.pregnantTag)
        radioNursing.setSelectedWithTag(patient.nursingTag)
        labelWeeks.text = patient.numberOfWeeks
        
        if let medication = patient.medicalHistory.medicalQuestions[0][5].answer{
            medication.setTextForArrayOfLabels(labelMedications)
        }
        
//        if let allergy = patient.medicalHistory.medicalQuestions[4][8].answer{
//            allergy.setTextForArrayOfLabels(labelAllergies)
//        }
        
        patient.reasonTodayVisit.setTextForArrayOfLabels(labelDentistVisitToday)
        radioDentalHealth.setSelectedWithTag(patient.dentalHealthTag)
        labelFloss.text = patient.floss
        labelBrush.text = patient.brush
        radioBristles.setSelectedWithTag(patient.bristlesTag)
        signaturePatient.image = signPatient
        labelDate1.text = patient.dateToday
        
        
        if let patientDetails = self.patient.patientDetails {
            
            var address1 = [String]()
            
            if patient.patientDetails!.city.count > 0 {
                address1.append(patient.patientDetails!.city)
            }
            if patient.patientDetails!.state.count > 0 {
                address1.append(patient.patientDetails!.state)
            }
            if patient.patientDetails!.zipCode.count > 0 {
                address1.append(patient.patientDetails!.zipCode)
            }
            
            address.removeAll()
            if patient.city.count > 0 {
                address.append(patient.city)
            }
            if patient.state.count > 0 {
                address.append(patient.state)
            }
            if patient.zip.count > 0 {
                address.append(patient.zip)
            }
            
            labelAddress.compare(val1: address.joined(separator: ", "), val2: address1.joined(separator: ", "))
            labelHomePhone.compare(val1: patient.homePhone, val2: patientDetails.homePhone.formattedPhoneNumber)
            labelCellPhone.compare(val1: patient.cellPhone, val2: patientDetails.cellPhone.formattedPhoneNumber)
            labelEmail.compare(val1: patient.email, val2: patientDetails.email)
            labelWorkPhone.compare(val1: patient.workPhone, val2: patientDetails.workPhone)
            labelSocialSecurity.compare(val1: patient.socialSecurityNumber.socialSecurityNumber, val2: patientDetails.socialSecurityNumber.socialSecurityNumber)
            
            if let insurance = patientDetails.primaryDentalInsurance {
                labelPrimaryName.compare(val1: insurance.name, val2: patient.primaryInsurance.insuredName)
                labelPrimaryCompanyName.compare(val1: insurance.companyName, val2: patient.primaryInsurance.companyName)
                labelPrimaryId.compare(val1: insurance.insuredID, val2: patient.primaryInsurance.insuredId)
                labelPrimaryGroup.compare(val1: insurance.groupNumber, val2: patient.primaryInsurance.groupNumber)
            }
            
            if let insurance = patientDetails.secondaryDentalInsurance {
                labelSecondaryName.compare(val1: insurance.name, val2: patient.secondaryInsurance.insuredName)
                labelSecondaryCompanyName.compare(val1: insurance.companyName, val2: patient.secondaryInsurance.companyName)
                labelSecondaryId.compare(val1: insurance.insuredID, val2: patient.secondaryInsurance.insuredId)
                labelSecondaryGroup.compare(val1: insurance.groupNumber, val2: patient.secondaryInsurance.groupNumber)
            }
        }
    }
    
    @IBAction override func onSubmitButtonPressed() {
        self.buttonBack?.isUserInteractionEnabled = false
        self.buttonSubmit?.isUserInteractionEnabled = false
        
        BRProgressHUD.show()
        DispatchQueue.main.async {
            ServiceManager.sendPatientDetails(self.patient, completion: { (result, error) in
                if result {
                    super.onSubmitButtonPressed()
                } else {
                    SystemConfig.sharedConfig?.checkConnectivity(completion: { (finished, errorMessage) in
                        BRProgressHUD.hide()
                        self.buttonBack?.isUserInteractionEnabled = true
                        self.buttonSubmit?.isUserInteractionEnabled = true
                        if finished {
                            self.showAlert("Something went wrong, Please contact our support team")
                        } else {
                            self.showAlert(errorMessage!)
                        }
                    })
                }
            })
        }
    }
}
extension NewPatientFormViewController : UITableViewDataSource,UITableViewDelegate {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let count = "\(CGFloat(self.arrayAllergy.count)/2.0)".contains(".5") ? self.arrayAllergy.count/2 + 1 : self.arrayAllergy.count/2
        return tableView.tag == 1 ? min(26, count) : count - 26
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 20.0
        
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! AllergyFormViewCell
        //  cell.delegate = self
        cell.tag = indexPath.row
        if tableView.tag == 1 {
            cell.configureCell(allgy1: self.arrayAllergy[indexPath.row * 2], allg2: self.arrayAllergy.count == (indexPath.row * 2) + 1 ? nil : self.arrayAllergy[(indexPath.row * 2) + 1])
        } else {
            cell.configureCell(allgy1: self.arrayAllergy[(indexPath.row * 2) + 52], allg2: self.arrayAllergy.count == (indexPath.row * 2) + 53 ? nil : self.arrayAllergy[(indexPath.row * 2) + 53])
        }
        
        return cell
    }
    
}
