//
//  NewPatient31ViewController.swift
//  MConsent Dentrix
//
//  Created by Berlin Raj on 03/06/17.
//  Copyright © 2017 SRS Web Solutions. All rights reserved.
//

import UIKit

class NewPatient31ViewController: MCViewController {

    @IBOutlet weak var labelTitle: UILabel!
    
    @IBOutlet weak var radioEmployed: RadioButton!
    @IBOutlet weak var radioPrimary: RadioButton!
    @IBOutlet weak var radioSecondary: RadioButton!
    @IBOutlet weak var radioSecondary2: RadioButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        labelTitle.text = patient.isParent == true ? "PARENT INFORMATION" : "PATIENT INFORMATION"
        loadvalue()
        // Do any additional setup after loading the view.
    }

    func loadvalue() {
        radioEmployed.isSelected = patient.isEmployed
        radioPrimary.isSelected = patient.havePrimaryInsurance
        radioPrimaryAction()
    }
    
    @IBAction func radioPrimaryAction() {
        if radioPrimary.isSelected {
            radioSecondary.isSelected = patient.haveSecondaryInsurance
            radioSecondary.isEnabled = true
            radioSecondary2.isEnabled = true
        } else {
            radioSecondary.isEnabled = false
            radioSecondary2.isEnabled = false
            patient.haveSecondaryInsurance = false
            radioSecondary.deselectAllButtons()
        }
    }
    
    func saveValues() {
        patient.isEmployed = radioEmployed.isSelected
        patient.havePrimaryInsurance = radioPrimary.isSelected
        patient.haveSecondaryInsurance = radioPrimary.isSelected && radioSecondary.isSelected
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func buttonBackAction() {
        saveValues()
        super.buttonBackAction()
    }
    

    @IBAction func buttonNextAction () {
        if radioEmployed.selected == nil {
            self.showAlert("PLEASE SELECT ALL THE REQUIRED FIELDS")
        } else if radioPrimary.selected == nil {
            self.showAlert("PLEASE SELECT ALL THE REQUIRED FIELD")
        } else {
            
            saveValues()
            
            if patient.isEmployed {
                let patientSignIn = self.storyboard?.instantiateViewController(withIdentifier: "NewPatient3VC") as! NewPatient3ViewController
                patientSignIn.patient = self.patient
                self.navigationController?.pushViewController(patientSignIn, animated: true)
            } else if patient.maritalStatus == 2 {
                let patientSignIn = self.storyboard?.instantiateViewController(withIdentifier: "NewPatient5VC") as! NewPatient5ViewController
                patientSignIn.patient = self.patient
                self.navigationController?.pushViewController(patientSignIn, animated: true)
            } else {
                let patientSignIn = self.storyboard?.instantiateViewController(withIdentifier: "NewPatient6VC") as! NewPatient6ViewController
                patientSignIn.patient = self.patient
                self.navigationController?.pushViewController(patientSignIn, animated: true)
            }
        }
    }
}
