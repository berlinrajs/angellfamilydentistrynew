//
//  NewPatient12ViewController.swift
//  AnthonyParrella
//
//  Created by Bala Murugan on 2/13/17.
//  Copyright © 2017 SRS Web Solutions. All rights reserved.
//

import UIKit

class NewPatient12ViewController: MCViewController {

    @IBOutlet weak var signaturePatient : SignatureView!
    @IBOutlet weak var labelDate : DateLabel!
    @IBOutlet weak var labelPatientName : UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        labelDate.todayDate = patient.dateToday
        labelPatientName.text = patient.fullName
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func onNextButtonPressed (withSender sender : UIButton){
        if !signaturePatient.isSigned() {
            self.showAlert("PLEASE SIGN THE FORM")
        }else if !labelDate.dateTapped{
            self.showAlert("PLEASE SELECT THE DATE")
        }else{
            let patientSignIn = self.storyboard?.instantiateViewController(withIdentifier: "NewPatientFormVC") as! NewPatientFormViewController
            patientSignIn.patient = self.patient
            patientSignIn.signPatient = signaturePatient.signatureImage()
            self.navigationController?.pushViewController(patientSignIn, animated: true)
        }
    }
}
