//
//  NewPatient10ViewController.swift
//  AnthonyParrella
//
//  Created by Bala Murugan on 2/11/17.
//  Copyright © 2017 SRS Web Solutions. All rights reserved.
//

import UIKit

class NewPatient10ViewController: MCViewController {

    @IBOutlet var tableViewQuestions: UITableView!
    @IBOutlet var buttonVerified: UIButton!
    @IBOutlet weak var labelHeading : UILabel!
    var selectedIndex: Int = 0

    override func viewDidLoad() {
        super.viewDidLoad()

        self.labelHeading.text = self.selectedIndex == 0 ? "MEDICAL HISTORY" : "DENTAL HISTORY"
        self.tableViewQuestions.reloadData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func buttonVerifiedAction(withSender sender: UIButton) {
        sender.isSelected = !sender.isSelected
    }
    
    @IBAction func onNextButtonPressed (withSender sender : UIButton){
        if buttonVerified.isSelected == false {
            self.showAlert("PLEASE CONFIRM THAT YOU HAVE ANSWERED ABOVE QUESTIONS")
        } else {
            if selectedIndex == 0 {
                let patientSignIn = self.storyboard?.instantiateViewController(withIdentifier: "NewPatient101VC") as! NewPatient101ViewController
                patientSignIn.patient = self.patient
                self.navigationController?.pushViewController(patientSignIn, animated: true)
            } else {
                let patientSignIn = self.storyboard?.instantiateViewController(withIdentifier: "NewPatient11VC") as! NewPatient11ViewController
                patientSignIn.patient = self.patient
                self.navigationController?.pushViewController(patientSignIn, animated: true)
            }
        }
    }
}

extension NewPatient10ViewController : UITableViewDataSource,UITableViewDelegate {
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return patient.medicalHistory.medicalQuestions[selectedIndex].count
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return selectedIndex == 0 || selectedIndex == 1 ? 55 : 42
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! PatientInfoCell
        cell.delegate = self
        cell.tag = indexPath.row
        cell.configCell(question: self.patient.medicalHistory.medicalQuestions[selectedIndex][indexPath.row])
        return cell
    }
}

extension NewPatient10ViewController : PatientInfoCellDelegate{
    
    func radioButtonTappedForCell(cell: UITableViewCell) {
        if selectedIndex == 0 && (cell.tag == 0 || cell.tag == 2){
            let placeHolder  =  cell.tag == 0 ? "PLEASE EXPLAIN" : "PLEASE LIST EACH ONE"
            PopupTextField.popUpView().showInViewController(self, WithTitle: "", placeHolder: placeHolder, textFormat: TextFormat.Default) { (popUpView, textField) in
                if textField.isEmpty{
                    (cell as! PatientInfoCell).radioButtonYes.setSelected(false)
                    self.patient.medicalHistory.medicalQuestions[self.selectedIndex][cell.tag].selectedOption = false
                }else{
                    self.patient.medicalHistory.medicalQuestions[self.selectedIndex][cell.tag].answer = textField.text
                }
                popUpView.close()
            }

        }else{
            PopupTextView.popUpView().showWithPlaceHolder("PLEASE LIST HERE", completion: { (popUp, textView) in
                if textView.isEmpty{
                    (cell as! PatientInfoCell).radioButtonYes.setSelected(false)
                    self.patient.medicalHistory.medicalQuestions[self.selectedIndex][cell.tag].selectedOption = false
                }else{
                    self.patient.medicalHistory.medicalQuestions[self.selectedIndex][cell.tag].answer = textView.text
                }
                popUp.close()
            })
        }
    }
}
