//
//  UpdateParentInfoViewController.swift
//  AngellFamilyDentistry
//
//  Created by Leojin Bose on 6/30/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

class UpdateParentInfoViewController: MCViewController {
    
    @IBOutlet weak var labelDate: UILabel!
    @IBOutlet weak var textFieldFirstName: MCTextField!
    @IBOutlet weak var textFieldLastName: MCTextField!
    @IBOutlet weak var buttonYes: UIButton!
    @IBOutlet weak var buttonNo: UIButton!
    
    @IBOutlet weak var textfieldMonth : UITextField!
    @IBOutlet weak var textfieldDate : UITextField!
    @IBOutlet weak var textfieldYear : UITextField!
    
    @IBOutlet weak var labelDateText: UILabel!
    
//    @IBOutlet weak var buttonBack: PDButton!
//    var isFromPreviousForm : Bool! = true
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        MonthInputView.addMonthPickerForTextField(textfieldMonth)
        labelDate.text = patient.dateToday
        // Do any additional setup after loading the view.
        self.loadValues()
        
    }
    
    func loadValues() {
        if patient.isParent {
            buttonYes.isSelected = true
            buttonNo.isSelected = false
            textFieldFirstName.isEnabled = true
            textFieldLastName.isEnabled = true
            textfieldMonth.isEnabled = true
            textfieldDate.isEnabled = true
            textfieldYear.isEnabled = true
            labelDateText.isEnabled = true
            textFieldFirstName.text = patient.parentFirstName
            textFieldLastName.text = patient.parentLastName
            textfieldMonth.text = patient.parentDateOfBirth?.components(separatedBy: " ").first
            textfieldDate.text = patient.parentDateOfBirth?.components(separatedBy: ", ").first?.components(separatedBy: " ").last
            textfieldYear.text = patient.parentDateOfBirth?.components(separatedBy: ", ").last
        }
    }
    
    func setValues() {
        if buttonYes.isSelected {
            patient.parentFirstName = textFieldFirstName.text
            patient.parentLastName = textFieldLastName.text
            patient.parentDateOfBirth = textfieldMonth.text! + " " + textfieldDate.text! + ", " + textfieldYear.text!
            patient.isParent = true
        } else {
            patient.parentFirstName = nil
            patient.parentLastName = nil
            patient.parentDateOfBirth = nil
            patient.isParent = false
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func buttonActionNext(_ sender: AnyObject) {
        self.view.endEditing(true)
//        if buttonYes.selected && (textFieldFirstName.isEmpty || textFieldLastName.isEmpty) {
//            let alert = Extention.alert("PLEASE ENTER THE FIRST AND LAST NAME")
//            self.presentViewController(alert, animated: true, completion: nil)
//            return
//        }
//        if buttonYes.selected && invalidDateofBirth {
//            let alert = Extention.alert("PLEASE ENTER THE VALID DATE OF BIRTH")
//            self.presentViewController(alert, animated: true, completion: nil)
//            return
//        }
        self.setValues()
            
        let addressInfoVC = self.storyboard?.instantiateViewController(withIdentifier: "kUpdateAddressInfoVC") as! UpdateAddressInfoViewController
        addressInfoVC.patient = patient
        self.navigationController?.pushViewController(addressInfoVC, animated: true)
    }
    
    
   @IBAction override func buttonBackAction() {
        self.setValues()
         super.buttonBackAction()
    }
    
    
    @IBAction func buttonActionYesNo(_ sender: UIButton) {
        sender.isSelected = true
        
        if sender == buttonYes {
            buttonNo.isSelected = false
            textFieldFirstName.isEnabled = true
            textFieldLastName.isEnabled = true
            textfieldMonth.isEnabled = true
            textfieldDate.isEnabled = true
            textfieldYear.isEnabled = true
            labelDateText.isEnabled = true
        } else {
            buttonYes.isSelected = false
            textFieldFirstName.isEnabled = false
            textFieldLastName.isEnabled = false
            textfieldMonth.isEnabled = false
            textfieldDate.isEnabled = false
            textfieldYear.isEnabled = false
            labelDateText.isEnabled = false
            textFieldLastName.text = ""
            textFieldFirstName.text = ""
            textfieldMonth.text = ""
            textfieldDate.text = ""
            textfieldYear.text = ""
        }
        patient.isParent = buttonYes.isSelected
    }
    var invalidDateofBirth: Bool {
        get {
            if textfieldMonth.isEmpty || textfieldDate.isEmpty || textfieldYear.isEmpty {
                return true
            } else if Int(textfieldDate.text!)! == 0{
                return true
            } else if !textfieldYear.text!.isValidYear {
                return true
            } else {
                
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "dd-MMM-yyyy"
                
                let todayDate = dateFormatter.date(from: dateFormatter.string(from: Date()))
                let currentDate = dateFormatter.date(from: "\(textfieldDate.text!)-\(textfieldMonth.text!)-\(textfieldYear.text!)")
                
                if todayDate == nil || currentDate == nil {
                    return true
                }
                if todayDate!.timeIntervalSince(currentDate!) < 0 {
                    return true
                }
                return false
            }
        }
    }

    
//    @IBAction func toolbarDoneButtonAction(sender: AnyObject) {
//        textFieldDateOfBirth.resignFirstResponder()
//        let dateFormatter = DateFormatter()
//        dateFormatter.dateFormat = "MMMM dd, yyyy"
//        textFieldDateOfBirth.text = dateFormatter.stringFromDate(datePicker.date).uppercaseString
//    }
//    
//    @IBAction func datePickerDateChanged(sender: AnyObject) {
//        let dateFormatter = DateFormatter()
//        dateFormatter.dateFormat = "MMMM dd, yyyy"
//        textFieldDateOfBirth.text = dateFormatter.stringFromDate(datePicker.date).uppercaseString
//    }


}
extension UpdateParentInfoViewController: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if textField == textfieldDate{
            return textField.formatDate(range, string: string)
        }else if textField == textfieldYear{
            return textField.formatNumbers(range, string: string, count: 4)
        }
        return true
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        return textField.resignFirstResponder()
    }
}
