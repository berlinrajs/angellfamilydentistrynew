//
//  UpdateAddressInfoViewController.swift
//  AngellFamilyDentistry
//
//  Created by Leojin Bose on 6/30/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import Foundation
import UIKit

class UpdateAddressInfoViewController: MCViewController {
    
//    var patient : PDPatient!
//    var isFromPreviousForm : Bool! = true
//    @IBOutlet weak var buttonBack: PDButton!

    
    @IBOutlet weak var textFieldAddressLine: MCTextField!
    
    @IBOutlet weak var textFieldState: MCTextField!
    @IBOutlet weak var textFieldCity: MCTextField!
    @IBOutlet weak var textFieldZipCode: MCTextField!
    @IBOutlet weak var textFieldEmail: MCTextField!
    @IBOutlet weak var textFieldPhone: MCTextField!
    @IBOutlet weak var textFieldCellPhone: MCTextField!
    @IBOutlet weak var textFieldWrkPhone: MCTextField!
    @IBOutlet weak var radioTexting : RadioButton!
    
    
    var arrayStates : [String] = [String]()
    var referenceDetails : String!
    var othersDetails : String!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //is18YearsOld ? "SOCIAL SECURITY NUMBER" :
        
//        buttonBack.hidden = isFromPreviousForm
        textFieldState.textFormat = .State
        textFieldZipCode.textFormat = .Zipcode
        textFieldEmail.textFormat = .Email
        textFieldPhone.textFormat = .Phone
        textFieldCellPhone.textFormat = .Phone
       
        textFieldWrkPhone.textFormat = .Phone
        self.loadValues()
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func loadValues() {
        textFieldAddressLine.text = patient.address
        textFieldState.text = patient.state
        textFieldCellPhone.text = patient.cellPhone
        textFieldWrkPhone.text = patient.workPhone
        
        textFieldZipCode.text = patient.zipCode
        textFieldCity.text = patient.city
        textFieldEmail.text = patient.email
        textFieldPhone.text = patient.homePhone
        radioTexting.setSelectedWithTag(patient.textingTag1)
        
        
    }
    
    func setValues() {
        patient.address = textFieldAddressLine.text
        patient.state = textFieldState.text
        
        
        patient.zipCode = textFieldZipCode.text
        patient.city = textFieldCity.text
        patient.email = textFieldEmail.text?.lowercased()
        patient.homePhone = textFieldPhone.text
        patient.cellPhone = textFieldCellPhone.text
        patient.workPhone = textFieldWrkPhone.text
        patient.textingTag1 = radioTexting.selected == nil ? 0 : radioTexting.selected.tag

    }
    
    
    @IBAction func buttonActionNext(_ sender: AnyObject) {
        self.view.endEditing(true)
        if !textFieldZipCode.isEmpty && !textFieldZipCode.text!.isZipCode {
            self.showAlert("PLEASE ENTER VALID ZIPCODE")
        } else if !textFieldPhone.isEmpty && !textFieldPhone.text!.isPhoneNumber {
            self.showAlert("PLEASE ENTER VALID HOME PHONE NUMBER")
        }else if !textFieldCellPhone.isEmpty && !textFieldCellPhone.text!.isPhoneNumber {
            self.showAlert("PLEASE ENTER VALID CELL PHONE NUMBER")
        }else if !textFieldWrkPhone.isEmpty && !textFieldWrkPhone.text!.isPhoneNumber {
            self.showAlert("PLEASE ENTER VALID WORK PHONE NUMBER")
        } else if !textFieldEmail.isEmpty && !textFieldEmail.text!.isValidEmail {
            self.showAlert("PLEASE ENTER VALID EMAIL")
        }else if radioTexting.selected == nil{
            self.showAlert("PLEASE SELECT THE REQUIRED FIELD")
        } else {
            self.setValues()
            let step3VC = self.storyboard?.instantiateViewController(withIdentifier: "AppointmentVC") as! AppointmentViewController
            step3VC.patient = patient
            navigationController?.pushViewController(step3VC, animated: true)
            
//            let contactInfoVC = self.storyboard?.instantiateViewController(withIdentifier: "kUpdateContactInfoVC") as! UpdateContactInfoViewController
//            contactInfoVC.patient = patient
//            self.navigationController?.pushViewController(contactInfoVC, animated: true)
        }

        
        //}
    }
    
   
    @IBAction override func buttonBackAction() {
        setValues()
        super.buttonBackAction()
    }
    
    func findEmptyTextField() -> UITextField? {
        let textFields = is18YearsOld ? [textFieldCity, textFieldPhone, textFieldState, textFieldZipCode, textFieldAddressLine] : [textFieldCity,  textFieldPhone, textFieldState, textFieldZipCode, textFieldAddressLine]
        for textField in textFields {
            if textField!.isEmpty {
                return textField
            }
        }
        return nil
    }
    
    var is18YearsOld : Bool {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MMMM dd, yyyy"
        dateFormatter.timeZone = TimeZone.autoupdatingCurrent
        let birthDate = dateFormatter.date(from: patient.dateOfBirth.capitalized)
        let ageComponents = Calendar.current.dateComponents([.year], from: birthDate!, to: Date())
        return ageComponents.year! >= 18
    }
}

extension UpdateAddressInfoViewController : UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if textField == textFieldZipCode {
            return textField.formatZipCode(range, string: string)
        }
        
        if textField == textFieldCellPhone {
            return textField.formatPhoneNumber(range, string: string)
        }
        if textField == textFieldWrkPhone {
            return textField.formatPhoneNumber(range, string: string)
        }
        if textField == textFieldPhone {
            
            return textField.formatPhoneNumber(range, string: string)
        }
        return true
    }
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}
