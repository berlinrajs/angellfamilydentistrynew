//
//  UpdateContactInfoViewController.swift
//  AngellFamilyDentistry
//
//  Created by Leojin Bose on 6/30/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

class UpdateContactInfoViewController: MCViewController {
    
    @IBOutlet weak var textFieldEmployer: MCTextField!
    @IBOutlet weak var textFieldEmployerPhoneNumber: MCTextField!
    @IBOutlet weak var textFieldEmergencyContact: MCTextField!
    @IBOutlet weak var textFieldEmergencyContactNumber: MCTextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        textFieldEmergencyContactNumber.textFormat = .Phone
        textFieldEmployerPhoneNumber.textFormat = .Phone
        self.loadValues()
        // Do any additional setup after loading the view.
    }
    
    func loadValues() {
        textFieldEmployer.text = patient.employerName
        textFieldEmployerPhoneNumber.text = patient.employerPhoneNumber
        textFieldEmergencyContact.text = patient.emergencyContactName
        textFieldEmergencyContactNumber.text = patient.emergencyContactPhoneNumber
    }
    
    func setValues() {
        if !textFieldEmployer.isEmpty {
            patient.employerName = textFieldEmployer.text
        } else {
            patient.employerName = nil
        }
        if !textFieldEmployerPhoneNumber.isEmpty {
            patient.employerPhoneNumber = textFieldEmployerPhoneNumber.text
        } else {
            patient.employerPhoneNumber = nil
        }
        patient.emergencyContactName = textFieldEmergencyContact.text
        patient.emergencyContactPhoneNumber = textFieldEmergencyContactNumber.text
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func buttonActionNext(_ sender: AnyObject) {
        self.view.endEditing(true)
        if !textFieldEmployerPhoneNumber.isEmpty && !textFieldEmployerPhoneNumber.text!.isPhoneNumber {
            self.showAlert("PLEASE ENTER VALID EMPLOYER PHONE NUMBER")
        } else if !textFieldEmergencyContactNumber.isEmpty && !textFieldEmergencyContactNumber.text!.isPhoneNumber {
            self.showAlert("PLEASE ENTER VALID EMERGENCY CONTACT NUMBER")
        } else {
            self.setValues()
            let step3VC = self.storyboard?.instantiateViewController(withIdentifier: "AppointmentVC") as! AppointmentViewController
            step3VC.patient = patient
            navigationController?.pushViewController(step3VC, animated: true)
//            let diseaseInfoVC = self.storyboard?.instantiateViewController(withIdentifier: "kUpdateDiseaseInfoVC") as! UpdateDiseaseInfoViewController
//            diseaseInfoVC.patient = patient
//            self.navigationController?.pushViewController(diseaseInfoVC, animated: true)
        }
    }
    
    @IBAction override func buttonBackAction() {
        self.setValues()
         super.buttonBackAction()
    }
    
}

extension UpdateContactInfoViewController : UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if (textField == textFieldEmergencyContactNumber || textField == textFieldEmployerPhoneNumber) {
            
            return textField.formatPhoneNumber(range, string: string)
        }
        return true
    }
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}
