//
//  HomeViewController.swift
//  AdultMedicalForm
//
//  Created by SRS Web Solutions on 06/05/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

class HomeViewControllerAuto: MCViewController {
    @IBOutlet var lblDate: UILabel!
    
    @IBOutlet weak var tableViewForms: UITableView!
    
    @IBOutlet weak var labelVersion: UILabel!
    @IBOutlet weak var labelNetwork: UILabel!
    @IBOutlet weak var labelPlace: UILabel!
    
    @IBOutlet weak var constraintTableViewTop: NSLayoutConstraint!
    @IBOutlet weak var constraintTableViewBottom: NSLayoutConstraint!
    
    var selectedForms : [Forms]! = [Forms]()
    var formList : [Forms]! = [Forms]()
    
    var signRefused: Bool! = false
    var signRefusalTag: Int! = 0
    var signRefusalOther: String! = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        BRProgressHUD.hide()
        if let text = Bundle.main.infoDictionary?[kCFBundleVersionKey as String] as? String {
            labelVersion.text = text
        }
        self.navigationController?.navigationBar .isHidden = true
        NotificationCenter.default.addObserver(self, selector: #selector(showAlertPopup), name: NSNotification.Name(rawValue: kFormsCompletedNotification), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(dateChangedNotification), name: NSNotification.Name(rawValue: kDateChangedNotification), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(IPAddressChangedNotificationReceived), name: NSNotification.Name(rawValue: kIPChangedNotification), object: nil)
        
        self.dateChangedNotification()
        self.IPAddressChangedNotificationReceived()
        labelPlace.text = MCAppDetails.sharedAppDetails!.cityState.uppercased()
        
        if MCAppDetails.formsLoadded == true {
            self.formList = MCAppDetails.sharedAppDetails!.formList
        }
    }
    
    func showAlertPopup() {
        self.showCustomAlert("PLEASE HANDOVER THE DEVICE BACK TO FRONT DESK")
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        NSObject.cancelPreviousPerformRequests(withTarget: self)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        DispatchQueue.global(qos: DispatchQoS.QoSClass.background).async {
            let defaults = UserDefaults.standard
            ServiceManager.loginWithUsername(defaults.value(forKey: kAppLoginUsernameKey) as! String, password: defaults.value(forKey: kAppLoginPasswordKey) as! String) { (success, result, error) -> (Void) in
                if success {
                    
                } else {
                    if error == nil {
                        
                    } else {
                        DispatchQueue.main.async(execute: {
                            MCAppDetails.loggedIn = false
                            MCAppDetails.hasPreviousUI = false
                            MCAppDetails.formsLoadded = false
                            (UIApplication.shared.delegate as! AppDelegate).checkAutologin()
                        })
                    }
                }
            }
        }
        for form in self.formList {
            form.isSelected = false
        }
        self.tableViewForms.reloadData()
    }
    
    func IPAddressChangedNotificationReceived() {
        let ipAddress = SystemConfig.sharedConfig!.hostIp!
        Reachability.reachabilityManager = AFNetworkReachabilityManager.init(forDomain: ipAddress)
        Reachability.reachabilityManager.setReachabilityStatusChange { (status) in
            let reachable = (status == .reachableViaWWAN || status == .reachableViaWiFi)
            self.labelNetwork.text = reachable ? "Connectivity : \nON" : "Connectivity : \nOFF"
            if !reachable {
                self.navigationController?.topViewController?.showAlert("Its seems like your device is not connected with the EMR server. Please go to settings to make sure.", buttonTitles: ["SETTINGS", "CANCEL"], styles: [UIAlertActionStyle.default, UIAlertActionStyle.destructive], completion: { (buttonIndex) in
                    if buttonIndex == 0 {
                        let settingsUrl = URL(string: UIApplicationOpenSettingsURLString)
                        if let url = settingsUrl {
                            UIApplication.shared.openURL(url)
                        }
                    }
                })
            }
        }
        Reachability.reachabilityManager.startMonitoring()
        self.labelNetwork.text = Reachability.reachabilityManager.isReachable == true ? "Connectivity : \nON" : "Connectivity : \nOFF"
    }
    
    func dateChangedNotification() {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MMM dd, yyyy"
        self.lblDate.text = dateFormatter.string(from: Date()).uppercased()
        
        DispatchQueue.global(qos: DispatchQoS.QoSClass.background).async {
            self.removeOldFiles()
        }
    }
    
    func removeOldFiles() {
        let documentsPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0]
        let filesToSearch = kOutputFileMustBeImage == true ? "jpg" : "pdf"
        
        let arrayFiles = Bundle.paths(forResourcesOfType: filesToSearch, inDirectory: documentsPath)
        
        for path in arrayFiles {
            let fileManager = FileManager.default
            
            if fileManager.fileExists(atPath: path) {
                do {
                    let fileAttributes = try fileManager.attributesOfItem(atPath: path)
                    let creationDate = fileAttributes[.creationDate] as! Date
                    if creationDate.timeIntervalSinceNow < -172800 {
                        try fileManager.removeItem(atPath: path)
                    }
                } catch {
                    
                }
            }
        }
    }
    
    @IBAction func buttonSettingsAction (_ sender: UIButton) {
        let subMenu = BRSubMenu.sharedMenu()
        if subMenu.selected == false {
            subMenu.showSubMenu(inView: self.view, fromRect: CGRect(x: sender.frame.maxX, y: sender.frame.maxY, width: 250.0, height: 240.0), withItems: ["REFRESH", "FORMS", "LOGOUT", "SERVER CONFIG"], animationType: BRSubMenuAnimationType.LeftDown, itemSelectionBlock: { (subMenu, selectedIndex) in
                if selectedIndex == 0 {
                    self.buttonRefreshAction()
                } else if selectedIndex == 1 {
                    self.buttonActionForms()
                } else if selectedIndex == 2 {
                    self.buttonLogoutAction()
                } else {
                    self.systemConfigAction()
                }
            })
        } else {
            subMenu.hideSubMenu()
        }
    }
    
    @IBAction func buttonActionForms() {
        PopupTextField.popUpView().showInViewController(self, WithTitle: "PASSWORD", placeHolder: "ENTER PASSWORD", textFormat: .SecureText) { (popup, textField) in
            popup.close()
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.3, execute: {
                if let password = UserDefaults.standard.value(forKey: kAppLoginPasswordKey) as? String, password == textField.text! {
                    let formVC = self.storyboard?.instantiateViewController(withIdentifier: "kFileListViewController") as! FileListViewController
                    self.navigationController?.pushViewController(formVC, animated: true)
                } else {
                    self.showAlert("INCORRECT PASSWORD")
                }
            })
        }
    }
    
    @IBAction func systemConfigAction () {
        let configVC = self.storyboard?.instantiateViewController(withIdentifier: "kAutoScanNetworkViewController") as! AutoScanNetworkViewController
        self.navigationController?.present(configVC, animated: true, completion: nil)
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesBegan(touches, with: event)
        let subMenu = BRSubMenu.sharedMenu()
        if subMenu.selected == true {
            subMenu.hideSubMenu()
        }
    }
    
    @IBAction func buttonRefreshAction () {
        BRProgressHUD.showWithTitle("AUTHENTICATING.....")
        MCAppDetails.loggedIn = false
        MCAppDetails.hasPreviousUI = false
        MCAppDetails.formsLoadded = false
        let defaults = UserDefaults.standard
        ServiceManager.loginWithUsername(defaults.value(forKey: kAppLoginUsernameKey) as! String, password: defaults.value(forKey: kAppLoginPasswordKey) as! String) { (success, result, error) -> (Void) in
            if success {
                if let errorMessage = MCAppDetails.errorFoundInDict(details: result!) {
                    BRProgressHUD.hide()
                    self.showAlert(errorMessage, completion: {
                        MCAppDetails.loggedIn = false
                        MCAppDetails.hasPreviousUI = false
                        MCAppDetails.formsLoadded = false
                        (UIApplication.shared.delegate as! AppDelegate).checkAutologin()
                    })
                    return
                }
                MCAppDetails.loggedIn = true
                MCAppDetails.sharedAppDetails = MCAppDetails.init(WithClinicDetails: result!)
                MCAppDetails.hasPreviousUI = false
                MCAppDetails.formsLoadded = false
                LoadingViewController.reloadUI(completion: { (appDetails) in
                    MCAppDetails.sharedAppDetails = appDetails
                    MCAppDetails.hasPreviousUI = true
                    
                    LoadingViewController.reloadForms(completion: { (appDetails) in
                        MCAppDetails.sharedAppDetails = appDetails
                        MCAppDetails.formsLoadded = true
                        (UIApplication.shared.delegate as! AppDelegate).checkAutologin()
                    })
                })
            } else {
                BRProgressHUD.hide()
                if error == nil {
                    self.showAlert("AUTHENTICATION FAILED, PLEASE CHECK YOUR INTERNET CONNECTION AND TRY AGAIN LATER")
                } else {
                    DispatchQueue.main.async(execute: {
                        MCAppDetails.loggedIn = false
                        MCAppDetails.hasPreviousUI = false
                        MCAppDetails.formsLoadded = false
                        (UIApplication.shared.delegate as! AppDelegate).checkAutologin()
                    })
                }
            }
        }
    }
    
    @IBAction func buttonLogoutAction () {
        self.showAlert("ARE YOU SURE, WANT TO LOGOUT?", buttonTitles: ["CANCEL", "LOGOUT"], styles: [UIAlertActionStyle.default, UIAlertActionStyle.destructive]) { (buttonIndex) in
            if buttonIndex == 1 {
                MCAppDetails.loggedIn = false
                MCAppDetails.hasPreviousUI = false
                MCAppDetails.formsLoadded = false
                (UIApplication.shared.delegate as! AppDelegate).checkAutologin()
            }
        }
    }
    
    @IBAction func btnNextAction(_ sender: AnyObject) {
        if !SystemConfig.hasSystemConfig {
            self.showAlert("PLEASE CONFIG YOU SERVER BY SELECTING THE OPTION \"SERVER CONFIG\" FROM SETTING MENU")
            return
        }
        
        selectedForms.removeAll()
        for (_, form) in formList.enumerated() {
            if form.isSelected == true {
                for subForm in form.subForms {
                    if subForm.isSelected == true {
                        selectedForms.append(subForm)
                    }
                }
            }
        }
        self.view.endEditing(true)
        
        if selectedForms.count > 0 {
            self.view.layoutIfNeeded()
            UIView.animate(withDuration: 0.2, animations: {
                self.view.layoutIfNeeded()
            }, completion: { (finished) in
            })
            if selectedForms.count > 0 {
                let patient = MCPatient(forms: selectedForms)
                patient.dateToday = lblDate.text
                patient.signRefused = self.signRefused
                patient.signRefusalOther = self.signRefusalOther
                patient.signRefusalTag = self.signRefusalTag
                let patientInfoVC = self.storyboard?.instantiateViewController(withIdentifier: "kPatientInfoVC") as! PatientInfoViewController
                patientInfoVC.patient = patient
                self.navigationController?.pushViewController(patientInfoVC, animated: true)
            }
        } else {
            self.showAlert("PLEASE SELECT ANY FORM")
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}


extension HomeViewControllerAuto : UITableViewDelegate {
    
    func tapGestureDidSelectAction(_ sender : UITapGestureRecognizer) {
        let indexPath = (sender.view as! FormsTableViewCell).indexPath!
        let form = formList[indexPath.section].subForms[indexPath.row]
        form.isSelected = !form.isSelected
        
        if form.isToothNumberRequired == true && form.isSelected {
            
            if form.formTitle == kOpiodForm {
                PopupTextField.popUpView().showInViewController(self, WithTitle: "ENTER PRESCRIPTION (If Any)", placeHolder: "PLEASE TYPE", textFormat: TextFormat.Default, completion: { (popUp, textField) in
                    if textField.isEmpty{
                        form.isSelected = false
                        form.toothNumbers = ""
                        self.tableViewForms.reloadData()
                    }else{
                        form.toothNumbers = textField.text!
                    }
                    popUp.close()
                })
                
            } else {
                PopupTextField.popUpView().showInViewController(self, WithTitle: "PLEASE ENTER TOOTH NUMBERS (Note: Separate with commas)", placeHolder: "01, 02, 15, 18", textFormat: TextFormat.AlphaNumeric, completion: { (popUp, textField) in
                    if textField.isEmpty{
                        form.isSelected = false
                        form.toothNumbers = ""
                        self.tableViewForms.reloadData()
                    }else{
                        form.toothNumbers = textField.text!
                    }
                    popUp.close()
                })
            }
           
        }else if form.formId != nil, let required = form.requiredValues, required.count > 0 {
            if form.isSelected {
                for question in required {
                    question.answer = nil
                }
                
                PopupField.popUpView().show(self, title: nil, questions: required, textFormats: nil, completion: { (popupView) in
                    
                })
            } else {
                for question in required {
                    question.answer = nil
                }
            }
        }
        
        tableViewForms.reloadRows(at: [indexPath], with: .fade)
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 102.0
    }
    
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let form = formList[section]
        let cell = tableView.dequeueReusableCell(withIdentifier: "cellHeader") as! FormsTableViewCell
        let headerView = cell.contentView.subviews[1] as! PDTableHeaderView
        headerView.labelFormName.text = form.formTitle
        
        let selectedForm = formList.filter { (obj) -> Bool in
            return obj.isSelected == true
        }
        let delayTime = DispatchTime.now() + Double(Int64(0.2 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)
        DispatchQueue.main.asyncAfter(deadline: delayTime) {
            if selectedForm.count > 0 {
                UIView.animate(withDuration: 0.3, animations: {
                    cell.contentView.alpha = form.isSelected == false ? 0.2 : 1.0
                })
            } else {
                UIView.animate(withDuration: 0.3, animations: {
                    cell.contentView.alpha = 1.0
                })
            }
        }
        
        
        headerView.tag = section
        if headerView.gestureRecognizers == nil {
            let tapGesture = UITapGestureRecognizer(target: self, action: #selector(tapGestureAction))
            headerView.addGestureRecognizer(tapGesture)
        }
        return cell.contentView
    }
    
    func tapGestureAction(_ sender : UITapGestureRecognizer) {
        let headerView = sender.view as! PDTableHeaderView
        let form = self.formList[headerView.tag]
        
        let selectedForm = formList.filter { (obj) -> Bool in
            return obj.isSelected == true
        }
        
        func displayRows() {
            if form.formTitle == kTreatment {
                let treatVC = self.storyboard?.instantiateViewController(withIdentifier: "kTreatmentPlanPatientVC") as! TreatmentPlanPatientVC
                treatVC.patient = MCPatient(forms: [form])
                treatVC.patient.dateToday = self.lblDate.text!
                self.navigationController?.pushViewController(treatVC, animated: true)
                return
            }
            self.view.layoutIfNeeded()
            constraintTableViewTop.constant = 50
            UIView.animate(withDuration: 0.3, animations: {
                self.view.layoutIfNeeded()
            }, completion: { (finished) in
                self.constraintTableViewBottom.constant = 30
                UIView.animate(withDuration: 0.3, animations: {
                    self.view.layoutIfNeeded()
                })
                form.isSelected = !form.isSelected
                var indexPaths : [IndexPath] = [IndexPath]()
                for (idx, _) in form.subForms.enumerated() {
                    let indexPath = IndexPath(row: idx, section: headerView.tag)
                    indexPaths.append(indexPath)
                }
                self.tableViewForms.beginUpdates()
                self.tableViewForms.insertRows(at: indexPaths, with: .fade)
                self.tableViewForms.endUpdates()
                let delayTime = DispatchTime.now() + Double(Int64(0.1 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)
                DispatchQueue.main.asyncAfter(deadline: delayTime) {
                    if indexPaths.count > 0 {
                        self.tableViewForms.scrollToRow(at: indexPaths.last!, at: .bottom, animated: true)
                    }
                    DispatchQueue.main.asyncAfter(deadline: delayTime) {
                        self.tableViewForms.isUserInteractionEnabled = true
                        self.tableViewForms.reloadData()
                    }
                }
            })
        }
        
        if selectedForm.count > 0 {
            self.tableViewForms.isUserInteractionEnabled = false
            selectedForm[0].isSelected = false
            let section = selectedForm[0].formTitle == kIntakeForms.keys.first ? 0 : selectedForm[0].formTitle == kScanCards.keys.first ? 1 : 2
            var indexPaths : [IndexPath] = [IndexPath]()
            
            for (idx, _) in selectedForm[0].subForms.enumerated() {
                let indexPath = IndexPath(row: idx, section: section)
                indexPaths.append(indexPath)
            }
            if form.formTitle == kIntakeForms.keys.first {
                for obj in self.formList {
                    for subForm in obj.subForms {
                        subForm.isSelected = false
                    }
                }
            }
            self.tableViewForms.beginUpdates()
            self.tableViewForms.deleteRows(at: indexPaths, with: .none)
            self.tableViewForms.endUpdates()
            
            self.view.layoutIfNeeded()
            constraintTableViewTop.constant = 50
            UIView.animate(withDuration: 0.3, animations: {
                self.view.layoutIfNeeded()
            }, completion: { (finished) in
                self.constraintTableViewBottom.constant = 100
                UIView.animate(withDuration: 0.3, animations: {
                    self.view.layoutIfNeeded()
                })
                //                if form.formTitle != selectedForm[0].formTitle {
                //                    let delayTime = DispatchTime.now() + Double(Int64(0.3 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)
                //                    DispatchQueue.main.asyncAfter(deadline: delayTime) {
                //                        displayRows()
                //                    }
                //                } else {
                let delayTime = DispatchTime.now() + Double(Int64(0.1 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)
                DispatchQueue.main.asyncAfter(deadline: delayTime) {
                    self.tableViewForms.isUserInteractionEnabled = true
                    self.tableViewForms.reloadData()
                }
                //                }
            })
        } else {
            displayRows()
        }
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cellSubForm") as! FormsTableViewCell
        let form = formList[indexPath.section].subForms[indexPath.row]
        let height = form.formTitle.heightWithConstrainedWidth(380.0, font: cell.labelFormName.font) + 17
        return height
    }
}

extension HomeViewControllerAuto : UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return formList.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let form = formList[section]
        return form.isSelected == true ? form.subForms.count : 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cellSubForm", for: indexPath) as! FormsTableViewCell
        let form = formList[indexPath.section].subForms[indexPath.row]
        cell.labelFormName.text = form.formTitle
        cell.imageViewCheckMark.isHidden = !form.isSelected
        cell.labelFormName.textColor = form.isSelected == true ? UIColor(red: 215/255.0, green: 207/255.0, blue: 122/255.0, alpha: 1.0) : UIColor.white
        cell.indexPath = indexPath
        
        if cell.gestureRecognizers == nil {
            let tapGesture = UITapGestureRecognizer(target: self, action: #selector(tapGestureDidSelectAction))
            cell.addGestureRecognizer(tapGesture)
        }
        cell.backgroundColor = UIColor.clear
        cell.contentView.backgroundColor = UIColor.clear
        return cell
    }
}
