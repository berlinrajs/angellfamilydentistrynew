//
//  CardImageCaptureVC.swift
//  AceDental
//
//  Created by SRS Web Solutions on 29/04/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

class CardImageCaptureVC: MCViewController {
    
    @IBOutlet weak var imageViewFront: ActionImageView!
    @IBOutlet weak var imageViewBack: ActionImageView!
    @IBOutlet weak var backButton: MCButton!
    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var labelDate: UILabel!
    
//    var isFromPreviousForm: Bool = true
    var isDrivingLicense: Bool = false
    var isFrontImageSelected: Bool = false
    var isBackImageSelected: Bool = false
    
    var frontPlaceHolderImage: UIImage? {
        get {
            return isDrivingLicense == true ? UIImage(named: "LicenseFront") : UIImage(named: "InsuranceFront")
        }
    }
    
    var backPlaceHolderImage: UIImage? {
        get {
            return isDrivingLicense == true ? UIImage(named: "LicenseBack") : UIImage(named: "InsuranceBack")
        }
    }
//    @IBOutlet weak var captureButtonFront: UIButton!
//    @IBOutlet weak var captureButtonBack: UIButton!

    var selectedButton: UIButton!
    
//    var overlayView: UIView {
//        get {
//            let imageView = UIImageView(frame: CGRectMake(0, 0, screenSize.width, screenSize.height))
//            imageView.backgroundColor = UIColor.clearColor()
//            imageView.image = UIImage(named: "overlay")
//            return imageView
//        }
//    }
    override func viewDidLoad() {
        super.viewDidLoad()

        labelDate.text = patient.dateToday
        labelTitle.text = isDrivingLicense ? "ADD/UPDATE PHOTO ID" : "ADD/UPDATE INSURANCE CARD"
        backButton.isHidden = isFromPreviousForm
        
        imageViewFront.cornerRadius = 5.0
        imageViewBack.cornerRadius = 5.0
        imageViewBack.borderColor = UIColor.clear
        imageViewFront.borderColor = UIColor.clear
        
        loadValues()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func  backAction(_ sender: AnyObject) {
        setValues()
        navigationController?.popViewController(animated: true)
    }
    
    func loadValues() {
        if let frontImage = patient.frontImage {
            isFrontImageSelected = true
            imageViewFront.image = frontImage
        } else {
            imageViewFront.image = self.frontPlaceHolderImage
        }
        if let backImage = patient.backImage {
            isBackImageSelected = true
            imageViewBack.image = backImage
        } else {
            imageViewBack.image = self.backPlaceHolderImage
        }
    }
    
    func setValues() {
        patient.frontImage = isFrontImageSelected ? self.imageViewFront.image : nil
        patient.backImage = isBackImageSelected ? self.imageViewBack.image : nil
    }
    
    @IBAction func nextAction(_ sender: AnyObject) {
        if isDrivingLicense && !isFrontImageSelected {
            self.showAlert("PLEASE CAPTURE FRONT SIDE OF THE CARD")
        } else if !isDrivingLicense && !isFrontImageSelected  {
            self.showAlert("PLEASE CAPTURE FRONT SIDE OF THE CARD")
        }  else {
            setValues()
            let formVC = self.storyboard?.instantiateViewController(withIdentifier: "kCardImageFormVC") as! CardImageFormVC
            formVC.patient = self.patient
            formVC.isDrivingLicense = self.isDrivingLicense
            navigationController?.pushViewController(formVC, animated: true)
        }
    }
    @IBAction func camButtonSelected(_ sender: UIButton) {
        if UIImagePickerController.isSourceTypeAvailable(.camera) {
            let picker = self.storyboard?.instantiateViewController(withIdentifier: "kCardImagePickerControllerNew") as! CardImagePickerControllerNew
            picker.delegate = self
            self.present(picker, animated: true) {
                self.selectedButton = sender
            }
        }
    }
}
extension CardImageCaptureVC: CardImageCaptureDelegate {
    func cardImagePicker(_ picker: CardImagePickerControllerNew, completedWithCardImage image: UIImage?) {
        if image != nil {
            if selectedButton.tag == 1 {
                imageViewFront.image = image
                isFrontImageSelected = true
                selectedButton.isSelected = true
                
                imageViewFront.layer.borderColor = UIColor.white.cgColor
            } else {
                imageViewBack.image = image
                isBackImageSelected = true
                selectedButton.isSelected = true
                
                imageViewBack.layer.borderColor = UIColor.white.cgColor
            }
        }
        picker.dismiss(animated: true) {
            self.selectedButton = nil
        }
    }
    func cardImagePickerDidCancel(_ picker: CardImagePickerControllerNew){
        picker.dismiss(animated: true) {
            self.selectedButton = nil
        }
    }
}
