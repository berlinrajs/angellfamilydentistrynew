//
//  CardImageFormVC.swift
//  AceDental
//
//  Created by SRS Web Solutions on 29/04/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

class CardImageFormVC: MCViewController {

    @IBOutlet weak var imageViewFront: ActionImageView!
    @IBOutlet weak var imageViewBack: ActionImageView!
    
//    @IBOutlet weak var buttonBack: UIButton!
//    @IBOutlet weak var buttonSubmit: UIButton!
    
    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var labelName: UILabel!
    @IBOutlet weak var labelDate: UILabel!
    @IBOutlet weak var labelBack: UILabel!
    
    
    
    var isDrivingLicense: Bool = false
    override func viewDidLoad() {
        super.viewDidLoad()
//        buttonSubmit.backgroundColor = UIColor.greenColor()

        imageViewFront.borderColor = UIColor.lightGray
        imageViewBack.borderColor = UIColor.lightGray
        
        labelTitle.text = isDrivingLicense ? "PHOTO ID" : "INSURANCE CARD"
        labelDate.text = "DATE: " + patient.dateToday
        
        labelName.text = "PATIENT NAME: " + patient.fullName.uppercased()
        
        self.imageViewFront.image = patient.frontImage
        if patient.backImage == nil {
            self.imageViewBack.isHidden = true
            self.labelBack.isHidden = true
        } else {
            self.imageViewBack.image = patient.backImage
            self.imageViewBack.isHidden = false
            self.labelBack.isHidden = false
        }
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
//    @IBAction func buttonActionBack(sender: AnyObject) {
//        self.navigationController?.popViewControllerAnimated(true)
//    }
//    
//    @IBAction func buttonActionSubmit(sender: AnyObject) {
//        if Reachability.isConnectedToNetwork() {
//            let pdfManager = PDFManager()
//            pdfManager.authorizeDrive(self.view) { (success) -> Void in
//                if success {
//                    self.buttonSubmit.hidden = true
//                    self.buttonBack.hidden = true
//                    pdfManager.createPDFForView(self.view, fileName: self.isDrivingLicense == true ? "DRIVER_LICENSE" : "INSURANCE_ CARD", patient: self.patient, completionBlock: { (finished) -> Void in
//                        if finished {
//                            self.patient = self.patient.patient()
//                            let formNames = (self.patient.selectedForms as NSArray).valueForKey("formTitle") as! [String]
//                            if formNames.contains(kDrivingLicense) && !self.isDrivingLicense {
//                                let cardCapture = self.storyboard?.instantiateViewControllerWithIdentifier("kCardImageCaptureVC") as! CardImageCaptureVC
//                                cardCapture.patient = self.patient
//                                cardCapture.isDrivingLicense = true
//                                self.navigationController?.pushViewController(cardCapture, animated: true)
//                            }
//                            else if formNames.contains(kDentalImplants) {
//                                let dentalImplantVC = self.storyboard?.instantiateViewControllerWithIdentifier("kDentalImplantsVC") as! DentalImplantsViewController
//                                dentalImplantVC.patient = self.patient
//                                self.navigationController?.pushViewController(dentalImplantVC, animated: true)
//                            } else if formNames.contains(kToothExtraction) {
//                                let toothExtractionVC = self.storyboard?.instantiateViewControllerWithIdentifier("kToothExtractionVC") as! ToothExtractionViewController
//                                toothExtractionVC.patient = self.patient
//                                self.navigationController?.pushViewController(toothExtractionVC, animated: true)
//                            } else if formNames.contains(kPartialDenture) {
//                                let dentureAdjustmentVC = self.storyboard?.instantiateViewControllerWithIdentifier("kDentureAdjustmentVC") as! DentureAdjustmentViewController
//                                dentureAdjustmentVC.patient = self.patient
//                                self.navigationController?.pushViewController(dentureAdjustmentVC, animated: true)
//                            } else if formNames.contains(kInofficeWhitening) {
//                                let inOfficeWhiteningVC = self.storyboard?.instantiateViewControllerWithIdentifier("kInOfficeWhiteningVC") as! InOfficeWhiteningViewController
//                                inOfficeWhiteningVC.patient = self.patient
//                                self.navigationController?.pushViewController(inOfficeWhiteningVC, animated: true)
//                            } else if formNames.contains(kTreatmentOfChild) {
//                                let childTreatmentVC = self.storyboard?.instantiateViewControllerWithIdentifier("kChildTreatmentVC") as! ChildTreatmentViewController
//                                childTreatmentVC.patient = self.patient
//                                self.navigationController?.pushViewController(childTreatmentVC, animated: true)
//                            } else if formNames.contains(kEndodonticTreatment) {
//                                let endodonticTreatmentVC = self.storyboard?.instantiateViewControllerWithIdentifier("kEndodonticTreatmentVC") as! EndodonticTreatmentViewController
//                                endodonticTreatmentVC.patient = self.patient
//                                self.navigationController?.pushViewController(endodonticTreatmentVC, animated: true)
//                            } else if formNames.contains(kDentalCrown) {
//                                let dentalCrownVC = self.storyboard?.instantiateViewControllerWithIdentifier("kDentureCrownVC") as! DentalCrownViewController
//                                dentalCrownVC.patient = self.patient
//                                self.navigationController?.pushViewController(dentalCrownVC, animated: true)
//                            }else if formNames.contains(kOpiodForm) {
//                                let opiodVC = self.storyboard?.instantiateViewControllerWithIdentifier("kOpiodViewController") as! OpiodViewController
//                                opiodVC.patient = self.patient
//                                self.navigationController?.pushViewController(opiodVC, animated: true)
//                            }else if formNames.contains(kGeneralConsent) {
//                                let generalVC = self.storyboard?.instantiateViewControllerWithIdentifier("kGeneralConsentVC") as! GeneralConsentViewController
//                                generalVC.patient = self.patient
//                                self.navigationController?.pushViewController(generalVC, animated: true)
//                            }  else {
//                                NSNotificationCenter.defaultCenter().postNotificationName(kFormsCompletedNotification, object: nil)
//                                self.navigationController?.popToRootViewControllerAnimated(true)
//                            }
//                        } else {
//                            self.buttonSubmit.hidden = false
//                            self.buttonBack.hidden = false
//                        }
//                    })
//                }
//            }
//        } else {
//            let alertController = UIAlertController(title: "Angell Family Dentistry", message: "Your device is not connected to internet. Please go to settings to connect.", preferredStyle: UIAlertControllerStyle.Alert)
//            let alertOkAction = UIAlertAction(title: "Settings", style: UIAlertActionStyle.Destructive) { (action) -> Void in
//                let settingsUrl = NSURL(string: UIApplicationOpenSettingsURLString)
//                if let url = settingsUrl {
//                    UIApplication.sharedApplication().openURL(url)
//                }
//            }
//            let alertCancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.Destructive) { (action) -> Void in
//                
//            }
//            alertController.addAction(alertOkAction)
//            alertController.addAction(alertCancelAction)
//            self.presentViewController(alertController, animated: true, completion: nil)
//        }
//    }
}
