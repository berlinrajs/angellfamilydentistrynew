//
//  ReleaseConfidentialViewController.swift
//   Angell Family Dentistry
//
//  Created by Office on 2/21/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

class ReleaseConfidentialViewController: MCViewController {

    @IBOutlet weak var labelDate: DateLabel!
    @IBOutlet weak var signatureView: SignatureView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        labelDate.todayDate = patient.dateToday
        loadValues()
        // Do any additional setup after loading the view.
    }
    
    func loadValues() {
        //labelDate.setDate = patient.isDate1Tapped
    }
    
    func setValues() {
        patient.isDate1Tapped = labelDate.dateTapped
        patient.signature1 = signatureView.signatureImage()
    }
        
    
    @IBAction func buttonActionNext(_ sender : AnyObject) {
        self.view.endEditing(true)
        if !signatureView.isSigned() {
            self.showAlert("PLEASE SIGN THE FORM")
        } else if !labelDate.dateTapped {
            self.showAlert("PLEASE SELECT DATE")
        } else {
            self.setValues()
            let authorizationFormVC = self.storyboard?.instantiateViewController(withIdentifier: "kAuthorizationFormVC") as! AuthorizationFormViewController
            authorizationFormVC.patient = patient
            self.navigationController?.pushViewController(authorizationFormVC, animated: true)
        }
    }
    @IBAction override func buttonBackAction() {
        self.setValues()
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}



