//
//  AuthorizationFormViewController.swift
//   Angell Family Dentistry
//
//  Created by Office on 2/22/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

class AuthorizationFormViewController: MCViewController {

    var textRanges : [NSRange]! = [NSRange]()

    @IBOutlet weak var labelReason: UILabel!
    @IBOutlet weak var labelDate: UILabel!
    @IBOutlet weak var labelPatientDetails: UILabel!
//    @IBOutlet weak var buttonSubmit: PDButton!
//    @IBOutlet weak var buttonBack: PDButton!
    @IBOutlet weak var imageViewSignature: UIImageView!
    @IBOutlet weak var labelSignedPerson: UILabel!
    @IBOutlet var labelClinicName: [UILabel]!
    
    @IBOutlet weak var constraintReasonHeight: NSLayoutConstraint!
    @IBOutlet weak var constraintPatientInfoHeight: NSLayoutConstraint!
    override func viewDidLoad() {
        super.viewDidLoad()
//        buttonSubmit.backgroundColor = UIColor.greenColor()
        
        patient.newClinicName.setTextForArrayOfLabels(labelClinicName)
        
        imageViewSignature.image = patient.signature1
        labelDate.text = patient.dateToday
        
        var patientInfo = "\n I"
        
        let patientName = getText(patient.fullName)
        patientInfo = patientInfo + " \(patientName)"
        textRanges.append(patientInfo.rangeOfText(patientName))
        
        patientInfo = patientInfo + " authorize Previous Clinic Name: \(getText(patient.previousClinicName))"
        textRanges.append(patientInfo.rangeOfText(getText(patient.previousClinicName)))
        
        patientInfo = patientInfo + " New Clinic Name: \(getText(patient.newClinicName))"
        textRanges.append(patientInfo.rangeOfText(getText(patient.newClinicName)))
        
        patientInfo = patientInfo + " Phone Number: \(getText(patient.phoneNumber))"
        textRanges.append(patientInfo.rangeOfText(getText(patient.phoneNumber)))
        
        labelSignedPerson.text = is18YearsOld ? "PATIENT" : "PARENT/GUARDIAN"
        
        let text = getText("N/A")
        
        if let fax = patient.faxNumber {
            patientInfo = patientInfo + " Fax Number: \(getText(fax))"
            textRanges.append(patientInfo.rangeOfText(getText(fax)))
        } else {
            patientInfo = patientInfo + " Fax Number: \(text)"
            textRanges.append(patientInfo.rangeOfText(text))
        }
        
        patientInfo = patientInfo + " Patient Name: \(patientName)"
        textRanges.append(patientInfo.rangeOfText(patientName))
        
        patientInfo = patientInfo + " D.O.B: \(getText(patient.dateOfBirth))"
        textRanges.append(patientInfo.rangeOfText(getText(patient.dateOfBirth)))
        
        patientInfo = patientInfo + " to disclose and provide copies of any and all clinical treatment, records, and information concerning my care (or child's if patient is under 18 years of age)  which is in the clinic's possession to be sent to the following dentist or entity:"
        let attributedString = NSMutableAttributedString(string: patientInfo)
        for range in textRanges {
            attributedString.addAttribute(NSUnderlineStyleAttributeName, value: NSUnderlineStyle.styleSingle.rawValue, range: range)
        }
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineSpacing = 10
        paragraphStyle.alignment = NSTextAlignment.justified
        attributedString.addAttributes([NSBaselineOffsetAttributeName: NSNumber(value: 0 as Float), NSParagraphStyleAttributeName : paragraphStyle], range: NSMakeRange(0, attributedString.length))
        labelPatientDetails.attributedText = attributedString
        
        
        constraintPatientInfoHeight.constant = labelPatientDetails.attributedText!.heightWithConstrainedWidth(screenSize.width - 60.0) * 2
        
        let reason = patient.reasonForTransfer == nil ? "N/A" : patient.reasonForTransfer!
        let patientReason = "Reason for Transfer: \(getText(reason))"
        let attributedStr = NSMutableAttributedString(string: patientReason)
        attributedStr.addAttribute(NSUnderlineStyleAttributeName, value: NSUnderlineStyle.styleSingle.rawValue, range: NSMakeRange(patientReason.count - getText(reason).count, getText(reason).count))
        attributedStr.addAttributes([NSBaselineOffsetAttributeName: NSNumber(value: 0 as Float), NSParagraphStyleAttributeName : paragraphStyle], range: NSMakeRange(0, attributedStr.length))
        labelReason.attributedText = attributedStr
        constraintReasonHeight.constant = labelReason.attributedText!.heightWithConstrainedWidth(screenSize.width - 60.0) * 2
        
    
        
        

    }

    
//    @IBAction func buttonActionBack(sender: AnyObject) {
//        self.navigationController?.popViewControllerAnimated(true)
//    }
    
    var is18YearsOld : Bool {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MMMM dd, yyyy"
        dateFormatter.timeZone = TimeZone.autoupdatingCurrent
        let birthDate = dateFormatter.date(from: patient.dateOfBirth.capitalized)
        let ageComponents = Calendar.current.dateComponents([.year], from: birthDate!, to: Date())
        return ageComponents.year! >= 18
    }
    
//    @IBAction func buttonActionSubmit(sender: AnyObject) {
//        
//        if !Reachability.isConnectedToNetwork() {
//            let alertController = UIAlertController(title: "Angell Family Dentistry", message: "Your device is not connected to internet. Please go to settings to connect.", preferredStyle: UIAlertControllerStyle.Alert)
//            let alertOkAction = UIAlertAction(title: "Settings", style: UIAlertActionStyle.Destructive) { (action) -> Void in
//                let settingsUrl = NSURL(string: UIApplicationOpenSettingsURLString)
//                if let url = settingsUrl {
//                    UIApplication.sharedApplication().openURL(url)
//                }
//            }
//            let alertCancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.Destructive) { (action) -> Void in
//                
//            }
//            alertController.addAction(alertOkAction)
//            alertController.addAction(alertCancelAction)
//            self.presentViewController(alertController, animated: true, completion: nil)
//            return
//        }
//        let pdfManager = PDFManager()
//        pdfManager.authorizeDrive(self.view) { (success) -> Void in
//            if success {
//                self.buttonSubmit.hidden = true
//                self.buttonBack.hidden = true
//                pdfManager.createPDFForView(self.view, fileName: "PATIENT_AUTHORIZATION", patient: self.patient, completionBlock: { (finished) -> Void in
//                    if finished {
//                        self.patient = self.patient.patient()
//                        let formNames = (self.patient.selectedForms as NSArray).valueForKey("formTitle") as! [String]
//                        if formNames.contains(kInsuranceCard) {
//                            let cardCapture = self.storyboard?.instantiateViewControllerWithIdentifier("kCardImageCaptureVC") as! CardImageCaptureVC
//                            cardCapture.patient = self.patient
//                            self.navigationController?.pushViewController(cardCapture, animated: true)
//                        } else if formNames.contains(kDrivingLicense) {
//                            let cardCapture = self.storyboard?.instantiateViewControllerWithIdentifier("kCardImageCaptureVC") as! CardImageCaptureVC
//                            cardCapture.patient = self.patient
//                            cardCapture.isDrivingLicense = true
//                            self.navigationController?.pushViewController(cardCapture, animated: true)
//                        }
//                       else if formNames.contains(kDentalImplants) {
//                            let dentalImplantVC = self.storyboard?.instantiateViewControllerWithIdentifier("kDentalImplantsVC") as! DentalImplantsViewController
//                            dentalImplantVC.patient = self.patient
//                            self.navigationController?.pushViewController(dentalImplantVC, animated: true)
//                        } else if formNames.contains(kToothExtraction) {
//                            let toothExtractionVC = self.storyboard?.instantiateViewControllerWithIdentifier("kToothExtractionVC") as! ToothExtractionViewController
//                            toothExtractionVC.patient = self.patient
//                            self.navigationController?.pushViewController(toothExtractionVC, animated: true)
//                        } else if formNames.contains(kPartialDenture) {
//                            let dentureAdjustmentVC = self.storyboard?.instantiateViewControllerWithIdentifier("kDentureAdjustmentVC") as! DentureAdjustmentViewController
//                            dentureAdjustmentVC.patient = self.patient
//                            self.navigationController?.pushViewController(dentureAdjustmentVC, animated: true)
//                        } else if formNames.contains(kInofficeWhitening) {
//                            let inOfficeWhiteningVC = self.storyboard?.instantiateViewControllerWithIdentifier("kInOfficeWhiteningVC") as! InOfficeWhiteningViewController
//                            inOfficeWhiteningVC.patient = self.patient
//                            self.navigationController?.pushViewController(inOfficeWhiteningVC, animated: true)
//                        } else if formNames.contains(kTreatmentOfChild) {
//                            let childTreatmentVC = self.storyboard?.instantiateViewControllerWithIdentifier("kChildTreatmentVC") as! ChildTreatmentViewController
//                            childTreatmentVC.patient = self.patient
//                            self.navigationController?.pushViewController(childTreatmentVC, animated: true)
//                        } else if formNames.contains(kEndodonticTreatment) {
//                            let endodonticTreatmentVC = self.storyboard?.instantiateViewControllerWithIdentifier("kEndodonticTreatmentVC") as! EndodonticTreatmentViewController
//                            endodonticTreatmentVC.patient = self.patient
//                            self.navigationController?.pushViewController(endodonticTreatmentVC, animated: true)
//                        } else if formNames.contains(kDentalCrown) {
//                            let dentalCrownVC = self.storyboard?.instantiateViewControllerWithIdentifier("kDentureCrownVC") as! DentalCrownViewController
//                            dentalCrownVC.patient = self.patient
//                            self.navigationController?.pushViewController(dentalCrownVC, animated: true)
//                        }else if formNames.contains(kOpiodForm) {
//                            let opiodVC = self.storyboard?.instantiateViewControllerWithIdentifier("kOpiodViewController") as! OpiodViewController
//                            opiodVC.patient = self.patient
//                            self.navigationController?.pushViewController(opiodVC, animated: true)
//                        }
//                        else if formNames.contains(kGeneralConsent) {
//                            let generalVC = self.storyboard?.instantiateViewControllerWithIdentifier("kGeneralConsentVC") as! GeneralConsentViewController
//                            generalVC.patient = self.patient
//                            self.navigationController?.pushViewController(generalVC, animated: true)
//                        }else if formNames.contains(kFeedbackForm) {
//                            let feedbackFormVC = self.storyboard?.instantiateViewControllerWithIdentifier("kFeedbackFormVC") as! FeedbackFormViewController
//                            self.navigationController?.pushViewController(feedbackFormVC, animated: true)
//                        } else {
//                            NSNotificationCenter.defaultCenter().postNotificationName(kFormsCompletedNotification, object: nil)
//                            self.navigationController?.popToRootViewControllerAnimated(true)
//                        }
//                    } else {
//                        self.buttonSubmit.hidden = false
//                        self.buttonBack.hidden = false
//                    }
//                })
//            }
//        }
//    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
