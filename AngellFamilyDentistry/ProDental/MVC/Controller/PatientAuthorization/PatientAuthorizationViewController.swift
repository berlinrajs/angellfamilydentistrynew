//
//  PatientAuthorizationViewController.swift
//   Angell Family Dentistry
//
//  Created by Office on 2/21/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

class PatientAuthorizationViewController: MCViewController {

//    var patient : PDPatient!
//    var isFromPreviousForm : Bool! = true
//
//    @IBOutlet weak var buttonBack: PDButton!
    @IBOutlet weak var textFieldFaxNumber: MCTextField!
    @IBOutlet weak var textFieldNewClinic: MCTextField!
    @IBOutlet weak var textFieldPatientNumber: MCTextField!
    @IBOutlet weak var textFieldPhoneNumber: MCTextField!
    @IBOutlet weak var textViewReasonForTransfer: MCTextView!

    
    override func viewDidLoad() {
        super.viewDidLoad()
//        self.buttonBack.hidden = isFromPreviousForm
        // Do any additional setup after loading the view.
        textFieldFaxNumber.textFormat = .Phone
        textFieldPhoneNumber.textFormat = .Phone
        
        loadValues()
        // Do any additional setup after loading the view.
    }
    
    func loadValues() {
        textFieldNewClinic.text = patient.newClinicName
        
        textFieldFaxNumber.text = patient.faxNumber
        textFieldPhoneNumber.text = patient.phoneNumber
        textViewReasonForTransfer.textValue = patient.reasonForTransfer
        //textViewReasonForTransfer.checkEdited()
    }
    
    func setValues() {
        patient.previousClinicName = "Angell Family Dentistry"
        patient.newClinicName = textFieldNewClinic.text
//        if !textFieldPhoneNumber.isEmpty {
//            patient.patientNumber = textFieldPhoneNumber.text
//        } else {
//            patient.patientNumber = nil
//        }
        if !textFieldFaxNumber.isEmpty {
            patient.faxNumber = textFieldFaxNumber.text
        } else {
            patient.faxNumber = nil
        }
        
            patient.reasonForTransfer = textViewReasonForTransfer.textValue!
        
        patient.phoneNumber = textFieldPhoneNumber.text!
    }
    
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @IBAction func buttonActionNext(_ sender : AnyObject) {
        self.view.endEditing(true)
        if textFieldNewClinic.isEmpty {
            self.showAlert("PLEASE ENTER NEW CLINIC NAME")
        } else if !textFieldPhoneNumber.text!.isPhoneNumber {
            self.showAlert("PLEASE ENTER VALID PHONE NUMBER")
        } else if !textFieldFaxNumber.isEmpty && !textFieldFaxNumber.text!.isPhoneNumber {
            self.showAlert("PLEASE ENTER VALID FAX NUMBER")
        } else {
            setValues()
            let releaseConfidentialVC = self.storyboard?.instantiateViewController(withIdentifier: "kReleaseConfidentialVC") as! ReleaseConfidentialViewController
            releaseConfidentialVC.patient = patient
            self.navigationController?.pushViewController(releaseConfidentialVC, animated: true)
        }
    }
    
    @IBAction override func buttonBackAction() {
        setValues()
        super.buttonBackAction()
    }

}




