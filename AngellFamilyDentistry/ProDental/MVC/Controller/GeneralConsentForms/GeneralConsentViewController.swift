//
//  GeneralConsentViewController.swift
//  SecureDental
//
//  Created by Leojin Bose on 5/30/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

class GeneralConsentViewController: MCViewController {

    @IBOutlet weak var signatureView: SignatureView!
    @IBOutlet weak var dateLabel: DateLabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        dateLabel.todayDate = patient.dateToday
        loadValues()
        // Do any additional setup after loading the view.
    }
    
    func loadValues() {
        //dateLabel.setDate = patient.isDate1Tapped
    }
    
    func setValues() {
        patient.isDate1Tapped = dateLabel.dateTapped
        patient.signature1 = signatureView.signatureImage()

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func buttonActionNext(_ sender: AnyObject) {
        self.view.endEditing(true)
        if !signatureView.isSigned() {
            self.showAlert("PLEASE SIGN THE FORM")
        } else if !dateLabel.dateTapped {
            self.showAlert("PLEASE SELECT DATE")
        } else {
            setValues()
            let formVC = self.storyboard?.instantiateViewController(withIdentifier: "kGeneralConsentFormVC") as! GeneralConsentFormViewController
            formVC.patient = patient
            self.navigationController?.pushViewController(formVC, animated: true)
        }
    }
}
