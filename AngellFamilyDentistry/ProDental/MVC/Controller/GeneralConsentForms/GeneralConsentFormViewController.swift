//
//  GeneralConsentFormViewController.swift
//  SecureDental
//
//  Created by Leojin Bose on 5/30/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

class GeneralConsentFormViewController: MCViewController {

    @IBOutlet weak var signatureView: UIImageView!
    @IBOutlet weak var labelName: UILabel!
    @IBOutlet weak var labelDate: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        labelDate.text = patient.dateToday
        labelName.text = patient.fullName
        signatureView.image = patient.signature1
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*@IBAction func buttonBackAction(sender: AnyObject) {
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    @IBAction func buttonsubmitAction(sender: AnyObject) {
        if !Reachability.isConnectedToNetwork() {
            let alertController = UIAlertController(title: "Angell Family Dentistry", message: "Your device is not connected to internet. Please go to settings to connect.", preferredStyle: UIAlertControllerStyle.Alert)
            let alertOkAction = UIAlertAction(title: "SETTINGS", style: UIAlertActionStyle.Destructive) { (action) -> Void in
                let settingsUrl = NSURL(string: UIApplicationOpenSettingsURLString)
                if let url = settingsUrl {
                    UIApplication.sharedApplication().openURL(url)
                }
            }
            let alertCancelAction = UIAlertAction(title: "CANCEL", style: UIAlertActionStyle.Destructive) { (action) -> Void in
                
            }
            alertController.addAction(alertOkAction)
            alertController.addAction(alertCancelAction)
            self.presentViewController(alertController, animated: true, completion: nil)
            return
        }
        let pdfManager = PDFManager()
        pdfManager.authorizeDrive(self.view) { (success) -> Void in
            if success {
                self.buttonSubmit.hidden = true
                self.buttonBack.hidden = true
                pdfManager.createPDFForView(self.view, fileName: "GENERAL_CONSENT", patient: self.patient, completionBlock: { (finished) -> Void in
                    if finished {
                        
                        if self.navigationController?.viewControllers.count > 6 {
                            self.navigationController?.viewControllers.removeRange(5...(self.navigationController?.viewControllers.count)! - 2)
                        }
                       // let formNames = (self.patient.selectedForms as NSArray).valueForKey("formTitle") as! [String]
                        
                        NSNotificationCenter.defaultCenter().postNotificationName(kFormsCompletedNotification, object: nil)
                        self.navigationController?.popToRootViewControllerAnimated(true)
                    } else {
                        self.buttonSubmit.hidden = false
                        self.buttonBack.hidden = false
                    }
                })
            }
        }
    }*/

}
