//
//  SelfieStep1VC.swift
//  Bellevue Dental
//
//  Created by Berlin Raj on 12/12/16.
//  Copyright © 2016 SRS Web Solutions. All rights reserved.
//

import UIKit
import AVFoundation
class SelfieStep1VC: MCViewController {

    @IBOutlet weak var imageViewSelfie: UIImageView!
    var imageCaptured: Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func buttonAddAction() {
        if AVCaptureDevice.authorizationStatus(forMediaType: AVMediaTypeVideo) ==  AVAuthorizationStatus.authorized
        {
            // Already Authorized
            if UIImagePickerController.isSourceTypeAvailable(.camera) {
                let picker = self.storyboard?.instantiateViewController(withIdentifier: "kSelfieImagePickerController") as! SelfieImagePickerController
                picker.delegate = self
                self.present(picker, animated: true, completion: nil)
            }
        }else if AVCaptureDevice.authorizationStatus(forMediaType: AVMediaTypeVideo) ==  AVAuthorizationStatus.denied{
            self.showAlert("YOU HAVE ALREADY DENIED THE PERMISSION FOR CAMERA ACCESSING!")
        }else{
            AVCaptureDevice.requestAccess(forMediaType: AVMediaTypeVideo, completionHandler: { (granted :Bool) -> Void in
                if granted == true
                {
                    DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.3, execute: {
                        if UIImagePickerController.isSourceTypeAvailable(.camera) {
                            let picker = self.storyboard?.instantiateViewController(withIdentifier: "kSelfieImagePickerController") as! SelfieImagePickerController
                            picker.delegate = self
                            self.present(picker, animated: true, completion: nil)
                        }
                    })
                }
                else
                {
                    // User Rejected
                    //do nothing
                }
            });
        }
    }
    
    @IBAction func buttonNextAction () {
        if imageCaptured == false {
            self.showAlert("PLEASE CAPTURE YOUR IMAGE")
        } else {
            let formVC = self.storyboard?.instantiateViewController(withIdentifier: "kSelfieFormVC") as! SelfieFormVC
            formVC.patient = self.patient
            formVC.selfieImage = self.imageViewSelfie.image
            self.navigationController?.pushViewController(formVC, animated: true)
        }
    }
}

//extension SelfieStep1VC: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
//    func imagePickerControllerDidCancel(picker: UIImagePickerController) {
//        picker.dismissViewControllerAnimated(true, completion: nil)
//    }
//    
//    func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : AnyObject]) {
//        if let image = info[UIImagePickerControllerOriginalImage] as? UIImage {
//            self.imageViewSelfie.image = image
//            self.imageCaptured = true
//        }
//        
//        picker.dismissViewControllerAnimated(true, completion: nil)
//    }
//}
extension SelfieStep1VC: SelfieImageCaptureDelegate {
    func selfieImagePicker(picker: SelfieImagePickerController, completedWithSelfieImage image: UIImage?) {
        if image != nil {
            imageViewSelfie.image = image
            imageCaptured = true
        }
        picker.dismiss(animated: true, completion: nil)
    }
    func selfieImagePickerDidCancel(picker: SelfieImagePickerController){
        picker.dismiss(animated: true, completion: nil)
    }
}
