//
//  MedicalHistoryFormViewController.swift
//  ABC Clinic
//
//  Created by Leojin Bose on 2/29/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

class MedicalHistoryFormViewController: MCViewController {

//    var patient : PDPatient!
//    
//    @IBOutlet weak var buttonBack: PDButton!
//    @IBOutlet weak var buttonSubmit: PDButton!
    @IBOutlet weak var constraintTableViewForm1Height: NSLayoutConstraint!
    @IBOutlet weak var constraintTableViewForm4Height: NSLayoutConstraint!
    
    @IBOutlet weak var labelName: UILabel!
    @IBOutlet weak var labelBirthDate: UILabel!
    @IBOutlet weak var labelDateCreated: UILabel!

    @IBOutlet weak var buttonAlergicOthers: UIButton!
    @IBOutlet weak var labelAlergicOthers: MCLabel!
    
    @IBOutlet weak var radioButtonSubstances: RadioButton!
    @IBOutlet weak var labelSubstances: MCLabel!
    
    @IBOutlet weak var radioButtonIllness: RadioButton!
    @IBOutlet weak var radioAntibiotics: RadioButton!
    @IBOutlet weak var labelIllness: MCLabel!
    
    @IBOutlet weak var textViewComments: MCTextView!
    @IBOutlet weak var imageViewSignature: UIImageView!
    @IBOutlet weak var labelDate: UILabel!
    @IBOutlet weak var labelDentistTreatment: UILabel!
    
    @IBOutlet var arrayButtons: [UIButton]!
    @IBOutlet weak var customView: UIView!
    
    
    @IBOutlet weak var labelMedication1: UILabel!
    @IBOutlet weak var labelMedication2: UILabel!
    @IBOutlet weak var labelMedication3: UILabel!
    @IBOutlet weak var labelMedication4: UILabel!
    @IBOutlet weak var labelMedication5: UILabel!
    @IBOutlet weak var labelMedication6: UILabel!
    @IBOutlet weak var labelMedication7: UILabel!

    @IBOutlet weak var labelCondition1: UILabel!
    @IBOutlet weak var labelCondition2: UILabel!
    @IBOutlet weak var labelCondition3: UILabel!
    @IBOutlet weak var labelCondition4: UILabel!
    @IBOutlet weak var labelCondition5: UILabel!
    @IBOutlet weak var labelCondition6: UILabel!
    @IBOutlet weak var labelCondition7: UILabel!
 
    override func viewDidLoad() {
        super.viewDidLoad()
//        buttonSubmit.backgroundColor = UIColor.greenColor()
        
        if patient.scanMedicationIndex == 2 || patient.scanMedicationIndex == 0{
            customView.isHidden = true
        }
        
        constraintTableViewForm1Height.constant = CGFloat(patient.medicalHistoryQuestions1.count * 26)
        constraintTableViewForm4Height.constant = (12 * 15) + 18

        labelName.text = "Patient Name: \(patient.fullName)"
        labelBirthDate.text = "Birth Date: \(patient.dateOfBirth!)"
        labelDateCreated.text = "Date Created: \(patient.dateToday!)"
        
        labelMedication1.text = patient.medication1
        labelMedication2.text = patient.medication2
        labelMedication3.text = patient.medication3
        labelMedication4.text = patient.medication4
        labelMedication5.text = patient.medication5
        labelMedication6.text = patient.medication6
        labelMedication7.text = patient.medication7
        
        labelCondition1.text = patient.condition1
        labelCondition2.text = patient.condition2
        labelCondition3.text = patient.condition3
        labelCondition4.text = patient.condition4
        labelCondition5.text = patient.condition5
        labelCondition6.text = patient.condition6
        labelCondition7.text = patient.condition7


        
        if let alergic = patient.othersTextForm3 {
            buttonAlergicOthers.isSelected = true
            buttonAlergicOthers.setTitleColor(UIColor.red, for: UIControlState())
            labelAlergicOthers.text = " " + alergic
        }
        if let substances = patient.controlledSubstances {
            radioButtonSubstances.isSelected = true
            labelSubstances.text = " " + substances
        } else if patient.controlledSubstancesClicked != nil {
            radioButtonSubstances.isSelected = false
        }
        
        if let illness = patient.otherIllness {
            radioButtonIllness.isSelected = true
            labelIllness.text = " " + illness
        } else {
            radioButtonIllness.isSelected = false
        }
        self.radioAntibiotics.isSelected = patient.antibioticsUsed
        self.labelDentistTreatment.text = self.radioAntibiotics.isSelected == true ? patient.medicationAndDosage : ""
        
        if let comment = patient.comments {
            textViewComments.text = comment
        }
        
        imageViewSignature.image = patient.signature1
        labelDate.text = patient.dateToday
        
        for (idx, buttonQuestion) in self.arrayButtons.enumerated() {
            let obj = patient.medicalHistoryQuestions2[idx]
            buttonQuestion.setTitle(" " + obj.question, for: UIControlState())
            if let selected = obj.isSelected {
                buttonQuestion.isSelected = selected
            } else {
                buttonQuestion.isSelected = false
            }
        }
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

//    @IBAction func buttonActionBack(sender: AnyObject) {
//        self.navigationController?.popViewControllerAnimated(true)
//    }
    
    
    
//    @IBAction func buttonActionSubmit(sender: AnyObject) {
//        
//        if !Reachability.isConnectedToNetwork() {
//            let alertController = UIAlertController(title: "ABC Clinic", message: "Your device is not connected to internet. Please go to settings to connect.", preferredStyle: UIAlertControllerStyle.Alert)
//            let alertOkAction = UIAlertAction(title: "Settings", style: UIAlertActionStyle.Destructive) { (action) -> Void in
//                let settingsUrl = NSURL(string: UIApplicationOpenSettingsURLString)
//                if let url = settingsUrl {
//                    UIApplication.sharedApplication().openURL(url)
//                }
//            }
//            let alertCancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.Destructive) { (action) -> Void in
//                
//            }
//            alertController.addAction(alertOkAction)
//            alertController.addAction(alertCancelAction)
//            self.presentViewController(alertController, animated: true, completion: nil)
//            return
//        }
//        let pdfManager = PDFManager()
//        pdfManager.authorizeDrive(self.view) { (success) -> Void in
//            if success {
//                self.buttonSubmit.hidden = true
//                self.buttonBack.hidden = true
//                pdfManager.createPDFForView(self.view, fileName: "MEDICAL_HISTORY_FORM", patient: self.patient, completionBlock: { (finished) -> Void in
//                    if finished {
//                        self.patient = self.patient.patient()
//                        let formNames = (self.patient.selectedForms as NSArray).valueForKey("formTitle") as! [String]
//                        if formNames.contains(kPatientAuthorization) {
//                            let patientAuthorizationVC = self.storyboard?.instantiateViewControllerWithIdentifier("kPatientAuthorizationVC") as! PatientAuthorizationViewController
//                            patientAuthorizationVC.patient = self.patient
//                            self.navigationController?.pushViewController(patientAuthorizationVC, animated: true)
//                        } else if formNames.contains(kInsuranceCard) {
//                            let cardCapture = self.storyboard?.instantiateViewControllerWithIdentifier("kCardImageCaptureVC") as! CardImageCaptureVC
//                            cardCapture.patient = self.patient
//                            self.navigationController?.pushViewController(cardCapture, animated: true)
//                        } else if formNames.contains(kDrivingLicense) {
//                            let cardCapture = self.storyboard?.instantiateViewControllerWithIdentifier("kCardImageCaptureVC") as! CardImageCaptureVC
//                            cardCapture.patient = self.patient
//                            cardCapture.isDrivingLicense = true
//                            self.navigationController?.pushViewController(cardCapture, animated: true)
//                        }else if formNames.contains(kDentalImplants) {
//                            let dentalImplantVC = self.storyboard?.instantiateViewControllerWithIdentifier("kDentalImplantsVC") as! DentalImplantsViewController
//                            dentalImplantVC.patient = self.patient
//                            self.navigationController?.pushViewController(dentalImplantVC, animated: true)
//                        } else if formNames.contains(kToothExtraction) {
//                            let toothExtractionVC = self.storyboard?.instantiateViewControllerWithIdentifier("kToothExtractionVC") as! ToothExtractionViewController
//                            toothExtractionVC.patient = self.patient
//                            self.navigationController?.pushViewController(toothExtractionVC, animated: true)
//                        } else if formNames.contains(kPartialDenture) {
//                            let dentureAdjustmentVC = self.storyboard?.instantiateViewControllerWithIdentifier("kDentureAdjustmentVC") as! DentureAdjustmentViewController
//                            dentureAdjustmentVC.patient = self.patient
//                            self.navigationController?.pushViewController(dentureAdjustmentVC, animated: true)
//                        } else if formNames.contains(kInofficeWhitening) {
//                            let inOfficeWhiteningVC = self.storyboard?.instantiateViewControllerWithIdentifier("kInOfficeWhiteningVC") as! InOfficeWhiteningViewController
//                            inOfficeWhiteningVC.patient = self.patient
//                            self.navigationController?.pushViewController(inOfficeWhiteningVC, animated: true)
//                        } else if formNames.contains(kTreatmentOfChild) {
//                            let childTreatmentVC = self.storyboard?.instantiateViewControllerWithIdentifier("kChildTreatmentVC") as! ChildTreatmentViewController
//                            childTreatmentVC.patient = self.patient
//                            self.navigationController?.pushViewController(childTreatmentVC, animated: true)
//                        } else if formNames.contains(kEndodonticTreatment) {
//                            let endodonticTreatmentVC = self.storyboard?.instantiateViewControllerWithIdentifier("kEndodonticTreatmentVC") as! EndodonticTreatmentViewController
//                            endodonticTreatmentVC.patient = self.patient
//                            self.navigationController?.pushViewController(endodonticTreatmentVC, animated: true)
//                        } else if formNames.contains(kDentalCrown) {
//                            let dentalCrownVC = self.storyboard?.instantiateViewControllerWithIdentifier("kDentureCrownVC") as! DentalCrownViewController
//                            dentalCrownVC.patient = self.patient
//                            self.navigationController?.pushViewController(dentalCrownVC, animated: true)
//                        } else if formNames.contains(kOpiodForm) {
//                            let opiodVC = self.storyboard?.instantiateViewControllerWithIdentifier("kOpiodViewController") as! OpiodViewController
//                            opiodVC.patient = self.patient
//                            self.navigationController?.pushViewController(opiodVC, animated: true)
//                        }else if formNames.contains(kGeneralConsent) {
//                            let generalVC = self.storyboard?.instantiateViewControllerWithIdentifier("kGeneralConsentVC") as! GeneralConsentViewController
//                            generalVC.patient = self.patient
//                            self.navigationController?.pushViewController(generalVC, animated: true)
//                        } else if formNames.contains(kFeedbackForm) {
//                            let feedbackFormVC = self.storyboard?.instantiateViewControllerWithIdentifier("kFeedbackFormVC") as! FeedbackFormViewController
//                            self.navigationController?.pushViewController(feedbackFormVC, animated: true)
//                        } else {
//                            NSNotificationCenter.defaultCenter().postNotificationName(kFormsCompletedNotification, object: nil)
//                            self.navigationController?.popToRootViewControllerAnimated(true)
//                        }
//                    } else {
//                        self.buttonSubmit.hidden = false
//                        self.buttonBack.hidden = false
//                    }
//                })
//            }
//        }
//    }
}


extension MedicalHistoryFormViewController : UICollectionViewDataSource {
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return collectionView.tag == 1 ? patient.medicalHistoryQuestions2.count : patient.medicalHistoryQuestions3.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cellMedicalHistory", for: indexPath) as! MedicalHistoryCollectionViewCell
        let obj = collectionView.tag == 1 ? patient.medicalHistoryQuestions2[indexPath.item] : patient.medicalHistoryQuestions3[indexPath.item]
        cell.configureCellOption(obj)
        return cell
    }
}

extension MedicalHistoryFormViewController : UITableViewDataSource, UITableViewDelegate {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView.tag == 1 {
            return patient.medicalHistoryQuestions1.count
        } else {
            let currentIndex = tableView.tag - 2
            let objects = patient.medicalHistoryQuestions4.filter { (obj) -> Bool in
                return obj.index >= (currentIndex * 12) && obj.index < ((currentIndex + 1) * 12)
            }
            return tableView.tag == 5 ? objects.count : objects.count
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if tableView.tag == 1 {
            return 26
        } else {
            let currentIndex = tableView.tag - 2
            let index = (currentIndex * 12) + indexPath.row
            return index == 4 || index == 17 ? 28 : 14
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView.tag == 1 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "cellMedicalHistoryFrom1", for: indexPath) as! MedicalHistoryFormTableViewCell1
            let obj = patient.medicalHistoryQuestions1[indexPath.row]
            cell.configureCell(obj)
            return cell
        } else {
            let currentIndex = tableView.tag - 2
            let index = (currentIndex * 12) + indexPath.row
            let cell = index == 4 || index == 17 ? tableView.dequeueReusableCell(withIdentifier: "cellMedicalHistoryFrom41", for: indexPath) as! MedicalHistoryStep1TableViewCell : tableView.dequeueReusableCell(withIdentifier: "cellMedicalHistoryFrom4", for: indexPath) as! MedicalHistoryStep1TableViewCell
            let obj = patient.medicalHistoryQuestions4[index]
            cell.configureCellOption(obj)
            let stringObj = ["Dementia","Artificial Joints","Asthma","Diabetes","Epilepsy or Seizures","Heart Condition","Heart Pacemaker","Kidney Problem","Lung disease / COPS","Stroke","Tuberculosis"]
            if let selected = obj.isSelected {
                cell.labelTitle.textColor = selected && stringObj.contains(obj.question) ? UIColor.red : UIColor.black
                cell.labelTitle.font = UIFont(name:selected ? "WorkSans-Medium" :  "WorkSans-Regular", size: cell.labelTitle.font.pointSize)
                cell.labelOther?.textColor = selected && stringObj.contains(obj.question) ? UIColor.red : UIColor.black
                cell.labelOther?.font = UIFont(name:selected ? "WorkSans-Medium" :  "WorkSans-Regular", size: cell.labelTitle.font.pointSize)
                cell.buttonYes.isSelected = selected
            } else {
                cell.labelTitle.textColor = UIColor.black
                cell.labelTitle.font = UIFont(name: "WorkSans-Regular", size: cell.labelTitle.font.pointSize)
                cell.labelOther?.textColor = UIColor.black
                cell.labelOther?.font = UIFont(name: "WorkSans-Regular", size: cell.labelTitle.font.pointSize)
                cell.buttonYes.deselectAllButtons()
            }
            return cell
        }
    }
}
