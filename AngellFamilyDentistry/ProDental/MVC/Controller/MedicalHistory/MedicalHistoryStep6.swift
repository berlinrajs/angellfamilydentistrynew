//
//  MedicalHistoryStep6.swift
//  AngellFamilyDentistry
//
//  Created by Berlin Raj on 25/04/17.
//  Copyright © 2017 SRS. All rights reserved.
//

import UIKit

class MedicalHistoryStep6: MCViewController {
    
    @IBOutlet weak var medication1: MCTextField!
    
    @IBOutlet weak var medication2: MCTextField!
    
    @IBOutlet weak var medication3: MCTextField!
    
    @IBOutlet weak var medication4: MCTextField!
    
    @IBOutlet weak var medication5: MCTextField!
    
    @IBOutlet weak var medication6: MCTextField!
    
    @IBOutlet weak var medication7: MCTextField!

    @IBOutlet weak var condition1: MCTextField!
    
    @IBOutlet weak var condition2: MCTextField!

    @IBOutlet weak var condition3: MCTextField!

    @IBOutlet weak var condition4: MCTextField!

    @IBOutlet weak var condition5: MCTextField!

    @IBOutlet weak var condition6: MCTextField!

    @IBOutlet weak var condition7: MCTextField!

    @IBOutlet weak var radioButtonScan: RadioButton!
    
    @IBOutlet weak var customView: UIView!
    

    override func viewDidLoad() {
        super.viewDidLoad()
        load()
        
        
        // Do any additional setup after loading the view.
    }
    
    
    func save()
    {
        patient.scanMedicationIndex = radioButtonScan.selected == nil ? 0 : radioButtonScan.selected.tag
        if patient.scanMedicationIndex == 2{
        patient.medication1 = medication1.text
        patient.medication2 = medication2.text
        patient.medication3 = medication3.text
        patient.medication4 = medication4.text
        patient.medication5 = medication5.text
        patient.medication6 = medication6.text
        patient.medication7 = medication7.text
        
        patient.condition1 = condition1.text
        patient.condition2 = condition2.text
        patient.condition3 = condition3.text
        patient.condition4 = condition4.text
        patient.condition5 = condition5.text
        patient.condition6 = condition6.text
        patient.condition7 = condition7.text
        }else{
            patient.medication1 = ""
            patient.medication2 = ""
            patient.medication3 = ""
            patient.medication4 = ""
            patient.medication5 = ""
            patient.medication6 = ""
            patient.medication7 = ""
            
            patient.condition1 = ""
            patient.condition2 = ""
            patient.condition3 = ""
            patient.condition4 = ""
            patient.condition5 = ""
            patient.condition6 = ""
            patient.condition7 = ""
        }
        
        
    }
    
    
    
    @IBAction func radioButtonAction(_ sender: RadioButton) {
        
        if sender.tag == 2
        {
            customView.isUserInteractionEnabled = true
            customView.alpha = 1
        }else if sender.tag == 1{
            customView.isUserInteractionEnabled = false
            customView.alpha = 0.5
            medication1.text = ""
            medication2.text = ""
            medication3.text = ""
            medication4.text = ""
            medication5.text = ""
            medication6.text = ""
            medication7.text = ""
            
            condition1.text = ""
            condition2.text = ""
            condition3.text = ""
            condition4.text = ""
            condition5.text = ""
            condition6.text = ""
            condition7.text = ""
            
        }
        
        
    }
    
    func load()
    {
        
        radioButtonScan.setSelectedWithTag(patient.scanMedicationIndex)
        if patient.scanMedicationIndex == 2
        {
            customView.isUserInteractionEnabled = true
            customView.alpha = 1
        }else if patient.scanMedicationIndex == 1{
            customView.isUserInteractionEnabled = false
            customView.alpha = 0.5
           
        }

        
        medication1.text = patient.medication1
        medication2.text = patient.medication2
        medication3.text = patient.medication3
        medication4.text = patient.medication4
        medication5.text = patient.medication5
        medication6.text = patient.medication6
        medication7.text = patient.medication7
        
        condition1.text = patient.condition1
        condition2.text = patient.condition2
        condition3.text = patient.condition3
        condition4.text = patient.condition4
        condition5.text = patient.condition5
        condition6.text = patient.condition6
        condition7.text = patient.condition7
        
    }
    @IBAction override func buttonBackAction() {
         save()
        super.buttonBackAction()
    }
   
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @IBAction func buttonActionNext(_ sender: Any) {
        
        save()
        let medicalHistoryStep5VC = medicalStoryBoard.instantiateViewController(withIdentifier: "kMedicalHistoryStep5VC") as! MedicalHistoryStep5ViewController
        medicalHistoryStep5VC.patient = patient
        self.navigationController?.pushViewController(medicalHistoryStep5VC, animated: true)

    }
    
    
    

}
