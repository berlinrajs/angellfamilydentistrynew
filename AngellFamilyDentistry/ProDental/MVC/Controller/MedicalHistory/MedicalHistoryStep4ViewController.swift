//
//  MedicalHistoryStep4ViewController.swift
//  ABC Clinic
//
//  Created by Leojin Bose on 2/26/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

class MedicalHistoryStep4ViewController: MCViewController {

    var currentIndex : Int = 0
    
    @IBOutlet weak var buttonNext: MCButton!
    @IBOutlet weak var tableViewQuestions: UITableView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var buttonVerified: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.activityIndicator.stopAnimating()
        buttonBack?.isHidden = false
    }
    
    func fetchData() {
        
    }
    
   
    @IBAction override func buttonBackAction() {
        if currentIndex == 0 {
            self.navigationController?.popViewController(animated: true)
        } else {
            buttonBack?.isUserInteractionEnabled = false
            buttonNext.isUserInteractionEnabled = false
            self.buttonVerified.isSelected = true
            self.activityIndicator.startAnimating()
            let delayTime = DispatchTime.now() + Double(Int64(0.4 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)
            DispatchQueue.main.asyncAfter(deadline: delayTime) {
                self.activityIndicator.stopAnimating()
                self.currentIndex = self.currentIndex - 1
                self.tableViewQuestions.reloadData()
                self.buttonBack?.isUserInteractionEnabled = true
                self.buttonNext.isUserInteractionEnabled = true
            }
        }
    }
    
    @IBAction func buttonVerifiedAction(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
    }
    
    @IBAction func buttonActionNext(_ sender: AnyObject) {
//        if self.fetchCompleted == true {
            if ((currentIndex + 1) * 16) < self.patient.medicalHistoryQuestions4.count {
//                if let _ = findEmptyValue() {
//                    let alert = Extention.alert("MISSING INFORMATION.\nPLEASE SELECT YES OR NO")
//                    self.presentViewController(alert, animated: true, completion: nil)
//                } else {
                if !buttonVerified.isSelected {
                    self.showAlert("PLEASE CONFIRM THAT YOU HAVE ANSWERED ABOVE QUESTIONS")
                } else {
                    buttonBack?.isUserInteractionEnabled = false
                    buttonNext.isUserInteractionEnabled = false
                    self.buttonVerified.isSelected = false
                    self.activityIndicator.startAnimating()
                    let delayTime = DispatchTime.now() + Double(Int64(0.4 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)
                    DispatchQueue.main.asyncAfter(deadline: delayTime) {
                        self.activityIndicator.stopAnimating()
                        self.currentIndex = self.currentIndex + 1
                        self.tableViewQuestions.reloadData()
                        self.buttonBack?.isUserInteractionEnabled = true
                        self.buttonNext.isUserInteractionEnabled = true
                    }
                }
                
            } else {
                if !buttonVerified.isSelected {
                    self.showAlert("PLEASE CONFIRM THAT YOU HAVE ANSWERED ABOVE QUESTIONS")
                } else {
                    patient.medicalHistoryQuestions4 = self.patient.medicalHistoryQuestions4
                    
                    CustomAlert.yesOrNoAlertView().showWithQuestion("ARE YOU TAKING MEDICATIONS?", completion: { (index) in
                        if index == 1 {
                            
                            let medicalHistoryStep5VC = medicalStoryBoard.instantiateViewController(withIdentifier: "medicalStep6") as! MedicalHistoryStep6
                            medicalHistoryStep5VC.patient = self.patient
                            self.navigationController?.pushViewController(medicalHistoryStep5VC, animated: true)
                        }else{
                            self.patient.medication1 = ""
                            self.patient.medication2 = ""
                            self.patient.medication3 = ""
                            self.patient.medication4 = ""
                            self.patient.medication5 = ""
                            self.patient.medication6 = ""
                            self.patient.medication7 = ""
                            
                            self.patient.condition1 = ""
                            self.patient.condition2 = ""
                            self.patient.condition3 = ""
                            self.patient.condition4 = ""
                            self.patient.condition5 = ""
                            self.patient.condition6 = ""
                            self.patient.condition7 = ""
                            self.patient.scanMedicationIndex = 0
                            let medicalHistoryStep5VC = medicalStoryBoard.instantiateViewController(withIdentifier: "kMedicalHistoryStep5VC") as! MedicalHistoryStep5ViewController
                            medicalHistoryStep5VC.patient = self.patient
                            self.navigationController?.pushViewController(medicalHistoryStep5VC, animated: true)
                        }
                    })
                   
                }
            }
//        }
    }
    
    func findEmptyValue() -> PDOption? {
        let objects = self.patient.medicalHistoryQuestions4.filter { (obj) -> Bool in
            return obj.index >= (currentIndex * 16) && obj.index < ((currentIndex + 1) * 16)
        }
        for question in objects {
            if question.isSelected == nil {
                return question
            }
        }
        return nil
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

extension MedicalHistoryStep4ViewController : MedicalHistoryCellDelegate {
    func radioButtonAction(_ sender: RadioButton) {
        if sender.tag == 4 {
            let obj = self.patient.medicalHistoryQuestions4[sender.tag]
            if obj.isSelected == true {
                ArtificialPopUp.popUpView().showInViewController(self, completion: { (popUpView, textFieldLong, jointType) in
                    popUpView.close()
                    if textFieldLong.isEmpty || !textFieldLong.text!.isValidYear {
//                        self.showAlert("ENTERED YEAR IS NOT VALID")
                        sender.isSelected = false
                        obj.isSelected = false
                        obj.answer = ""
                    } else if jointType.count == 0 {
//                        self.showAlert("JOINT TYPE NOT SELECTED")
                        sender.isSelected = false
                        obj.isSelected = false
                        obj.answer = ""
                    } else {
                        obj.answer = jointType + ", " + textFieldLong.text!
                    }
                })
            } else {
                obj.answer = ""
            }
        } else if sender.tag == 17 {
            let obj = self.patient.medicalHistoryQuestions4[sender.tag]
            if obj.isSelected == true {
                
                PopupTextView.popUpView().showWithPlaceHolder("TYPE DETAILS HERE", completion: { (popUp, textView) in
                    if textView.isEmpty{
                        obj.answer = ""
                        sender.isSelected = false
                        obj.isSelected = false
                    }else{
                        obj.answer = textView.textValue!
                    }
                    popUp.close()
                })
                
//                PopupTextField.popUpView().showInViewController(self, WithTitle: "MORE DETAILS?", placeHolder: "TYPE HERE", textFormat: TextFormat.Default, completion: { (popUpView, textField) in
//                    popUpView.close()
//                    if !textField.isEmpty {
//                        obj.answer = textField.text!
//                    } else {
//                        sender.selected = false
//                        obj.answer = ""
//                        obj.isSelected = false
//                    }
//                })
            } else {
                obj.answer = ""
            }
        }
        self.tableViewQuestions.reloadData()
    }
}


extension MedicalHistoryStep4ViewController : UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let objects = self.patient.medicalHistoryQuestions4.filter { (obj) -> Bool in
            return obj.index >= (currentIndex * 16) && obj.index < ((currentIndex + 1) * 16)
        }
        return objects.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cellMedicalHistoryStep4", for: indexPath) as! MedicalHistoryStep1TableViewCell
        let index = (currentIndex * 16) + indexPath.row
        let obj = self.patient.medicalHistoryQuestions4[index]
        cell.configureCellOption(obj)
        if let selected = obj.isSelected {
            cell.buttonYes.isSelected = selected
        } else {
            cell.buttonYes.deselectAllButtons()
        }
        cell.buttonYes.tag = index
        cell.buttonNo.tag = index
        cell.delegate = self
        return cell
    }
}
