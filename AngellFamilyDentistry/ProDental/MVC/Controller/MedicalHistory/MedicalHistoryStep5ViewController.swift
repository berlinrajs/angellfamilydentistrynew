//
//  MedicalHistoryStep5ViewController.swift
//  ABC Clinic
//
//  Created by Leojin Bose on 2/29/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

class MedicalHistoryStep5ViewController: MCViewController {
    
    @IBOutlet weak var signatureView: SignatureView!
    @IBOutlet weak var labelDate: DateLabel!
    @IBOutlet weak var radioButton: RadioButton!
    @IBOutlet var viewPopup: PDView!
    @IBOutlet weak var viewShadow: UIView!
    @IBOutlet weak var textViewOthers: PDTextView!
    @IBOutlet weak var textViewComments: MCTextView!
    @IBOutlet weak var radioAntibiotics: RadioButton!
    
    
    
    var medicationAndDosage: String = ""
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        signatureView.layer.cornerRadius = 3.0
//        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(MedicalHistoryStep5ViewController.setDateOnLabel))
//        tapGesture.numberOfTapsRequired = 1
//        labelDate.addGestureRecognizer(tapGesture)
        
        labelDate.todayDate = patient.dateToday
        loadValues()
        // Do any additional setup after loading the view.
    }
    
    
    
    
    func loadValues() {
        //labelDate.setDate = patient.isDate1Tapped
        radioAntibiotics.setSelectedWithTag(patient.antibioticsUsed == true ? 1 : 2)
        radioButton.setSelectedWithTag(patient.otherIllness == nil ? 2 : 1)
        textViewComments.textValue = patient.comments
        //textViewComments.checkEdited()
        self.medicationAndDosage = patient.medicationAndDosage
    }
    
    @IBAction func buttonTreatmentAction(WithSender sender: UIButton) {
        if sender.tag == 1 {
            PopupTextField.popUpView().showInViewController(self, WithTitle: "What is the medication and dosage you are supposed to take?", placeHolder: "TYPE HERE", textFormat: TextFormat.Default, completion: { (popUpView, textField) in
                popUpView.close()
                if !textField.isEmpty {
                    self.medicationAndDosage = textField.text!
                } else {
                    self.medicationAndDosage = ""
                    sender.isSelected = false
                }
            })
        } else {
            self.medicationAndDosage = ""
        }
    }
    
    func setValues() {
        patient.isDate1Tapped = labelDate.dateTapped
        patient.comments = textViewComments.textValue
        patient.antibioticsUsed = radioAntibiotics.isSelected
        patient.signature1 = signatureView.signatureImage()
        patient.medicationAndDosage = self.medicationAndDosage
        
     //   patient.scanMedication = checkBoxScan.isSelected
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setDateOnLabel() {
        labelDate.text = patient.dateToday
        labelDate.textColor = UIColor.black
    }
    
    @IBAction override func buttonBackAction() {
        self.setValues()
        super.buttonBackAction()
    }
    
    
    
    @IBAction func buttonActionNext(_ sender: AnyObject) {
        self.view.endEditing(true)
        if !signatureView.isSigned() {
            self.showAlert("PLEASE SIGN THE FORM")
        } else if !labelDate.dateTapped {
            self.showAlert("PLEASE SELECT DATE")
        } else {
            self.setValues()
            let medicalHistoryFormVC = medicalStoryBoard.instantiateViewController(withIdentifier: "kMedicalHistoryFormVC") as! MedicalHistoryFormViewController
            medicalHistoryFormVC.patient = patient
            self.navigationController?.pushViewController(medicalHistoryFormVC, animated: true)
        }
    }
    
   
    
    @IBAction func buttonActionYesNo(_ sender: RadioButton) {
        if sender.tag == 2 {
            
            patient.otherIllness = ""
        } else {
           
            PopupTextView.popUpView().showWithPlaceHolder("IF YES TYPE HERE", completion: { (popUp, textView) in
                if textView.isEmpty{
                    self.patient.otherIllness = ""
                    self.radioButton.setSelectedWithTag(2)
                    
                }else{
                    self.patient.otherIllness = textView.textValue!
                }
                popUp.close()
            })
        }
    }
    
   
}


