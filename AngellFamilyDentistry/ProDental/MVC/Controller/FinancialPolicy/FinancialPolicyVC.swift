//
//  FinancialPolicyViewController.swift
//  WestgateSmiles
//
//  Created by Leojin Bose on 2/25/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

class FinancialPolicyVC: MCViewController {
    
    @IBOutlet weak var signatureView: SignatureView!
    @IBOutlet weak var labelDate: DateLabel!
    @IBOutlet weak var labelName: UILabel!
    @IBOutlet weak var labelContent: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        labelName.text = patient.isParent == true ? patient.parentFirstName + " " + patient.parentLastName : patient.fullName
        labelContent.text = labelContent.text!.replacingOccurrences(of: "kClinicName", with: MCAppDetails.sharedAppDetails!.clinicName)
        labelDate.todayDate = patient.dateToday
    }
    
    
    @IBAction func buttonActionSubmit() {
        self.view.endEditing(true)
        if !signatureView.isSigned() {
            self.showAlert("PLEASE SIGN THE FORM")
        } else if !labelDate.dateTapped {
            self.showAlert("PLEASE SELECT DATE")
        } else {
            
            let financialPolicyFormVC = self.storyboard?.instantiateViewController(withIdentifier: "kFinancialPolicyFormVC") as! FinancialPolicyFormVC
            financialPolicyFormVC.patient = patient
            financialPolicyFormVC.signature1 = signatureView.signatureImage()
            self.navigationController?.pushViewController(financialPolicyFormVC, animated: true)
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
