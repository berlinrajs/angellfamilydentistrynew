//
//  FinancialPolicyFormViewController.swift
//  WestgateSmiles
//
//  Created by Leojin Bose on 2/25/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

class FinancialPolicyFormVC: MCViewController {

    var signature1: UIImage!
    
    @IBOutlet weak var labelDate: UILabel!
    @IBOutlet weak var imageViewSignature: UIImageView!
    @IBOutlet weak var labelClinicName: UILabel!
    
    @IBOutlet weak var labelPatientName: UILabel!
    @IBOutlet weak var labelDOB: UILabel!
    
    @IBOutlet weak var labelSignedBy: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        imageViewSignature.image = self.signature1
        labelDate.text = patient.dateToday
        labelClinicName.text = MCAppDetails.sharedAppDetails!.clinicName
        labelPatientName.text = patient.fullName
        labelDOB.text = patient.dateOfBirth
        labelSignedBy.text = patient.isParent == true ? patient.parentFirstName + " " + patient.parentLastName : patient.fullName
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
