//
//  EndodonticTreatmentViewController.swift
//   Angell Family Dentistry
//
//  Created by Office on 2/23/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

class EndodonticTreatmentViewController: MCViewController {

//    var patient : PDPatient!
//    var isFromPreviousForm : Bool! = true
//    @IBOutlet weak var buttonBack: PDButton!
    
    
    @IBOutlet weak var labelName: UILabel!
    @IBOutlet weak var signatureView: SignatureView!
    @IBOutlet weak var labelDate: DateLabel!
    @IBOutlet weak var labelText: UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        labelName.text = patient.fullName
        labelText.text = labelText.text?.replacingOccurrences(of: "kDoctorName", with: patient.dentistName)
        
        labelDate.todayDate = patient.dateToday
        loadValues()
        // Do any additional setup after loading the view.
    }
    
    func loadValues() {
        //labelDate.setDate = patient.isDate1Tapped
    }
    
    func setValues() {
        patient.isDate1Tapped = labelDate.dateTapped
        patient.signature1 = signatureView.signatureImage()
    }
    

    @IBAction override func buttonBackAction() {
        self.setValues()
        super.buttonBackAction()
    }
    
    @IBAction func buttonActionNext(_ sender: AnyObject) {
        if !signatureView.isSigned() {
            self.showAlert("PLEASE SIGN THE FORM")
        } else if !labelDate.dateTapped {
            self.showAlert("PLEASE SELECT DATE")
        } else {
            setValues()
            let endodonticTreatmentFormVC = self.storyboard?.instantiateViewController(withIdentifier: "kEndodonticTreatmentFormVC") as! EndodonticTreatmentFormViewController
            endodonticTreatmentFormVC.patient = patient
            self.navigationController?.pushViewController(endodonticTreatmentFormVC, animated: true)
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
