//
//  DentalImplantsFormViewController.swift
//   Angell Family Dentistry
//
//  Created by Office on 2/24/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

class DentalImplantsFormViewController: MCViewController {

    var textRanges : [NSRange]! = [NSRange]()
    
    @IBOutlet var labelDate: UILabel!
    @IBOutlet var imageViewSignature: UIImageView!
    
    @IBOutlet weak var labelDetails: UILabel!
    @IBOutlet weak var scrollView: UIScrollView!
    
    @IBOutlet var viewSignature: UIView!
    @IBOutlet var labelText1: UILabel!
    @IBOutlet var labelText2: UILabel!
    @IBOutlet var labelText3: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
//        buttonSubmit.backgroundColor = UIColor.greenColor()
        
        imageViewSignature.image = patient.signature1
        labelDate.text = patient.dateToday
        
        
        
        var patientInfo = "Part 1 -Patient & Doctor Information\n\nPatient Name:"
        
        let patientName = getText(patient.fullName)
        patientInfo = patientInfo + " \(patientName)"
        textRanges.append(patientInfo.rangeOfText(patientName))
        
        patientInfo = patientInfo + "\nD.O.B: \(getText(patient.dateOfBirth))"
        textRanges.append(patientInfo.rangeOfText(getText(patient.dateOfBirth)))
        
        patientInfo = patientInfo + "\nDoctor Name: \(getText(patient.dentistName))"
        textRanges.append(patientInfo.rangeOfText(getText(patient.dentistName)))
        
        let attributedString = NSMutableAttributedString(string: patientInfo)
        for range in textRanges {
            attributedString.addAttribute(NSUnderlineStyleAttributeName, value: NSUnderlineStyle.styleSingle.rawValue, range: range)
        }
        
        attributedString.addAttributes([NSBaselineOffsetAttributeName: NSNumber(value: 0 as Float)], range: NSMakeRange(0, attributedString.length))
        labelDetails.attributedText = attributedString

        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        scrollView.contentSize = CGSize(width: screenSize.width, height: screenSize.height * 4)
        
        var viewSignatureFrame = self.viewSignature.frame
        viewSignatureFrame.origin.y = (screenSize.height * 4) - 120
        viewSignatureFrame.origin.x = 60.0
        viewSignatureFrame.size.width = screenSize.width - 120.0
        viewSignature.frame = viewSignatureFrame
        self.scrollView.addSubview(viewSignature)
        
        let labelFrame1 = CGRect(x: 30.0, y: screenSize.height + 30.0, width: screenSize.width - 60.0, height: screenSize.height - 60.0)
        let labelFrame2 = CGRect(x: 30.0, y: (screenSize.height * 2) + 30.0, width: screenSize.width - 60.0, height: screenSize.height - 60.0)
        let labelFrame3 = CGRect(x: 30.0, y: (screenSize.height * 3) + 30.0, width: screenSize.width - 60.0, height: screenSize.height - 180.0)

        labelText1.frame = labelFrame1
        labelText2.frame = labelFrame2
        labelText3.frame = labelFrame3
        scrollView.addSubview(labelText1)
        scrollView.addSubview(labelText2)
        scrollView.addSubview(labelText3)

    }
//    
//    
//    
//    
//    @IBAction func buttonActionSubmit(sender: AnyObject) {
//
//        if !Reachability.isConnectedToNetwork() {
//            let alertController = UIAlertController(title: "Angell Family Dentistry", message: "Your device is not connected to internet. Please go to settings to connect.", preferredStyle: UIAlertControllerStyle.Alert)
//            let alertOkAction = UIAlertAction(title: "Settings", style: UIAlertActionStyle.Destructive) { (action) -> Void in
//                let settingsUrl = NSURL(string: UIApplicationOpenSettingsURLString)
//                if let url = settingsUrl {
//                    UIApplication.sharedApplication().openURL(url)
//                }
//            }
//            let alertCancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.Destructive) { (action) -> Void in
//                
//            }
//            alertController.addAction(alertOkAction)
//            alertController.addAction(alertCancelAction)
//            self.presentViewController(alertController, animated: true, completion: nil)
//            return
//        }
//        let pdfManager = PDFManager()
//        pdfManager.authorizeDrive(self.view) { (success) -> Void in
//            if success {
//                self.buttonSubmit.hidden = true
//                self.buttonBack.hidden = true
//                pdfManager.createPDFForScrollView(self.scrollView, fileName: "DENTAL_IMPLANTS", patient: self.patient, completionBlock: { (finished) -> Void in
//                    if finished {
//                        self.patient = self.patient.patient()
//                        let formNames = (self.patient.selectedForms as NSArray).valueForKey("formTitle") as! [String]
//                        if formNames.contains(kToothExtraction) {
//                            let toothExtractionVC = self.storyboard?.instantiateViewControllerWithIdentifier("kToothExtractionVC") as! ToothExtractionViewController
//                            toothExtractionVC.patient = self.patient
//                            self.navigationController?.pushViewController(toothExtractionVC, animated: true)
//                        } else if formNames.contains(kPartialDenture) {
//                            let dentureAdjustmentVC = self.storyboard?.instantiateViewControllerWithIdentifier("kDentureAdjustmentVC") as! DentureAdjustmentViewController
//                            dentureAdjustmentVC.patient = self.patient
//                            self.navigationController?.pushViewController(dentureAdjustmentVC, animated: true)
//                        } else if formNames.contains(kInofficeWhitening) {
//                            let inOfficeWhiteningVC = self.storyboard?.instantiateViewControllerWithIdentifier("kInOfficeWhiteningVC") as! InOfficeWhiteningViewController
//                            inOfficeWhiteningVC.patient = self.patient
//                            self.navigationController?.pushViewController(inOfficeWhiteningVC, animated: true)
//                        } else if formNames.contains(kTreatmentOfChild) {
//                            let childTreatmentVC = self.storyboard?.instantiateViewControllerWithIdentifier("kChildTreatmentVC") as! ChildTreatmentViewController
//                            childTreatmentVC.patient = self.patient
//                            self.navigationController?.pushViewController(childTreatmentVC, animated: true)
//                        } else if formNames.contains(kEndodonticTreatment) {
//                            let endodonticTreatmentVC = self.storyboard?.instantiateViewControllerWithIdentifier("kEndodonticTreatmentVC") as! EndodonticTreatmentViewController
//                            endodonticTreatmentVC.patient = self.patient
//                            self.navigationController?.pushViewController(endodonticTreatmentVC, animated: true)
//                        } else if formNames.contains(kDentalCrown) {
//                            let dentalCrownVC = self.storyboard?.instantiateViewControllerWithIdentifier("kDentureCrownVC") as! DentalCrownViewController
//                            dentalCrownVC.patient = self.patient
//                            self.navigationController?.pushViewController(dentalCrownVC, animated: true)
//                        }else if formNames.contains(kOpiodForm) {
//                            let opiodVC = self.storyboard?.instantiateViewControllerWithIdentifier("kOpiodViewController") as! OpiodViewController
//                            opiodVC.patient = self.patient
//                            self.navigationController?.pushViewController(opiodVC, animated: true)
//                        } else if formNames.contains(kGeneralConsent) {
//                            let generalVC = self.storyboard?.instantiateViewControllerWithIdentifier("kGeneralConsentVC") as! GeneralConsentViewController
//                            generalVC.patient = self.patient
//                            self.navigationController?.pushViewController(generalVC, animated: true)
//                        }else if formNames.contains(kFeedbackForm) {
//                            let feedbackFormVC = self.storyboard?.instantiateViewControllerWithIdentifier("kFeedbackFormVC") as! FeedbackFormViewController
//                            self.navigationController?.pushViewController(feedbackFormVC, animated: true)
//                        } else {
//                            NSNotificationCenter.defaultCenter().postNotificationName(kFormsCompletedNotification, object: nil)
//                            self.navigationController?.popToRootViewControllerAnimated(true)
//                        }
//                    } else {
//                        self.buttonSubmit.hidden = false
//                        self.buttonBack.hidden = false
//                    }
//                })
//            }
//        }
//    }
    
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension DentalImplantsFormViewController : UIScrollViewDelegate {
    
}
