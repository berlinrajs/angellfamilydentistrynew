//
//  ConsentTextView.swift
//  MConsentSample
//
//  Created by Berlin on 23/03/17.
//  Copyright © 2017 SRS Web Solutions. All rights reserved.
//

import UIKit

class ConsentControls: NSObject {

    class func ConsentTextView() -> UITextView {
        let textView = Bundle.main.loadNibNamed("ConsentControls", owner: nil, options: nil)!.first as! UITextView
        textView.textContainerInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0);
        return textView
    }
    
    class func ConsentSignature() -> ConsentSignature {
        return Bundle.main.loadNibNamed("ConsentControls", owner: nil, options: nil)![4] as! ConsentSignature
    }
    
    class func ConsentDate() -> ConsentDate {
        return Bundle.main.loadNibNamed("ConsentControls", owner: nil, options: nil)![3] as! ConsentDate
    }
    
    class func ConsentSignatureDate() -> PDFSignature {
        return Bundle.main.loadNibNamed("ConsentControls", owner: nil, options: nil)![2] as! PDFSignature
    }
    
    class func PDFLabel() -> PDFLabel {
        return Bundle.main.loadNibNamed("ConsentControls", owner: nil, options: nil)![1] as! PDFLabel
    }
}


class ConsentSignature: UIView {
    @IBOutlet weak var signatureView: SignatureView!
    @IBOutlet weak var labelTitle: UILabel!
}

class ConsentDate: UIView {
    @IBOutlet weak var dateLabel: DateLabel!
    @IBOutlet weak var labelTitle: UILabel!
}


class PDFSignature : UIView{
    @IBOutlet weak var signatureImageView : UIImageView!
    @IBOutlet weak var labelTitleSignature : UILabel!
    @IBOutlet weak var labelDate : UILabel!
}

class PDFLabel : UIView{
    @IBOutlet weak var labelTitle : MCLabel!
    
    func setText(_ ques : MCQuestion) {
        var ansEdited = "__________"
        if let ans = ques.answer{
            ansEdited = ans
        }
        let str = (ques.question.uppercased() + " : " + ansEdited) as NSString
        let range = str.range(of: ansEdited)
        let attributedString = NSMutableAttributedString(string: str as String)
        attributedString.addAttribute(NSUnderlineStyleAttributeName, value: NSUnderlineStyle.styleSingle.rawValue, range: range)
        labelTitle.attributedText = attributedString
    }
}
