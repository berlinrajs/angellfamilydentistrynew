//
//  PatientCareFormCell.swift
//  NewEnglandDental
//
//  Created by Berlin Raj on 18/11/16.
//  Copyright © 2016 SRS Web Solutions. All rights reserved.
//

import UIKit

class PatientCareFormCell: UITableViewCell {

    @IBOutlet weak var labelTitle: UILabel!
    
    @IBOutlet weak var labelSubTitle: UILabel!
    
    @IBOutlet weak var labelAnswer: UILabel!
    @IBOutlet weak var labelAnswer2: UILabel?
    @IBOutlet weak var buttonCheck: UIButton!
    @IBOutlet weak var constraintTitleWidth: NSLayoutConstraint?
    
    var question: MCQuestion!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func configWithQuestion(question: MCQuestion) {
        self.question = question
        
        self.labelTitle.text = question.question.components(separatedBy: ",")[0]
        if question.question.components(separatedBy: ",").count > 1 {
            self.labelSubTitle.text = question.question.components(separatedBy: ",")[1]
        } else {
            self.labelSubTitle.text = ""
        }
        self.labelAnswer.text = question.answer
        self.labelAnswer.isHidden = !question.isAnswerRequired
        
        buttonCheck.isSelected = question.selectedOption
    }
    
    func configWithSubQuestion(question: MCQuestion) {
        self.question = question
        
        self.labelTitle.text = question.question.components(separatedBy: " & ")[0]
        if question.question.components(separatedBy: " & ").count > 1 {
            self.labelSubTitle.text = question.question.components(separatedBy: " & ")[1]
        } else {
            self.labelSubTitle.text = ""
        }
        
        self.labelAnswer.text = question.answer?.components(separatedBy: " & ")[0]
        if (question.answer?.components(separatedBy: " & ").count)! > 1 {
            self.labelAnswer2?.text = question.answer?.components(separatedBy: " & ")[1]
        } else {
            self.labelAnswer2?.text = ""
        }
        
        labelAnswer.isHidden = false
        labelAnswer2?.isHidden = false
        
        buttonCheck.isSelected = question.selectedOption
    }
    
//    override func layoutSubviews() {
//        if let title = self.labelTitle.text {
//            self.constraintTitleWidth?.constant = min(80.0, title.widthWithConstrainedHeight(CGRectGetHeight(self.labelTitle.frame), font: self.labelTitle.font!))
//        }
//        super.layoutSubviews()
//    }
}
