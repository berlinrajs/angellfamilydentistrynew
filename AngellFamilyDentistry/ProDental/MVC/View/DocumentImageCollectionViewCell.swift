//
//  DocumentImageCollectionViewCell.swift
//  LuxDentalAuto
//
//  Created by Berlin Raj on 08/07/17.
//  Copyright © 2017 srs. All rights reserved.
//

import UIKit

class DocumentImageCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var previewImageView: MCImageView!
}
