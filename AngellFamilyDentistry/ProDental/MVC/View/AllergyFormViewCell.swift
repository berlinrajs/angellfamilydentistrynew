//
//  AllergyFormViewCell.swift
//  MConsent Dentrix
//
//  Created by Berlin Raj on 30/05/17.
//  Copyright © 2017 SRS Web Solutions. All rights reserved.
//

import UIKit

class AllergyFormViewCell: UITableViewCell {

    @IBOutlet var labelAllergyName1 : UILabel!
    @IBOutlet var labelAllergyName2 : UILabel!
    
    @IBOutlet var labelAllergyDescription1 : UILabel!
    @IBOutlet var labelAllergyDescription2 : UILabel!
    
    @IBOutlet var buttonSelection1 : UIButton!
    @IBOutlet var buttonSelection2 : UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    func configureCell(allgy1 : Allergy, allg2: Allergy?)  {
        labelAllergyName1.text = allgy1.allergyName.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        buttonSelection1.isSelected = allgy1.isSelected
        if allgy1.allergyDescription != "no" && allgy1.allergyDescription != "yes" {
            labelAllergyDescription1.text = allgy1.allergyDescription
            labelAllergyDescription1.isHidden = false
        } else {
            labelAllergyDescription1.isHidden = true
        }
        
        if let allergy2 = allg2 {
            labelAllergyName2.text = allergy2.allergyName.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
            buttonSelection2.isSelected = allergy2.isSelected
            
            if allergy2.allergyDescription != "no" && allergy2.allergyDescription != "yes" {
                labelAllergyDescription2.text = allergy2.allergyDescription
                labelAllergyDescription2.isHidden = false
            } else {
                labelAllergyDescription2.isHidden = true
            }
            
            buttonSelection2.isHidden = false
        } else {
            labelAllergyName2.text = ""
            buttonSelection2.isHidden = true
            labelAllergyDescription2.isHidden = true
        }
        
        self.backgroundColor = UIColor.clear
        self.contentView.backgroundColor = UIColor.clear
    }
}
