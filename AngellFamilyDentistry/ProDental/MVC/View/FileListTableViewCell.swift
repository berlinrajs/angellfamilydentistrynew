//
//  FileListTableViewCell.swift
//  MConsent For OD
//
//  Created by Berlin Raj on 04/07/17.
//  Copyright © 2017 SRS Web Solutions. All rights reserved.
//

import UIKit

class FileListTableViewCell: UITableViewCell {

    @IBOutlet weak var previewImageView: UIImageView!
    @IBOutlet weak var labelFormName: UILabel!
    @IBOutlet weak var labelDate: UILabel!
    
    var formFile: FormFile!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    class func creationDateStringFrom(Date date: Date) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MMM dd, yyyy hh:mm a"
        return dateFormatter.string(from: date)
    }
    
    func configWith(Path path: String) {
        let fileManager = FileManager.default
        do {
            let attributes = try fileManager.attributesOfItem(atPath: path)
//            previewImageView.image = attributes[NSFileCreationDate]
            
            let creationDate = attributes[FileAttributeKey.creationDate] as! Date
            labelDate.text = FileListTableViewCell.creationDateStringFrom(Date: creationDate)
            
        } catch {
            
        }
        previewImageView.image = kOutputFileMustBeImage == true ?  UIImage(named: "jpgIcon") :  UIImage(named: "pdfIcon")
        self.labelFormName.text = fileManager.displayName(atPath: path)
    }

    func configWith(FormFile file: FormFile) {
        self.formFile = file
        previewImageView.image = kOutputFileMustBeImage == true ?  UIImage(named: "jpgIcon") :  UIImage(named: "pdfIcon")
        self.labelFormName.text = FileManager.default.displayName(atPath: file.filePath)
        labelDate.text = file.creationDateTime
    }
}

class FileListSectionHeader: UITableViewHeaderFooterView {
    
    var labelHeader: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        self.labelHeader.frame = CGRect(x: 8, y: 0, width: self.frame.width - 16, height: self.frame.height)
    }
    
    override init(reuseIdentifier: String?) {
        super.init(reuseIdentifier: reuseIdentifier)
        
        self.backgroundColor = UIColor.darkGray
        self.labelHeader = UILabel()
        self.labelHeader.font = UIFont.boldSystemFont(ofSize: 17)
        self.addSubview(labelHeader)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
