//
//  PatientCareTableViewCell.swift
//  NewEnglandDental
//
//  Created by Berlin Raj on 16/11/16.
//  Copyright © 2016 SRS Web Solutions. All rights reserved.
//

import UIKit

class PatientCareTableViewCell: UITableViewCell {

    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var buttonCheck: UIButton!
    
    var question: MCQuestion!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func configWithQuestion(question: MCQuestion) {
        self.question = question
        
        self.labelTitle.text = question.question
//        if question.question.componentsSeparatedByString(",").count > 1 {
//            self.labelSubTitle.text = question.question.componentsSeparatedByString(",")[1]
//        } else {
//            self.labelSubTitle.text = ""
//        }
        
        
        buttonCheck.isSelected = question.selectedOption
    }
}
class PatientCareHeader: UITableViewHeaderFooterView {
    
    var labelHeading: UILabel!
    var buttonAction: ((_ selected: Bool) -> Void)?
    var buttonYes: RadioButton!
    
    var title: String? {
        get {
            return labelHeading == nil ? nil : labelHeading.text
        }
        set {
            self.labelHeading.text = newValue
        }
    }
    
    func setTitle(title: String, selected: Bool, buttonAction : @escaping ((_ selected: Bool) -> Void)) {
        self.title = title
        self.buttonAction = buttonAction
        self.buttonYes.isSelected = selected
    }
    
    
    func tapAction() {
        self.buttonYes.isSelected = !self.buttonYes.isSelected
        self.buttonAction?(buttonYes.isSelected)
    }
    
    @IBAction func buttonYesAction() {
        self.buttonAction?(buttonYes.isSelected)
    }
    
    override init(reuseIdentifier: String?) {
        super.init(reuseIdentifier: reuseIdentifier)
        
        self.labelHeading = UILabel()
        labelHeading.textColor = UIColor.white
        labelHeading.font = UIFont(name: "WorkSans-Medium", size: 18)
        self.addSubview(labelHeading)
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(PatientCareHeader.tapAction))
        tapGesture.numberOfTapsRequired = 1
        self.labelHeading.isUserInteractionEnabled = true
        self.labelHeading.addGestureRecognizer(tapGesture)
        
        self.buttonYes = RadioButton()
        buttonYes.setImage(UIImage(named: "WhiteRadioButtonUnChecked"), for: UIControlState.normal)
        buttonYes.setImage(UIImage(named: "WhiteRadioButtonChecked"), for: UIControlState.selected)
        buttonYes.titleLabel?.font = UIFont.systemFont(ofSize: 20)
        buttonYes.setTitle(" YES", for: UIControlState.normal)
        buttonYes.addTarget(self, action: #selector(PatientCareHeader.buttonYesAction), for: UIControlEvents.touchUpInside)
        
        let buttonNo = RadioButton()
        buttonNo.setImage(UIImage(named: "WhiteRadioButtonUnChecked"), for: UIControlState.normal)
        buttonNo.setImage(UIImage(named: "WhiteRadioButtonChecked"), for: UIControlState.selected)
        buttonNo.titleLabel?.font = UIFont.systemFont(ofSize: 20)
        buttonNo.setTitle(" NO", for: UIControlState.normal)
        buttonNo.addTarget(self, action: #selector(PatientCareHeader.buttonYesAction), for: UIControlEvents.touchUpInside)
        
        self.addSubview(buttonYes)
        self.addSubview(buttonNo)
        buttonYes.groupButtons = [buttonNo]
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        self.labelHeading.frame = CGRect(x:5, y:0, width:self.frame.width - 175, height:self.frame.height)
        self.buttonYes.frame = CGRect(x:self.labelHeading.frame.maxX, y:0, width:85, height:self.frame.height)
        if let buttonNo = self.buttonYes.groupButtons[1] as? RadioButton {
            buttonNo.frame = CGRect(x:self.buttonYes.frame.maxX, y:0, width:85, height:self.frame.height)
        }
    }
}

class PatientCareFormHeader: UITableViewHeaderFooterView {
    var labelHeading: UILabel!
    
    var title: String? {
        get {
            return labelHeading == nil ? nil : labelHeading.text
        }
        set {
            self.labelHeading.text = newValue
        }
    }
    
    init(reuseIdentifier: String?, withTitleColor color: UIColor) {
        super.init(reuseIdentifier: reuseIdentifier)
        
        self.labelHeading = UILabel()
        labelHeading.textColor = color
        labelHeading.font = UIFont(name: "WorkSans-Medium", size: 14)
        self.addSubview(labelHeading)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        self.labelHeading.frame = CGRect(x:5, y:0, width:self.frame.width - 10, height:self.frame.height)
    }
}

