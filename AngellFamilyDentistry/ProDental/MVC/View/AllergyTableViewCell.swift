//
//  AllergyTableViewCell.swift
//  CommunityDentalCare
//
//  Created by Bala Murugan on 10/7/16.
//  Copyright © 2016 Bala Murugan. All rights reserved.
//

import UIKit

class AllergyTableViewCell: UITableViewCell {
    
    @IBOutlet var labelAllergyName : UILabel!
    @IBOutlet var buttonSelection : RadioButton!
//    @IBOutlet var labelDescription : UILabel?
    var delegate: PatientInfoCellDelegate?
    
    var allergy: Allergy!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    func configureCell(_ allgy : Allergy, patnt : MCPatient)  {
        self.allergy = allgy
        labelAllergyName.text = allgy.allergyName
        buttonSelection.isSelected = allgy.isSelected
//        if let labl = labelDescription {
//            if allgy.isSelected {
//                labl.text = allgy.allergyDescription
//            }
//        }
        self.backgroundColor = UIColor.clear
        self.contentView.backgroundColor = UIColor.clear
    }
    
    
    
    func configureFormCell(_ allgy : Allergy)  {
        labelAllergyName.text = allgy.allergyName
        buttonSelection.isSelected = allgy.isSelected
//        if let labl = labelDescription{
//            if allgy.isSelected {
//                labl.text = allgy.allergyDescription
//            }
//        }
    }
    
    @IBAction func buttonYesAction () {
        allergy.isSelected = buttonSelection.isSelected
        delegate?.radioButtonTappedForCell(cell: self)
    }
}
