//
//  FolderListTableViewCell.swift
//  MConsent
//
//  Created by Berlin Raj on 07/07/17.
//  Copyright © 2017 SRS Web Solutions. All rights reserved.
//

import UIKit

protocol FolderListTableViewCellDelegate {
    func folderSelected()
}

class FolderListTableViewCell: UITableViewCell {

    @IBOutlet weak var labelFolderName: UILabel!
    @IBOutlet weak var labelDetails: UILabel!
    @IBOutlet weak var imageIcon: UIImageView!
    
    var delegate: FolderListTableViewCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func buttonSelectAction() {
        delegate?.folderSelected()
    }
    
    func customizeImage(ForFile file: TOSMBSessionFile) {
        if file.directory || file.isShareRoot {
            imageIcon?.image = #imageLiteral(resourceName: "ArrowRight")
        } else {
            imageIcon?.image = file.name.hasSuffix(".pdf") ? #imageLiteral(resourceName: "pdfIcon") : #imageLiteral(resourceName: "jpgIcon")
        }
    }
}
