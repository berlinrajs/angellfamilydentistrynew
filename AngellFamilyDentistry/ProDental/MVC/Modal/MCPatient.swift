//
//  MCPatient.swift
//  WestgateSmiles
//
//  Created by Leojin Bose on 2/19/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit


class MCPatient: NSObject {
    
    required override init() {
        super.init()
    }
    
    init(forms : [Forms]) {
        super.init()
        self.selectedForms = forms
    }
    
    func patient() -> MCPatient {
        let patient = MCPatient(forms: self.selectedForms)
        patient.dateToday = self.dateToday
        patient.dentistName = self.dentistName
        patient.firstName = self.firstName
        patient.lastName = self.lastName
        patient.preferredName = self.preferredName
        patient.middleInitial = self.middleInitial
        patient.dateOfBirth = self.dateOfBirth
        patient.medicalHistoryQuestions1 = self.medicalHistoryQuestions1
        patient.medicalHistoryQuestions2 = self.medicalHistoryQuestions2
        patient.medicalHistoryQuestions3 = self.medicalHistoryQuestions3
        patient.medicalHistoryQuestions4 = self.medicalHistoryQuestions4
        patient.toothExtractionQuestions1 = self.toothExtractionQuestions1
        patient.toothExtractionQuestions2 = self.toothExtractionQuestions2
        return patient
    }
    var patientDetails: PatientDetails?
    {
        willSet {
            
        } didSet {
            if let detail = patientDetails {
//                self.firstName = detail.firstName
//                self.lastName = detail.lastName
//                
//                let dateFormatter = DateFormatter(dateFormat: "dd/MM/yyyy hh:mm:ss a")
//                let birthDate = dateFormatter.date(from: detail.dateOfBirth)
//                dateFormatter.dateFormat = "MMM dd, yyyy"
//                
//                self.dateOfBirth = dateFormatter.string(from: birthDate!)
                
                self.address = detail.address
                self.city = detail.city
                self.state = detail.state
                self.zipCode = detail.zipCode
                self.gender = detail.gender
                self.socialSecurityNumber = detail.socialSecurityNumber
                self.email = detail.email
                self.homePhone = detail.homePhone
                self.workPhone = detail.workPhone
                self.cellPhone = detail.cellPhone
                self.patientNumber = detail.patientNumber
                self.maritalStatus = detail.maritalStatus
                self.drivingLicense = detail.drivingLicense
                self.providerID = detail.providerId
            } else {
                self.address = nil
                self.city = nil
                self.state = nil
                self.zipCode = nil
                self.gender = nil
                self.socialSecurityNumber = nil
                self.email = nil
                self.homePhone = nil
                self.workPhone = nil
                self.cellPhone = nil
                self.patientNumber = nil
                self.maritalStatus = nil
                self.drivingLicense = nil
                self.providerID = nil
            }
        }
    }
    
    var selectedForms : [Forms]!
    var firstName : String!
    var lastName : String!
    var initial : String!
    var phoneNumber : String!
    var middleInitial : String!
    
    var dateOfBirth : String!
    var dateToday : String!
    var dentistName: String! = ""
    var providerID: String!
    var doctorName: String!
    
    var parentFirstName: String!
    var parentLastName: String!
    var parentDateOfBirth: String!
    
    var is18YearsOld : Bool {
        return patientAge >= 18
    }
    
    var fullName: String {
        if initial != nil && initial.count > 0 {
            return "\(firstName!) \(initial!) \(lastName!)"
        }else{
            return "\(firstName!) \(lastName!)"
        }
    }
    var patientAge: Int! {
        get {
            if dateOfBirth == nil || dateOfBirth.count == 0 {
                return 0
            }
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = kCommonDateFormat
            
            let numberOfDays = (Date().timeIntervalSince(dateFormatter.date(from: self.dateOfBirth)!))/(3600 * 24)
            return Int(numberOfDays/365.2425)
        }
    }
    
    var isParent: Bool! = false
    var isEmployed: Bool! = false
    var havePrimaryInsurance: Bool! = false
    var haveSecondaryInsurance: Bool! = false
    
//    var preferredName : String!
    var address: String!
    var city: String!
    var state: String!
    var zip: String!
    var socialSecurityNumber: String!
    var maritalStatus: Int!
    var gender: Int!
    var preferredName : String = ""
    
    var homePhone : String!
    var workPhone: String!
    var cellPhone: String!
    var email: String!
    var drivingLicense : String!
    var extensionCode : String!
    
    var employer : String!
    var employerAddress : String!
    var employerCity: String!
    var employerState: String!
    var employerZip: String!
    var occupation : String!
    
    var reference : String!
    var familyMembers : String!
    var previousDentist : String!
    var lastVisit : String!
    
    var spouseName : String!
    var spouseEmployer : String!
    var spouseWorkPhone : String!
    var spouseExtension : String!
    var spouseSocialSecurity : String!
    var spouseBirthDate : String!
    var spouseDrivingLicense : String!
    
    var responsibleName : String!
    var responsibleRelationTag : Int = 0
    var responsibleRelation : String!
    var responsibleWorkPhone : String!
    var responsibleHomePhone : String!
    var responsibleExtension : String!
    var responsibleBillingAddress : String!
    var responsibleSocialSecurity : String!
    var responsibleEmployer : String!
    var responsibleDrivingLicense : String!
    
    var primaryInsurance: Insurance = Insurance()
    var secondaryInsurance: Insurance = Insurance()
    
    var nearByName : String!
    var nearByRelation : String!
    var nearByWorkPhone : String!
    var nearByHomePhone : String!
    
    var personalPhysicianTag : Int = 2
    var personalPhysicianName : String!
    var personalPhysicianPhone : String!
    var personalPhysicianLastVisit : String!
    var physicalHealthTag : Int = 0
    var birthControlTag : Int = 0
    var pregnantTag : Int = 0
    var nursingTag : Int = 0
    var numberOfWeeks : String!
    
    var medicalHistory : MedicalHistory = MedicalHistory()
    
    var reasonTodayVisit : String!
    var dentalHealthTag : Int = 0
    var bristlesTag : Int = 0
    var brushTag : Int = 0
    var brush : String!
    var flossTag : Int = 0
    var floss : String!
    
    // patient health history
    var oftenBrushTag : Int! = 0
    var oftenFlossTag : Int! = 0
    var oftenBrush : String!
    var ofterFloss : String!
    var radioRUwoman : Int! = 2
    var radioWomanContra : Int! = 0
    var radioRUPregnant : Int! = 0
    var radioRUNursing : Int! = 0
    var radioWomanMeno : Int! = 0
    var medCommentsValue : String!
    
    var womanDeliveryDate : String!
    var womanSymptoms : String!
    
    var problemQuestions: [[MCQuestion]]!
    var sectionQuestions: [MCQuestion]!
    
    //PATIENT AUTHORIZATION
    var previousClinicName : String!
    var newClinicName : String!
    var patientNumber : String!
    var faxNumber : String!
    var reasonForTransfer : String!
    
    //PRIVACY PRACTICES
    var privacyRelationName: String!
    
    //INFORMED CONSENT FORM
    var isSignedByRelative: Bool!
    var relationNameForTreatment: String!
    var relationshipForTreatment: String!
    var signature1ForTreatment: UIImage!
    var signature2ForTreatment: UIImage!
    
    //Acknowledgement
    var signRefused: Bool! = false
    var signRefusalTag: Int! = 0
    var signRefusalOther: String! = ""
    
    
    // medication
    
    var medication1 :String!
    var medication2 :String!
    var medication3 :String!
    var medication4 :String!
    var medication5 :String!
    var medication6 :String!
    var medication7 :String!
    
    var condition1 : String!
    var condition2 : String!
    var condition3 : String!
    var condition4 : String!
    var condition5 : String!
    var condition6 : String!
    var condition7 : String!
    
    
    //NEW PATIENT
    
//    var parentFirstName : String?
//    var parentLastName : String?
//    var parentDateOfBirth : String?
    var addressLine : String!
    var aptNumber : String!
//    var city : String!
//    var state : String! = "MN"
    var zipCode : String!
//    var socialSecurityNumber : String?
//    var email : String!
    var employerName : String?
    var employerPhoneNumber : String?
    var emergencyContactName : String!
    var emergencyContactPhoneNumber : String!
    var lastDentalVisit : String?
    var reasonForTodayVisit : String?
    var isHavingPain : Bool! = false
    var painLocation : String?
    var anyConcerns : String?
    var hearAboutUs : String?
    var signature1 : UIImage!
    var signature2 : UIImage!
    var signature3 : UIImage!
    var signature4 : UIImage!
    var signature5 : UIImage!
    var relationship: String?
    var cellPhoneNumber : String!
    var textingTag : Int = 0
    var workNumber : String = "N/A"
    
    var maritialStatus : Int? = 0
    var radioHearAboutUsTag : Int? = 0
    
    var referredByDetails : String!
    var othersDetails : String!
    
    var isCallAvail: Bool?
    var isTextAvail: Bool?
    
    var appointmentName1 : String = ""
    var appointmentPhone1 : String = ""
    var appointmentRelationship1 : String = ""
    var appointmentTag1 : Int = 0
    var appointmentHealthTag1 : Int = 0
    var appointmentFinancialTag1 : Int = 0
    var appointmentName2 : String = ""
    var appointmentPhone2 : String = ""
    var appointmentRelationship2 : String = ""
    var appointmentTag2 : Int = 0
    var appointmentHealthTag2 : Int = 0
    var appointmentFinancialTag2 : Int = 0
    
    // UPDATE PATIENT INFO
    
    //var isParent : Bool = false

    var representativeTag: Int = 1
    var textingTag1 : Int = 0

    
    // UPDATE HIPPA..
    
    var UpdateHippasignature1 : UIImage! = nil
    var representativeName : String!
    var representativeRelationship : String!
    
    //PATIENT AUTHORIZATION
//    var previousClinicName : String!
//    var newClinicName : String!
//    var patientNumber : String!
//    var faxNumber : String?
//    var reasonForTransfer : String?
    
    //CHILD TREATMENT
    var selectedOptions : [String]!
    var otherTreatment : String?
    var treatmentDate : String?
    
    //TOOTH EXTRACTION
    var othersText1 : String?
    var othersText2 : String?
    var toothExtractionQuestions1 : [PDOption]!
    var toothExtractionQuestions2 : [PDOption]!
    var prognosisProcedure : String?
    
    //    var signature3 : UIImage!
    //    var signature4 : UIImage!
    
    //MEDICAL HISTORY
    var medicalHistoryQuestions1 : [PDQuestion]!
    var medicalHistoryQuestions2 : [PDOption]!
    var medicalHistoryQuestions3 : [PDOption]!
    var medicalHistoryQuestions4 : [PDOption]!
    var isWomen : Bool = false
    var controlledSubstances : String?
    var controlledSubstancesClicked : Bool! = false
    var othersTextForm3 : String?
    var comments : String?
    var antibioticsUsed: Bool = false
    var medicationAndDosage: String = ""
    var otherIllness : String?
    var scanMedicationIndex : Int! = 0
    
    
    
    var isDate1Tapped: Bool = false
    var isDate2Tapped: Bool = false
    var isDate3Tapped: Bool = false
    var isDate4Tapped: Bool = false
    
    var frontImage: UIImage?
    var backImage: UIImage?
}

class Insurance: NSObject {
    var relationTag: Int = 0
    var relation : String!
    var insuredName : String!
    var insuredBirthdate : String!
    var insuredId : String!
    var insuredEmployer : String!
    var companyName : String!
    var companyAddress : String!
    var companyPhone : String!
    var groupNumber : String!
}

class MedicalHistory : NSObject{
    
    var medicalQuestions : [[MCQuestion]]!
    
    var allergies: [Allergy]! = [Allergy]()
    
    required override init() {
        super.init()
        
        let quest1 : [String] = ["Are you currently under the care of a physician?",
                                 "Do you smoke or use tobacco in any form?",
                                 "Are you taking any prescription/over-the-counter or herbal supplement drugs?",
                                 "Have you ever taken Fosamax, or any other bisphosphonate?",
                                 "Have you been told that you snore or hold your breath while sleeping or wake up gasping for breath",
                                 "Have you ever had any medical conditions?"]
        
//        let quest2 : [String] = ["Abnormal Bleeding",
//                                 "Alcohol / Drug Abuse",
//                                 "Anemia",
//                                 "Arthritis",
//                                 "Artificial Bones / Joints / Valves",
//                                 "Asthma",
//                                 "Blood Transfusion",
//                                 "Cancer / Chemotherapy",
//                                 "Colitis",
//                                 "Congenital Heart Defect",
//                                 "Diabetes",
//                                 "Difficulty Breathing",
//                                 "Emphysema",
//                                 "Epilepsy",
//                                 "Fainting Spells"]
//        
//        let quest3 : [String] = ["Frequent Headaches",
//                                 "Glaucoma",
//                                 "Hay Fever",
//                                 "Heart Attack",
//                                 "Heart Murmur",
//                                 "Heart Surgery",
//                                 "Hemophilia",
//                                 "Hepatitis",
//                                 "Herpes / Fever Blisters",
//                                 "High Blood Pressure",
//                                 "HIV+ / AIDS",
//                                 "Hospitalized for Any Reason",
//                                 "Kidney Problems",
//                                 "Liver Disease",
//                                 "Low Blood Pressure"]
//        
//        let quest4 : [String] = ["Lupus",
//                                 "Mitral Valve Prolapse",
//                                 "Pacemaker",
//                                 "Psychiatric Treatment",
//                                 "Radiation Treatment",
//                                 "Rheumatic / Scarlet Fever",
//                                 "Seizures",
//                                 "Shingles",
//                                 "Sickle Cell Disease",
//                                 "Sinus Problems",
//                                 "Stroke",
//                                 "Thyroid Problems",
//                                 "Tuberculosis (TB)",
//                                 "Ulcers",
//                                 "Venereal Disease"]
//        
//        let quest5 : [String] = ["Aspirin",
//                                 "Codeine",
//                                 "Dental Anesthetics",
//                                 "Erythromycin",
//                                 "Jewelry Metals",
//                                 "Latex",
//                                 "Penicillin",
//                                 "Tetracycline",
//                                 "Other"]
        
        let quest6 : [String] = ["Has your doctor told you that you require antibiotics before dental treatment?",
                                 "Are you currently in pain?",
                                 "Have you ever had a serious / difficult problem associated with any previous dental work?",
                                 "Do you or have you ever experienced pain / discomfort in your jaw joint (TMJ / TMD)?",
                                 "Do you like your smile?",
                                 "Do your gums ever bleed?"]
        
//        self.medicalQuestions = [MCQuestion.arrayOfQuestions(questions: quest1),MCQuestion.arrayOfQuestions(questions: quest2),MCQuestion.arrayOfQuestions(questions: quest3),MCQuestion.arrayOfQuestions(questions: quest4),MCQuestion.arrayOfQuestions(questions: quest5),MCQuestion.arrayOfQuestions(questions: quest6)]
        self.medicalQuestions = [MCQuestion.arrayOfQuestions(questions: quest1), MCQuestion.arrayOfQuestions(questions: quest6)]
        self.medicalQuestions[0][0].isAnswerRequired = true
        self.medicalQuestions[0][2].isAnswerRequired = true
        self.medicalQuestions[0][5].isAnswerRequired = true
        
//        self.medicalQuestions[4][8].isAnswerRequired = true
        
        
    }
}

class InsuranceAuto: NSObject {
    
    var name: String!
    var companyName: String!
    var insuredID: String!
    var relation: Relationship?
    var group: String!
    var groupNumber: String!
    
    override init() {
        super.init()
    }
    
    init(details: [String: String]) {
        super.init()
        name = details["name"]
        companyName = details["companyName"]
        insuredID = details["insuredID"]
        if details["relation"]!.isEmpty == false {
            relation = Relationship(relation: details["relation"]!)
        }
        group = details["group"]
        groupNumber = details["groupNumber"]
    }
}

enum Relationship: Int {
    case SELF = 1
    case SPOUSE
    case CHILD
    case OTHER
    
    
    init(relation: String) {
        switch relation {
        case "1": self.init(rawValue: 1)!
            break
        case "2": self.init(rawValue: 2)!
            break
        case "3": self.init(rawValue: 3)!
            break
        default: self.init(rawValue: 4)!
            break
        }
    }
    
    var index: Int {
        switch self {
        case .SELF: return 1
        case .SPOUSE: return 2
        case .CHILD: return 3
        default: return 4
        }
    }
    
    var text : String {
        switch self {
        case .SELF: return "Self"
        case .SPOUSE: return "Spouse"
        case .CHILD: return "Child"
        default: return "Other"
        }
    }
}

enum MaritalStatus: Int {
    case SINGLE = 1
    case MARRIED
    case CHILD
    case OTHER
}

enum Gender: Int {
    case MALE = 1
    case FEMALE
}

class PatientDetails : NSObject {
    var dateOfBirth : String!
    var firstName : String!
    var lastName : String!
    var preferredName : String! = ""
    var address : String!
    var city : String!
    var state : String!
    var zipCode : String!
    var country : String!
    var gender : Int!
    var socialSecurityNumber : String!
    var email : String!
    var homePhone : String!
    var workPhone: String!
    var cellPhone: String!
    var patientNumber : String! = ""
    var patientGUID: String!
    var maritalStatus: Int!
    var drivingLicense: String!
    var existingAlertIds: [String] = [String]()
    var providerId : String!
    
    var primaryDentalInsurance: InsuranceAuto?
    var secondaryDentalInsurance: InsuranceAuto?
    var primaryMedicalInsurance: InsuranceAuto?
    var secondaryMedicalInsurance: InsuranceAuto?
    
    init(WithPatient patient: MCPatient) {
        super.init()
        self.city = patient.city
        self.dateOfBirth = patient.dateOfBirth
        self.email = patient.email
        self.socialSecurityNumber = patient.socialSecurityNumber
        self.address = patient.address
        self.zipCode = patient.zipCode
        self.gender = patient.gender
        self.firstName = patient.firstName
        self.state = patient.state
        self.homePhone = patient.homePhone
        self.lastName = patient.lastName
        self.maritalStatus = patient.maritalStatus
        self.workPhone = patient.workPhone
        self.cellPhone = patient.cellPhone
    }
    
    init(details : [String: AnyObject]) {
        super.init()
        self.city = getValue(details["City"])
        self.dateOfBirth = getValue(details["DateOfBirth"])
        self.email = getValue(details["Email"])
        self.socialSecurityNumber = getValue(details["SocialSecurity"])
        self.address = getValue(details["Address"])
        self.zipCode = getValue(details["Zip"])
        self.gender = getValue(details["Gender"]) == "" ? nil : Int(getValue(details["Gender"]))
        self.patientNumber = getValue(details["Id"])
        self.patientGUID = getValue(details["PatientGuid"])
        self.state = getValue(details["State"])
        self.homePhone = getValue(details["HomePhone"]).formattedPhoneNumber
        self.workPhone = getValue(details["WorkPhone"]).formattedPhoneNumber
        self.cellPhone = getValue(details["CellPhone"]).formattedPhoneNumber
        self.lastName = getValue(details["LastName"])
        self.firstName = getValue(details["FirstName"])
        self.maritalStatus = 0
//            getValue(details["MaritalStatus"]) == "" ? 0 : Int(getValue(details["MaritalStatus"]))
        self.drivingLicense = getValue(details["driving_license_number"])
        self.existingAlertIds = [String]()
        
        if let alertDetails = details["ExistingAlertIds"] as? [String] {
            for alertId in alertDetails {
                self.existingAlertIds.append(alertId)
            }
        }
        self.providerId = getValue(details["ProviderId"])
        
//        if  details["AllergyList"] as? [NSDictionary] != nil
//        {
//            for dict in details["AllergyList"] as! [NSDictionary] {
//                let allgy: Allergy = Allergy()
//                allgy.allergyId = dict["AllergyId"] as! String
//                allgy.allergyDescription = ((dict["AllergyDescription"] as! String).uppercased() == "NO" || (dict["AllergyDescription"] as! String).uppercased() == "YES") ? "" : dict["AllergyDescription"] as! String
//                allgy.allergyName = dict["AllergyName"] as! String
//                allgy.isSelected = (dict["AllergyDescription"] as! String).uppercased() == "NO" ? false : true
//                self.allergySelected.append(allgy)
//            }
//        }
        
        func getName(_ nameString: String) -> String {
            var fullName = nameString
            let names = fullName.components(separatedBy: "_")
            if names.count == 2 {
                let firstName = getValue(names[0] as AnyObject?)
                let lastName = getValue(names[1] as AnyObject?)
                fullName = "\(firstName) \(lastName)"
            }
            return fullName
        }
        
        
        if getValue(details["PrimaryDentalinsuranceAvailable"]) == "YES" {
            let insuranceDetails = ["name": getName(getValue(details["PrimaryDentalinsuranceSubName"])), "insuredID": getValue(details["PrimaryDentalinsuranceSubId"]), "companyName": getValue(details["PrimaryDentalinsuranceCarrierName"]), "relation": getValue(details["PrimaryDentalinsuranceRelation"]), "group": getValue(details["PrimDentalinsuranceGroupPlan"]), "groupNumber": getValue(details["PrimaryDentalinsuranceGroupId"])]
            self.primaryDentalInsurance = InsuranceAuto(details: insuranceDetails)
        }
        
        if getValue(details["SecondaryDentalinsuranceAvailable"]) == "YES" {
            
            let insuranceDetails = ["name": getName(getValue(details["SecondaryDentalinsuranceSubName"])), "insuredID": getValue(details["SecondaryDentalinsuranceSubId"]), "companyName": getValue(details["SecondaryDentalinsuranceCarrierName"]), "relation": getValue(details["SecondaryDentalinsuranceRelation"]), "group": getValue(details["SecondaryDentalinsuranceGroupPlan"]), "groupNumber": getValue(details["SecondaryDentalinsuranceGroupId"])]
            self.secondaryDentalInsurance = InsuranceAuto(details: insuranceDetails)
        }
        
        if getValue(details["PrimaryMedicalinsuranceAvailable"]) == "YES" {
            let insuranceDetails = ["name": getName(getValue(details["PrimaryMedicalinsuranceSubName"])), "insuredID": getValue(details["PrimaryMedicalinsuranceSubId"]), "companyName": getValue(details["PrimaryMedicalinsuranceCarrierName"]), "relation": getValue(details["PrimaryMedicalinsuranceRelation"]), "group": getValue(details["PrimMedicalinsuranceGroupPlan"]), "groupNumber": getValue(details["PrimaryMedicalinsuranceGroupId"])]
            self.primaryMedicalInsurance = InsuranceAuto(details: insuranceDetails)
        }
        
        if getValue(details["SecondaryMedicalinsuranceAvailable"]) == "YES" {
            let insuranceDetails = ["name": getName(getValue(details["SecondaryMedicalinsuranceSubName"])), "insuredID": getValue(details["SecondaryMedicalinsuranceSubId"]), "companyName": getValue(details["SecondaryMedicalinsuranceCarrierName"]), "relation": getValue(details["SecondaryMedicalinsuranceRelation"]), "group": getValue(details["SecondaryMedicalinsuranceGroupPlan"]), "groupNumber": getValue(details["SecondaryMedicalinsuranceGroupId"])]
            self.secondaryMedicalInsurance = InsuranceAuto(details: insuranceDetails)
        }
    }
    
    func getValue(_ value: AnyObject?) -> String {
        if value is String {
            let characterSet = NSCharacterSet.whitespaces
            return (value as! String).trimmingCharacters(in: characterSet)
        }
        return ""
    }
}
