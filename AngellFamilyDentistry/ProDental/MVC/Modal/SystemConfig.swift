//
//  SystemConfig.swift
//  MConsent For OD
//
//  Created by Berlin Raj on 05/07/17.
//  Copyright © 2017 SRS Web Solutions. All rights reserved.
//

import UIKit

private let sharedBiosService = TONetBIOSNameService()

class SystemConfig: NSObject, NSCoding {
    
    var hostName: String!
    var hostIp: String!
    
    class func checkConnectivity(WithIp ipString: String, completion: @escaping ((Bool, String?)->Void)) {
        let params = ["userName": "srssoftwares", "password": "srssoftwares", "grant_type": "password"] as [String : AnyObject]
        ServiceManager.postDataToServer(baseUrlString: "http://\(ipString):1050/", serviceName: "token", parameters: params, success: { (result) in
            if let _ = (result as AnyObject)["access_token"] as? String {
                completion(true, nil)
            } else {
                completion(false, "PLEASE CHECK YOUR NETWORK CONNECTIVITY, AND MAKE SURE THE DENTRIX SERVER IS ON")
            }
        }) { (error) in
            completion(false, "PLEASE CHECK YOUR NETWORK CONNECTIVITY, AND MAKE SURE THE DENTRIX SERVER IS ON")
        }
    }
    
    func checkConnectivity(completion: @escaping ((Bool, String?)->Void)) {
        let params = ["userName": "srssoftwares", "password": "srssoftwares", "grant_type": "password"] as [String : AnyObject]
        ServiceManager.postDataToServer(baseUrlString: "http://\(self.hostIp!):1050/", serviceName: "token", parameters: params, success: { (result) in
            if let _ = (result as AnyObject)["access_token"] as? String {
                completion(true, nil)
            } else {
                completion(false, "PLEASE CHECK YOUR NETWORK CONNECTIVITY, AND MAKE SURE THE DENTRIX SERVER IS ON")
            }
        }) { (error) in
            completion(false, "PLEASE CHECK YOUR NETWORK CONNECTIVITY, AND MAKE SURE THE DENTRIX SERVER IS ON")
        }
    }
    
    class var sharedConfig: SystemConfig? {
        get {
            if let data = UserDefaults.standard.value(forKey: kInitialConfigDictKey) as? Data {
                let systemConfig = NSKeyedUnarchiver.unarchiveObject(with: data) as! SystemConfig
                return systemConfig
            } else {
                return nil
            }
        } set {
            UserDefaults.standard.set(NSKeyedArchiver.archivedData(withRootObject: newValue!), forKey: kInitialConfigDictKey)
            UserDefaults.standard.synchronize()
        }
    }
    
    class var isNewVersion: Bool {
        get {
            if let version = UserDefaults.standard.value(forKey: kVersionKey) as? String {
                if version != Bundle.main.infoDictionary?[kCFBundleVersionKey as String] as? String {
                    return true
                }
            }
            return false
        }
    }
    
    override init() {
        super.init()
    }
    
    class var hasSystemConfig: Bool {
        get {
            return UserDefaults.standard.bool(forKey: kHasSystemConfigKey)
        } set {
            UserDefaults.standard.set(newValue, forKey: kHasSystemConfigKey)
            UserDefaults.standard.synchronize()
        }
    }
    
//    class var googleConfigured: Bool {
//        get {
//            return UserDefaults.standard.bool(forKey: kHasGoogleConfigured)
//        } set {
//            UserDefaults.standard.set(newValue, forKey: kHasGoogleConfigured)
//            UserDefaults.standard.synchronize()
//        }
//    }
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(self.hostName, forKey: "hostName")
        aCoder.encode(self.hostIp, forKey: "hostIp")
        
//        aCoder.encode(self.authName, forKey: "authName")
//        aCoder.encode(self.authPassword, forKey: "authPassword")
//        
//        aCoder.encode(self.folderPath, forKey: "folderName")
    }
    
//    func resetIpAddress (_ completion: @escaping ((Bool)-> Void)) {
//        sharedBiosService.resolveIPAddress(withName: self.hostName, type: TONetBIOSNameServiceType.workStation, success: { (ipAddressString) in
//            self.hostIp = ipAddressString!
//            SystemConfig.sharedConfig = self
//            completion(true)
//        }, failure: {
//            completion(false)
//        })
//    }
    
    required init(coder decoder: NSCoder) {
        super.init()
        
        self.hostName = (decoder.decodeObject(forKey: "hostName") ?? "") as! String
        self.hostIp = (decoder.decodeObject(forKey: "hostIp") ?? "") as! String
        
//        self.authName = (decoder.decodeObject(forKey: "authName") ?? "") as! String
//        self.authPassword = (decoder.decodeObject(forKey: "authPassword") ?? "") as! String
//        
//        self.folderPath = (decoder.decodeObject(forKey: "folderName") ?? "/") as! String
    }
}

//extension SystemConfig {
//    func appendPath(path: String) {
//        if self.folderPath == nil {
//            self.folderPath = path
//        } else if path.hasSuffix("/") {
//            self.folderPath = self.folderPath + path
//        } else {
//            self.folderPath = self.folderPath + path + "/"
//        }
//    }
//    
//    func removeLastTreeFromPath() {
//        if self.folderPath == nil {
//            return
//        }
//        var pathArray = self.folderPath.components(separatedBy: "/")
//        pathArray.removeLast()
//        pathArray.removeLast()
//        self.folderPath = pathArray.joined(separator: "/")
//        
//        self.folderPath = self.folderPath + "/"
//    }
//    
//    var selectedFolder: String! {
//        get {
//            var pathFolders = self.folderPath.components(separatedBy: "/")
//            pathFolders.removeLast()
//            return pathFolders.last! == "" ? "/" : pathFolders.last!
//        }
//    }
//}
