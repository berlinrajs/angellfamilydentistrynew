//
//  WorkStationConfig.swift
//  MC For ES
//
//  Created by Berlin Raj on 09/08/17.
//  Copyright © 2017 SRS Web Solutions. All rights reserved.
//

import UIKit

private let sharedBiosService = TONetBIOSNameService()

class WorkStationConfig: NSObject, NSCoding {
    
    var hostName: String!
    var hostIp: String!
    var authName: String!
    var authPassword: String!
    var folderPath: String!
    
    override init() {
        super.init()
    }
    
    class var sharedConfig: WorkStationConfig? {
        get {
            if let data = UserDefaults.standard.value(forKey: kInitialWorkSationConfigDictKey) as? Data {
                let stationConfig = NSKeyedUnarchiver.unarchiveObject(with: data) as! WorkStationConfig
                return stationConfig
            } else {
                return nil
            }
        } set {
            UserDefaults.standard.set(NSKeyedArchiver.archivedData(withRootObject: newValue!), forKey: kInitialWorkSationConfigDictKey)
            UserDefaults.standard.synchronize()
        }
    }
    
    class var hasSystemConfigured: Bool {
        get {
            return UserDefaults.standard.bool(forKey: kHasWorkStationConfigKey)
        } set {
            UserDefaults.standard.set(newValue, forKey: kHasWorkStationConfigKey)
            UserDefaults.standard.synchronize()
        }
    }
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(self.hostName, forKey: "hostName")
        aCoder.encode(self.hostIp, forKey: "hostIp")
        
        aCoder.encode(self.authName, forKey: "authName")
        aCoder.encode(self.authPassword, forKey: "authPassword")
        
        aCoder.encode(self.folderPath ?? "/", forKey: "folderName")
    }
    
    func resetIpAddress (_ completion: @escaping ((Bool)-> Void)) {
        sharedBiosService.resolveIPAddress(withName: self.hostName, type: TONetBIOSNameServiceType.workStation, success: { (ipAddressString) in
            self.hostIp = ipAddressString!
            WorkStationConfig.sharedConfig = self
            completion(true)
        }, failure: {
            completion(false)
        })
    }
    
    required init(coder decoder: NSCoder) {
        super.init()
        
        self.hostName = (decoder.decodeObject(forKey: "hostName") ?? "") as! String
        self.hostIp = (decoder.decodeObject(forKey: "hostIp") ?? "") as! String
        
        self.authName = (decoder.decodeObject(forKey: "authName") ?? "") as! String
        self.authPassword = (decoder.decodeObject(forKey: "authPassword") ?? "") as! String
        
        self.folderPath = (decoder.decodeObject(forKey: "folderName") ?? "/") as! String
    }
}
extension WorkStationConfig {
    func appendPath(path: String) {
        if self.folderPath == nil {
            self.folderPath = path
        } else if path.hasSuffix("/") {
            self.folderPath = self.folderPath + path
        } else {
            self.folderPath = self.folderPath + path + "/"
        }
    }
    
    func removeLastTreeFromPath() {
        if self.folderPath == nil {
            return
        }
        var pathArray = self.folderPath.components(separatedBy: "/")
        pathArray.removeLast()
        pathArray.removeLast()
        self.folderPath = pathArray.joined(separator: "/")
        self.folderPath = self.folderPath + "/"
    }
    
    var selectedFolder: String! {
        get {
            var pathFolders = self.folderPath.components(separatedBy: "/")
            pathFolders.removeLast()
            return pathFolders.last! == "" ? "/" : pathFolders.last!
        }
    }
    
    var completeFolderPath: String {
        get {
            var pathArray = self.folderPath.components(separatedBy: "/")
            pathArray.insert(self.hostIp, at: 1)
            return pathArray.joined(separator: "/")
        }
    }
}
