//
//  MCQuestion.swift
//  ProDental
//
//  Created by Leojin Bose on 2/26/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

class MCQuestion: NSObject, NSCoding {
    var question : String!
    var isAnswerRequired : Bool!
    var answer : String?
    var selectedOption : Bool! {
        didSet {
            if selectedOption == nil || selectedOption == false {
                self.answer = nil
            }
        }
    }
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(self.question as Any, forKey: "question")
        aCoder.encode(self.isAnswerRequired, forKey: "isAnswerRequired")
        aCoder.encode(self.answer, forKey: "answer")
        aCoder.encode(self.selectedOption, forKey: "selectedOption")
    }
    
    required init(coder decoder: NSCoder) {
        super.init()
        
        self.question = decoder.decodeObject(forKey: "question") as! String
        self.isAnswerRequired = decoder.decodeObject(forKey: "isAnswerRequired") as! Bool
        self.answer = decoder.decodeObject(forKey: "answer") as? String
        self.selectedOption = decoder.decodeObject(forKey: "selectedOption") as! Bool
    }
    
    init(question: String) {
        super.init()
        self.question = question
        self.isAnswerRequired = false
        self.selectedOption = false
    }
    
    init(dict : NSDictionary) {
        super.init()
        self.question = dict["question"] as! String
        self.selectedOption = false
        self.isAnswerRequired = (dict["verification"] as! String) == "Yes"
    }
    
    class func getObjects (_ arrayResult : NSArray) -> [MCQuestion] {
        var questions  = [MCQuestion]()
        for dict in arrayResult {
            let obj = MCQuestion(dict: dict as! NSDictionary)
            questions.append(obj)
        }
        return questions
    }
    
    class func arrayOfQuestions(questions: [String]) -> [MCQuestion]{
        var arrayQuestions = [MCQuestion]()
        for string in questions {
            let question = MCQuestion(question: string)
            question.selectedOption = false
            arrayQuestions.append(question)
        }
        return arrayQuestions
    }
    
    class func getArrayofQuestions (questionStrings : [String]) -> [MCQuestion] {
        var questions  = [MCQuestion]()
        for question in questionStrings {
            let obj = MCQuestion(question: question)
            questions.append(obj)
        }
        return questions
    }
    
    class func getJSONObject(_ responseString : String) -> AnyObject? {
        do {
            let object = try JSONSerialization.jsonObject(with: responseString.data(using: String.Encoding.utf8)!, options: JSONSerialization.ReadingOptions.allowFragments)
            return object as AnyObject?
        } catch {
            return nil
        }
    }
}
