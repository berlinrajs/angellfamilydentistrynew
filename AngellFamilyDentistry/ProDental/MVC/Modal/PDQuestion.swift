//
//  PDQuestion.swift
//   Angell Family Dentistry
//
//  Created by Leojin Bose on 2/26/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

class PDQuestion: NSObject {
    var question : String!
    var isAnswerRequired : Bool!
    var answer : String?
    var answer1 : String?
    var selectedOption : Bool? = false {
        didSet {
            if selectedOption == false {
                answer = nil
            }
        }
    }

    
    init(dict : NSDictionary) {
        super.init()
        self.question = dict["question"] as! String
        self.isAnswerRequired = (dict["verification"] as! String) == "Yes"
    }
    
    class func getObjects (_ arrayResult : NSArray) -> [PDQuestion] {
        var questions  = [PDQuestion]()
        for dict in arrayResult {
            let obj = PDQuestion(dict: dict as! NSDictionary)
            obj.selectedOption = false
            questions.append(obj)
        }
        return questions
    }
    
    
    class func getJSONObject(_ responseString : String) -> AnyObject? {
        do {
            let object = try JSONSerialization.jsonObject(with: responseString.data(using: String.Encoding.utf8)!, options: JSONSerialization.ReadingOptions.allowFragments)
            return object as AnyObject
        } catch {
            return nil
        }
    }

    
    class func fetchQuestionsForm1(_ completion:(_ result : [PDQuestion]?, _ success : Bool) -> Void) {
        
        let responseString = "{\"Status\":\"success\",\"posts\":[{\"question\":\"Are you under a physician's care now?\",\"verification\":\"Yes\"},{\"question\":\"Have you ever had a serious head or neck injury?\",\"verification\":\"Yes\"},{\"question\":\"Have you ever taken any medication containing Bisphosphonates? (Fosamax, Zometa, Boniva)\",\"verification\":\"Yes\"},{\"question\":\"Have you ever been hospitalized or had a major operation?\",\"verification\":\"Yes\"},{\"question\":\"Do you use tobacco?\",\"verification\":\"No\"},{\"question\":\"Pre-diabetes\",\"verification\":\"No\"},{\"question\":\"Are you taking blood thinners?\",\"verification\":\"Yes\"},{\"question\":\"Do you have your routine INR tests done?\",\"verification\":\"Yes\"}]}"
        
        
        
        
        
        let jsonObject = PDQuestion.getJSONObject(responseString)
        completion(self.getObjects(jsonObject!["posts"] as! NSArray), true)
        
        // neck injury
        //   {\"question\":\"Are you taking any medications, pills or drugs?\",\"verification\":\"Yes\"},
        
//        ServiceManager.fetchDataFromService("general_api.php", parameters: nil, success: { (result) -> Void in
//            completion(result: self.getObjects(result["posts"] as! NSArray), success: true)
//            print(result)
//            }) { (error) -> Void in
//                completion(result: nil, success: false)
//        }
    }
}

class PDOption : NSObject {
    var question : String!
    var isSelected : Bool?
    var index : Int!
    var answer: String!
    
    init(value : String) {
        super.init()
        self.question = value
    }
    
    class func getObjects (_ arrayResult : NSArray) -> [PDOption] {
        var questions  = [PDOption]()
        for (idx, value) in arrayResult.enumerated() {
            let obj = PDOption(value: value as! String)
            obj.isSelected = false
            obj.index = idx
            questions.append(obj)
        }
        return questions
    }
    
    
    class func fetchQuestionsForm2(_ completion:(_ result : [PDOption]?, _ success : Bool) -> Void) {
        let responseString = "{\"Status\":\"success\", \"posts\":[\"Pregnant \\/ trying to get pregnant\", \"Nursing\", \"Taking oral contraceptives\", \"None\"]}"
        
        let jsonObject = PDQuestion.getJSONObject(responseString)
        completion(self.getObjects(jsonObject!["posts"] as! NSArray), true)
        
//        ServiceManager.fetchDataFromService("women_api.php", parameters: nil, success: { (result) -> Void in
//            completion(result: self.getObjects(result["posts"] as! NSArray), success: true)
//            print(result)
//            }) { (error) -> Void in
//                completion(result: nil, success: false)
//        }
    }
    
    class func fetchQuestionsForm3(_ completion:(_ result : [PDOption]?, _ success : Bool) -> Void) {
        
        let responseString = "{\"Status\":\"success\",\"posts\":[\"Aspirin\",\"Penicillin\",\"Codeine\",\"Acrylic\",\"Metal\",\"Latex\",\"Sulfa drugs\",\"Local anesthetics\", \"Others\"]}"
        
        let jsonObject = PDQuestion.getJSONObject(responseString)
        completion(self.getObjects(jsonObject!["posts"] as! NSArray), true)
        
        
//        ServiceManager.fetchDataFromService("alergic_api.php", parameters: nil, success: { (result) -> Void in
//            completion(result: self.getObjects(result["posts"] as! NSArray), success: true)
//            print(result)
//            }) { (error) -> Void in
//                completion(result: nil, success: false)
//        }
    }
    
    class func fetchQuestionsForm4(_ completion:(_ result : [PDOption]?, _ success : Bool) -> Void) {
        
        let responseString = "{\"Status\":\"success\",\"posts\":[\"AIDS\\/HIV Positive\",\"Dementia\",\"Anaphylaxis\",\"Anemia\",\"Artificial Joints\",\"Asthma\",\"Blood Disease\",\"Blood Transfusion\",\"Cancer\",\"Chemotherapy\",\"Cold sores\",\"Diabetes\",\"Chemical Dependency\",\"Epilepsy or Seizures\",\"Excessive Bleeding\",\"Excessive Thirst\",\"Fainting \\/ Dizziness\",\"Heart Condition\",\"Heart Pacemaker\",\"Hepatitis A\",\"Hepatitis B or C\",\"High Blood Pressure\",\"Kidney Disease\",\"Leukemia\",\"Liver Disease\",\"Low Blood Pressure\",\"Lung Disease \\/ COPD\",\"Osteoporosis\",\"Pain in Jaw Joints\",\"Radiation Treatments\",\"Rheumatoid Arthritis\",\"Shingles\",\"Sickle Cell Disease\",\"Sinus Trouble\",\"Stomach\\/Intestinal Disease\",\"Stroke\",\"Thyroid Condition\",\"Tonsillitis\",\"Tuberculosis\",\"Ulcers\",\"Hard of Hearing\"]}"
        
        let jsonObject = PDQuestion.getJSONObject(responseString)
        completion(self.getObjects(jsonObject!["posts"] as! NSArray), true)
    }
    
    class func fetchQuestionsToothExtractionForm1(_ completion:(_ result : [PDOption]?, _ success : Bool) -> Void) {
        let responseString = "{\"Status\":\"success\",\"posts\":[\"Pain\",\"Infection\",\"Decay\",\"Gum Disease\",\"Broken tooth\",\"Non Restorable\"]}"
        
        let jsonObject = PDQuestion.getJSONObject(responseString)
        completion(self.getObjects(jsonObject!["posts"] as! NSArray), true)
    }
    
    class func fetchQuestionsToothExtractionForm2(_ completion:(_ result : [PDOption]?, _ success : Bool) -> Void) {
        let responseString = "{\"Status\":\"success\",\"posts\":[\"No Treatment\",\"Root Canal Therapy\",\"Filling\",\"Crowns\",\"Gum Treatment\"]}"
        
        let jsonObject = PDQuestion.getJSONObject(responseString)
        completion(self.getObjects(jsonObject!["posts"] as! NSArray), true)
    }
}
