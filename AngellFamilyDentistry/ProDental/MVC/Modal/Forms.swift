//
//  Forms.swift
//  WestgateSmiles
//
//  Created by samadsyed on 2/18/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

class Signature: NSObject, NSCoding {
    var title: String!
    var signature: UIImage?
    
    init(title: String) {
        super.init()
        self.title = title
    }
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(self.title, forKey: "title")
        aCoder.encode(self.signature == nil ? nil : UIImageJPEGRepresentation(self.signature!, 1.0), forKey: "signature")
    }
    
    required init(coder decoder: NSCoder) {
        super.init()
        
        self.title = decoder.decodeObject(forKey: "title") as? String
        self.signature = decoder.decodeObject(forKey: "signature") == nil ? nil : UIImage(data: decoder.value(forKey: "signature") as! Data)
    }
}

class Forms: NSObject, NSCoding {
    
    var isDataFetched: Bool = false
    var formTitle : String!
    var isSelected : Bool! {
        willSet {
            if newValue == false && self.subForms != nil {
                for form in subForms {
                    form.isSelected = false
                }
            }
        }
    }
    var index : Int!
    var formId: String!
    var signatures: [Signature]?
    var requiredValues: [MCQuestion]?
    var formContent: String!
    
    var subForms : [Forms]!
    var isToothNumberRequired: Bool!
    var isDentistNameRequired: Bool!
    var toothNumbers : String!
    
    override init() {
        super.init()
    }
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(self.isDataFetched as Any, forKey: "isDataFetched")
        aCoder.encode(self.formTitle, forKey: "formTitle")
        aCoder.encode(self.isSelected, forKey: "isSelected")
        aCoder.encode(self.index, forKey: "index")
        aCoder.encode(self.formId, forKey: "formId")
        aCoder.encode(self.signatures == nil ? nil : self.signatures!, forKey: "signatures")
        aCoder.encode(self.requiredValues == nil ? nil : self.requiredValues!, forKey: "requiredValues")
        aCoder.encode(self.formContent, forKey: "formContent")
        aCoder.encode(self.subForms == nil ? nil : self.subForms, forKey: "subForms")
        aCoder.encode(self.isToothNumberRequired, forKey: "isToothNumberRequired")
        aCoder.encode(self.toothNumbers, forKey: "toothNumbers")
    }
    
    required init(coder decoder: NSCoder) {
        super.init()
        
        self.isDataFetched = decoder.decodeObject(forKey: "isDataFetched") as! Bool
        self.formTitle = decoder.decodeObject(forKey: "formTitle") as! String
        self.isSelected = decoder.decodeObject(forKey: "isSelected") as! Bool
        self.index = decoder.decodeObject(forKey: "index") as! Int
        self.formId = decoder.decodeObject(forKey: "formId") as? String
        self.signatures = decoder.decodeObject(forKey: "signatures") == nil ? nil : decoder.decodeObject(forKey: "signatures") as? [Signature]
        self.requiredValues = decoder.decodeObject(forKey: "requiredValues") == nil ? nil : decoder.decodeObject(forKey: "requiredValues") as? [MCQuestion]
        self.formContent = decoder.decodeObject(forKey: "formContent") as? String
        self.subForms = decoder.decodeObject(forKey: "subForms") == nil ? nil : decoder.decodeObject(forKey: "subForms") as? [Forms]
        self.isToothNumberRequired = decoder.decodeObject(forKey: "isToothNumberRequired") as? Bool
        self.toothNumbers = decoder.decodeObject(forKey: "toothNumbers") as? String
    }
    
    class func getAllForms (_ completion :@escaping (_ isConnectionfailed: Bool, _ forms : [Forms]?) -> Void) {
        
        ServiceManager.fetchDataFromGetService(baseUrl, serviceName: "consentform_api.php", parameters: ["appkey": MCAppDetails.sharedAppDetails!.appKey] as [String: AnyObject], success: { (result) in
            if let res = result as? NSDictionary, res["Post"] as! String == "Success" {
                let consentForms = res["posts"] as! [[String: String]]
                let forms = [kIntakeForms, kScanCards, kConsentFormsAuto]
                //add api consnet foms to all consent forms array
                
                let formObj = getFormObjects(MainForms: forms as! [[String : [String]]],ManualConsents: kAllConsentForms, APIConsnets: consentForms, isSubForm: false)
                
                completion(false, formObj)
            } else {
                completion(false, [Forms]())
            }
        }) { (error) in
            completion(true, [Forms]())
        }
    }
    
    func getFormDetails(_ completion :@escaping (_ success: Bool, _ error: Error?) -> Void) {
        ServiceManager.fetchDataFromGetService(baseUrl, serviceName: "consentform_detailsapi.php", parameters: ["consentkey": self.formId!, "projectkey" : MCAppDetails.sharedAppDetails!.appKey] as [String: AnyObject], success: { (result) in
            if let res = result["posts"] as? [String: String] {
                if let values = res["Frontend"]?.components(separatedBy: ","), values.count > 0 {
                    self.requiredValues = [MCQuestion]()
                    for value in values {
                        if !value.isEmpty {
                            let question = MCQuestion(question: value)
                            question.answer = ""
                            self.requiredValues?.append(question)
                        }
                    }
                }
                if let signatures = res["Signature"]?.components(separatedBy: ","), signatures.count > 0 {
                    self.signatures = [Signature]()
                    for signature in signatures {
                        let sign = Signature(title: signature)
                        self.signatures?.append(sign)
                    }
                }
                self.formContent = res["Text"]
                self.isDentistNameRequired = self.formContent.contains("kDentistName")
                self.isToothNumberRequired = self.formContent.contains("kToothNumber")
                self.isDataFetched = true
                completion(true, nil)
            } else {
                completion(false, NSError(domain: "", code: 0, userInfo: [NSLocalizedDescriptionKey: "Something went wrong, please try again later"]) as Error)
            }
        }) { (error) in
            completion(false, error)
        }
    }
    
    fileprivate class func getFormObjects (MainForms forms : [[String : [String]]],ManualConsents manualConsentForms: [String], APIConsnets consentForms: [[String: String]], isSubForm : Bool) -> [Forms] {
        var formList: [Forms] = [Forms]()
        var index = 0
        for formDict in forms {
            let formObject = Forms()
            formObject.formTitle = formDict.keys.first!
            formObject.isSelected = false
            formObject.index = index
            index = index + 1
            
            if formObject.formTitle == kConsentForms {
                formObject.subForms = [Forms]()
                
                for (_, form) in manualConsentForms.enumerated() {
                    let formManualObj = Forms()
                    formManualObj.isSelected = false
                    formManualObj.formId = nil
                    formManualObj.index = index
                    index = index + 1
                    formManualObj.formTitle = form
                    formManualObj.isToothNumberRequired = toothNumberRequired.contains(form)
                    formManualObj.isDentistNameRequired = kDentistNameNeededForms.contains(form)
                    formObject.subForms.append(formManualObj)
                }
                
                for (_, form) in consentForms.enumerated() {
                    let consentFormObj = Forms()
                    consentFormObj.isSelected = false
                    consentFormObj.formTitle = form["Name"]?.uppercased()
                    consentFormObj.formId = form["Key"]
                    consentFormObj.isToothNumberRequired = false
                    consentFormObj.isDentistNameRequired = false
                    consentFormObj.index = index
                    index = index + 1
                    formObject.subForms.append(consentFormObj)
                }
            } else {
                formObject.subForms = [Forms]()
                for (_, form) in formDict.values.first!.enumerated() {
                    let formObj = Forms()
                    formObj.isSelected = false
                    formObj.formTitle = form
                    formObj.index = index
                    index = index + 1
                    formObj.isToothNumberRequired = toothNumberRequired.contains(form)
                    formObject.subForms.append(formObj)
                }
            }
            formList.append(formObject)
        }
        return formList
    }
//    fileprivate class func getFormObjects (MainForms forms : [String], ManualConsents manualConsentForms: [String], APIConsnets consentForms: [[String: String]], isSubForm : Bool) -> [Forms] {
//        var formList : [Forms]! = [Forms]()
//        for (idx, form) in forms.enumerated() {
//            let formObj = Forms()
//            formObj.isSelected = false
//            formObj.index = isSubForm ? idx + consentIndex : idx
//            formObj.formTitle = form
//            formObj.isToothNumberRequired = toothNumberRequired.contains(form)
//            formObj.isDentistNameRequired = kDentistNameNeededForms.contains(form)
//            if formObj.formTitle == kConsentForms {
//                formObj.subForms = [Forms]()
//                for (_, form) in consentForms.enumerated() {
//                    let consentFormObj = Forms()
//                    consentFormObj.isSelected = false
//                    consentFormObj.formTitle = form["Name"]?.uppercased()
//                    consentFormObj.formId = form["Key"]
//                    consentFormObj.isToothNumberRequired = false
//                    consentFormObj.isDentistNameRequired = false
//                    consentFormObj.index = idx + consentIndex
//                    formObj.subForms.append(consentFormObj)
//                }
//            }
//            
//            formList.append(formObj)
//        }
//        return formList
//    }
}
