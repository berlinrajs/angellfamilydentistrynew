//
//  AppDetails.swift
//  MConsentOpenDental
//
//  Created by Berlin Raj on 24/04/17.
//  Copyright © 2017 SRS Web Solutions. All rights reserved.
//

import UIKit

enum EMRType: Int {
    case none = 0, openDental, eagleSoft, dentrix
}

class MCAppDetails: NSObject, NSCoding {
    
    var formList: [Forms]!
    
    var imageBackground: UIImage?
//    var imageBanner: UIImage?
    var topLogo: UIImage?
    var imageBackButton: UIImage?
    var imageNextButton: UIImage?
    var imageAddressLogo: UIImage?
    
    var imageBackgroundName: String!
//    var imageBannerName: String!
    var topLogoName: String!
    var imageBackButtonName: String!
    var imageNextButtonName: String!
    var imageAddressLogoName: String!
    
    var clinicName: String!
    var dentistNames: [String]!
    
    var street: String!
    var city: String!
    var state: String!
    var zipCode: String!
    
    var phoneNumber: String!
    var faxNumber: String!
    var email: String!
    
    var baseURL: String!
    var appEmrType: EMRType!
    var appKey: String!
    
    var cityState: String! {
        get {
            return self.city + ", " + self.state
        }
    }
    
    var address: String! {
        get {
            var addressArray = [String]()
            if self.street != nil && !self.street.isEmpty {
                addressArray.append(self.street)
            }
            if self.city != nil && !self.city.isEmpty {
                addressArray.append(self.city)
            }
            if self.state != nil && !self.state.isEmpty {
                addressArray.append(self.state)
            }
            if self.zipCode != nil && !self.zipCode.isEmpty {
                addressArray.append(self.zipCode)
            }
            return addressArray.joined(separator: ", ")
        }
    }
    
    var appName: String! {
        get {
            return self.clinicName.uppercased()
        }
    }
    
    class func errorFoundInDict(details: [String: AnyObject]) -> String? {
        if details["ClinicName"] as! String == "" {
            return "CLINIC NAME NOT FOUND"
        }
        if details["Place"] as! String == "" {
            return "CITY NOT FOUND"
        }
        if details["State"] as! String == "" {
            return "STATE NOT FOUND"
        }
        if details["BaseUrl"] as! String == "" {
            return "API NOT FOUND"
        }
        if details["DentistName"] as! String == "" {
            return "DENTIST NAME NOT FOUND"
        }
        if details["emrtype"] as! String != "3" {
            return "EMR TYPE MISMATCH"
        }
        if details["AppKey"] as! String == "" {
            return "APPKEY COULD NOT BE EMPTY"
        }
        if (details["AppKey"] as! String).contains(" ") {
            return "INVALID APP KEY"
        }
        if details["backgroundImage"] as! String == "" {
            return "EMPTY IMAGE FOUND, PLEASE UPLOAD EVERY IMAGES IN ADMIN PANEL AND CONTINUE"
        }
        if details["topLogo"] as! String == "" {
            return "EMPTY IMAGE FOUND, PLEASE UPLOAD EVERY IMAGES IN ADMIN PANEL AND CONTINUE"
        }
//        if details["imageBanner"] as! String == "" {
//            return "EMPTY IMAGE FOUND, PLEASE UPLOAD EVERY IMAGES IN ADMIN PANEL AND CONTINUE"
//        }
        if details["backbuttonImage"] as! String == "" {
            return "EMPTY IMAGE FOUND, PLEASE UPLOAD EVERY IMAGES IN ADMIN PANEL AND CONTINUE"
        }
        if details["buttonImage"] as! String == "" {
            return "EMPTY IMAGE FOUND, PLEASE UPLOAD EVERY IMAGES IN ADMIN PANEL AND CONTINUE"
        }
        if details["pdfLogo"] as! String == "" {
            return "EMPTY IMAGE FOUND, PLEASE UPLOAD EVERY IMAGES IN ADMIN PANEL AND CONTINUE"
        }

        return nil
//        let baseUrl = details["BaseUrl"] as! String
//        
//        if let ipAddresses = Reachability.getIPAddresses() {
//            if baseUrl.contains(ipAddresses) {
//                return nil
//            }
//        } else {
//            return "PLEASE CONNECT TO A NETWORK AND TRY AGAIN"
//        }
//        
//        return "IP ADDRESS WRONG"
    }
    
    class var sharedAppDetails: MCAppDetails? {
        get {
            if let data = UserDefaults.standard.value(forKey: kInitialValuesDictKey) as? Data {
                let appDetails = NSKeyedUnarchiver.unarchiveObject(with: data) as! MCAppDetails
                return appDetails
            } else {
                return nil
            }
        } set {
            UserDefaults.standard.set(NSKeyedArchiver.archivedData(withRootObject: newValue!), forKey: kInitialValuesDictKey)
            UserDefaults.standard.synchronize()
        }
    }
    
    class var isNewVersion: Bool {
        get {
            if let version = UserDefaults.standard.value(forKey: kVersionKey) as? String {
                if version != Bundle.main.infoDictionary?[kCFBundleVersionKey as String] as? String {
                    return true
                }
            }
            return false
        }
    }
    
    override init() {
        super.init()
    }
    
    init(WithClinicDetails details: [String: AnyObject]) {
        super.init()
        
        self.formList = [Forms]()
        
        self.baseURL = details["BaseUrl"] as! String
        self.appEmrType = EMRType(rawValue: Int(details["emrtype"] as! String) ?? 3)
        self.appKey = details["AppKey"] as! String
        
        self.dentistNames = (details["DentistName"] as! String).components(separatedBy: "||")
        self.clinicName = details["ClinicName"] as! String
        
        self.street = details["address"] as! String
        self.city = details["Place"] as! String
        self.state = details["State"] as! String
        self.zipCode = details["zip"] as! String
        self.email = details["Email"] as! String
        self.phoneNumber = (details["phone"] as! String).formattedPhoneNumber
        self.faxNumber = (details["fax"] as! String).formattedPhoneNumber
        
        self.imageBackgroundName = details["backgroundImage"] as! String
//        self.imageBannerName = details["bannerImage"] as! String
        self.topLogoName = details["topLogo"] as! String
        self.imageBackButtonName = details["backbuttonImage"] as! String
        self.imageNextButtonName = details["buttonImage"] as! String
        self.imageAddressLogoName = details["pdfLogo"] as! String
    }
    
    class var hasPreviousUI: Bool {
        get {
            return UserDefaults.standard.bool(forKey: kInitialUILoaddedKey)
        } set {
            UserDefaults.standard.set(newValue, forKey: kInitialUILoaddedKey)
            UserDefaults.standard.synchronize()
        }
    }
    
    class var loggedIn: Bool {
        get {
            return UserDefaults.standard.bool(forKey: kAppLoggedInKey)
        } set {
            UserDefaults.standard.set(newValue, forKey: kAppLoggedInKey)
            UserDefaults.standard.synchronize()
        }
    }
    
    class var formsLoadded: Bool {
        get {
            return UserDefaults.standard.bool(forKey: kFormsLoaddedkey)
        } set {
            UserDefaults.standard.set(newValue, forKey: kFormsLoaddedkey)
            UserDefaults.standard.synchronize()
        }
    }
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(self.imageBackground == nil ? nil : UIImagePNGRepresentation(self.imageBackground!), forKey: "BackgroundImage")
//        aCoder.encode(self.imageBanner == nil ? nil : UIImagePNGRepresentation(self.imageBanner!), forKey: "imageBanner")
        aCoder.encode(self.topLogo == nil ? nil : UIImagePNGRepresentation(self.topLogo!), forKey: "topLogo")
        aCoder.encode(self.imageBackButton == nil ? nil : UIImagePNGRepresentation(self.imageBackButton!), forKey: "BackArrow")
        aCoder.encode(self.imageNextButton == nil ? nil : UIImagePNGRepresentation(self.imageNextButton!), forKey: "ButtonBg")
        aCoder.encode(self.imageAddressLogo == nil ? nil : UIImagePNGRepresentation(self.imageAddressLogo!), forKey: "LogoWithAddress")
        aCoder.encode(self.appEmrType.rawValue, forKey: "appEmrType")
        
        aCoder.encode(self.baseURL, forKey: "baseURL")
        aCoder.encode(self.appKey, forKey: "appKey")
        
        aCoder.encode(self.clinicName, forKey: "clinicName")
        aCoder.encode(self.dentistNames, forKey: "dentistNames")
        
        aCoder.encode(self.street, forKey: "street")
        aCoder.encode(self.city, forKey: "place")
        aCoder.encode(self.state, forKey: "state")
        aCoder.encode(self.zipCode, forKey: "zipCode")
        aCoder.encode(self.email, forKey: "email")
        aCoder.encode(self.phoneNumber, forKey: "phoneNumber")
        aCoder.encode(self.faxNumber, forKey: "faxNumber")
        
        aCoder.encode(self.imageBackgroundName, forKey: "imageBackgroundName")
//        aCoder.encode(self.imageBannerName, forKey: "imageBannerName")
        aCoder.encode(self.topLogoName, forKey: "topLogoName")
        aCoder.encode(self.imageBackButtonName, forKey: "imageBackButtonName")
        aCoder.encode(self.imageNextButtonName, forKey: "imageNextButtonName")
        aCoder.encode(self.imageAddressLogoName, forKey: "imageAddressLogoName")
        
        aCoder.encode(self.formList, forKey: "Forms")
    }
    
    required init(coder decoder: NSCoder) {
        super.init()
        
        if let forms = decoder.decodeObject(forKey: "Forms") as? [Forms] {
            self.formList = forms
        } else {
            self.formList = [Forms]()
        }
        
        
        if MCAppDetails.hasPreviousUI == true {
            if let data = decoder.decodeObject(forKey: "BackgroundImage") as? Data {
                self.imageBackground = UIImage(data: data)
            } else {
                self.imageBackground = nil
            }
//            if let data = decoder.decodeObject(forKey: "imageBanner") as? Data {
//                self.imageBanner = UIImage(data: data)
//            } else {
//                self.imageBanner = nil
//            }
            if let data = decoder.decodeObject(forKey: "topLogo") as? Data {
                self.topLogo = UIImage(data: data)
            } else {
                self.topLogo = nil
            }
            if let data = decoder.decodeObject(forKey: "BackArrow") as? Data {
                self.imageBackButton = UIImage(data: data)
            } else {
                self.imageBackButton = nil
            }
            if let data = decoder.decodeObject(forKey: "ButtonBg") as? Data {
                self.imageNextButton = UIImage(data: data)
            } else {
                self.imageNextButton = nil
            }
            if let data = decoder.decodeObject(forKey: "LogoWithAddress") as? Data {
                self.imageAddressLogo = UIImage(data: data)
            } else {
                self.imageAddressLogo = nil
            }
        } else {
            self.imageBackground = nil
//            self.imageBanner = nil
            self.imageBackButton = nil
            self.imageNextButton = nil
            self.imageAddressLogo = nil
        }
        
        self.appKey = decoder.decodeObject(forKey: "appKey") as! String
        self.baseURL = decoder.decodeObject(forKey: "baseURL") as! String
        self.appEmrType = EMRType(rawValue: decoder.decodeObject(forKey: "appEmrType") as? Int ?? 3)
        
        self.clinicName = decoder.decodeObject(forKey: "clinicName") as! String
        self.dentistNames = decoder.decodeObject(forKey: "dentistNames") as! [String]
        
        self.phoneNumber = decoder.decodeObject(forKey: "phoneNumber") as? String ?? ""
        self.faxNumber = decoder.decodeObject(forKey: "faxNumber") as? String ?? ""
        self.email = decoder.decodeObject(forKey: "email") as! String
        
        self.street = decoder.decodeObject(forKey: "street") as? String ?? ""
        self.city = decoder.decodeObject(forKey: "place") as! String
        self.state = decoder.decodeObject(forKey: "state") as! String
        self.zipCode = decoder.decodeObject(forKey: "zipCode") as? String ?? ""
        
        self.imageBackgroundName = decoder.decodeObject(forKey: "imageBackgroundName") as! String
//        self.imageBannerName = decoder.decodeObject(forKey: "imageBannerName") as! String
        self.topLogoName = decoder.decodeObject(forKey: "topLogoName") as? String ?? ""
        self.imageBackButtonName = decoder.decodeObject(forKey: "imageBackButtonName") as! String
        self.imageNextButtonName = decoder.decodeObject(forKey: "imageNextButtonName") as! String
        self.imageAddressLogoName = decoder.decodeObject(forKey: "imageAddressLogoName") as! String
    }
//    
//    func fetchImages() {
//        BRProgressHUD.showWithTitle("Loading User Interface...")
//        
//        //        https://srsadmin.mconsent.net/uploads/Sari0001/Sari0001_backgroundimage.png
//        //        https://srsadmin.mconsent.net/uploads/Sari0001/Sari0001_buttonimage.png
//        //        https://srsadmin.mconsent.net/uploads/Sari0001/Sari0001_pdflogo.png
//        //        https://srsadmin.mconsent.net/uploads/Sari0001/Sari0001_toplogo.png
//        //        https://srsadmin.mconsent.net/uploads/Sari0001/Sari0001_backbutton.png
//        
//        func getImagefromURL(url: URL, completionBlock:@escaping (_ image : UIImage?) -> Void) {
//            
//            URLSession.shared.dataTask(with: url) { (data, response, error) in
//                if error == nil {
//                    completionBlock(UIImage(data: data!))
//                } else {
//                    completionBlock(nil)
//                }
//                }.resume()
//        }
//        
//        getImagefromURL(url: URL(string: self.initialImageURL + "\(self.appKey)_backgroundimage.png")!) { (image1) in
//            self.imageBackground = image1
//            getImagefromURL(url: URL(string: self.initialImageURL + "\(self.appKey)_buttonimage.png")!) { (image2) in
//                self.imageNextButton = image2
//                getImagefromURL(url: URL(string: self.initialImageURL + "\(self.appKey)_toplogo.png")!) { (image3) in
//                    self.imageLogo = image3
//                    getImagefromURL(url: URL(string: self.initialImageURL + "\(self.appKey)_pdflogo.png")!) { (image4) in
//                        self.imageAddressLogo = image4
//                        getImagefromURL(url: URL(string: self.initialImageURL + "\(self.appKey)_backbutton.png")!) { (image5) in
//                            DispatchQueue.main.async {
//                                self.imageBackButton = image5
//                            }
//                        }
//                    }
//                }
//            }
//        }
//        
//    }
}
