//
//  FormFile.swift
//  MConsent For OD
//
//  Created by Berlin Raj on 04/07/17.
//  Copyright © 2017 SRS Web Solutions. All rights reserved.
//

import UIKit

class FormFile: NSObject {
    
    var filePath: String!
    var creationDate: Date!
    var creationDateString: String!
    var creationDateTime: String!
    
    init(Path path: String) {
        super.init()
        
        self.filePath = path
        do {
            let fileAttribute = try FileManager.default.attributesOfItem(atPath: path)
            self.creationDate = fileAttribute[FileAttributeKey.creationDate] as! Date
        } catch {
            self.creationDate = Date()
        }
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MMM dd, yyyy hh:mm:ss a"
        
        self.creationDateTime = dateFormatter.string(from: self.creationDate)
        
        dateFormatter.dateFormat = "MMM dd, yyyy"
        self.creationDateString = dateFormatter.string(from: self.creationDate)
    }
}
