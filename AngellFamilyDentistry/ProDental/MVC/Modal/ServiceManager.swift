//
//  ServiceManager.swift
//   Angell Family Dentistry
//  1050
//  Created by Bose on 02/02/16.
//  Copyright © 2016  Angell Family Dentistry. All rights reserved.
//

import UIKit
let baseUrl = "https://srsadmin.mconsent.net/api/"
var hostUrl: String! {
    get {
        return SystemConfig.sharedConfig == nil ? "http://192.168.1.253:8080/" : "http://\(SystemConfig.sharedConfig!.hostIp!):1050/"
    }
}

extension AFHTTPSessionManager {
    static let sharedManager: AFHTTPSessionManager = AFHTTPSessionManager(baseURL: nil)
}

class ServiceManager: NSObject {
    
    static var sharedManager = AFHTTPSessionManager()
    
    class func fetchDataFromPostService(_ baseUrlString: String, serviceName: String, parameters : [String : AnyObject]?, success:@escaping (_ result : AnyObject) -> Void, failure :@escaping (_ error : Error) -> Void) {
        
        let manager = AFHTTPSessionManager.sharedManager
//        (baseURL: URL(string: baseUrlString), sessionConfiguration: URLSessionConfiguration.default)
        manager.responseSerializer.acceptableContentTypes = ["text/html"]
        manager.post(baseUrlString + serviceName, parameters: parameters, progress: { (progress) in
            
        }, success: { (task, result) in
            success(result as AnyObject)
        }) { (task, error) in
            failure(error)
        }
    }
    
    class func fetchDataFromGetService(_ baseUrlString: String, serviceName: String, parameters : [String : AnyObject]?, success:@escaping (_ result : AnyObject) -> Void, failure :@escaping (_ error : Error) -> Void) {
        let manager = AFHTTPSessionManager.sharedManager
//        (baseURL: URL(string: baseUrlString), sessionConfiguration: URLSessionConfiguration.default)
        
        manager.responseSerializer.acceptableContentTypes = ["text/html"]
        manager.get(baseUrlString + serviceName, parameters: parameters, progress: { (progress) in
            
        }, success: { (task, result) in
            success(result as AnyObject)
        }) { (task, error) in
            failure(error)
        }
    }
    
    class func getImagefromURL(url: URL, completionBlock:@escaping (_ image : UIImage?) -> Void) {
        
        URLSession.shared.dataTask(with: url) { (data, response, error) in
            if error == nil {
                completionBlock(UIImage(data: data!))
            } else {
                completionBlock(nil)
            }
            }.resume()
    }
    
    class func getDataFromServer(baseUrlString: String, serviceName: String, parameters : [String : AnyObject]?, success: @escaping (_ result : AnyObject) -> Void, failure : @escaping (_ error : Error) -> Void) {
        
        let manager = AFHTTPSessionManager.sharedManager
        manager.responseSerializer.acceptableContentTypes = ["text/html", "application/json"]
        if let accessToken = UserDefaults.standard.value(forKey: kAccessToken) as? String {
            manager.requestSerializer.setValue(accessToken, forHTTPHeaderField: "Authorization")
        }
        manager.get(baseUrlString + serviceName, parameters: parameters, progress: { (progress) in
            
        }, success: { (task, result) in
            if result != nil {
                if let accessToken = (result as AnyObject)["access_token"] as? String {
                    UserDefaults.standard.set("Bearer \(accessToken)", forKey: kAccessToken)
                    UserDefaults.standard.synchronize()
                }
                success(result as AnyObject)
            } else {
                failure(NSError(errorMessage: "Something went wrong"))
            }
        }) { (task, error) in
            failure(error)
        }
    }
    
    class func postDataToServer(baseUrlString: String, serviceName: String, parameters : [String : AnyObject]?, success: @escaping(_ result : AnyObject) -> Void, failure : @escaping(_ error : Error) -> Void) {
        
        let manager = AFHTTPSessionManager.sharedManager
        manager.responseSerializer.acceptableContentTypes = ["text/html", "application/json"]
        
        if let accessToken = UserDefaults.standard.value(forKey: kAccessToken) as? String {
            manager.requestSerializer.setValue(accessToken, forHTTPHeaderField: "Authorization")
        }
        
        manager.post(baseUrlString + serviceName, parameters: parameters, progress: { (progress) -> Void in
            
        }, success: { (task, result) -> Void in
            success(result as AnyObject)
        }) { (task, error) -> Void in
            print(baseUrlString + serviceName + error.localizedDescription)
            failure(error)
        }
    }
    
    class func loginWithUsername(_ userName: String, password: String, completion: @escaping (_ success: Bool, _ result: [String: AnyObject]?, _ error: Error?) -> Void) {
        ServiceManager.fetchDataFromPostService(baseUrl, serviceName: "getClinicdetailsByEmail.php", parameters: ["email": userName, "password": password] as [String: AnyObject], success: { (result) in
            if result["status"] as! String == "Success" {
                if let emrType = (result["results"] as AnyObject)["emrtype"] as? String, emrType == "3" {
                    completion(true, result["results"] as? [String: AnyObject], nil)
                } else {
                    completion(false, nil, NSError(domain: "", code: 0, userInfo: [NSLocalizedDescriptionKey: "Your EMR type supposed to be \"DENTRIX\""]) as Error)
                }
            } else {
                completion(false, nil, NSError(domain: "", code: 0, userInfo: [NSLocalizedDescriptionKey: result["Error"] as! String]) as Error)
            }
        }) { (error) in
            completion(false, nil, nil)
        }
    }
    
//    class func sendPatientDetails(_ patient: MCPatient, completion:@escaping (_ result : Bool, _ error: Error?) -> Void) {
//        
//        func getNAText(_ text: String?) -> String! {
//            if text == nil || text == "N/A" {
//                return ""
//            }
//            return text!
//        }
//        
//        let dateFormatter = DateFormatter()
//        dateFormatter.dateFormat = "MMM dd, yyyy"
//        let date = dateFormatter.date(from: patient.dateOfBirth)
//        dateFormatter.dateFormat = "yyyy-MM-dd"
//        
//        let dateFormatter1 = DateFormatter()
//        dateFormatter1.dateFormat = "EEE MMM dd, yyyy"
//        let dateString = dateFormatter1.string(from: Date())
//        
//        var xmlString = "<PatientRecord><PatGuid>"
//        
//        if let patientDetails = patient.patientDetails {
//            xmlString.append(patientDetails.patientGUID.lowercased())
//        } else {
//            xmlString.append("0")
//        }
//        
//        xmlString.append("</PatGuid><LastName>\(getNAText(patient.lastName)!)</LastName><FirstName>\(getNAText(patient.firstName)!)</FirstName><FamPos>1</FamPos><Gender>\((patient.gender == nil ? (patient.patientDetails != nil ? (patient.patientDetails!.gender ?? 0) : 0) : patient.gender!))</Gender><BirthDate>\(dateFormatter.string(from: date!))</BirthDate><Address><Street1>\(getNAText(patient.address)!)</Street1><City>\(getNAText(patient.city)!)</City><State>\(getNAText(patient.state)!)</State><Zipcode>\(getNAText(patient.zip)!)</Zipcode></Address><ProvId1>2222</ProvId1><Id2>123</Id2><DriversLicense>\(getNAText(patient.drivingLicense)!)</DriversLicense><ContactInfo><Contact><ContactText>\(getNAText(patient.cellPhone)!)</ContactText><ContactType>Mobile</ContactType><Preferred>false</Preferred></Contact><Contact><ContactText>\(getNAText(patient.homePhone)!)</ContactText><ContactType>Home</ContactType><Preferred>false</Preferred></Contact><Contact><ContactText>\(getNAText(patient.workPhone)!)</ContactText><ContactType>Work</ContactType><Preferred>false</Preferred></Contact><Contact><ContactText>\(getNAText(patient.email)!)</ContactText><ContactType>Email</ContactType><Preferred>false</Preferred></Contact></ContactInfo></PatientRecord>")
//
////        let xmlString = "<?xml version=\"1.0\" encoding=\"utf-16\"?><PatientRecord><PatGuid>\(patient.patientDetails!.patientGUID)</PatGuid><LastName>\(patient.lastName)</LastName><FirstName>\(patient.firstName)</FirstName><ProvId1>2222</ProvId1></PatientRecord>"
//        
//        var params = ["xml": xmlString] as [String: AnyObject]
//        if let patientDetails = patient.patientDetails {
//            params["PatientGuid"] = (patientDetails.patientGUID.isEmpty ? "0" : patientDetails.patientGUID.lowercased()) as AnyObject
//        }
//        var allergyIds = [String]()
//        var allergyNotes = [String]()
//        if let allergyArray = patient.medicalHistory.allergies {
//            for allergy in allergyArray {
//                if allergy.isSelected == true {
//                    allergyIds.append(allergy.allergyId)
//                    allergyNotes.append("\(allergy.allergyName!) - \(dateString) - \(allergy.allergyDescription!)")
//                }
//            }
//        }
//        params["alertIds"] = allergyIds as AnyObject
//        params["alertNote"] = allergyNotes.joined(separator: " , ") as AnyObject
//        
//        let manager = AFHTTPSessionManager.sharedManager
//        manager.responseSerializer.acceptableContentTypes = ["text/html", "application/json"]
//        
//        if let accessToken = UserDefaults.standard.value(forKey: kAccessToken) as? String {
//            manager.requestSerializer.setValue(accessToken, forHTTPHeaderField: "Authorization")
//        }
//        
//        let serviceName = patient.patientDetails == nil ? "api/patients/AddPatient" : "api/patients/EditPatient"
//        manager.post(hostUrl + serviceName, parameters: params, progress: { (progress) -> Void in
//            
//        }, success: { (task, result) in
//            if let _ = patient.patientDetails {
//                completion(true, nil)
//            } else if let patientId = (result as AnyObject)["PatientId"] as? String {
//                patient.patientDetails = PatientDetails(WithPatient: patient)
//                patient.patientDetails?.patientNumber = "\(patientId)"
//                patient.patientDetails?.patientGUID = (result as AnyObject)["PatientGuid"] as? String ?? ""
//                completion(true, nil)
//            } else {
//                completion(false, NSError(errorMessage: "SOMETHING WENT WRONG\nTRY AGAIN LATER"))
//            }
//        }) { (task, error) in
//            completion(false, error)
//        }
//    }

    class func sendPatientDetails(_ patient: MCPatient, completion:@escaping (_ result : Bool, _ error: Error?) -> Void) {
        
        func getNAText(_ text: String?) -> String! {
            if text == nil || text == "N/A" {
                return ""
            }
            return text!
        }
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MMM dd, yyyy"
        let date = dateFormatter.date(from: patient.dateOfBirth)
        dateFormatter.dateFormat = "yyyy-MM-dd"
        
        var xmlString = "<?xml version=\"1.0\" encoding=\"utf-16\"?><PatientRecord><PatGuid>"
        
        var providerId: String!
        if patient.patientDetails == nil {
            providerId = (patient.providerID == nil || patient.providerID.isEmpty) ? "DSDA" : patient.providerID
        } else {
            providerId = patient.patientDetails!.providerId ?? "DSDA"
        }
        
        if let patientDetails = patient.patientDetails {
            xmlString.append(patientDetails.patientGUID.lowercased())
        } else {
            xmlString.append("0")
        }
        
        xmlString.append("</PatGuid><LastName>\(getNAText(patient.lastName)!)</LastName><FirstName>\(getNAText(patient.firstName)!)</FirstName><FamPos>1</FamPos><Gender>\((patient.gender == nil ? (patient.patientDetails != nil ? (patient.patientDetails!.gender ?? 1) : 1) : patient.gender!))</Gender><BirthDate>\(dateFormatter.string(from: date!))</BirthDate><Address><Street1>\(getNAText(patient.address)!)</Street1><City>\(getNAText(patient.city)!)</City><State>\(getNAText(patient.state)!)</State><Zipcode>\(getNAText(patient.zipCode)!)</Zipcode></Address><ProvId1>\(providerId!)</ProvId1><DriversLicense>\(getNAText(patient.drivingLicense)!)</DriversLicense><ContactInfo><Contact><ContactText>\(getNAText(patient.cellPhone)!)</ContactText><ContactType>Mobile</ContactType><Preferred>false</Preferred></Contact><Contact><ContactText>\(getNAText(patient.homePhone)!)</ContactText><ContactType>Home</ContactType><Preferred>false</Preferred></Contact><Contact><ContactText>\(getNAText(patient.workPhone)!)</ContactText><ContactType>Work</ContactType><Preferred>false</Preferred></Contact><Contact><ContactText>\(getNAText(patient.email)!)</ContactText><ContactType>Email</ContactType><Preferred>false</Preferred></Contact></ContactInfo></PatientRecord>")
        
//        <Id2>123</Id2>
//        var patientGUID: String = "0"
//        if let patientDetails = patient.patientDetails {
//            patientGUID = patientDetails.patientGUID.lowercased()
//        }
//        
//        let xmlString = "<?xml version=\"1.0\" encoding=\"utf-16\"?><PatientRecord><PatGuid>\(patientGUID)</PatGuid><LastName>\(patient.lastName!)</LastName><FirstName>\(patient.firstName!)</FirstName><ProvId1>2222</ProvId1></PatientRecord>"
        
        var params = ["xml": xmlString] as [String: AnyObject]
        if let patientDetails = patient.patientDetails {
            params["PatientGuid"] = (patientDetails.patientGUID.isEmpty ? "0" : patientDetails.patientGUID.lowercased()) as AnyObject
        }
       
        let manager = AFHTTPSessionManager.sharedManager
        manager.responseSerializer.acceptableContentTypes = ["text/html", "application/json"]
        if let accessToken = UserDefaults.standard.value(forKey: kAccessToken) as? String {
            manager.requestSerializer.setValue(accessToken, forHTTPHeaderField: "Authorization")
        }
        
//        manager.requestSerializer.setValue("application/json; charset=utf-8", forHTTPHeaderField: "Content-Type")
//        manager.requestSerializer.setValue("application/json", forHTTPHeaderField: "Accept")
        
        manager.post(hostUrl! + (patient.patientDetails == nil ? "api/patients/AddPatient/" : "api/patients/EditPatient/"), parameters: params, progress: { (progress) in
            
        }, success: { (task, result) in
            if let _ = patient.patientDetails {
                completion(true, nil)
            } else if let patientId = (result as AnyObject)["PatientId"] as? String {
                patient.patientDetails = PatientDetails(WithPatient: patient)
                patient.patientDetails?.patientNumber = "\(patientId)"
                patient.patientDetails?.patientGUID = (result as AnyObject)["PatientGuid"] as? String ?? ""
                completion(true, nil)
            } else {
                completion(false, NSError(errorMessage: "SOMETHING WENT WRONG\nTRY AGAIN LATER"))
            }
        }) { (task, error) in
            completion(false, error)
        }
    }
    
    class func uploadFile(_ fileName: String, patientName: String, formName: String, pdfData: Data, completion: @escaping (_ success: Bool, _ errorMessage: String?)-> Void) {
        // https://mconsent.net/admin/api/activity_api.php?client_name=pp&formname=MedicalHistoryform&task_data=image&email=srs@gmail.com
        
        let manager = AFHTTPSessionManager.sharedManager
//        (baseURL: URL(string: "https://mconsent.net/admin/api/")!, sessionConfiguration: URLSessionConfiguration.default.copy() as? URLSessionConfiguration)
        let appDetails = MCAppDetails.sharedAppDetails!
        manager.responseSerializer.acceptableContentTypes = ["text/html", "application/json"]
        manager.post("https://mconsent.net/admin/api/activity_api.php?", parameters: ["client_name": appDetails.clinicName.fileName, "patient_name": patientName, "formname": formName, "appkey": appDetails.appKey], constructingBodyWith: { (data) in
            data.appendPart(withFileData: pdfData, name: "task_data", fileName: fileName, mimeType: fileName.hasSuffix(".pdf") ? "application/pdf" : "image/jpeg")
        }, progress: { (progress) in
            
        }, success: { (task, result) in
            completion(true, nil)
            print("RESULT: \(String(describing: result))")
        }, failure: { (task, error) in
            completion(false, error.localizedDescription)
            print("ERROR: \(error.localizedDescription)")
        })
    }
    
    class func uploadFormToServer(fileName: String, formName: String, pdfData: Data, completion: @escaping (_ success: Bool, _ errorMessage: String?)-> Void) {
        let folderName = formName.replacingOccurrences(of: "_", with: " ")
        var formFolderName: String!
        formFolderName = folderName
//        if hipaaFolder.contains(folderName){
//            formFolderName = "PHI HIPAA"
//        }else if newPatientFolder.contains(folderName){
//            formFolderName = "NEW PATIENT FORMS"
//        }else if dentalInsurFolder.contains(folderName){
//            formFolderName = "DENTAL INSURANCE"
//        }else if patientIdFolder.contains(folderName){
//            formFolderName = "PATIENT ID"
//        }else if consentFormFolder.contains(folderName){
//            formFolderName = "CONSENT FORMS"
//        }else{
//            formFolderName = folderName
//        }
        
        let patientId = fileName.components(separatedBy: "-").last!.components(separatedBy: ".")[0]
       // let params = ["patientId": patientId, "fileName": formName.replacingOccurrences(of: "_", with: " ")]
        let params = ["patientId": patientId, "fileName": formFolderName]
        let manager = AFHTTPSessionManager.sharedManager
        manager.responseSerializer.acceptableContentTypes = ["text/html", "application/json"]
        
        if let accessToken = UserDefaults.standard.value(forKey: kAccessToken) as? String {
            manager.requestSerializer.setValue(accessToken, forHTTPHeaderField: "Authorization")
        }
        
        manager.post(hostUrl! + "api/patients", parameters: params, constructingBodyWith: { (formData) in
            formData.appendPart(withFileData: pdfData, name: "file", fileName: fileName, mimeType: "application/pdf")
        }, progress: { (progress) in
            
        }, success: { (task, result) in
            print("RESULT: \(result ?? "NIL")")
             completion(true, nil)
        }, failure: { (task, error) in
            SystemConfig.sharedConfig?.checkConnectivity(completion: { (success, errorMessage) in
                if success {
                    completion(false, "SOMETHING WENT WRONG\nPLEASE CONTACT OUR SUPPORT")
                } else {
                    completion(false, errorMessage!)
                }
            })
            print(error)
        })
    }
}
