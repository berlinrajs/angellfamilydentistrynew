//
//  Allergy.swift
//  DiamondDentalAuto
//
//  Created by Bala Murugan on 10/7/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

class Allergy: NSObject {
    
    var allergyId : String!
    var allergyName : String!
    var allergyDescription: String!
    var isSelected : Bool! {
        willSet {
            if newValue == false {
                self.allergyDescription = "no"
            } else {
                self.allergyDescription = "yes"
            }
        }
    }
    
    init(details : AnyObject) {
        super.init()
        allergyId = details["alertId"] as? String
        allergyName = (details["alertName"] as? String)?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        allergyDescription = "no"
        isSelected = false
    }
    
    override init() {
        super.init()
    }
    
    class func getAllData(_ arrayObjects : [AnyObject]?) -> [Allergy]? {
        guard let arrayObjects = arrayObjects,  arrayObjects.count > 0 else {
            // Value requirements not met, do something
            return nil
        }
        var allgys = [Allergy]()
        for dict in arrayObjects {
            let allgy = Allergy(details: dict)
            allgys.append(allgy)
        }
        return allgys
    }
    
    class func getAllergyList( _ success:@escaping (_ result : [Allergy]) -> Void, failure :@escaping (_ error : Error?) -> Void) {
        DispatchQueue.main.async {
            ServiceManager.getDataFromServer(baseUrlString: hostUrl, serviceName: "api/medicalalerts/", parameters: nil, success: { (result) in
                let response = result as! [AnyObject]
                if response.count > 0 {
                    let allgy = Allergy.getAllData(response)
                    if let _ = allgy {
                        success(allgy!)
                    } else {
                        failure(NSError(errorMessage: "No data found") as Error)
                    }
                }else{
                    failure(NSError(errorMessage: "No data found") as Error)
                }
            }) { (error) in
                SystemConfig.sharedConfig?.checkConnectivity(completion: { (success, errorMessage) in
                    if success {
                        failure(NSError(errorMessage: "SOMETHING WENT WRONG\nPLEASE CONTACT OUR SUPPORT") as Error)
                    } else {
                        failure(NSError(errorMessage: errorMessage!) as Error)
                    }
                })
            }
        }
    }
    
    class func updateAllergyList(_ params : [String: AnyObject], success:@escaping (_ result : AnyObject) -> Void, failure :@escaping (_ error : Error?) -> Void) {
        DispatchQueue.main.async {
            ServiceManager.postDataToServer(baseUrlString: hostUrl, serviceName: "api/patients/UpdateAlert", parameters: params, success: { (result) in
                success(result)
            }) { (error) in
                SystemConfig.sharedConfig?.checkConnectivity(completion: { (success, errorMessage) in
                    if success {
                        failure(NSError(errorMessage: "SOMETHING WENT WRONG\nPLEASE CONTACT OUR SUPPORT") as Error)
                    } else {
                        failure(NSError(errorMessage: errorMessage!) as Error)
                    }
                })
            }
        }
    }
    
    
}
