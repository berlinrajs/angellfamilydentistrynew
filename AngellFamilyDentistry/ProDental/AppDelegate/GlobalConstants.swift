//
//  Constants.swift
//  MConsentForms
//
//  Created by Berlin Raj on 20/09/16.
//  Copyright © 2016 SRS Web Solutions. All rights reserved.
//

public enum TextFormat : Int {
    case Default = 0
    case SocialSecurity
    case ToothNumber
    case Phone
    case ExtensionCode
    case Zipcode
    case Number
    case Date
    case Month
    case Year
    case DateInCurrentYear
    case DateIn1980
    case Time
    case MiddleInitial
    case State
    case Amount
    case Email
    case SecureText
    case AlphaNumeric
    case DrivingLicense
    case NumbersWithoutValidation
}

//KEYS
let kFormsCompletedNotification = "kAllFormsCompletedSuccessfully"
let kDateChangedNotification = "kDateChangedToNextDateNotificaion"
let kIPChangedNotification = "kIPChangedNotificaion"
let kAccessToken = "ServerAccessToken"

let kAppLoggedInKey = "kApploggedIn"
let kFormsLoaddedkey = "kFormsLoadded"
let kAppLoginUsernameKey = "kAppLoginUserName"
let kAppLoginPasswordKey = "kAppLoginPassword"
let kInitialUILoaddedKey = "kInitialValuesLoadded"
let kInitialValuesDictKey = "kInitialValuesDict"

let kInitialConfigDictKey = "kInitialConfigDict"
let kHasSystemConfigKey = "kHasSystemConfig"
let kHasWorkStationConfigKey = "kHasWorkStationConfig"
let kInitialWorkSationConfigDictKey = "kInitialWorkSationConfigDict"

let kVersionKey = "kVersionKey"
let kServerImagePath = "https://srsadmin.mconsent.net/uploads/"

//VARIABLES
let kKeychainItemLoginName = "MConsent Auto: Google Login"
let kKeychainItemName = "MConsent Auto: Google Drive"
let kClientId = "264578207443-ei58vuuqt9vbsk8n39m0c72ii5nc5nkd.apps.googleusercontent.com"
let kClientSecret = "M77sFAbNhJjpguUkdUvXaSHd"
var kFolderName: String! {
    get {
        if let appDetails = MCAppDetails.sharedAppDetails {
            return "MConsent_\(appDetails.clinicName!.fileName)_Auto"
        } else {
            return "MConsentAuto"
        }
    }
}

let kDentistNameNeededForms: [String] = [kDentalCrown, kDentalImplants, kPartialDenture, kEndodonticTreatment]
let kCommonDateFormat = "MMM dd, yyyy"

let kGoogleID = "demo@srswebsolutions.com"
let kGooglePassword = "Srsweb123#"
let kOutputFileMustBeImage: Bool = false
//END OF VARIABLES

//FORMS
let kNewPatientSignInForm = "NEW PATIENT SIGN IN FORM"
let kChildSignInForm = "CHILD SIGN IN FORM"
let kMedicalHistoryForm = "MEDICAL HISTORY FORM"
let kPatientAuthorization = "RECORD RELEASE"

let kDentalImplants = "DENTAL IMPLANTS"
let kToothExtraction = "TOOTH EXTRACTION"
let kPartialDenture = "PARTIAL DENTURE ADJUSTMENTS"
let kInofficeWhitening = "INOFFICE WHITENING"
let kTreatmentOfChild = "TREATMENT OF A MINOR CHILD"
let kEndodonticTreatment = "ENDODONTIC TREATMENT"
let kDentalCrown = "DENTAL CROWN"
let kOpiodForm = "OPIOID FORM"
let kGeneralConsent = "GENERAL CONSENT"
let kUpdatePatientInformation = "UPDATE PATIENT INFORMATION"
let kUpdateHIPAA = "UPDATE PHI OR HIPAA"
let kFeedBack = "CUSTOMER REVIEW FORM"


//CONSENT FORMS
let kConsentForms = "CONSENT FORMS"


let kInsuranceCard = "SCAN INSURANCE CARD"
let kDrivingLicense = "SCAN PHOTO ID"
let kSelfieForm = "PATIENT PHOTO"
let kDocumentScan = "SCAN DOCUMENTS"

let hipaaFolder = [kUpdateHIPAA]
let newPatientFolder = [kChildSignInForm,kMedicalHistoryForm,kNewPatientSignInForm,kUpdatePatientInformation]
let consentFormFolder = [kDentalCrown,kDentalImplants,kEndodonticTreatment,kGeneralConsent,kInofficeWhitening,kOpiodForm,kToothExtraction,kTreatmentOfChild,kPatientAuthorization]
let dentalInsurFolder = [kInsuranceCard]
let patientIdFolder = [kDrivingLicense]

// END OF FORMS

let toothNumberRequired: [String] = [kToothExtraction, kDentalCrown, kEndodonticTreatment, kOpiodForm]
//Replace '""' with form names
let kTreatment = "TREATMENT PLAN"
let kAllConsentForms = [kDentalImplants, kToothExtraction, kPartialDenture, kInofficeWhitening, kTreatmentOfChild, kEndodonticTreatment, kDentalCrown, kOpiodForm, kGeneralConsent]
let kIntakeForms = ["INTAKE FORMS" : [kNewPatientSignInForm, kChildSignInForm, kUpdatePatientInformation,kUpdateHIPAA, kMedicalHistoryForm, kPatientAuthorization]]
let kConsentFormsAuto = [kConsentForms: []]
let kScanCards = ["SCAN CARDS": [kInsuranceCard, kDrivingLicense,kDocumentScan]]
let kTreatmentPlan = [kTreatment: []]


