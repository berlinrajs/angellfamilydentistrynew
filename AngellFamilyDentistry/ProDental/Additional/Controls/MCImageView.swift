//
//  MCImageView.swift
//  MConsentSample
//
//  Created by Berlin on 04/04/17.
//  Copyright © 2017 SRS Web Solutions. All rights reserved.
//

import UIKit

class MCImageView: UIImageView {

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    
    var borderWidth: CGFloat = 0.0 {
        didSet {
            layer.borderWidth = borderWidth
        }
    }

}


class BackgroundImageView: MCImageView {
    override func willMove(toSuperview newSuperview: UIView?) {
        super.willMove(toSuperview: newSuperview)
        self.clipsToBounds = true
        self.contentMode = .scaleAspectFill
        if let details = MCAppDetails.sharedAppDetails {
            self.image = details.imageBackground
        }
    }
}

class LogoImageView: MCImageView {
    override func willMove(toSuperview newSuperview: UIView?) {
        super.willMove(toSuperview: newSuperview)
        self.clipsToBounds = true
        self.contentMode = .scaleAspectFit
        if let details = MCAppDetails.sharedAppDetails {
            self.image = details.topLogo ?? UIImage(named: "Logo")
        } else {
            self.image = UIImage(named: "Logo")
        }
        self.backgroundColor = UIColor.clear
    }
}

class AddressLogoImageview: MCImageView {
    override func willMove(toSuperview newSuperview: UIView?) {
        super.willMove(toSuperview: newSuperview)
        self.clipsToBounds = true
        self.contentMode = .scaleAspectFit
        if let details = MCAppDetails.sharedAppDetails {
            self.image = details.imageAddressLogo
        }
    }
}
