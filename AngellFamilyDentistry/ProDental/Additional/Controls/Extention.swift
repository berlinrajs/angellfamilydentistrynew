//
//  Extention.swift
//  WestgateSmiles
//
//  Created by Berlin Raj on 20/09/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit
// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}

// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func > <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l > r
  default:
    return rhs < lhs
  }
}

// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func <= <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l <= r
  default:
    return !(rhs < lhs)
  }
}

public let screenSize = UIScreen.main.bounds


public func getText(_ text : String) -> String {
    return "  \(text)"
}

extension DateFormatter {
    public convenience init(dateFormat format: String) {
        self.init()
        self.dateFormat = format
    }
}

extension UIViewController {
    
    func showCustomAlert(_ message: String) {
        CustomAlert.alertView().showWithTitle(message) { 
            
        }
    }
    
    func showAlert(_ title: String, message : String) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
        let alertOkAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.destructive) { (action) -> Void in
            
        }
        alertController.addAction(alertOkAction)
        self.present(alertController, animated: true, completion: nil)
    }
    func showAlert(_ message : String) {
        let title = MCAppDetails.sharedAppDetails == nil ? "MConsent Open Dental" : MCAppDetails.sharedAppDetails!.appName
        let alertController = UIAlertController(title:title, message: message, preferredStyle: UIAlertControllerStyle.alert)
        let alertOkAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.destructive) { (action) -> Void in
            
        }
        alertController.addAction(alertOkAction)
        self.present(alertController, animated: true, completion: nil)
    }
    func showAlert(_ message : String, completion: @escaping () -> Void) {
        let title = MCAppDetails.sharedAppDetails == nil ? "MConsent Open Dental" : MCAppDetails.sharedAppDetails!.appName
        let alertController = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
        let alertOkAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.destructive) { (action) -> Void in
            completion()
        }
        alertController.addAction(alertOkAction)
        self.present(alertController, animated: true, completion: nil)
    }
    func showAlert(_ message : String, buttonTitles: [String], completion: @escaping (_ buttonIndex: Int) -> Void) {
        let title = MCAppDetails.sharedAppDetails == nil ? "MConsent Open Dental" : MCAppDetails.sharedAppDetails!.appName
        let alertController = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
        for (idx, title) in buttonTitles.enumerated() {
            let alertOkAction = UIAlertAction(title: title.uppercased(), style: UIAlertActionStyle.destructive) { (action) -> Void in
                completion(idx)
            }
            alertController.addAction(alertOkAction)
        }
        
        self.present(alertController, animated: true, completion: nil)
    }
    
    func showAlert(_ message : String, buttonTitles: [String], styles: [UIAlertActionStyle], completion: @escaping (_ buttonIndex: Int) -> Void) {
        let title = MCAppDetails.sharedAppDetails == nil ? "MConsent Open Dental" : MCAppDetails.sharedAppDetails!.appName
        let alertController = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
        for (idx, title) in buttonTitles.enumerated() {
            let alertOkAction = UIAlertAction(title: title.uppercased(), style: styles[idx]) { (action) -> Void in
                completion(idx)
            }
            alertController.addAction(alertOkAction)
        }
        
        self.present(alertController, animated: true, completion: nil)
    }
}


extension NSError {
    
    convenience init(errorMessage : String) {
        self.init(domain: "Error", code: 101, userInfo: [NSLocalizedDescriptionKey : errorMessage])
    }
    
}

extension UILabel {
    func setAttributedText() {
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.alignment = NSTextAlignment.justified
        let attributedString = NSAttributedString(string: self.text!,
            attributes: [
                NSParagraphStyleAttributeName: paragraphStyle,
                NSBaselineOffsetAttributeName: NSNumber(value: 0 as Float),
                NSFontAttributeName : self.font
            ])
        self.attributedText = attributedString
    }
    
    func compare(val1: String?, val2: String?) {
        if val1 != val2 {
            textColor = UIColor.red
        }
    }
}


extension UITextField {
    var isEmpty : Bool {
        return self.text?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines).lengthOfBytes(using: String.Encoding.utf8) == 0
    }
    
    func getText() -> String {
        if self.isEmpty{
            return "N/A"
        }
        return self.text!
    }
    
    func setSavedText(text : String?) {
        if text == nil || text == "N/A" {
            self.text = ""
        }else{
            self.text = text
        }
    }
    
    func formatPhoneNumber(_ range: NSRange, string: String) -> Bool {
        if string.rangeOfCharacter(from: NSCharacterSet(charactersIn: "01234567890").inverted) != nil {
            return false
        }
        let newRange = self.text!.index(self.text!.startIndex, offsetBy: range.location)..<self.text!.index(self.text!.startIndex, offsetBy: range.location + range.length)
        let newString = self.text!.replacingCharacters(in: newRange, with: string)
        
        let components = newString.components(separatedBy: CharacterSet.decimalDigits.inverted)
        
        let decimalString : String = components.joined(separator: "")
        let length = decimalString.count
        let decimalStr = decimalString as NSString
        
        
        let hasLeadingOne = length > 0 && decimalStr.character(at: 0) == (1 as unichar)
        if length == 0 || (length > 10 && !hasLeadingOne) || length > 11
        {
            let newLength = self.text!.count + string.count - range.length as Int
            return (newLength > 10) ? false : true
        }
        var index = 0 as Int
        let formattedString = NSMutableString()
        
        if hasLeadingOne
        {
            formattedString.append("1 ")
            index += 1
        }
        if (length - index) > 3
        {
            let areaCode = decimalStr.substring(with: NSMakeRange(index, 3))
            formattedString.appendFormat("(%@)", areaCode)
            index += 3
        }
        if length - index > 3
        {
            let prefix = decimalStr.substring(with: NSMakeRange(index, 3))
            formattedString.appendFormat("%@-", prefix)
            index += 3
        }
        
        let remainder = decimalStr.substring(from: index)
        formattedString.append(remainder)
        self.text = formattedString as String
        return false
    }
    
    func formatZipCode(_ range: NSRange, string: String) -> Bool {
        if string.rangeOfCharacter(from: NSCharacterSet(charactersIn: "01234567890").inverted) != nil {
            return false
        }
        let newRange = self.text!.index(self.text!.startIndex, offsetBy: range.location)..<self.text!.index(self.text!.startIndex, offsetBy: range.location + range.length)
        let newString = self.text!.replacingCharacters(in: newRange, with: string)
        if newString.count > 5 {
            return false
        }
        return true
    }
    
    func formatNumbers(_ range: NSRange, string: String) -> Bool {
        if string.rangeOfCharacter(from: NSCharacterSet(charactersIn: "01234567890").inverted) != nil {
            return false
        }
        let newRange = self.text!.index(self.text!.startIndex, offsetBy: range.location)..<self.text!.index(self.text!.startIndex, offsetBy: range.location + range.length)
        let newString = self.text!.replacingCharacters(in: newRange, with: string)
        if newString.count > 9 {
            return false
        }
        return true
    }
    
    func formatNumbers(_ range: NSRange, string: String, count : Int) -> Bool {
        if string.rangeOfCharacter(from: CharacterSet(charactersIn: "01234567890").inverted) != nil {
            return false
        }
        let newRange = self.text!.index(self.text!.startIndex, offsetBy: range.location)..<self.text!.index(self.text!.startIndex, offsetBy: range.location + range.length)
        let newString = self.text!.replacingCharacters(in: newRange, with: string)
        if newString.count > count {
            return false
        }
        return true
    }
    
    func formatAmount(_ range: NSRange, string: String) -> Bool {
        if string.rangeOfCharacter(from: NSCharacterSet(charactersIn: "01234567890.").inverted) != nil {
            return false
        }
        let newRange = self.text!.index(self.text!.startIndex, offsetBy: range.location)..<self.text!.index(self.text!.startIndex, offsetBy: range.location + range.length)
        let floatAmount = self.text!.replacingCharacters(in: newRange, with: string).replacingOccurrences(of: "$", with: "").replacingOccurrences(of: ",", with: "")
        
        let numberFormatter = NumberFormatter()
        
        let inputNumber = floatAmount.contains(".") ? NSNumber(value: Double(floatAmount) ?? 0.0) : NSNumber(value: Int(floatAmount) ?? 0)
        if inputNumber == 0 {
            self.text = ""
            return false
        }
        numberFormatter.numberStyle = NumberFormatter.Style.currency
        var formattedNumberList = [numberFormatter.string(from: inputNumber)?.components(separatedBy: ".").first ?? ""]
        
        if floatAmount.contains(".") {
            formattedNumberList.append(floatAmount.components(separatedBy: ".").last ?? "")
        }
        
        self.text = formattedNumberList.joined(separator: ".")
        
        return false
    }
    
    func formatExt(_ range: NSRange, string: String) -> Bool {
        if string.rangeOfCharacter(from: NSCharacterSet(charactersIn: "01234567890").inverted) != nil {
            return false
        }
        let newRange = self.text!.index(self.text!.startIndex, offsetBy: range.location)..<self.text!.index(self.text!.startIndex, offsetBy: range.location + range.length)
        let newString = self.text!.replacingCharacters(in: newRange, with: string)
        if newString.count > 3 {
            return false
        }
        return true
    }
    func formatMiddleName(_ range: NSRange, string: String) -> Bool {
        let newRange = self.text!.index(self.text!.startIndex, offsetBy: range.location)..<self.text!.index(self.text!.startIndex, offsetBy: range.location + range.length)
        let newString = self.text!.replacingCharacters(in: newRange, with: string).trimmingCharacters(in: CharacterSet.letters.inverted)
        if newString.count > 1 {
            return false
        } else {
            text = newString
        }
        return false
    }
    
    func formatFamilyMembersCount(_ range: NSRange, string: String) -> Bool{
        if string.rangeOfCharacter(from: NSCharacterSet(charactersIn: "01234567890").inverted) != nil {
            return false
        }
        let newRange = self.text!.index(self.text!.startIndex, offsetBy: range.location)..<self.text!.index(self.text!.startIndex, offsetBy: range.location + range.length)
        let newString = self.text!.replacingCharacters(in: newRange, with: string)
        if newString.count > 2 {
            return false
        }
        return true
    }
    func formatNumbers(_ range: NSRange, string: String, count : Int, limit: Int?) -> Bool {
        if string.rangeOfCharacter(from: NSCharacterSet(charactersIn: "01234567890").inverted) != nil {
            return false
        }
        let newRange = self.text!.index(self.text!.startIndex, offsetBy: range.location)..<self.text!.index(self.text!.startIndex, offsetBy: range.location + range.length)
        let newString = self.text!.replacingCharacters(in: newRange, with: string)
        if newString.count > count {
            return false
        }
        if limit != nil && limit > 0 {
            return Int(newString) <= limit
        }
        return true
    }
    
    
    func formatDate(_ range: NSRange, string: String) -> Bool {
        if string.rangeOfCharacter(from: NSCharacterSet(charactersIn: "01234567890").inverted) != nil {
            return false
        }
        let newRange = self.text!.index(self.text!.startIndex, offsetBy: range.location)..<self.text!.index(self.text!.startIndex, offsetBy: range.location + range.length)
        let newString = self.text!.replacingCharacters(in: newRange, with: string)
        if newString.count > 2 {
            return false
        }
        //        let maximumDateForMonth: [String: Int] = ["": 31, "JAN": 31, "FEB": 29, "MAR": 31 ,"APR": 30 ,"MAY": 31 ,"JUN": 30 ,"JUL": 31 ,"AUG": 31 ,"SEP": 30 ,"OCT": 31 ,"NOV": 30 ,"DEC": 31]
        if Int(newString) > 31 {
            return false
        }
        return true
    }
    

    func formatAmericanCreditCardNumber(_ range: NSRange, string: String) -> Bool {
        if string.rangeOfCharacter(from: NSCharacterSet(charactersIn: "01234567890").inverted) != nil {
            return false
        }
        let newRange = self.text!.index(self.text!.startIndex, offsetBy: range.location)..<self.text!.index(self.text!.startIndex, offsetBy: range.location + range.length)
        let newString = self.text!.replacingCharacters(in: newRange, with: string)
        
        let components = newString.components(separatedBy: CharacterSet.decimalDigits.inverted)
        
        let decimalString : String = components.joined(separator: "")
        let length = decimalString.count
        let decimalStr = decimalString as NSString
        
        
        if length == 0 || length > 15
        {
            let newLength = self.text!.count + string.count - range.length as Int
            return (newLength > 15) ? false : true
        }
        var index = 0 as Int
        let formattedString = NSMutableString()
        
        
        if (length - index) > 5
        {
            let areaCode = decimalStr.substring(with: NSMakeRange(index, 5))
            formattedString.appendFormat("%@ ", areaCode)
            index += 5
        }
        if length - index > 5
        {
            let prefix = decimalStr.substring(with: NSMakeRange(index, 5))
            formattedString.appendFormat("%@ ", prefix)
            index += 5
        }
        if length - index > 5
        {
            let prefix = decimalStr.substring(with: NSMakeRange(index, 5))
            formattedString.appendFormat("%@ ", prefix)
            index += 5
        }
        
        let remainder = decimalStr.substring(from: index)
        formattedString.append(remainder)
        self.text = formattedString as String
        return false
    }
    
    func formatCreditCardNumber(_ range: NSRange, string: String) -> Bool {
        if string.rangeOfCharacter(from: NSCharacterSet(charactersIn: "01234567890").inverted) != nil {
            return false
        }
        let newRange = self.text!.index(self.text!.startIndex, offsetBy: range.location)..<self.text!.index(self.text!.startIndex, offsetBy: range.location + range.length)
        let newString = self.text!.replacingCharacters(in: newRange, with: string)
        
        let components = newString.components(separatedBy: CharacterSet.decimalDigits.inverted)
        
        let decimalString : String = components.joined(separator: "")
        let length = decimalString.count
        let decimalStr = decimalString as NSString
        
        
        if length == 0 || length > 16
        {
            let newLength = self.text!.count + string.count - range.length as Int
            return (newLength > 16) ? false : true
        }
        var index = 0 as Int
        let formattedString = NSMutableString()
        
        
        if (length - index) > 4
        {
            let areaCode = decimalStr.substring(with: NSMakeRange(index, 4))
            formattedString.appendFormat("%@ ", areaCode)
            index += 4
        }
        if length - index > 4
        {
            let prefix = decimalStr.substring(with: NSMakeRange(index, 4))
            formattedString.appendFormat("%@ ", prefix)
            index += 4
        }
        if length - index > 4
        {
            let prefix = decimalStr.substring(with: NSMakeRange(index, 4))
            formattedString.appendFormat("%@ ", prefix)
            index += 4
        }
        
        let remainder = decimalStr.substring(from: index)
        formattedString.append(remainder)
        self.text = formattedString as String
        return false
    }
    func formatInitial(_ range: NSRange, string: String) -> Bool {
        if string.rangeOfCharacter(from: CharacterSet(charactersIn: "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz,").inverted) != nil {
            return false
        }
        let newRange = text!.index(text!.startIndex, offsetBy: range.location)..<text!.index(text!.startIndex, offsetBy: range.location + range.length)
        let newString = text!.replacingCharacters(in: newRange, with: string)
        if newString.count > 1 {
            return false
        }
        return true
    }
    func formatSocialSecurityNumber(_ range: NSRange, string: String) -> Bool {
        if string.rangeOfCharacter(from: CharacterSet(charactersIn: "01234567890").inverted) != nil {
            return false
        }
        let newRange = self.text!.index(self.text!.startIndex, offsetBy: range.location)..<self.text!.index(self.text!.startIndex, offsetBy: range.location + range.length)
        let newString = self.text!.replacingCharacters(in: newRange, with: string)
        
        let components = newString.components(separatedBy: CharacterSet.decimalDigits.inverted)
        
        let decimalString : String = components.joined(separator: "")
        let length = decimalString.count
        let decimalStr = decimalString as NSString
        
        
        let hasLeadingOne = length > 0 && decimalStr.character(at: 0) == (1 as unichar)
        
        if length == 0 || (length > 9 && !hasLeadingOne) || length > 10
        {
            let newLength = self.text!.count + string.count - range.length as Int
            
            return (newLength > 9) ? false : true
        }
        var index = 0 as Int
        let formattedString = NSMutableString()
        
        if hasLeadingOne
        {
            formattedString.append("1 ")
            index += 1
        }
        if (length - index) > 3
        {
            let areaCode = decimalStr.substring(with: NSMakeRange(index, 3))
            formattedString.appendFormat("%@-", areaCode)
            index += 3
        }
        if length > 3 && length - index > 2
        {
            let prefix = decimalStr.substring(with: NSMakeRange(index, 2))
            formattedString.appendFormat("%@-", prefix)
            index += 2
        }
        
        let remainder = decimalStr.substring(from: index)
        formattedString.append(remainder)
        self.text = formattedString as String
        return false
    }
    
    func formatLicense(_ range: NSRange, string: String, count : Int) -> Bool {
        let newRange = self.text!.index(self.text!.startIndex, offsetBy: range.location)..<self.text!.index(self.text!.startIndex, offsetBy: range.location + range.length)
        let newString = self.text!.replacingCharacters(in: newRange, with: string)
        if newString.count > count {
            return false
        }
        return true
    }
    func formatToothNumbers(_ range: NSRange, string: String) -> Bool {
        if string.lengthOfBytes(using: String.Encoding.utf8) == 0 {
            return true
        }
        if string.rangeOfCharacter(from: NSCharacterSet(charactersIn: "01234567890").inverted) != nil {
            return false
        }
        
        let newRange = self.text!.index(self.text!.startIndex, offsetBy: range.location)..<self.text!.index(self.text!.startIndex, offsetBy: range.location + range.length)
        let textFieldString = self.text!.replacingCharacters(in: newRange, with: string)
        let textString = textFieldString.components(separatedBy: ",")
        
        if textFieldString.count > 2 {
            let lastString = textFieldString.substring(from: textFieldString.index(textFieldString.startIndex, offsetBy: textFieldString.count - 1))
            let lastTwoStrings = textFieldString.substring(from: textFieldString.index(textFieldString.startIndex, offsetBy: textFieldString.count - 2))
            let lastThreeStrings = textFieldString.substring(from: textFieldString.index(textFieldString.startIndex, offsetBy: textFieldString.count - 3))
            
            if lastTwoStrings == ",," {
                return false
            }
            if lastString == "," && lastThreeStrings.components(separatedBy: ",").count == 3 {
                let requiredString = textFieldString.substring(to: textFieldString.index(textFieldString.startIndex, offsetBy: textFieldString.count - 2)) + "0" + lastTwoStrings
                self.text = requiredString
                return false
            }
            
        } else {
            if textFieldString.count == 2 {
                let lastString = textFieldString.substring(from: textFieldString.index(textFieldString.startIndex, offsetBy: textFieldString.count - 1))
                if lastString == "," {
                    self.text = "0" + textFieldString
                    return false
                }
            }
            if textFieldString == "," {
                return false
            }
        }
        for text in textString {
            if text == "0" {
                return true
            }
            if text == "00" {
                return false
            }
            if Int(text) > 35 {
                return false
            }
        }
        return true
    }
}

extension UITextView {
    var isEmpty : Bool {
        return self.text?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines).lengthOfBytes(using: String.Encoding.utf8) == 0
    }
}

extension String {
    
    var trimSpaceAndLine: String {
        return self.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
    }
    
    var fileName : String {
        return self.replacingOccurrences(of: " - ", with: "_").replacingOccurrences(of: " ", with: "_").replacingOccurrences(of: "/", with: "_OR_").replacingOccurrences(of: "'", with: "").replacingOccurrences(of: "-", with: "_")
    }
    
    func setText() -> String {
        return self.isEmpty ? "N/A" : self
    }
    
    var isValidIP: Bool {
        let charcter  = CharacterSet(charactersIn: "0123456789.")
        let inputString = self.components(separatedBy: charcter)
        let filtered = inputString.joined(separator: "")
        if filtered.count > 0 {
            return false
        }
        let ipComponents = self.components(separatedBy: ".")
        if ipComponents.count != 4 {
            return false
        }
        for component in ipComponents {
            guard let mask = Int(component) else {
                return false
            }
            if mask > 255 {
                return false
            }
        }
        return true
    }
    
    public func rangeOfText(_ text : String) -> NSRange {
        return NSMakeRange(self.count - text.count, text.count)
    }
    
    func heightWithConstrainedWidth(_ width: CGFloat, font: UIFont) -> CGFloat {
        let constraintRect = CGSize(width: width, height: CGFloat.greatestFiniteMagnitude)
        let boundingBox = self.boundingRect(with: constraintRect, options: NSStringDrawingOptions.usesLineFragmentOrigin, attributes: [NSFontAttributeName: font], context: nil)
        return boundingBox.size.height
    }
    
    var isValidEmail : Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: self)
    }
    
    var socialSecurityNumber: String {
        get {
            if self.count != 9 {
                return self
            }
            var ssn: String = ""
            for char in self {
                ssn.append(char)
                if ssn.count == 3 || ssn.count == 6 {
                    ssn = ssn + "-"
                }
            }
            return ssn
        }
    }
    
    var isValidYear: Bool {
        if self.count != 4 {
            return false
        }
        let components = (Calendar.current as NSCalendar).components(NSCalendar.Unit.year, from: Date())
        if Int(self) > components.year {
            return false
        }
        return true
    }
    
    
    var isCreditCard: Bool {
        let charcter  = NSCharacterSet(charactersIn: "01234567890").inverted
        let inputString = self.components(separatedBy: charcter)
        let filtered = inputString.joined(separator: "")
        return filtered.count == 16
    }
    var isAmericanCreditCard: Bool {
        let charcter  = NSCharacterSet(charactersIn: "+0123456789").inverted
        let inputString = self.components(separatedBy: charcter)
        let filtered = inputString.joined(separator: "")
        return filtered.count == 15
    }

    
    var isPhoneNumber: Bool {
        let charcter  = NSCharacterSet(charactersIn: "0123456789").inverted
        let inputString = self.components(separatedBy: charcter)
        let filtered = inputString.joined(separator: "")
        return filtered.count == 10
    }
    var numbers: String {
        let charcter  = NSCharacterSet(charactersIn: "0123456789").inverted
        let inputString = self.components(separatedBy: charcter)
        let filtered = inputString.joined(separator: "")
        return filtered
    }
    
    var formattedPhoneNumber: String {
        let charcter  = NSCharacterSet(charactersIn: "0123456789").inverted
        let inputString = self.components(separatedBy: charcter)
        
        var phoneString = (inputString.joined(separator: ""))
        if phoneString.count != 10 {
            return phoneString
        }
        phoneString.insert("(", at: phoneString.startIndex)
        phoneString.insert(")", at: phoneString.index(phoneString.startIndex, offsetBy: 4))
        phoneString.insert("-", at: phoneString.index(phoneString.startIndex, offsetBy: 8))
        
        return phoneString
    }
    
    var isZipCode: Bool {
        let charcter  = NSCharacterSet(charactersIn: "0123456789").inverted
        let inputString = self.components(separatedBy: charcter)
        let filtered = inputString.joined(separator: "")
        return filtered.count == 5
    }
    
    var isSocialSecurityNumber: Bool {
        let charcter  = NSCharacterSet(charactersIn: "0123456789").inverted
        let inputString = self.components(separatedBy: charcter)
        let filtered = inputString.joined(separator: "")
        return filtered.count == 9
    }
    var isValidMRN: Bool {
        let charcter  = NSCharacterSet(charactersIn: "0123456789").inverted
        let inputString = self.components(separatedBy: charcter)
        let filtered = inputString.joined(separator: "")
        return filtered.count >= 7 && filtered.count <= 8
    }
    
    var isValidExt: Bool {
        let charcter  = NSCharacterSet(charactersIn: "0123456789").inverted
        let inputString = self.components(separatedBy: charcter)
        let filtered = inputString.joined(separator: "")
        return filtered.count == 3
    }
    
    func setTextForArrayOfLabels(_ arrayOfLabels: [UILabel]) {
        
        if arrayOfLabels.count == 0 {
            return
        }
        
        let wordArray = self.components(separatedBy: " ")
        
        var textToCheck: NSString = ""
        
        for string in wordArray {
            let previousLength = textToCheck.length
            let label = arrayOfLabels[0]
            textToCheck = textToCheck.length == 0 ? ((textToCheck as String) + string) as NSString: ((textToCheck as String) + " " + string) as NSString
            
            let size = textToCheck.size(attributes: [NSFontAttributeName: label.font])
            if size.height > label.frame.height || size.width > label.frame.width {
                var array = arrayOfLabels
                array.removeFirst()
                ((self as NSString).replacingCharacters(in: NSMakeRange(0, previousLength), with: "").trimmingCharacters(in: CharacterSet.whitespaces)).setTextForArrayOfLabels(array)
                return
            } else {
                label.text = textToCheck as String
            }
        }
    }
  
}

extension NSAttributedString {
    func heightWithConstrainedWidth(_ width: CGFloat) -> CGFloat {
        let constraintRect = CGSize(width: width, height: CGFloat.greatestFiniteMagnitude)
        let boundingBox = self.boundingRect(with: constraintRect, options: [.usesLineFragmentOrigin, .usesFontLeading], context: nil)
        return ceil(boundingBox.height) + 10
    }

    var trimmedAttributedText: NSAttributedString {
        var originalText: NSMutableAttributedString = NSMutableAttributedString.init(attributedString: self)
        
        if originalText.length > 0 {
            let last: NSAttributedString = originalText.attributedSubstring(from: NSMakeRange(originalText.length - 1, 1))
            if last.string == "\n" {
                originalText = originalText.attributedSubstring(from: NSMakeRange(0, originalText.length - 1)) as! NSMutableAttributedString
            }
        }
        
        return originalText
    }
}

