//
//  BRProgressHUD.swift
//  MDRegistration
//
//  Created by Berlin Raj on 20/09/16.
//  Copyright © 2016 SRS Web Solutions. All rights reserved.
//

class BRProgressHUD: UIView {
    
    static var hudView: BRProgressHUD!
    
    var labelTitle: UILabel!
    var activityView: UIActivityIndicatorView!
    
    class func sharedInstance() -> BRProgressHUD! {
        if hudView == nil {
            hudView = BRProgressHUD(frame: CGRect(x: 0, y: 0, width: screenSize.width, height: screenSize.height))
            hudView.backgroundColor = UIColor.black.withAlphaComponent(0.5)
            
            hudView.activityView = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.whiteLarge)
            hudView.activityView.frame = CGRect(x: 0, y: 0, width: 50, height: 50)
            hudView.activityView.center = hudView.center
            hudView.addSubview(hudView.activityView)
            
            hudView.labelTitle = UILabel(frame: CGRect.zero)
            hudView.labelTitle.textColor = UIColor.white
            hudView.labelTitle.numberOfLines = 0
            hudView.labelTitle.textAlignment = NSTextAlignment.center
            hudView.labelTitle.font = UIFont.systemFont(ofSize: 20)
            hudView.addSubview(hudView.labelTitle)
            hudView.alpha = 0.0
        }
        return hudView
    }
    
    func setTitle(_ text: String) {
        self.labelTitle.frame = CGRect(x: 0, y: self.activityView.frame.maxY + 2, width: 300, height: 80)
        self.labelTitle.text = text
        self.labelTitle.sizeToFit()
        self.labelTitle.frame = CGRect(x: 0, y: self.activityView.frame.maxY + 2, width: 300, height: self.labelTitle.frame.height)
        self.labelTitle.center.x = screenSize.width/2
    }
    
    class func showWithTitle(_ title: String) {
        let hudView = sharedInstance()
        hudView?.activityView.startAnimating()
        hudView?.setTitle(title)
        UIApplication.shared.delegate?.window!!.addSubview(hudView!)
        UIApplication.shared.delegate?.window!!.bringSubview(toFront: hudView!)
        hudView?.isHidden = false
//        UIView.animate(withDuration: 0.2, animations: {
            hudView?.alpha = 1.0
//            hudView?.transform = CGAffineTransform.init(scaleX: 1.5, y: 1.5)
//        }) { (finished) in
//            UIView.animate(withDuration: 0.2, animations: {
                hudView?.transform = CGAffineTransform.identity
//            })
//        }
    }
    
    class func show() {
        let hudView = sharedInstance()
        hudView?.activityView.startAnimating()
        hudView?.setTitle("")
        UIApplication.shared.delegate?.window!!.addSubview(hudView!)
        UIApplication.shared.delegate?.window!!.bringSubview(toFront: hudView!)
        hudView?.isHidden = false
        UIView.animate(withDuration: 0.2, animations: {
            hudView?.alpha = 1.0
            hudView?.transform = CGAffineTransform.init(scaleX: 1.5, y: 1.5)
        }) { (finished) in
            UIView.animate(withDuration: 0.2, animations: {
                hudView?.transform = CGAffineTransform.identity
            })
        }
    }
    class func hide() {
        let hudView = sharedInstance()
        UIView.animate(withDuration: 0.2, animations: {
            hudView?.transform = CGAffineTransform.init(scaleX: 1.5, y: 1.5)
        }) { (finished) in
            UIView.animate(withDuration: 0.2, animations: {
                hudView?.transform = CGAffineTransform.identity
            }) { (finished) in
                hudView?.removeFromSuperview()
                hudView?.isHidden = true
                hudView?.alpha = 0.0
                hudView?.activityView.stopAnimating()
            }
        }
    }
}
