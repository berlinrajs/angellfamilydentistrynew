//
//  DateInputView.swift
//  Secure Dental
//
//  Created by Berlin Raj on 20/09/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

class DateInputView: UIView {
    var textField: UITextField!
    var datePicker: UIDatePicker!
    var toolbar: UIToolbar!
    
    var arrayStates: [String]!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.datePicker = UIDatePicker(frame: CGRect(x: 0, y: 0, width: screenSize.width, height: 260))
        datePicker.addTarget(self, action: #selector(datePickerDateChanged(_:)), for: UIControlEvents.valueChanged)
        
        self.toolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: screenSize.width, height: 44))
        
        let barbuttonDone = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.plain, target: self, action: #selector(donePressed))
        toolbar.items = [UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil), barbuttonDone]
        
        self.addSubview(datePicker)
    }
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    @IBAction func datePickerDateChanged(_ sender: AnyObject) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = datePicker.datePickerMode == .time ? "hh:mm a" : kCommonDateFormat
        textField.text = dateFormatter.string(from: datePicker.date).uppercased()
    }
    func donePressed() {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = datePicker.datePickerMode == .time ? "hh:mm a" : kCommonDateFormat
        textField.text = dateFormatter.string(from: datePicker.date).uppercased()
        textField.resignFirstResponder()
    }
    
    class func addDatePickerForTextField(_ textField: UITextField) {
        let dateListView = DateInputView(frame: CGRect(x: 0, y: 0, width: screenSize.width, height: 260))
        textField.inputView = dateListView
        textField.inputAccessoryView = dateListView.toolbar
        dateListView.textField = textField
        dateListView.datePicker.datePickerMode = UIDatePickerMode.date
        
        let dateString = "1 Jan \((Calendar.current as NSCalendar).component(NSCalendar.Unit.year, from: Date()))"
        let df = DateFormatter()
        df.dateFormat = "dd MM yyyy"
        let date = df.date(from: dateString)
        if let unwrappedDate = date {
            dateListView.datePicker.setDate(unwrappedDate, animated: false)
        }
    }
    
    class func addDatePickerForDateOfBirthTextField(_ textField: UITextField) {
        let dateListView = DateInputView(frame: CGRect(x: 0, y: 0, width: screenSize.width, height: 260))
        textField.inputView = dateListView
        textField.inputAccessoryView = dateListView.toolbar
        dateListView.textField = textField
        dateListView.datePicker.datePickerMode = UIDatePickerMode.date
        let dateString = "1 Jan 1980"
        let df = DateFormatter()
        df.dateFormat = "dd MM yyyy"
        let date = df.date(from: dateString)
        if let unwrappedDate = date {
            dateListView.datePicker.setDate(unwrappedDate, animated: false)
        }
    }
    
    
    class func addTimePickerForTextField(_ textField: UITextField) {
        let dateListView = DateInputView(frame: CGRect(x: 0, y: 0, width: screenSize.width, height: 260))
        textField.inputView = dateListView
        textField.inputAccessoryView = dateListView.toolbar
        dateListView.textField = textField
        dateListView.datePicker.datePickerMode = UIDatePickerMode.time
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "hh:mm a"
        
        dateListView.datePicker.setDate(dateFormatter.date(from: "12:00 am")!, animated: true)
    }
}

class MonthInputView: UIView {
    var toolbar: UIToolbar!
    var pickerMonth : UIPickerView!
    var textField: UITextField!
    var arrayMonths = ["JANUARY","FEBRUARY","MARCH","APRIL","MAY","JUNE","JULY","AUGUST","SEPTEMBER","OCTOBER","NOVEMBER","DECEMBER"]
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        pickerMonth = UIPickerView(frame: frame)
        pickerMonth.dataSource = self
        pickerMonth.delegate = self
        self.toolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: screenSize.width, height: 44))
        let barbuttonDone = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.plain, target: self, action: #selector(donePressed))
        let barButtonClear = UIBarButtonItem(title: "Clear", style: UIBarButtonItemStyle.plain, target: self, action: #selector(cancelPressed))
        toolbar.items = [barButtonClear, UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil), barbuttonDone]
        self.addSubview(pickerMonth)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func cancelPressed() {
        textField.resignFirstResponder()
        textField.text = ""
    }
    
    func donePressed() {
        let string1 = arrayMonths[pickerMonth.selectedRow(inComponent: 0)]
        textField.text = string1.substring(with: string1.startIndex ..< string1.index(string1.startIndex, offsetBy: 3))
        textField.resignFirstResponder()
    }
    
    class func addMonthPickerForTextField(_ textField: UITextField) {
        let monthListView = MonthInputView(frame: CGRect(x: 0, y: 0, width: screenSize.width, height: 260))
        textField.inputView = monthListView
        textField.inputAccessoryView = monthListView.toolbar
        monthListView.textField = textField
    }
}

extension MonthInputView : UIPickerViewDataSource,UIPickerViewDelegate{
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return arrayMonths.count
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return arrayMonths[row]
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        let string1 = arrayMonths[row]
        
        //let index1 = string1.startIndex.advancedBy(3)
        
        textField.text = string1.substring(with: string1.startIndex ..< string1.index(string1.startIndex, offsetBy: 3))
        //  textfieldMonth.text = arrayMonths[row]
    }
}

