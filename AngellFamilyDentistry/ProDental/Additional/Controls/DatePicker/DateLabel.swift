//
//  DateLabel.swift
//  ProDental
//
//  Created by Berlin Raj on 20/09/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

class DateLabel: MCLabel {
    
    var todayDate: String?
    var placeHolder: String = "Tap to date"
    
    override func awakeFromNib() {
        super.awakeFromNib()
        backgroundColor = UIColor.white
        self.isUserInteractionEnabled = true
        
        font = UIFont(name: "WorkSans-Regular", size: 26)!
        textAlignment = NSTextAlignment.center
        text = placeHolder
        textColor = UIColor.lightGray
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(labelDateTapped))
        tapGesture.numberOfTapsRequired = 1
        addGestureRecognizer(tapGesture)
    }
    
    func labelDateTapped() {
        if isForTime {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "hh:mm a"
            
            text = dateFormatter.string(from: Date()).uppercased()
            textColor = UIColor.black
        } else {
            text = todayDate
            textColor = UIColor.black
        }
    }
    
    var dateTapped: Bool {
        get {
            return text != placeHolder
        }
    }
    
    func setDatee() {
        labelDateTapped()
    }
    func reset() {
        text = placeHolder
        textColor = UIColor.lightGray
    }
    
    var setDate: Bool = false {
        didSet {
            if setDate == true {
                labelDateTappedd(nil)
            }
        }
    }
    func labelDateTappedd(_ sender: AnyObject?) {
        text = todayDate
        textColor = UIColor.black
    }
    
    var isForTime: Bool = false {
        willSet {
            if newValue == true {
                self.placeHolder = "Tap to time"
                self.text = self.placeHolder
            } else {
                self.placeHolder = "Tap to date"
                self.text = self.placeHolder
            }
        }
    }
}
