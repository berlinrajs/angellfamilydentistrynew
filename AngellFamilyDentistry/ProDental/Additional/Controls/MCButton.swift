//
//  MCButton.swift
//  FutureDentistry
//
//  Created by Berlin Raj on 20/09/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

class MCButton: UIButton {

    override func draw(_ rect: CGRect) {
        super.draw(rect)
        layer.borderColor = borderColor.cgColor
        layer.borderWidth = borderWidth
        layer.cornerRadius = cornerRadius
        clipsToBounds = true
    }
    
    var borderColor: UIColor = UIColor.white {
        didSet {
            layer.borderColor = borderColor.cgColor
        }
    }
    
    var cornerRadius: CGFloat = 3.0 {
        didSet {
            layer.cornerRadius = cornerRadius
            clipsToBounds = true
        }
    }
    
    var borderWidth: CGFloat = 1.0 {
        didSet {
            layer.borderWidth = borderWidth
        }
    }
}

class BackButton: MCButton {
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.setBackgroundImage(MCAppDetails.sharedAppDetails!.imageBackButton, for: UIControlState.normal)
    }
}

class NextButton: MCButton {
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.setBackgroundImage(MCAppDetails.sharedAppDetails!.imageNextButton, for: UIControlState.normal)
    }
}
