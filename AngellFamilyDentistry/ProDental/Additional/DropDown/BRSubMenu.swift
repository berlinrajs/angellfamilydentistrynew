//
//  BRSubMenu.swift
//  MConsent Dentrix
//
//  Created by Berlin Raj on 07/06/17.
//  Copyright © 2017 SRS Web Solutions. All rights reserved.
//

import UIKit

enum BRSubMenuAnimationType: Int {
    case LeftDown = 0
    case LeftUp
    case RightDown
    case RightUp
    case CenterDown
    case CenterUp
}

class BRSubMenu: MCView {
    
    static var sharedSubMenu: BRSubMenu!
    
    @IBInspectable var subMenuRowHeight: CGFloat!
    var completion: ((BRSubMenu, Int)-> Void)?
    
    var items: [String]! {
        willSet {
            
        } didSet {
            self.tableView.reloadData()
        }
    }
    var animationType: BRSubMenuAnimationType!
    var selected: Bool! = false
    
    @IBOutlet var tableView: UITableView!
    @IBOutlet weak var backgroundImageView: UIImageView!
    var tableRowHeight: CGFloat!
    
    
    class func sharedMenu() -> BRSubMenu {
        
        if sharedSubMenu == nil {
            sharedSubMenu = Bundle.main.loadNibNamed("BRSubMenu", owner: nil, options: nil)?[0] as! BRSubMenu
            
            sharedSubMenu.backgroundColor = UIColor.clear
            
            sharedSubMenu.tableView.backgroundColor = UIColor.clear
            //            sharedSubMenu.tableView.separatorStyle = UITableViewCellSeparatorStyle.none
            //            sharedSubMenu.tableView.separatorColor = UIColor.clear
            sharedSubMenu.tableView.layer.cornerRadius = 3.0
            sharedSubMenu.tableView.layer.masksToBounds = true
            
            sharedSubMenu.cornerRadius = 5.0
            sharedSubMenu.layer.masksToBounds = false
        }
        
        return sharedSubMenu
    }
    
    //    override func willMove(toSuperview newSuperview: UIView?) {
    //        super.willMove(toSuperview: newSuperview)
    //        self.backgroundImageView.willMove(toSuperview: self)
    //    }
    
    func showSubMenu(inView view: UIView, fromRect rect: CGRect, withItems menuItems: [String], animationType animate: BRSubMenuAnimationType, itemSelectionBlock completionBlock: ((BRSubMenu, Int)-> Void)?) {
        
        self.backgroundImageView.willMove(toSuperview: self)
        if selected == true {
            return
        }
        self.completion = completionBlock
        self.frame = CGRect(x: rect.origin.x, y: rect.origin.y, width: 0.0, height: 0.0)
        self.items = menuItems
        self.tableView.reloadData()
        self.animationType = animate
        view.addSubview(self)
        
        UIView.animate(withDuration: 0.3, delay: 0.0, options: UIViewAnimationOptions.curveEaseInOut, animations: {
            switch self.animationType.rawValue {
            case 0:
                self.frame = CGRect(x: rect.origin.x - rect.width, y: rect.origin.y, width: rect.width, height: rect.height)
            case 1:
                self.frame = CGRect(x: rect.origin.x - rect.width, y: rect.origin.y - rect.height, width: rect.width, height: rect.height)
            case 2:
                self.frame = CGRect(x: rect.origin.x, y: rect.origin.y, width: rect.width, height: rect.height)
            case 3:
                self.frame = CGRect(x: rect.origin.x, y: rect.origin.y - rect.height, width: rect.width, height: rect.height)
            case 4:
                self.frame = CGRect(x: rect.origin.x - (rect.width/2), y: rect.origin.y, width: rect.width, height: rect.height)
            case 5:
                self.frame = CGRect(x: rect.origin.x - (rect.width/2), y: rect.origin.y - rect.height, width: rect.width, height: rect.height)
            default:
                self.frame = CGRect(x: rect.origin.x, y: rect.origin.y, width: rect.width, height: rect.height)
            }
        }) { (finished) in
            self.selected = true
        }
    }
    func hideSubMenu() {
        if selected == false {
            return
        }
        let rect = self.frame
        UIView.animate(withDuration: 0.3, delay: 0.0, options: UIViewAnimationOptions.curveEaseInOut, animations: {
            switch self.animationType.rawValue {
            case 0:
                self.frame = CGRect(x: rect.origin.x + rect.width, y: rect.origin.y, width: 0.0, height: 0.0)
            case 1:
                self.frame = CGRect(x: rect.origin.x + rect.width, y: rect.origin.y + rect.height, width: 0.0, height: 0.0)
            case 2:
                self.frame = CGRect(x: rect.origin.x, y: rect.origin.y, width: 0.0, height: 0.0)
            case 3:
                self.frame = CGRect(x: rect.origin.x, y: rect.origin.y + rect.height, width: 0.0, height: 0.0)
            case 4:
                self.frame = CGRect(x: rect.origin.x + (rect.width/2), y: rect.origin.y, width: 0.0, height: 0.0)
            case 5:
                self.frame = CGRect(x: rect.origin.x + (rect.width/2), y: rect.origin.y + rect.height, width: 0.0, height: 0.0)
            default:
                self.frame = CGRect(x: rect.origin.x, y: rect.origin.y, width: 0.0, height: 0.0)
            }
            self.layoutIfNeeded()
        }, completion: { (finished) in
            self.selected = false
            //            self.removeFromSuperview()
        })
    }
}
extension BRSubMenu: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return items != nil ? 1 : 0
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell = tableView.dequeueReusableCell(withIdentifier: "Cell") as? BRSubMenuCell
        if cell == nil {
            cell = BRSubMenuCell(style: UITableViewCellStyle.default, reuseIdentifier: "Cell")
        }
        cell!.title = items[indexPath.row].uppercased()
        
        return cell!
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cell.backgroundColor = UIColor.clear
        cell.selectionStyle = UITableViewCellSelectionStyle.none
        
        cell.separatorInset = UIEdgeInsets.zero
        cell.preservesSuperviewLayoutMargins = false
        cell.layoutMargins = UIEdgeInsets.zero
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.hideSubMenu()
        self.completion?(self, indexPath.row)
    }
    
    class BRSubMenuCell: UITableViewCell {
        
        var labelTitle: UILabel!
        
        override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
            super.init(style: style, reuseIdentifier: reuseIdentifier)
            
            labelTitle = UILabel()
            labelTitle.textColor = titleColor
            labelTitle.numberOfLines = 0
            labelTitle.font = UIFont(name: "WorkSans-Regular", size: 26)
            labelTitle.textAlignment = NSTextAlignment.center
            self.addSubview(labelTitle)
        }
        override func layoutSubviews() {
            super.layoutSubviews()
            labelTitle.frame = CGRect(x: 0, y: 0, width: self.frame.width, height: self.frame.height)
        }
        
        required init?(coder aDecoder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
        
        var title: String! {
            willSet {
                self.labelTitle.text = newValue.uppercased()
            } didSet {
                
            }
        }
        var titleColor: UIColor = UIColor.white {
            willSet {
                self.labelTitle.textColor = newValue
            } didSet {
                
            }
        }
        var titleFont: UIFont = UIFont(name: "WorkSans-Regular", size: 17)! {
            willSet {
                
            } didSet {
                self.labelTitle.font = titleFont
            }
        }
    }
}
