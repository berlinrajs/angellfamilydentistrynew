//
//  ArtificialPopUp.swift
//  FusionDental
//
//  Created by Berlin Raj on 20/09/16.
//  Copyright © 2016 SRS. All rights reserved.

import UIKit

class ArtificialPopUp: UIView {
    
    class func popUpView() -> ArtificialPopUp {
        return Bundle.main.loadNibNamed("ArtificialPopUp", owner: nil, options: nil)!.first as! ArtificialPopUp
    }
    
    @IBOutlet weak var textFieldYear: MCTextField!
    @IBOutlet weak var radioBoneType: RadioButton!
    
    var completion:((ArtificialPopUp, UITextField, String)->Void)?
    
    func showInViewController (_ viewController: UIViewController?, completion : @escaping (_ popUpView: ArtificialPopUp, _ textFieldLong : UITextField, _ examType: String) -> Void) {
        textFieldYear.text = ""
        textFieldYear.placeHolderColor = UIColor.lightGray
        radioBoneType.deselectAllButtons()
        
        self.frame = CGRect(x: 0, y: 0, width: screenSize.width, height: screenSize.height)
        self.completion = completion
        if viewController != nil {
            viewController!.view.addSubview(self)
        } else {
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            appDelegate.window!.addSubview(self)
        }
        self.subviews[0].transform = CGAffineTransform(scaleX: 0.1, y: 0.1)
        UIView.beginAnimations(nil, context: nil)
        UIView.setAnimationDuration(0.3)
        self.subviews[0].transform = CGAffineTransform.identity
        UIView.commitAnimations()
    }
    
    @IBAction func buttonActionOK() {
        completion?(self, self.textFieldYear, radioBoneType.selected == nil ? "" : radioBoneType.selected.tag == 1 ? "HIP" : radioBoneType.selected.tag == 2 ? "KNEE" : "SHOULDER")
    }
    func close() {
        self.removeFromSuperview()
    }
}
extension ArtificialPopUp: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        return textField.formatNumbers(range, string: string, count: 4)
    }
}
