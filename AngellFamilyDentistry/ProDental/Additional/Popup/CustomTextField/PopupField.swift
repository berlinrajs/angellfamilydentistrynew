//
//  PopupButton.swift
//  FusionDental
//
//  Created by Leojin Bose on 5/28/16.
//  Copyright © 2016 SRS. All rights reserved.
//

import UIKit

class PopupField: UIView {
    
    static let sharedInstance = Bundle.main.loadNibNamed("PopupField", owner: nil, options: nil)!.first as! PopupField
    var completion:((PopupField)->Void)?
    
    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var constraintViewHeight: NSLayoutConstraint!
    @IBOutlet weak var constraintLabelHeight: NSLayoutConstraint!
    
    var questions: [MCQuestion]?
    var textFormats: [TextFormat]?
    
    class func popUpView() -> PopupField {
        return Bundle.main.loadNibNamed("PopupField", owner: nil, options: nil)!.first as! PopupField
    }
    
    override func awakeFromNib() {
        
    }
    
    func show(_ inViewController : UIViewController?, title : String?, questions: [MCQuestion]?, textFormats: [TextFormat]?, completion : @escaping (_ popupView: PopupField) -> Void) {
        self.questions = questions
        self.textFormats = textFormats
        self.completion = completion
        self.tableView.reloadData()
        if let controller = inViewController {
            self.frame = controller.view.frame
            controller.view.addSubview(self)
        } else {
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            self.frame = appDelegate.window!.frame
            appDelegate.window!.addSubview(self)
        }
        
        func setHeight(_ height : CGFloat) {
            if let questions = questions {
                let viewHeight = height + CGFloat(90 * questions.count) + 78
                constraintViewHeight.constant = viewHeight
            }
        }
        
        if let t = title {
            labelTitle.text = t
            let labelHeight = t.heightWithConstrainedWidth(404, font: labelTitle.font) + 10
            constraintLabelHeight.constant = labelHeight
            setHeight(labelHeight)
        } else {
            setHeight(labelTitle.frame.height)
        }
        self.subviews[0].transform = CGAffineTransform(scaleX: 0.1, y: 0.1)
        UIView.beginAnimations(nil, context: nil)
        UIView.setAnimationDuration(0.3)
        self.subviews[0].transform = CGAffineTransform.identity
        UIView.commitAnimations()
    }
    
    func close() {
        self.removeFromSuperview()
    }
    
    @IBAction func buttonActionOK(_ sender: AnyObject) {
        self.removeFromSuperview()
        completion?(self)
    }
}

extension PopupField: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let fields = self.questions {
            return fields.count
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell = tableView.dequeueReusableCell(withIdentifier: "Cell")
        if cell == nil {
            cell = Bundle.main.loadNibNamed("PopupField", owner: nil, options: nil)![1] as? UITableViewCell
        }
        cell?.backgroundColor = UIColor.clear
        cell?.contentView.backgroundColor = UIColor.clear
        let obj = questions![indexPath.row]
        let textField = cell?.contentView.viewWithTag(101) as! MCTextField
        textField.placeholder = obj.question.uppercased()
        if let arrayTextFormat = textFormats {
            textField.textFormat = arrayTextFormat[indexPath.row]
        } else {
            textField.textFormat = obj.question.uppercased() == "TOOTH NUMBER" ? .AlphaNumeric : .AlphaNumeric
        }
        textField.accessibilityIdentifier = "\(indexPath.row)"
        textField.text = obj.answer
        if textField.delegateMCTextField == nil {
            textField.delegateMCTextField = self
        }
        return cell!
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 90
    }
}

extension PopupField : UITextFieldDelegate {
    func textFieldDidEndEditing(_ textField: UITextField) {
        let obj = questions![Int(textField.accessibilityIdentifier!)!]
        obj.answer = textField.text
        
    }
}
