//
//  FosamaxPopup.swift
//  FusionDental
//
//  Created by Berlin Raj on 20/09/16.
//  Copyright © 2016 SRS. All rights reserved.

import UIKit

class FosamaxPopup: UIView {
    
    class func popUpView() -> FosamaxPopup {
        return Bundle.main.loadNibNamed("FosamaxPopup", owner: nil, options: nil)!.first as! FosamaxPopup
    }
    
    @IBOutlet weak var textFieldLong: MCTextField!
    @IBOutlet weak var radioExamType: RadioButton!
    
    var completion:((FosamaxPopup, UITextField, String)->Void)?
    
    func showInViewController (_ viewController: UIViewController?, completion : @escaping (_ popUpView: FosamaxPopup, _ textFieldLong : UITextField, _ examType: String) -> Void) {
        textFieldLong.text = ""
        textFieldLong.placeHolderColor = UIColor.lightGray
        radioExamType.deselectAllButtons()
        
        self.frame = CGRect(x: 0, y: 0, width: screenSize.width, height: screenSize.height)
        self.completion = completion
        if viewController != nil {
            viewController!.view.addSubview(self)
        } else {
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            appDelegate.window!.addSubview(self)
        }
        self.subviews[0].transform = CGAffineTransform(scaleX: 0.1, y: 0.1)
        UIView.beginAnimations(nil, context: nil)
        UIView.setAnimationDuration(0.3)
        self.subviews[0].transform = CGAffineTransform.identity
        UIView.commitAnimations()
    }
    
    @IBAction func buttonActionOK() {
        completion?(self, self.textFieldLong, radioExamType.selected == nil ? "" : radioExamType.selected.tag == 1 ? "ORAL" : "IV")
    }
    func close() {
        self.removeFromSuperview()
    }
}
